import javax.swing.*;
import java.awt.*;

public class paintComponentEx extends JFrame {
	Container contentPane;	
	paintComponentEx() {
		setTitle("새로운 버튼 만들기");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());		
		MyButton b = new MyButton("New Button");
		b.setOpaque(true);
		b.setBackground(Color.CYAN);
		contentPane.add(b);
		setSize(250,200);
		setVisible(true);
	}

	// JButton을 상속받아 새로운 버튼을 구현한다.
	// 이 버튼은 기존 버튼에 항상 버튼의 내접원을 가진다.
	class MyButton extends JButton {
		MyButton(String s) { // 생성자
			super(s); // JButton의 생성자 호출
		}
		
		// 새로운 버튼을 그린다.
		public void paintComponent(Graphics g) {
			 // JButton으로 하여금 기본 버튼 모양을 그리도록 한다.
			super.paintComponent(g);
			
			// JButton의 기본 모양 위에 추가적으로 빨간색으로 버튼 크기만한 타원을 그린다.
			g.setColor(Color.RED);
			g.drawOval(0,0,this.getWidth()-1, this.getHeight()-1);
		}	
	}
	
	public static void main(String [] args) {
		new paintComponentEx();
	}
} 