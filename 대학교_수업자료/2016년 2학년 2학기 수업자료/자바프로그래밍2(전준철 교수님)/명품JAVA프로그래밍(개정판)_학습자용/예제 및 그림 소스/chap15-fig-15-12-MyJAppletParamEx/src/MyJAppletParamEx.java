import java.awt.*;
import javax.swing.*;

public class MyJAppletParamEx extends JApplet {
	String text=null;
	int x=0;
	int y=0;
	int fontSize=10;
	public void init() {
		text = getParameter("text"); // <param>태그의 text 속성의 값을 읽는다.
		try {
			x =  Integer.parseInt(getParameter("xpos"));
			y =  Integer.parseInt(getParameter("ypos"));
			fontSize =  Integer.parseInt(getParameter("fontsize"));
		}catch(NumberFormatException e) {}
		
		// MyPanel을 컨텐트팬으로 사용한다.
		setContentPane(new MyPanel());
	}
	public void start() {}
	public void stop() {}
	public void destroy() {}
	
	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) { // 애플릿을 그린다.
			super.paintComponent(g);
			if(text == null) 
				return; // <param> 태그에 text 속성을 가진 문자열을 전달받지 못할 경우
			g.setColor(Color.YELLOW);
			g.fillRect(0,0, getWidth(), getHeight());
			g.setColor(Color.RED);
			g.setFont(new Font("SanSerif", Font.ITALIC, fontSize));
			g.drawString(text, x, y);
		}
	}
}
