import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;
import java.net.URL;

public class AudioFrameEx extends JFrame {
	AudioFrameEx() {
		setTitle("JFrame에서 오디오 연주");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(new MyPanel());
		setSize(300, 150);
		setVisible(true);
	}

	// 컨텐트팬으로 사용할 패널
	class MyPanel extends JPanel {
		AudioClip clip = null; // 오디오 클립 객체
		JButton btn[] = new JButton [2]; // "Play", "Stop" 버튼
		
		MyPanel() {
			setBackground(Color.ORANGE);
			setLayout(new FlowLayout());
			
			// 두 개의 버튼에 등록한 Action 리스너
			MyActionListener listener = new MyActionListener();
			btn[0] = new JButton("Play");
			btn[1] = new JButton("Stop");
			for(int i=0; i<btn.length; i++) {
				add(btn[i]);
				btn[i].addActionListener(listener);
			}
			
			// "ToYou.mid" 파일의 인터넷 경로명을 결정한다.
			URL audioURL = getClass().getResource("ToYou.mid");
			
			// "ToYou.mid" 파일을 로드하여 연주 가능한 형식으로 만든다.
			clip  = Applet.newAudioClip(audioURL);
		}
		
		class MyActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().equals("Play")) // "Play" 버튼이 선택된 경우
					clip.play();
				else // "Stop" 버튼이 선택된 경우
					clip.stop();
			}
		}
	}
	public static void main(String[] args) {
		new AudioFrameEx();
	}
}
