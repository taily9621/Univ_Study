import java.awt.*;
import javax.swing.*;

class FlickeringLabel extends JLabel implements Runnable{
	
	public FlickeringLabel(String text) { // 생성자
		super(text); // JLabel 생성자 호출
		setOpaque(true); // 배경색 변경이 가능하도록 설정
		
		//스레드 객체 생성
		Thread th = new Thread(this);
		
		// 스레드 실행
		th.start();
	}

	// 스레드 코드.
	public void run() {
		int n=0;
		while(true) { // 무한 루프
			if(n == 0)
				setBackground(Color.YELLOW);
			else
				setBackground(Color.GREEN);

			// n을 0과 1로 번갈아 변경
			if(n == 0) n = 1;
			else n = 0;
			
			try {
				Thread.sleep(500); // 0.5초동안 잠을 잔다.
			}
			catch(InterruptedException e) {
				return; // 예외가 발생하면 스레드 종료
			}
		}
	}
}

public class FlickeringLabelEx extends JFrame {
	public FlickeringLabelEx() {
		setTitle("FlickeringLabelEx 예제");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new FlowLayout());

		// 깜박이는  레이블 생성
		FlickeringLabel fLabel = new FlickeringLabel(" 깜박 ");

		// 깜박이지 않는  레이블 생성
		JLabel label = new JLabel(" 안깜박 ");

		// 깜박이는  레이블 생성
		FlickeringLabel fLabel2 = new FlickeringLabel(" 여기도 깜박 ");

		 // 컨텐트팬에 레이블 부착
		c.add(fLabel);
		c.add(label);
		c.add(fLabel2);
		
		setSize(300,150);
		setVisible(true);
	}
	public static void main(String[] args) {
		new FlickeringLabelEx();
	}
}







