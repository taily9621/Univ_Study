import java.awt.*;
import java.applet.*;

public class MyAppletEx extends Applet {
	String text=null;
	int x; 
	int y;
	int fontSize;
	
	public void init() { // 애플릿이 생성되는 초기에 단 한 번 호출
		text = "Hello. It's Applet"; // 출력할 문자열
		x = 30; // text가 츨력될 x 위치=30
		y = 30; // text가 츨력될 y 위치=30
		fontSize = 20; // 폰트 크기 20
	}
	public void start() {}
	public void stop() {}
	public void destroy() {}
	
	// 애플릿을 그린다.
	public void paint(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillRect(0,0, getWidth(), getHeight()); // 애플릿 전체를 노란색으로 채운다.
		g.setColor(Color.RED);
		g.setFont(new Font("SanSerif", Font.ITALIC, fontSize));
		g.drawString(text, x, y); // text를 (x,y) 위치에 빨간색으로 출력한다.
	}
}
