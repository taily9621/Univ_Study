import java.io.*;

public class FileOutputStreamEx {
	public static void main(String[] args) {
	        try {
        		FileOutputStream fout = new FileOutputStream("C:\\test.out");
        		FileInputStream fin = null;
	        	for (int i=0; i<10; i++) {
        			int n = 10-i; // 계산의 결과를 저장
        			fout.write(n); // 파일에 결과값을 바이너리로 저장
	        	}
        	    fout.close(); //스트림을 닫는다.
	            fin = new FileInputStream("C:\\test.out"); // 결과가 저장된 파일을 다시 입력 스트림에 연결
        	    int c=0;
        		while ((c = fin.read()) != -1) {
        			System.out.print(c + " "); // 계산된 값과 일치하는지 확인
	        	}
        		fin.close();
	        } catch (IOException e) {
        		System.out.println("입출력 오류");
	        }
	}
}
