abstract class DObject {
	public DObject next;
	public DObject() { next = null; }
	abstract public void draw();
}
public class AbstractError {
	public static void main(String [] args) {
		DObject obj;
		obj = new DObject(); // 컴파일 오류
		obj.draw(); // 컴파일 오류
	}
}