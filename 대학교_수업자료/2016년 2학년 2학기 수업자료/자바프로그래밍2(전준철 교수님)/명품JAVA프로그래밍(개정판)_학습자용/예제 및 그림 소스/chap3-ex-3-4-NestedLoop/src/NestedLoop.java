public class NestedLoop {
	public static void main (String[] args) {
		int i, j;

		for (i = 1;i < 10; i++,System.out.println()) { // 단에 대한 반복
			for (j = 1;j < 10; j++,System.out.print('\t')) { // 각 단의 곱셈 반복
				System.out.print(i + "*" + j + "=" + i*j);
			}
		}
	}
}
