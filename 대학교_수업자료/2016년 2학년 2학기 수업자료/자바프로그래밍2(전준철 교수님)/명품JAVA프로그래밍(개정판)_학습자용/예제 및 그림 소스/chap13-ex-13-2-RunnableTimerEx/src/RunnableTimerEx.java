import java.awt.*;
import javax.swing.*;

class TimerRunnable implements Runnable {
	JLabel timerLabel;// 타이머 값이 출력된 레이블
	
	public TimerRunnable(JLabel timerLabel) {
		this.timerLabel = timerLabel;
	}

	// 스레드 코드. 
	// run()이 종료하면 스레드 종료
	public void run() {
		int n=0; // 타이머 카운트 값
		while(true) { // 무한 루프
			timerLabel.setText(Integer.toString(n)); // 타이머 값을 레이블에 출력
			n++; // 카운트 증가
			try {
				Thread.sleep(1000); // 1초동안 잠을 잔다.
			}
			catch(InterruptedException e) {
				return; // 예외가 발생하면 스레드 종료
			}
		}
	}
}

public class RunnableTimerEx extends JFrame {
	public RunnableTimerEx() {
		setTitle("RunnableTimerEx 예제");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new FlowLayout());

		// 타이머 값을 출력할 레이블 생성
		JLabel timerLabel = new JLabel();
		timerLabel.setFont(new Font("Gothic", Font.ITALIC, 80));
		
		// 타이머 스레드로 사용할 Runnable 객체 생성. 
		// 타이머 값을 출력할 레이블 컴포넌트를 생성자에 전달한다.
		TimerRunnable runnable = new TimerRunnable(timerLabel);
		
		Thread th = new Thread(runnable); // 스레드 객체 생성
		
		c.add(timerLabel); // 컨텐트팬에 레이블 부착
		
		setSize(300,150);
		setVisible(true);
		
		th.start(); // 타이머 스레드의 실행을 시작시킨다.
	}
	public static void main(String[] args) {
		new RunnableTimerEx();
	}
}







