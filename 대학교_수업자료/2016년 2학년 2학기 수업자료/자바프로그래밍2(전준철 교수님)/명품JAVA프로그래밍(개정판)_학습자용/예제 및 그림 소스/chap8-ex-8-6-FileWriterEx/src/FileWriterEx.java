import java.io.*;

public class FileWriterEx {
	public static void main(String[] args) {
		InputStreamReader in = new InputStreamReader(System.in); // 콘솔과 연결된 입력 문자 스트림 생성
		FileWriter fout = null;
		int c;
		try {
			fout = new FileWriter("c:\\tmp\\test.txt"); // 파일과 연결된 출력 문자 스트림 생성
			while ((c = in.read()) != -1) {
				fout.write(c); // 콘솔에서 입력받은 문자 파일에 출력
			}
            in.close();
            fout.close();
		} catch (IOException e) {
			System.out.println("입출력  오류");
		}
	}
}
