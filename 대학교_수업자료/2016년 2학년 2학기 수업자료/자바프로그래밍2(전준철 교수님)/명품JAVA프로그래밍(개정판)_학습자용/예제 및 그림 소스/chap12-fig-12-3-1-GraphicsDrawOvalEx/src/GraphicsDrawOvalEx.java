import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class GraphicsDrawOvalEx extends JFrame {
	GraphicsDrawOvalEx() {
		setTitle("drawOval ���  ����");
		MyPanel panel = new MyPanel();
		add(panel, BorderLayout.CENTER);
		setSize(200, 150);
		setVisible(true);
	}

	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) {
			g.setColor(Color.RED);
			g.drawOval(20,20, 80, 80);
		}	
	}
	
	public static void main(String [] args) {
		new GraphicsDrawOvalEx();
	}
} 