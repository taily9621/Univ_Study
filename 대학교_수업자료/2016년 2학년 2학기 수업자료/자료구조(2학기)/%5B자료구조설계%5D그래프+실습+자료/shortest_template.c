#include<stdio.h>
#include<stdlib.h>
#include<limits.h>


#define BIG_INT  100000
#define MAX_SIZE 10
#define FOUND   1
#define NOT_FOUND -1 //define necessary values


void graphfromfile(char* filename, int graph[][MAX_SIZE], int* n);
int choose(int* distance, int* found, int n);
void shortest(int v, int graph[][MAX_SIZE], int* distance, int* found, int* pre_vertex, int n);
void print(int* pre_vertex, int n);
//define functions and variables 

int main(int argc, char** argv) {
	int graph[MAX_SIZE][MAX_SIZE];
	int distance[MAX_SIZE];
	int found[MAX_SIZE];
	int pre_vertex[MAX_SIZE];
	int v;
	int n; // num of vertex and nodes

		   //perform functions with corresponding arguments 
	graphfromfile("test10.txt", graph, &n); //파일 명은 입력받는 파일에 따라 바꿔준다.
	shortest(0, graph, distance, found, pre_vertex, n);
	print(pre_vertex, n);
	return 0;
}


void graphfromfile(char* filename, int graph[][MAX_SIZE], int* n) { //define load graph function 
	FILE* fp = fopen(filename, "r");
	int v, w; // start vertex, end vertex
	int weight;
	//get the data from file 

	int i, j;


	if (fp == NULL) {
		printf("file not found exception.");
		exit(1);
	}

	fscanf(fp, "%d", n);
	if (*n > MAX_SIZE) { printf("too many vertex"); exit(1); } //define exceptions 

	for (i = 0; i<*n; ++i) {
		for (j = 0; j<*n; ++j) {
			graph[i][j] = BIG_INT;
		}
	}//자료를 읽어 자료의 값을 행렬로 입력함 


	while (!feof(fp)) {
		fscanf(fp, "%d %d %d", &v, &w, &weight);
		graph[v][w] = weight;
	} //파일의 첫번째 값은 시작 vertex, 두번째는 끝 vertex 마지막은 가중치로 입력하여 그래프를 구성한다.
}


int choose(int* distance, int* found, int n) {
	int i;
	int min = INT_MAX;
	int minpos = -1;


	for (i = 0; i<n; ++i) {
		if (found[i] > NOT_FOUND) continue;
		if (distance[i] >= min) continue;
		//검색하여 현재 최단 거리보다 거리가 크면 필요 없으므로 무시한다.
		min = distance[i];
		minpos = i;//최단거리보다 짧은 새로운 최단경로가 나오면 선택한다. 
	}
	return minpos;
}


void shortest(int v, int graph[][MAX_SIZE], int* distance, int* found, int* pre_vertex, int n) {
	int i, u, w;
	//int pre_vertex;


	// init
	for (i = 0; i<n; ++i) {
		distance[i] = graph[v][i];
		found[i] = NOT_FOUND;
		if (distance[i] > 0 && distance[i] < BIG_INT) pre_vertex[i] = v;
		else pre_vertex[i] = -1;
	}


	distance[v] = 0;
	found[v] = FOUND;

	for (i = 0; i<n - 1; ++i) {
		u = choose(distance, found, n); //가장 가까운 정점을 찾는다.
		found[u] = FOUND;


		for (w = 0; w<n; ++w) {  // 최단 경로상의 정점들로부터 인접정점들까지의 모든 distance를 계산한다.
			if (found[w] > NOT_FOUND) continue;
			if (distance[u] + graph[u][w] >= distance[w]) continue;


			distance[w] = distance[u] + graph[u][w];
			pre_vertex[w] = u; //더 가까운 경로가 나오면 그 값을 선택한다. 
		}
	}
}


void print(int* pre_vertex, int n) {
	int i;
	int a = 0;
	int cv; // current vertex
	for (i = 0; i<n; ++i) {
		cv = i;
		//출력한다. 

		printf("0에서 부터 %d까지 가장 짧은 경로  :[%d]", cv, cv);
		while (pre_vertex[cv] >= 0) {
			printf("<-[%d]", pre_vertex[cv]);
			cv = pre_vertex[cv];
		}

		printf("\n");
	}


}
[출처] [자료구조] 다익스트라 알고리즘 최단경로 찾기 | 작성자 연J