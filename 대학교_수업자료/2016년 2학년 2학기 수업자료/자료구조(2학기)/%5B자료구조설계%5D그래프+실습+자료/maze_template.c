// 인접 리스트를 이용한 미로 찾기

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#define MAX_VERTICES 50
int visited[MAX_VERTICES];
int TRUE = 1;
int FALSE = 0;

typedef struct GraphNode
{
	int vertex;
	struct GraphNode *link;
} GraphNode;

typedef struct GraphType {
	int n; // 정점의개수
	GraphNode *adj_list[MAX_VERTICES];
} GraphType;

// 그래프초기화
void graph_init(GraphType *g)
{
	int v;
	g->n=0;
	for(v=0;v<MAX_VERTICES;v++)
		g->adj_list[v]=NULL;
}

// 정점 삽입 연산
void insert_vertex(GraphType *g, int v)
{
	//	# 코드 작성 (정점 삽입)
}

// 간선 삽입 연산, v를 u의인접리스트에삽입한다.
void insert_edge(GraphType *g, int u, int v)
{
	//	# 코드 작성 (간선 삽입)
}

// 인접 리스트로 표현된 그래프에 대한 깊이 우선 탐색
void dfs_list(GraphType *g, int v, int d)
{
	//	# 코드 작성 (깊이우선 탐색)
}

GraphType* makeMaze(GraphType *g){

	//	# 코드 작성 (초기화)
	//	힌트 : graph_init 함수 이용


	//	# 코드 작성 (정점, 간선 삽입)
	//	힌트 : insert_vertex, insert_edge 함수 이용


	return g;
}

void help()
{
	printf("\n*****************************\n");
	printf("미로 찾기를 시작하시겠습니까?(y/n)\n");
	printf("*****************************\n");
}
 
void main()
{
	char command;
	int k = 0;

	// 미로 생성
	GraphType *g= (GraphType *)malloc(sizeof(GraphType));
	g = makeMaze(g);

	do{
		help();
		command = getchar();
		fflush(stdin);
		

		switch(command){

		case 'y':
			// # 코드 작성 (미로 찾기)
			//	힌트 : dfs_list 함수 이용

			break;
		}
	} while(command != 'n');
}