

// 이진 탐색 트리를 사용한 학생 정보 관리

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#define MAX_CHAR_SIZE	 100

// 데이터 형식
typedef struct {
	char name[MAX_CHAR_SIZE];		// 키필드
	char studNum[MAX_CHAR_SIZE];
	char phone[MAX_CHAR_SIZE];
	char major[MAX_CHAR_SIZE];
} element;

// 노드의 구조
typedef struct TreeNode {
	element key;
	struct TreeNode *left, *right;
} TreeNode;

// 만약 e1 > e2 -> -1 반환
// 만약 e1 == e2 -> 0  반환 
// 만약 e1 < e2 -> 1 반환
int compare(element e1, element e2)
{
	return strcmp(e1.name, e2.name);
}

// 이진 탐색 트리 출력 함수
void display(TreeNode *p)
{
	if( p != NULL ) {
		printf("(");
		display(p->left);
		printf("%s", p->key.name);
		display(p->right);
		printf(")");
	}
}
// 이진 탐색 트리 탐색 함수
TreeNode *search(TreeNode *root, element key) 
{    
	TreeNode *p=root;
	while(p != NULL){ 
		/*
		# 코드 작성
		1. 비교연산 후 작으면 왼쪽, 크면 오른쪽, 같으면 현재 위치
		(compare(element e1, element e2) 이용)
		*/
		
	}
	return p; 	// 탐색에 실패했을 경우 NULL 반환
}

// key를 이진 탐색 트리 root에 삽입한다. 
// key가 이미 root안에 있으면 삽입되지 않는다.
void insert_node(TreeNode **root, element key) 
{
	TreeNode *p, *t; // p는 부모 노드, t는 자식 노드 
	TreeNode *n;	 // n은 새로운 노드

	t = *root;
	p = NULL;
	// 탐색을 먼저 수행 
	while (t != NULL){
		if( compare(key, t->key)==0 ) return;
		p = t;
		if( compare(key , t->key)<0 ) t = t->left;
		else t = t->right;
	}
	// item이 트리 안에 없으므로 삽입 가능
	n = (TreeNode *) malloc(sizeof(TreeNode));
	if( n == NULL ) return;
	// 데이터 복사
	n->key = key;
	n->left = n->right = NULL;
	// 부모 노드와 링크 연결
	/*
	# 코드 작성
	1.부모 노드가 있을 때
	2.부모 노드가 없을 때
	(compare(element e1, element e2)) 이용
	*/
	
}
// 삭제 함수
void delete_node(TreeNode **root, element key)
{
	TreeNode *p, *child, *succ, *succ_p, *t;

	// key를 갖는 노드 t를 탐색, p는 t의 부모노드
	p = NULL;
	t = *root;
	while( t != NULL && compare(t->key, key) != 0 ){
		p = t;
		t = ( compare(key, t->key)<0 ) ? t->left : t->right;
	}
	if( t == NULL ) { 	// 탐색트리에 없는 키
		printf("key is not in the tree");
		return;
	}
	// 단말노드인 경우
	if( (t->left==NULL) && (t->right==NULL) ){ 
		if( p != NULL ){
			if( p->left == t )	 
				p->left = NULL;
			else   p->right = NULL;
		}
		else					// 부모노드가 없으면 루트
			*root = NULL;
	}
	// 하나의 자식만 가지는 경우
	else if((t->left==NULL)||(t->right==NULL)){
		child = (t->left != NULL) ? t->left : t->right;
		if( p != NULL ){
			if( p->left == t )	// 부모노드를 자식노드와 연결 
				p->left = child;
			else p->right = child;
		}
		else
			*root = child;
	}
	else{		// 두개의 자식을 가지는 경우
		succ_p = t;

		/*
		# 코드 작성
		1.오른쪽 서브 트리에서 후속자를 찾는다.
		2.후속자를 찾아서 계속 왼쪽으로 이동한다.
		3.후속자의 부모와 자식을 연결 
		4.후속자를 현재 노드로 이동한다.
		*/


	}
	free(t);
}
//
void help()
{
	printf("**************\n");
	printf("i: 입력\n");
	printf("d: 삭제\n");
	printf("s: 탐색\n");
	printf("p: 출력\n");
	printf("c: 파일 저장\n");
	printf("o: 파일 열기\n");
	printf("q: 종료\n");
	
	printf("**************\n");
}
// 이진 탐색 트리를 사용하는 영어 사전 프로그램 
void main()
{
	char command;
	element e;
	TreeNode *root=NULL;
	TreeNode *tmp;

	do{
		help();
		command = getchar();
		fflush(stdin);
	
		/* 
		# 코드 작성
		1. insert_node(TreeNode **root, element key)
		2. delete_node(TreeNode **root, element key)
		3. display(TreeNode *p)
		4. search(TreeNode *root, element key)
		5. 파일 저장
		6. 파일 열기
		*/


	} while(command != 'q');
}