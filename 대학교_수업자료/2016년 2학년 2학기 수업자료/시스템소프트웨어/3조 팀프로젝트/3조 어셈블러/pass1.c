#include "myHeader.h"

int find_location(char symbol[])//위치찾기 : op테이블 심볼과 real.s 파일의 라벨이 같으면 i값을 리턴
{
	int i;//반복문을 돌리기 위한 변수 i
	for(i=1; i<MAX; i++)//1부터 100까지 반복
	{
		if( strcmp( symbol, sc[i].label ) == 0 )//optable의 심볼 값이랑 real.s 파일의 라벨값이 같으면 i 값을 반환
		{
			return i;
		}
	}
	return 0;
}

int variable_checking(int sc_count)//opcode에서 RESB 또는 RESW 또는 BYTE인지 체크
{
	int i, tmp = 0, flag = 0;
	//반복문을 돌리기 위한 변수 i, 문자를 정수로 변환하고 나서 저장할 변수 공간 tmp,
 	//BYTE에서 1인지 0인지 구분하기 위해 쓰이는 flag변수

	if( strcmp( sc[sc_count].opcode, "RESW" ) == 0 )//opcode가 RESB 일때
	{
		tmp = atoi( sc[sc_count].symbol );//문자를 정수로 변환하고 tmp에 저장
		return tmp * 3;//tmp값에 3을 곱한 값을 반환
	}
	else if( strcmp( sc[sc_count].opcode, "BYTE" ) == 0 )//opcode가 BYTE 일때
	{

		for(i=2;i<strlen(sc[sc_count].symbol)-1;i++)//real.s 의 symbol길이 만큼 반복
			tmp++;//tmp값을 1만큼 증가
		
		return tmp;//반복문이 끝나면 tmp값을 반환
	}
	else if( strcmp( sc[sc_count].opcode, "RESB" ) == 0 )//RESB 일때
	{
		tmp = atoi( sc[sc_count].symbol );//symbol값 문장을 정수로 변환 후 tmp에 저장
		return tmp * 1;//tmp값에 1을 곱한 값을 반환
	}
	else return 3;//RESW 또는 RESB 또는 BYTE가 아니면 3을 반환
}
