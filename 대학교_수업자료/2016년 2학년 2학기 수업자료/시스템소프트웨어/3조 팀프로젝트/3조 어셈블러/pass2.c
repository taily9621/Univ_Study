#include "myHeader.h"

void load_optable(){
	FILE* ptr;	//optable 을 불러올 파일 변수
	int i = 0;

	if( ( ptr = fopen( "optable", "r") ) == NULL ){
			//optable을 불러옵니다. 만약 파일을 열 수 없으면 에러를 표시합니다.
		fprintf(stderr,"Optable Open Error");
		return;
	}
	while( !feof(ptr) ){
		fscanf(ptr, "%s %s", opt[i].instruction_word, opt[i].opcode );
			//불러온 opcode들을 헤더파일에서 만든 구조체에 넣습니다. 하나 넣은 후 다음줄로 넘어갑니다.
		i++;
	}
}



void make_obj_prog(int sc_count, char total[]){
	int i,j,k=0;
	int temp;
	int idx;
	char* objectcode;
	char code[5];
	char tmp1[7] = {'\0'}; //
	char tmp2[10];
	int line_length=0,line;
	int startLocNum[16]={0,};

	FILE* ptr = fopen("obj_prog.txt", "w");	

	if( ptr == NULL ){
		fprintf(stderr,"Object Program Open Error!");
		return;
	}
	//오브젝트를 적을 txt를 불러오거나 생성할때 문제가 생기면 에러
	
	fprintf(ptr, "H %s %s %s\n", sc[0].label, sc[0].loc, total);	
	//첫줄 H 이름 시작주소 전체길이 를 적는다.

	for(i=1; i<sc_count; i++){
		if( strcmp( sc[i].opcode, "RESW" ) == 0 || strcmp( sc[i].opcode, "RESB" ) == 0)
			continue;
	//RESW나 RESB를 만나면 아무것도 하지않고 넘어간다.
		else if(strcmp( sc[i].opcode, "WORD" ) == 0)
		{	
			temp = atoi(sc[i].symbol);
			for(j=5;j>=0;j--){		
	//word에서는 큰 숫자가 나오면 hexa_check로 16진수를 나타낼 수 없기 때문에 여기서 mod 16을 통해 수를 적절히 배치해준다
				tmp1[j] = '0'+temp%16;
				temp = temp/16;
			}
			tmp1[6]='\0';
			hexa_check(tmp1,6);//16진수 나타낼 때 알파벳들이 제대로 나오게 하기 위해 
			strcpy(sc[i].code,tmp1);
		}
		else if(strcmp( sc[i].opcode, "BYTE" ) == 0){
			if(sc[i].symbol[0]=='C'){
				for(j=2;j<strlen(sc[i].symbol)-1;j++){ 	//C'~'
					tmp2[j-2] = sc[i].symbol[j]; 	//필요없는 부분은 없애는 과정
				}
				tmp2[j-2]='\0';
				for(j=0;j<strlen(tmp2);j++){
					temp = tmp2[j]; 
		//temp는 그 문자의 아스키코드 값
					sc[i].code[j*2+1] = '0'+temp%16; 
		//문자의 경우 아무리 커도 FF를 넘지 않기 때문에 2자리면 충분히 그 값을 나타낼 수 있음
					sc[i].code[j*2] = '0'+temp/16;	//
				}
				sc[i].code[j*2]='\0';
				hexa_check(sc[i].code,strlen(sc[i].code));
			}	
			else if(sc[i].symbol[0]=='X'){
				for(j=2;j<strlen(sc[i].symbol)-1;j++){ 	//X'~'
					sc[i].code[j-2] = sc[i].symbol[j]; 
		//X의 경우엔 X'05'에서 바로 05로 저장
				}
				sc[i].code[j-2]='\0';
			}	
		}
		//BYTE 에서 C일경우 배열에 C'내용' 통째로 복사한 후, 내용을 제외한 C'~'를 지웁니다.
		//그 후, 내용부분인 아스키코드를 16진수로 바꾼 후 배열6자리에 형식에 맞게 넣습니다.
		else{
			idx = find_location( sc[i].symbol );
		
			for(j=3; j>=0; j--){
				code[j] = sc[idx-1].loc[j+2];
			}
			code[4] = '\0';
			strcat( sc[i].code, code );
		}
		

	}
	
	for(i=1; i<sc_count; i=i+j-1){
		for(j=i; line_length<30&&j<sc_count ;j++){
			if( strcmp(sc[j].opcode, "RESW" ) == 0|| strcmp( sc[j].opcode, "RESB" ) == 0)
				continue;
				//이 부분을 continue로 안넘어가면 목적코드에 나오지도 않는 메모리 할당한 것까지 길이에 계산하게 
			line_length += variable_checking(j);// 길이 확인
		}
 		if(line_length<16)
			fprintf(ptr,"T %s 0%X ",sc[i-1].loc,line_length);
		else
 			fprintf(ptr,"T %s %X ",sc[i-1].loc,line_length);
		for(k=i; k<j; k++)//j의 값은 그 길이까지의 줄 위치이기 때문에 i부터 시작해서 j까지	
			fprintf(ptr, "%s ", sc[k].code);
		fprintf(ptr,"\n");
		line_length = 0;
	}
	// for end
	fprintf( ptr, "E %s\n",sc[0].loc);
	fclose( ptr );
}

void find_opcode_in_optable( int sc_c, char chk[] ){
	int i;
	for(i=0; i<MAX; i++){
		if( strcmp( chk, opt[i].instruction_word ) == 0 ){
			//load_optable()에서 만든 opt구조체와 단어 chk와 비교해 찾습니다.
			strcpy( sc[sc_c].code, opt[i].opcode );
			//만약 찾게되면 sc구조체에 있는 code부분에 찾은 opt구조체의 opcode를 복사합니다.
			break;
			//만약 찾게되면 거기서 끝냅니다.
		}
	}
}

//hexa_check에서 매개변수에 length 추가했어요. BYTE에서 문자길이가 6을 넘어갈 수 있어서
//만약에 C'ABCDEF' 이런식으로 주면 원래의 5부터 0까지로는 다 16진수로 바꿔줄 수 없어서
void hexa_check( char* loc, int length){
	int i;
	for(i=length-1; i>=0; i--){
		if( loc[i] < 'A' && loc[i] > '9' ){
			loc[i] = 'A'+ loc[i] - '9' - 1;
		}
		if( loc[i] > 'F' ){
			loc[i] = '0' + loc[i] - 'F' - 1;
			loc[i-1]++;
		}
	}
	//아스키 코드에서 0~9와 A~F사이의 간격을 없애줍니다.
}

