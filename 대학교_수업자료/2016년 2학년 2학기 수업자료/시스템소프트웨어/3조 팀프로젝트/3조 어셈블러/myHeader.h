#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX 100//배열 최대 갯수 

typedef struct{
	char opcode[10];//op코드
	char instruction_word[10];//코드의 16진수 표기
}table;//optable파일에 기록된 opcode와 코드에 해당하는 16진수 값을 저장해 놓는 구조체

typedef struct{
	char label[10];
	char opcode[10];
	char symbol[10];
	char loc[10];
	char code[10];
}sourceCodes;//SIC 파일에서 label, opcode, symbol, loc, code 부분으로 나눠진 다음 저장되어 지는 구조체 

table opt[MAX];//테이블 구조체 배열
sourceCodes sc[MAX];//소스코드 구조체 배열

//1pass//
int find_location(char[]);//위치를 찾는 함수
int variable_checking(int);//opcode에서 RESB 또는 RESW 또는 BYTE인지 체크하는 함수

//2pass//
void find_opcode_in_optable( int sc_c, char chk[] );//파일에 있는 opcode와 optable에 있는 opcode를 비교해서 코드를 찾아서 저장하는 함
void hexa_check( char* loc,int length );// sourceCodes에 loc가 16진수로 코드로 알맞게 짜여져 있는지 검사하는 함수
void make_obj_prog(int, char[]);//objcode파일 생성 함수 
void load_optable();//optable파일을 불러오는 함수
