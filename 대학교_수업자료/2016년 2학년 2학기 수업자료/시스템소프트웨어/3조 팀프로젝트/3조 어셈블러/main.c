#include "myHeader.h"

int main(int argc, char* argv[])
{
	char* token;//토큰
	char str[MAX];//헤더의 구조체 SourceCode 의 배열 
	char start_loc[7];//주소길이
	char total_count[7] = "000000";//첫 번째 주소
	int sc_count = 0;//헤더 구조체 SourceCode 의 갯수(구조체 갯수)
	int i, hexchk, flag;//i는 반복문용 hexchk는 16진수 체크용 flag는 토큰을 나눴는지 확인해주는 역할
	int jump;
	//jump는 헤더의 SourceCoded의 구조체 sc에서 opcode부분에서 RESW 또는 RESB 또는 BYTE 부분을 
	//처리하기 위한 변수
	FILE* ptr = fopen("real.s", "r");//real.s파일 읽기 모드로 불러오기
	
	if( ptr == NULL )//SIC코드파일 열기에러 확인
	{
		fprintf(stderr,"File Open Error!");
	}
	
	load_optable();//SIC op테이블 불러오기
	while( fgets( str, MAX, ptr ) != NULL )
	//파일열기로 불러온 'real.s' 의 ptr를 str에 다 입력할 때 까지 반복문 실행
	{
		flag = 0;//flag는 0으로 시작

		if( str[0] != '\t' )//첫 번째 부분이 탭 띄어쓰기가 아닐 경우
		{
			token = strtok( str, "\t\n");
			//탭 띄어쓰기 또는 한 줄 띄어쓰기 단위로 끊어낸 다음 끊어낸 문장을 token에 저장 
			flag = 1;//토큰으로 잘라서  token에 저장하면 flag를 1증가 
			strcpy( sc[sc_count].label, token );
			//끊어낸 토큰을 SourceCode의 label에 붙여넣는다.
			//여기서 real.s 파일에 기록된 op테이블 문장을 토큰으로 자른 다음
			//SourceCode에 있는 배열 label에 저장한다.(ex)ADD,LDA,STA
		}
		else//첫 번째 부분이 탭 띄어쓰기가 맞으면 
			strcpy( sc[sc_count].label, "");//탭 띄어쓰기일 경우 빈칸을  label에 붙여넣는다.	
		
		if( !flag ) token = strtok( str, "\t\n");
		//플래그가 안섰을 경우 탭 띄어쓰기 또는 한 줄 띄어쓰기로 str을 토큰으로 나눈 뒤 token에 저장
		else token = strtok( NULL, "\t\n");
		//플래그가 1 경우 NULL로 탭 띄어쓰기 또는 한 줄 띄어쓰기로 str을 토큰으로 나눈 뒤 token에 저장

		find_opcode_in_optable(sc_count, token);//token이 optable에 존재하는지 체크
		strcpy( sc[sc_count].opcode, token);//token을 real.s opcode에 넣음 
		
		token = strtok( NULL, "\t\n" );//token을 NULL로 초기
			
		if( sc_count == 0 )//count가 0일 경우 : SIC 첫 번째 부분일 때
		{
			for(i=0; i< 6 - strlen( token ); i++)//첫 번째 주소 입력
			{
				start_loc[i] = '0';
			}
			start_loc[i] = '\0';//주소 바로 다음에 끝 문장 입력

			strcat( start_loc, token );//start_loc와 token을 붙임
		}
		else//count가 0이 아니면
		{
			strcpy( sc[sc_count].symbol, token );//token을 real.s 의 symbol에 입력
			jump = variable_checking(sc_count);//jump를 variable_checking의 결과값으로 설정
			hexchk = jump/3;//hexchk을 jump값을 3으로 나눈 값으로 설정
			for(i=0; i<hexchk; i++)//hexchk 값 만큼 반복 
			{
				start_loc[5] += 3;//start_loc[5]에 들어있는 값에서 3만큼 증가
				total_count[5] += 3;//total_count[5]에 들어잇는 값에서 3만큼 증가
				hexa_check(start_loc,6);//start_loc로 hexa_check 돌림
				hexa_check(total_count,6);//total_count로 hexa_check 돌림
				jump -= 3;//jump 값은 3만큼 빼줌
			}
			start_loc[5] += jump;//start_loc[5]에 들어있는 값에서 jump값 만큼 더함
			total_count[5] += jump;//total_count[5]에 들어잇는 값에서 jumpr값 만큼 더함
			hexa_check(start_loc,6);//다시 start_loc로 hexa_check 돌림
			hexa_check(total_count,6);//다시 total_count로 hexa_check 돌림
		}
		strcpy( sc[sc_count].loc, start_loc );//start_loc 문장을 real.s 문서의 loc 값으로 입력 
		sc_count++;//sc_count값을 1증가 
	}
	make_obj_prog(sc_count, total_count);//오브젝트 파일 생성
	fclose(ptr);//파일 종료
	return 0;
}
