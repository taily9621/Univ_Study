#include<stdio.h>
#include<string.h>

#define MAX 100

typedef struct{
	char opcode[10];
	char mnemonic[10];
}table;

typedef struct{
	char label[10];
	char opcode[10];
	char symbol[10];
	char loc[10];
	char code[10];
}sourceCodes;

table t[MAX];
sourceCodes sc[MAX];

void make_object(int, char[]);
void load_table();
int find_location(char[]);
int variable_checking(int);
