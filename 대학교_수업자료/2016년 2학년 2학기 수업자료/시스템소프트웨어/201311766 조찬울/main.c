#include "myHeader.h"

void table_check( int sc_c, char chk[] ){
	int i;
	for(i=0; i<MAX; i++){
		if( strcmp( chk, t[i].mnemonic ) == 0 ){
			strcpy( sc[sc_c].code, t[i].opcode );
			break;
		}
	}
}

void hexa_check( char* loc ){
	int i;
	int diff;

	for(i=5; i>=0; i--){
		if( loc[i] < 'A' && loc[i] > '9' ){
			diff = loc[i] - '9' - 1;
			loc[i] = 'A' + diff;
		}
		if( loc[i] > 'F' ){
			diff = loc[i] - 'F' - 1;
			loc[i] = '0' + diff;
			loc[i-1]++;
		}
	}
}

int main(int argc, char* argv[]){
	char* token;
	char str[MAX];
	char start_loc[7];
	char total_count[7] = "000000";
	int sc_count = 0;
	int i, hexchk, flag;
	int jump, op;
	FILE* ptr = fopen("real.s", "r");
	
	if( ptr == NULL ){
		fprintf(stderr,"File Open Error!");
		return;
	}
	
	load_table();
	while( fgets( str, MAX, ptr ) != NULL ){
		flag = 0;

		/*for(i=0; i<strlen(str);i++){
			printf("Boolean : %d > ", str[i] == '\t');
			printf("%c\n",str[i]);
		}*/

		if( str[0] != '\t' ){
			token = strtok( str, "\t\n");
			flag = 1;
			strcpy( sc[sc_count].label, token );
		}
		else
			strcpy( sc[sc_count].label, "");	

		//printf("LABEL : %s flag = %d\n", token, flag );
		
		if( !flag ) token = strtok( str, "\t\n");
		else token = strtok( NULL, "\t\n");
		
		//printf("OPCODE : %s\n", token);

		table_check(sc_count, token);
		strcpy( sc[sc_count].opcode, token);
		
		token = strtok( NULL, "\t\n" );
		
		//printf("SYM or LOC : %s\n", token );		
		if( sc_count == 0 ){
			for(i=0; i< 6 - strlen( token ); i++){
				start_loc[i] = '0';
			}
			start_loc[i] = '\0';

			strcat( start_loc, token );
		}
		else{
			strcpy( sc[sc_count].symbol, token );
			jump = variable_checking(sc_count);
			hexchk = jump/3;
			for(i=0; i<hexchk; i++){
				start_loc[5] += 3;
				total_count[5] += 3;
				hexa_check(start_loc);
				hexa_check(total_count);
				jump -= 3;
			}
			start_loc[5] += jump;
			total_count[5] += jump;
			hexa_check(start_loc);
			hexa_check(total_count);
		}
		strcpy( sc[sc_count].loc, start_loc );
		sc_count++;
	} // while end
	make_object(sc_count, total_count);
	fclose(ptr);
	return 0;
}
