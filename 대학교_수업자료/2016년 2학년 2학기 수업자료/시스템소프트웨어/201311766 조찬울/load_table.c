#include "myHeader.h"

void load_table(){
	FILE* ptr;
	int i = 0;

	if( ( ptr = fopen( "optable", "r") ) == NULL ){
		fprintf(stderr,"Optable Open Error");
		return;
	}
	while( !feof(ptr) ){
		fscanf(ptr, "%s %s", t[i].mnemonic, t[i].opcode );
		i++;
	}
}
