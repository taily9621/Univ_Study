#include "myHeader.h"

int variable_checking(int sc_count){
	int i, tmp, flag = 0;

	if( strcmp( sc[sc_count].opcode, "RESW" ) == 0 ){
		tmp = atoi( sc[sc_count].symbol );
		return tmp * 3;
	}
	else if( strcmp( sc[sc_count].opcode, "BYTE" ) == 0 ){
		tmp = 0;
		for(i=0; i<strlen( sc[sc_count].symbol); i++){
			if( sc[sc_count].symbol[i] == '\'' && flag == 0 )
				flag = 1;
			else if( sc[sc_count].symbol[i] == '\'' && flag == 1)
				flag = 0;
			if( flag == 0 ) continue;
			else if(flag == 1 ) tmp++;
		}
		return tmp;
	}
	else if( strcmp( sc[sc_count].opcode, "RESB" ) == 0 ){
		tmp = atoi( sc[sc_count].symbol );
		return tmp * 1;
	}
	else return 3;
}
