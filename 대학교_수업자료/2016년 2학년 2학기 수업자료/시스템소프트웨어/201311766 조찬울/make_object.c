#include "myHeader.h"

void make_object(int sc_count, char total[]){
	int i,j;
	int idx;
	char* obj_code;
	char code[5];
	FILE* ptr = fopen("obj_prog.obj", "w");

	if( ptr == NULL ){
		fprintf(stderr,"Object Program Open Error!");
		return;
	}

	fprintf(ptr, "H %s %s %s\n", sc[0].label, sc[0].loc, total);
	code[4] = '\0';
	for(i=1; i<sc_count; i++){
		if( strcmp( sc[i].opcode, "RESW" ) == 0 || strcmp( sc[i].opcode, "WORD" ) == 0 
			|| strcmp( sc[i].opcode, "RESB" ) == 0 || strcmp( sc[i].opcode, "BYTE" ) == 0 )
			break;
			
		idx = find_location( sc[i].symbol );
		
		for(j=3; j>=0; j--){
			code[j] = sc[idx-1].loc[j+2];
		}
		strcat( sc[i].code, code );
		fprintf( ptr, "T %s 03 %s\n", sc[i-1].loc, sc[i].code );
	}// for end
	fprintf( ptr, "E %s\n",sc[0].loc);
	fclose( ptr );
}
