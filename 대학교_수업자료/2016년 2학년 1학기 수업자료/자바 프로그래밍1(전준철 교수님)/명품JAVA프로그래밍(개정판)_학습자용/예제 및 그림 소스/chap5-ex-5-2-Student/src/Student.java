class Person {
	int age;			// Student 클래스에서 접근 가능
	public String name;	// Student 클래스에서 접근 가능
	protected int height;	// Student 클래스에서 접근 가능
	private int weight;	// Student 클래스에서 접근 불가
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getWeight() {
		return weight;
	}
}

public class Student extends Person {
	void set() {
		age = 30;
		name = "홍길동";
		height = 175;
		setWeight(99);

	}
	public static void main(String[] args) {
		Student s = new Student();
		s.set();
	}
}