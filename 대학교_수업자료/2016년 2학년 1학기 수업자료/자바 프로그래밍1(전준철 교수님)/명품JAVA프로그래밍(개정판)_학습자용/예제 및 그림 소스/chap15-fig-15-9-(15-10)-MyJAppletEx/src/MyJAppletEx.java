import java.awt.*;
import javax.swing.*;

public class MyJAppletEx extends JApplet {
	String text=null;
	int x;
	int y;
	int fontSize;
	
	public void init() { // 애플릿이 생성되는 초기에 단 한 번 호출
		text = "Hello. It's Applet"; // 출력할 문자열
		x = 30; // text가 츨력될 x 위치=30
		y = 30; // text가 츨력될 y 위치=30
		fontSize = 20; // 폰트 크기 20
		
		// MyPanel을 컨텐트팬으로 사용
		setContentPane(new MyPanel());
	}
	public void start() {}
	public void stop() {}
	public void destroy() {}
	
	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) {// 스윙 애플릿 내부를 그린다.
			super.paintComponent(g);
			g.setColor(Color.YELLOW);
			g.fillRect(0,0, getWidth(), getHeight()); // 애플릿 전체를 노란색으로 채운다.
			g.setColor(Color.RED);
			g.setFont(new Font("SanSerif", Font.ITALIC, fontSize));
			g.drawString(text, x, y); // text를 (x,y) 위치에 빨간색으로 출력한다.
		}
	}
}
