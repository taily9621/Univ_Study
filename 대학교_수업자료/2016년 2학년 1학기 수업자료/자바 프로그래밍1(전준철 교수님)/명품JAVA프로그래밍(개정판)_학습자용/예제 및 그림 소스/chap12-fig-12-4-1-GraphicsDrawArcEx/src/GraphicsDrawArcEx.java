import javax.swing.*;
import java.awt.*;

public class GraphicsDrawArcEx extends JFrame {
	Container contentPane;
	GraphicsDrawArcEx() {
		setTitle("drawArc ���  ����");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = getContentPane();
		MyPanel panel = new MyPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		setSize(200, 300);
		setVisible(true);
	}

	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.RED);
			g.drawArc(20,100,80,80,90,270);
		}	
	}
	
	public static void main(String [] args) {
		new GraphicsDrawArcEx();
	}
}