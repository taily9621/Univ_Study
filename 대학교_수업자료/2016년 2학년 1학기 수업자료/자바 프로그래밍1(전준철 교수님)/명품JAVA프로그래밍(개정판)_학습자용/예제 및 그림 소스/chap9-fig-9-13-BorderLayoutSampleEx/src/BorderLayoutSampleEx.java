import javax.swing.*;
import java.awt.*;

public class BorderLayoutSampleEx extends JFrame {
	BorderLayoutSampleEx() {
		setTitle("BorderLayout Samples");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(new JButton("add"), BorderLayout.NORTH);
		add(new JButton("sub"), BorderLayout.SOUTH);
		add(new JButton("mul"), BorderLayout.EAST);
		add(new JButton("div"), BorderLayout.WEST);
		add(new JButton("Calculate"), BorderLayout.CENTER);

		setSize(300, 200);
		setVisible(true);
	}
	public static void main(String[] args) {
		new BorderLayoutSampleEx();
	}
}
