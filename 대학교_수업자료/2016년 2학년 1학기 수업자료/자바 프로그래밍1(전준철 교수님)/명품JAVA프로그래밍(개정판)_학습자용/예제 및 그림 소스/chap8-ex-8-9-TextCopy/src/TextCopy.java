import java.io.*;

public class TextCopy {
	public static void main(String[] args){
		File src = new File("c:\\windows\\system.ini"); // 복사할 파일
		File dst = new File("c:\\tmp\\system.txt"); // 복사될 파일
		FileReader fr = null;
		FileWriter fw = null;
		BufferedReader in = null;
		BufferedWriter out = null;
		int c;
		
		try {
			fr = new FileReader(src); // 파일 입력 문자 스트림에 연결
			fw = new FileWriter(dst); // 파일 출력 문자 스트림에 연결
			in = new BufferedReader(fr); // 버퍼 입력 스트림에 연결
			out = new BufferedWriter(fw); // 버퍼 출력 스트림에 연결
			while ((c = in.read()) != -1) {
				out.write((char)c);
			}
            in.close();
            out.close();
            fr.close();
            fw.close();
		} catch (IOException e) {
			System.out.println("파일 복사 오류");
		}
	}
}