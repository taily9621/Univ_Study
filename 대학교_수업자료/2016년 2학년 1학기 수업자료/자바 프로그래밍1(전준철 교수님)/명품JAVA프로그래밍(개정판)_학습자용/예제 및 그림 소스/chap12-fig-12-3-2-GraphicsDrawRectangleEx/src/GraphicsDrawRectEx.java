import javax.swing.*;
import java.awt.*;

public class GraphicsDrawRectEx extends JFrame {
	Container contentPane;
	GraphicsDrawRectEx() {
		setTitle("drawRect ���  ����");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = getContentPane();
		MyPanel panel = new MyPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		setSize(200, 150);
		setVisible(true);
	}

	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.RED);
			g.drawRect(20,20,80,80);
		}	
	}
	
	public static void main(String [] args) {
		new GraphicsDrawRectEx();
	}
} 