import javax.swing.*;
import java.applet.*;
import java.net.URL;
	
public class AudioJAppletEx  extends JApplet {
	AudioClip clip=null; // 오디오 클립 저장 및 재생 객체
	
	public void init() {
		setContentPane(new MyPanel());
		
		// "ToYou.mid" 파일의 인터넷 경로명을 결정한다.
		URL audioURL = getClass().getResource("ToYou.mid");
		
		// "ToYou.mid" 파일을 로드하여 연주 가능한 형식으로 만든다.
		clip  = Applet.newAudioClip(audioURL);
	}
	public void start() {
		if(clip != null)
			clip.play(); // 오디오 클립의 음악을 연주한다.
		((MyPanel)getContentPane()).setText("오디오 연주가 시작되었습니다.");
	}
	public void stop() {
		if(clip != null)
			clip.stop(); // 오디오 클립의 연주를 멈춘다.
	}
	
	// 연주 상태를 보여주는 패널
	class MyPanel extends JPanel {
		JLabel label = new JLabel();
		MyPanel() {
			add(label);
		}
		void setText(String text) {
			label.setText(text);
		}
	}
}