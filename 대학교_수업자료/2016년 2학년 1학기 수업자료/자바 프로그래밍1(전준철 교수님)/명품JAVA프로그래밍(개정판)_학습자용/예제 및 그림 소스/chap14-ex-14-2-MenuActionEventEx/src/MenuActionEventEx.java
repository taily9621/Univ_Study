import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class MenuActionEventEx extends JFrame {
	Container contentPane;
	JLabel label = new JLabel("Hello");
	
	MenuActionEventEx() {
		setTitle("Menu에 Action 리스너 만들기  예제");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = getContentPane();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(label, BorderLayout.CENTER);
		createMenu();
		setSize(250,200);
		setVisible(true);
	}
	
	// 메뉴바와 File 메뉴를 생성하고 File 메뉴에 4개의 메뉴아이템을 삽입한다.
	void createMenu() {
		JMenuBar mb = new JMenuBar(); // 메뉴바 생성
		JMenuItem [] menuItem = new JMenuItem [4];
		String[] itemTitle = {"Color", "Font", "Top", "Bottom"};
		JMenu fileMenu = new JMenu("Text");
		
		// 4 개의 메뉴아이템을 File 메뉴에 삽입한다.
		for(int i=0; i<menuItem.length; i++) {
			menuItem[i] = new JMenuItem(itemTitle[i]); // 메뉴아이템 생성
			menuItem[i].addActionListener(new MenuActionListener()); //메뉴아이템에 Action 리스너 등록
			fileMenu.add(menuItem[i]); // 메뉴아이템을 File 메뉴에 삽입
		}
		mb.add(fileMenu); // 메뉴바에 File 메뉴 삽입
		setJMenuBar(mb); // 프레임에 메뉴바를 삽입한다.
	}
	
	// Action 리스너로서, 메뉴아이템이 선택되었을 때 처리한다.
	class MenuActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String cmd = e.getActionCommand();
			if(cmd.equals("Color")) // Color 메뉴아이템 선택된 경우
				label.setForeground(Color.BLUE);
			else if(cmd.equals("Font")) // Font 메뉴아이템 선택된 경우 
				label.setFont(new Font("Ravie", Font.ITALIC, 30));
			else if(cmd.equals("Top"))  // Top 메뉴아이템 선택된 경우
				label.setVerticalAlignment(SwingConstants.TOP);				
			else  // Bottom 메뉴아이템 선택된 경우
				label.setVerticalAlignment(SwingConstants.BOTTOM);				

		}
		
	}
	public static void main(String [] args) {
		new MenuActionEventEx();
	}
} 