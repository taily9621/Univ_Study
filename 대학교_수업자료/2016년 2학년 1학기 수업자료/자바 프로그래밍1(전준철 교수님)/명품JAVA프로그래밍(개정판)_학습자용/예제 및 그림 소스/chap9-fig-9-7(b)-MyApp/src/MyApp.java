import javax.swing.*;

class MyFrame extends JFrame {
	MyFrame() {
		setTitle("첫번째 프레임");
		setSize(300,300);
		setVisible(true);
	}
}

public class MyApp {
	public static void  main(String [] args) {
		new MyFrame();
	}
}
