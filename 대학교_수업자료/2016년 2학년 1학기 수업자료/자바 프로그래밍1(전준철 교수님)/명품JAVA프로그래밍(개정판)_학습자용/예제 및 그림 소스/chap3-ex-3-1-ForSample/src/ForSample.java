public class ForSample {
	public static void main (String[] args) {
		int i, j;

		for (j=0,i=1; i <= 10; i++) { 
			j = j + i;
			System.out.print(i);
			if (i==10) { // 10이면 + 대신 = 출력하고 덧셈 결과 출력
				System.out.print("=");
				System.out.print(j);
			}
			else // 1~9까지는 + 출력
				System.out.print("+");
		}
	}
}