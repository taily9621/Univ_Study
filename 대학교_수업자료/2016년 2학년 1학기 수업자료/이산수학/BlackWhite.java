package ball;
import java.util.*;
public class BlackWhite {

	public static void main(String[] args){
		int Black, White;
		Scanner scan = new Scanner(System.in);
		System.out.println("검은 공의 갯수를 입력하세요.");
		Black = scan.nextInt();
		System.out.println("흰 공의 갯수를 입력하세요.");
		White = scan.nextInt();
		if(oddNumber(White)){ //흰 공이 짝수인 경우
			System.out.print("마지막으로 남는 공의 색깔 : Black");
		} else{
			System.out.println("마지막으로 남는 공의 색깔 : White");
		}
	}
	
	private static boolean oddNumber(int input){ //짝수 / 홀수 판별
		if(input % 2 == 0) //짝수일 경우, True 를 return.
			return true;
		return false;
	}
}
