// Symbol
// ------
// Represents a symbol.

package ca.uottawa.nudd049.uprove.logic;

public class Symbol extends Formula {
	private char letter;
	
	public Symbol(char l) {
		// Input must be a letter between a and z.
		if(charIsEnglishLetter(l)) {
			letter = l;
		} else {
			throwException();
		}
	}
	
	public char getLetter() {
		return letter;
	}
	
	public String toString() {
		char[] charArr = new char[1];
		charArr[0] = letter;
		return new String(charArr);
	}
	
	public static boolean charIsEnglishLetter(char l) {
		return ((l >= 'a') && (l <= 'z')) || ((l >= 'A') && (l <= 'Z'));
	}
	
	private void throwException() throws IllegalArgumentException {
		throw new IllegalArgumentException("Input to Symbol(char) must be an English letter");
	}
}