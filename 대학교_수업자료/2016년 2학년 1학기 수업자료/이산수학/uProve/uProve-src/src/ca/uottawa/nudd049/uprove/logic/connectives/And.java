// And
// --
// The connective And.

package ca.uottawa.nudd049.uprove.logic.connectives;

import ca.uottawa.nudd049.uprove.logic.*;

public class And extends Connective {
	public And(Formula f1, Formula f2) {
		super(f1,f2);
	}
	
	protected void checkInput(Formula f1, Formula f2) {
		if(f1==null || f2==null)
			throw new IllegalArgumentException("An 'and' requires two formulas");
	}
	
	public String symbolToString() {
		return "&";
	}
}