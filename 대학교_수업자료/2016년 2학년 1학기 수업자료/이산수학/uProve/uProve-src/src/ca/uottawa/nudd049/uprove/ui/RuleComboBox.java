// RuleComboBox
// ------------
// This class handels how to show the help text for the rules.

package ca.uottawa.nudd049.uprove.ui;

import java.awt.event.*;
import javax.swing.*;

public class RuleComboBox extends JComboBox {
	private JTextArea text;
	private String[] explanation;
	
	public RuleComboBox(JTextArea text) {
		this.text = text;
		
		String[] ruleNames = { "-- select a rule --", "&E", "&I", "Assumption", "Copy", "Df", "DN", "EFQ", "->E", "->I", "~E", "~I", "\\/E", "\\/I"};
		String[] explanation = {
				"",
				"With And Elimination, a line with p & q as its formula can be transformed into a line with either p or q as its formula. The list of dependencies does not change.",
				"By clicking on two lines the formulas on these lines can be \"and:ed\" together. The list of dependencies is the union of the first line's list of dependencies and the second line's list of dependencies.",
				"An assumption can always be made. The resulting line depends only on itself.",
				"Any line can be copied into a new line. The resulting line will have the same formula and the same dependencies as the copied one.",
				"A line with the formula (p <-> q) can be transform into a line with the formula (p -> q) & (q -> p) and vice versa. The list of dependencies does not change.",
				"If a line has ~~p as its formula, DN (Double Negation) can be applied to obtain a line with p as its formula. The list of dependencies does not change.",
				"Without changing the dependencies, a line with _|_ as its formula can be transformed into a line with any formula.",
				"By selecting one line of the form p -> q and one of the form p, you can get a line with the formula q. The list of dependencies is the union of the first line's list of dependencies and the second line's list of dependencies.",
				"If one line is an assumption, that line together with another line can be clicked to introduce an implication. If the first line had the formula p and the second formula q, the resulting formula will be p -> q.",
				"By selecting two lines, one with ~p as its formula and one with p as its formula, a new line with a contradiction as its formula can be added. The list of dependencies is the union of the first line's list of dependencies and the second line's list of dependencies.",
				"An assumption with the formula p can be selected together with a line that has a contradiction as its formula. The resulting line will have ~p as its formula. The list of dependencies is the same as the second line's list of dependencies except that any occurrence of the first line is removed.",
				"In order to eliminate the connective or, five lines must be selected. The first one is the line that contains the or. The second is where the left half of the or is assumed, and the forth is where the right haft of the or is assumed. The third and fifth are the results of each subproof respectively, and these must contain the same formula. The resulting line has the formula stated on line three and five as its formula. When it comes to dependencies, firstly line two is removed from line three's dependencies and line four is removed from line five's dependencies. When that is done the resulting line of dependencies is the union of the dependencies listed on line one, three and five.",
				"By selecting any line, say with formula p, both p \\/ q and q \\/ p can be inferred. The list of dependencies will not change." 				
		};
		this.explanation = explanation;
		
		setModel(new javax.swing.DefaultComboBoxModel(ruleNames));
	    addActionListener(new RuleComboBoxActionListener());
	}
	
	private class RuleComboBoxActionListener implements ActionListener {
    	public void actionPerformed(ActionEvent ae) {
    		text.setText(explanation[((JComboBox)ae.getSource()).getSelectedIndex()]);
    	}		
	}
}
