// Connective
// ----------
// This is the superclass of all the connectives. It is also a subclass of Formula.

package ca.uottawa.nudd049.uprove.logic;

public abstract class Connective extends Formula {
	protected Formula formula1 = null;
	protected Formula formula2 = null;
	
	protected abstract void checkInput(Formula f1, Formula f2);	
	public abstract String symbolToString();
	
	public Connective(Formula f1, Formula f2) {
		checkInput(f1, f2);
		formula1 = f1;
		formula2 = f2;
	}
	
	public Formula getSubformula1() {
		return formula1;
	}

	public Formula getSubformula2() {
		return formula2;
	}
	
	public String toString() {
		return toString(false);
	}
	
	public String toString(boolean skipParenthesesOnce) {
		// This function is overridden in Not 
		String s = new String();
		if(!skipParenthesesOnce)
			s += '(';
		if(formula1 != null)
			s += formula1.toString() + ' ';
		s += symbolToString();
		if(formula2 != null)
			s += ' ' + formula2.toString();
		if(!skipParenthesesOnce)
			s += ')';		
		return s;
	}
}