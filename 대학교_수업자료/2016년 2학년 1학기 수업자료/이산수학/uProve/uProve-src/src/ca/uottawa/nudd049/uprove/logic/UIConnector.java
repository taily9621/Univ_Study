// UIConnector
// -----------
// This class is responsible for connecting the ui and logic layers. 

package ca.uottawa.nudd049.uprove.logic;

import ca.uottawa.nudd049.uprove.persistence.*;
import ca.uottawa.nudd049.uprove.ui.*;
import java.io.*;

public class UIConnector {
	private GUIMain gui;
	private Proof proof = null;
	private ProofLine[] suggestions;
	
	public void registerGUI(GUIMain gui) {
		this.gui = gui;
	}
	
	public void newClicked() {
		UIConnectorNew uicNew = new UIConnectorNew(this);
		GUINew guiNew = new GUINew(uicNew, gui);
		uicNew.registerGUI(guiNew);
		guiNew.setVisible(true);
	}
	
	public void exitClicked() {
		System.exit(0);
	}
	
	public void startNewProof(Proof newProof) {
		proof=newProof;
		refreshGUI();
	}
	
	public void resetSuggestions() {
		updateSuggestions(new boolean[0]);
	}
	
	public void updateSuggestions(boolean[] selectedLines) {
		if(proof.isProofCompleted())
			return;
		
		int length = countSelected(selectedLines);
		
		ProofLine[] plaAll = proof.getProofLines();
		ProofLine[] plaSelected = new ProofLine[length];
		int plaSelectedIndex = 0;
		
		for(int i = 0; i < plaAll.length && i < selectedLines.length; i++) {
			if(selectedLines[i])
				plaSelected[plaSelectedIndex++] = plaAll[i];
		}
		
		RuleTester rt = new RuleTester();
		suggestions = rt.test(plaSelected, proof.getNextLineNumber());
		
		showNewSuggestions();
	}
	
	public void showNewSuggestions() {
		gui.emptySuggestionPanel();
		for(int i = 0; i < suggestions.length; i++) {
			String[] s = suggestions[i].toStrings();
			gui.createAndAddSuggestion(s[0],s[1],s[2],s[3]);
		}
	}
	
	public void addSuggestionAsProofLine(int index) {		
		if(suggestions[index].hasPlaceholder()) {
			FormulaEnterer fe = new FormulaEnterer(gui);
			Formula f = fe.getFormulaFromUser("Enter formula to replace placeholder.");	
			if(f != null)
				suggestions[index].changePlaceholder(f);
			else
				return;
		}
		
		proof.addProofLine(suggestions[index]);
		refreshGUI();
	}
	
	public void refreshGUI() {
		gui.emptyProofPanel();
		
		ProofLine[] pla = proof.getProofLines();
		for(int i = 0; i < pla.length; i++) {
			String[] s = pla[i].toStrings();
			gui.createAndAddProofLine(s[0],s[1],s[2],s[3]);
		}
		
		gui.setConclusion(proof.getConclusion().toString(true));
		
		resetSuggestions();
		
		if(proof.isProofCompleted())
			gui.proofCompleted();
	}
	
	private int countSelected(boolean[] arr) {
		int length = 0;
		for(int i = 0; i < arr.length; i++) {
			if(arr[i])
				length++;
		}
		return length;
	}
	
	public void saveProof(File file) {
		if(proof == null) {
			gui.showErrorMessage("Unable to save proof since no proof is active.");
			return;
		}
		try {
			ProofToFile ptf = new ProofToFile();
			ptf.write(proof,file);
		} catch(Exception e) {
			gui.showErrorMessage("The proof could not be saved!\n\nReason: " + e.toString());
		}
	}

	public void openProof(File file) {
		try {
			ProofFromFile pff = new ProofFromFile();
			proof = pff.read(file);
			refreshGUI();
		} catch(Exception e) {
			gui.showErrorMessage("The proof could not be saved!\n\nReason: " + e.toString());
		}
	}
}