// Contradiction
// -------------
// This class represents a contradiction.

package ca.uottawa.nudd049.uprove.logic;

public class Contradiction extends Formula {
	public String toString() {
		// The wanted symbol (⊥) is not appearing correctly in the GUI
		return new String("_|_");
	}
}