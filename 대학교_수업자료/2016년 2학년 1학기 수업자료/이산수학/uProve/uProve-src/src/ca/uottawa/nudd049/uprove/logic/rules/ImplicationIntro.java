// Implication Introduction
// ========================
// Input:
//		j					(j)		p				Assumption
//		b1,...,bu			(k)		q
// Output:
//		{b1,...,bu}/j		(m)		p -> q			j,k ->I

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class ImplicationIntro extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extract the two formulas and put them together with an Implication
		Formula f1 = pla[0].getFormula();
		Formula f2 = pla[1].getFormula();
		Formula f = new Implication(f1,f2);
		
		// Extract the latter dependencies and remove the first ProofLine from it
		Dependency d2 = pla[1].getDependency();
		Dependency d = new Dependency(d2,pla[0]);
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first ProofLine should be an assumption
		if(!(pla[0].getJustification() instanceof Assumption))
				return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 2;
	}
	
	public String toString() {
		return lineNumberToString() + "->I";
	}
}