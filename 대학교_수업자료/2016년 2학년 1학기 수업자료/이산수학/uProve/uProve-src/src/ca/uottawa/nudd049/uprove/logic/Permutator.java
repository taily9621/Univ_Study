// Permutator
// ----------
// This is a helper class for the Rule. Permutator is responsible for producing all possible
// permutations of a given set of objects (in this case ProofLines).

package ca.uottawa.nudd049.uprove.logic;

import java.util.*;

public class Permutator {
	private Object[] objArr;
	private Vector<int[]> permutations;
	
	public Permutator(Object[] o) {
		objArr = o;
		permutations = new Vector<int[]>();

		startPermutation();
	}
	
	public int getNumberOfPermutations() {
		return permutations.size();
	}
	
	public Object[] getPermutation(int index) {
		int[] permutation = permutations.get(index);
		Object[] result = new Object[permutation.length];
		for(int i = 0; i < permutation.length; i++) {
			result[i] = objArr[permutation[i]];
		}
		return result;
	}
	
	private void startPermutation() {
		int[] pos = new int[objArr.length];
		for(int i = 0; i < objArr.length; i++)
			pos[i] = i;
		
		int[] partial = new int[objArr.length];
		
		permute(pos, partial);
	}
	
	private void permute(int[] pos, int[] partial) {
		for(int i = 0; i < pos.length; i++) {
			int currentPos = pos[i];
			partial[partial.length-pos.length] = currentPos;
			
			if(pos.length == 1) {
				addPermutationToList(partial);
			} else {
				int[] rest = new int[pos.length-1];
				for(int oldIndex = 0, nextIndex = 0; oldIndex < pos.length; oldIndex++) {
					if(i != oldIndex)
						rest[nextIndex++] = pos[oldIndex];
				}
				permute(rest, partial);
			}
		}
	}
	
	private void addPermutationToList(int[] partial) {
		int[] toSave = partial.clone();
		permutations.add(toSave);
	}
}