// Rule
// ----
// Superclass for all rules, and subclass of Justification.

package ca.uottawa.nudd049.uprove.logic;

import java.util.Vector;

public abstract class Rule extends Justification {
	protected ProofLine[] originalProofLines = null;

	public abstract int getNumberOfArguments();
	public abstract boolean checkArguments(ProofLine[] pla);
	protected abstract ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof);	

	// Applies the Rule to the ProofLines in pla. Returns possible results, or null if
	// the lines cannot be applied.
	public ProofLine[] apply(ProofLine[] pla, int nextLineInProof) {
		if(pla == null || pla.length != getNumberOfArguments())
			return null;
		
		if(!checkArguments(pla))
			return null;
		
		ProofLine[] result = applyWithoutChecks(pla, nextLineInProof);
		
		if(result != null)
			originalProofLines = pla;
		
		return result;
	}
	
	// Try to apply the rule to the list of ProofLines sent. However, now they are rearranged
	// internally to get all possibilities.
	public ProofLine[] tryApply(ProofLine[] pla, int nextLineInProof) {
		if(pla == null || pla.length != getNumberOfArguments())
			return null;

		Vector<ProofLine> result = new Vector<ProofLine>();
		if(getNumberOfArguments() == 0) {
			// No permutations are possible, the Rule is called directly
			ProofLine[] plaResult = apply(pla, nextLineInProof);
			if(plaResult != null) {
				for(int i = 0; i < plaResult.length; i++)
					result.add(plaResult[i]);
			}
		} else {
			Permutator permutator = new Permutator(pla);
			for(int i = 0; i < permutator.getNumberOfPermutations(); i++) {	
				ProofLine[] plaPermutated = castFromObjToPL(permutator.getPermutation(i));
				if(checkArguments(plaPermutated)) {
					// Create a new rule, do the actual application and add the resulting ProofLine
					// to the list of lines to return.
					try {
						ProofLine[] plaResult = ((Rule)getClass().newInstance()).apply(plaPermutated, nextLineInProof);
						for(int j = 0; j < plaResult.length; j++) {
							result.add(plaResult[j]);
						}
					} catch(Exception e) {
						// The rule clearly could not be applied. Ignore the error.
					}
				}				
			}
		}
		
		return result.toArray(new ProofLine[result.size()]);
	}
		
	public ProofLine[] getProofLines() {
		return originalProofLines;
	}

	private static ProofLine[] castFromObjToPL(Object[] plaPermutatedObj) {
		ProofLine[] plaPermutated = new ProofLine[plaPermutatedObj.length];
		
		for(int j = 0; j < plaPermutatedObj.length; j++)
			plaPermutated[j] = (ProofLine)plaPermutatedObj[j];
		
		return plaPermutated;
	}
}