// And Introduction
// ================
// Input:
//		a1,...,an			(j)		p
//		b1,...,bu			(k)		q
// Output:
//		a1,...,an,b1,...,bu	(m)		p & q			j,k &I

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class AndIntro extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extract the two formulas and put them together with an And
		Formula f1 = pla[0].getFormula();
		Formula f2 = pla[1].getFormula();
		Formula f = new And(f1,f2);
		
		// Extract the two dependencies and put them together
		Dependency d1 = pla[0].getDependency();
		Dependency d2 = pla[1].getDependency();
		Dependency d = new Dependency(d1,d2);
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// As long as two formulas are given, they can be put together 
		return true;
	}
	
	public int getNumberOfArguments() {
		return 2;
	}
	
	public String toString() {
		return lineNumberToString() + "&I";
	}
}
