// Placeholder
// -----------
// This class represents a Placeholder.

package ca.uottawa.nudd049.uprove.logic;

public class Placeholder extends Formula {
	public String toString() {
		// The wanted symbol (Φ) is not appearing correctly in the GUI
		return new String("[?]");
	}
}