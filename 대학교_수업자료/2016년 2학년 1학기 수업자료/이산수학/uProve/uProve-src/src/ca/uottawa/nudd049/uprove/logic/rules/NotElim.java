// Not Elimination
// ===============
// Input:
//		a1,...,an				(j)		~p
//		b1,...,bu				(k)		p
// Output:
//		a1,...,an,b1,...,bu		(m)		[contradiction]			j,k ~E

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class NotElim extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Create the resulting formula
		Formula f = new Contradiction();
		
		// Extract the two dependencies and put them together
		Dependency d1 = pla[0].getDependency();
		Dependency d2 = pla[1].getDependency();
		Dependency d = new Dependency(d1,d2);
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// Extract the formulas from the ProofLines
		Formula f1 = pla[0].getFormula();
		Formula f2 = pla[1].getFormula();
		
		// The first formula must be a Not
		if(!(f1 instanceof Not))
			return false;
		
		// Extract the subformula from the first formula and compare this with the second formula
		Formula f1sub = ((Connective)f1).getSubformula1();		
		if(!f1sub.equals(f2))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 2;
	}
	
	public String toString() {
		return lineNumberToString() + "~E";
	}
}