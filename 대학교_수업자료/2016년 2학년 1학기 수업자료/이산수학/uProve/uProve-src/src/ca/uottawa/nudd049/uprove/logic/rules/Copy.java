// Copy
// ================
// Input:
//		a1,...,an			(j)		p
// Output:
//		a1,...,an			(m)		p			j Copy

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;

public class Copy extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Copy the dependencies and the formula
		Dependency d = pla[0].getDependency();
		Formula f = pla[0].getFormula(); 
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// Anything goes
		return true;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
	
	public String toString() {
		return lineNumberToString() + "Copy";
	}
}