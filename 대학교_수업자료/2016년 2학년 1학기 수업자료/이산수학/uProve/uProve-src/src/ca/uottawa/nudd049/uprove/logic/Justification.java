// Justification
// -------------
// This class represents a ProofLine's justification

package ca.uottawa.nudd049.uprove.logic;

abstract public class Justification {

	public String lineNumberToString() {
		String result = new String();
		
		if(this instanceof Rule)
			result += ProofLine.getLineNumbersAsString(((Rule)this).originalProofLines);
		
		if(result.length() > 0)
			result += " ";
		
		return result;
	}
	
	public String toString() {
		String result = lineNumberToString();		
		result += getClass().getSimpleName();

		return result;
	}
}