// Main
// ----
// Starts the actual program.

package ca.uottawa.nudd049.uprove.logic;

import ca.uottawa.nudd049.uprove.ui.GUIMain;

public class Main {
	public static void main(String[] args) {
		// Create UIConnector and GUI
		UIConnector uic = new UIConnector();
		GUIMain gui = new GUIMain(uic);
				
		// Register the gui with the UIConnector 
		uic.registerGUI(gui);
		
		// Show the GUI
		gui.setVisible(true);
	}
	
	public static String getVersion() {
		return "1.0.0";
	}
	
	// Returns an array of the names of all rules currently available
	public static String[] getRules() {
		String[] result = {
				"AndElim",
				"AndIntro",
				"Assumption",
				"Copy",
				"Df1",
				"Df2",
				"DN",
				"EFQ",
				"ImplicationElim",
				"ImplicationIntro",
				"NotElim",
				"NotIntro",
				"OrElim",
				"OrIntro"
		};
		
		return result;
	}
}