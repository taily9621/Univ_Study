// Proof
// -----
// This class represents a Proof (as a Vector of ProofLines).

package ca.uottawa.nudd049.uprove.logic;

import java.util.Vector;

public class Proof {	
	private Vector<ProofLine> proofLines;
	private Formula conclusion;
	
	public Proof() {
		proofLines = new Vector<ProofLine>();
	}
	
	public void addProofLine(ProofLine pl) {
		proofLines.add(pl);
	}
	
	public void addPremise(Formula f) {
		ProofLine pl = new ProofLine(new Dependency(),getNextLineNumber(),f,new Premise());
		pl.getDependency().addProofLine(pl);
		addProofLine(pl);
	}
	
	public void setConclusion(Formula f) {
		conclusion = f;
	}

	public int getNextLineNumber() {
		if(proofLines.size() == 0)
			return 1;
		else {
			return (proofLines.get(proofLines.size() - 1)).getLineNumber() + 1;
		}
	}
	
	public ProofLine getProofLine(int index) {
		return proofLines.get(index);
	}
	
	public ProofLine[] getProofLines() {
		return proofLines.toArray(new ProofLine[proofLines.size()]);
	}
	
	public Formula getConclusion() {
		return conclusion;
	}
	
	public boolean isProofCompleted() {
		if(proofLines.size() == 0)
			return false;
		
		ProofLine lastProofLine = proofLines.get(proofLines.size() - 1);
		if(!lastProofLine.getDependency().containsOnlyPremises())
			return false;
		
		return lastProofLine.getFormula().toString().equals(conclusion.toString()); 
	}
}