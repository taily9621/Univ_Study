// DN
// ================
// Input:
//		a1,...,an				(j)		~~p
// Output:
//		a1,...,an				(k)		p					j DN

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class DN extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Create the resulting formula
		Formula f = ((Connective)((Connective)pla[0].getFormula()).getSubformula1()).getSubformula1();
		
		// Create the resulting dependency
		Dependency d = pla[0].getDependency();
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first line must be a Not
		Formula f = pla[0].getFormula();
		if(!(f instanceof Not))
			return false;
		
		// The subformula in the Not must be a Not too
		if(!(((Connective)f).getSubformula1() instanceof Not))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
}