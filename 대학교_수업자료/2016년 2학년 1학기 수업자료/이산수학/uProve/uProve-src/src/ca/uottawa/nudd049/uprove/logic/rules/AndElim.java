// And Elimination
// ===============
// Input:
//		a1,...,an	(j)		p & q
// Output:
//		a1,...,an	(k)		p				j &E
//		a1,...,an	(k)		q				j &E

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class AndElim extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Get the formula in the inplut ProofLine (e.g. p & q)
		Formula formula = pla[0].getFormula();
		
		// Extract the two subformulas (e.g. p, q)
		Formula formula1 = ((Connective)formula).getSubformula1();
		Formula formula2 = ((Connective)formula).getSubformula2();
		
		// Get the dependency since that will be the same
		Dependency dependency = pla[0].getDependency();
		
		// Construct the two resulting ProofLines
		ProofLine result1 = new ProofLine(dependency,nextLineInProof,formula1,this,0);
		ProofLine result2 = new ProofLine(dependency,nextLineInProof,formula2,this,1);
		
		//Package result
		ProofLine[] result = {result1,result2};
		
		return result;
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// If the top-most formula is not And, AndElim cannot be applied
		return pla[0].getFormula() instanceof And;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
	
	public String toString() {
		return lineNumberToString() + "&E";
	}
}
