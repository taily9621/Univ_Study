// Dependency
// ----------
// This class handles a ProofLine's dependencies

package ca.uottawa.nudd049.uprove.logic;

import ca.uottawa.nudd049.uprove.logic.rules.Assumption;

public class Dependency {
	private ProofLine[] pla;
	
	public Dependency() {
		pla = new ProofLine[0];
	}
	
	public Dependency(ProofLine[] pla) {
		this.pla = pla;
	}
	
	// Create a new Dependency by calculating the union between two dependencies (d1 and d2)
	public Dependency(Dependency d1, Dependency d2) {
		pla = mergeProofLineArrays(d1.getProofLines(),d2.getProofLines());
	}
	
	// Create a new Dependency by copying all, except one (pl), of another Dependency's (d) ProofLines 
	public Dependency(Dependency d, ProofLine pl) {
		pla = removeProofLineFromArray(d.getProofLines(),pl);
	}
	
	public ProofLine[] getProofLines() {
		return pla;
	}
	
	public void addProofLine(ProofLine pl) {		
		ProofLine[] plaNew = new ProofLine[pla.length + 1];
		for(int i = 0; i < pla.length; i++)
			plaNew[i] = pla[i];
		plaNew[pla.length] = pl;

		pla = plaNew;
	}
	
	
	public boolean containsOnlyPremises() {	
		for(int i = 0; i < pla.length; i++) {
			if(pla[i].getJustification() instanceof Assumption)
				return false;
		}
		
		return true;
	}
	
	public String toString() {
		return ProofLine.getLineNumbersAsString(pla);
	}
	
	// Merge pla1 and pla2, the result should be in order and without duplicates
	private ProofLine[] mergeProofLineArrays(ProofLine[] pla1, ProofLine[] pla2) {
		// pla1 and pla2 are assumed to be in order and without duplicates
		
		// Create an array (initial size = size of pla1 + size of pla2)
		ProofLine[] plaMerged = new ProofLine[pla1.length + pla2.length];
		
		int pos1 = (pla1.length!=0)?0:-1;
		int pos2 = (pla2.length!=0)?0:-1;
		int pos = 0;

		// Loop until both pos1 and pos2 are -1. -1 indicates that there are no more elements in that
		// array
		while(pos1 != -1 || pos2 != -1) {
			ProofLine nextProofLine;
			
			if(pos1 == -1) {
				// If the array 1 is empty, just take the first element from array 2
				nextProofLine = pla2[pos2++];
			} else if(pos2 == -1) {
				// If the array 2 is empty, just take the first element from array 1
				nextProofLine = pla1[pos1++];
			} else {
				// If none is empty, we have to compare. The one that should go first is the one
				// with the smallest line number
				
				ProofLine pl1 = pla1[pos1];
				ProofLine pl2 = pla2[pos2];
				
				if(pl1.getLineNumber() == pl2.getLineNumber()) {
					// If the line numbers are the same, only one of them should be put in the
					// resulting array
					pos1++;
					pos2++;
					nextProofLine = pl1;
				} else if(pl1.getLineNumber() < pl2.getLineNumber()) {
					pos1++;
					nextProofLine = pl1;
				} else {
					pos2++;
					nextProofLine = pl2;
				}
			}
			
			plaMerged[pos++] = nextProofLine;

			if(pos1 >= pla1.length)
				pos1 = -1;			
			if(pos2 >= pla2.length)
				pos2 = -1;			
		}
		
		return trimArray(plaMerged, pos);
	}
	
	// Remove plToRemove from plaFull (if there is any plToRemove in plaFull)
	private ProofLine[] removeProofLineFromArray(ProofLine[] plaFull, ProofLine plToRemove) {
		ProofLine[] plaResult = new ProofLine[plaFull.length];
		int pos = 0;
		
		for(int i = 0; i < plaFull.length; i++) {
			if(!plaFull[i].equals(plToRemove))
				plaResult[pos++] = plaFull[i];
		}
		
		return trimArray(plaResult, pos);
	}

	// Reduce the array plaMerged to size trimLength
	private ProofLine[] trimArray(ProofLine[] plaIn, int trimLength) { 
		ProofLine[] plaTrimmed;
		
		if(trimLength < plaIn.length) {
			// Reduction needed
			plaTrimmed = new ProofLine[trimLength];
			for(int i = 0; i < trimLength; i++)
				plaTrimmed[i] = plaIn[i];
		} else {
			// No reduction needed
			plaTrimmed = plaIn;
		}
		
		return plaTrimmed;
	}
}