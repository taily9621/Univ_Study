// FormulaParser
// -------------
// This class is responsible for parsing a String into a Proof.

package ca.uottawa.nudd049.uprove.logic;

import ca.uottawa.nudd049.uprove.logic.connectives.*; 

public class FormulaParser {
	public Formula parse(String text) {
		Formula result = null;
		
		try {
			// Remove unnecessary chars
			text = text.trim();
			text = tryRemoveOuterParentheses(text);
			
			// Try each possibility in turn (the order depends on the scoping rules) 
			result = tryParseAsSymbol(text);
			if(result == null)
				result = tryParseAsContradiction(text);
			if(result == null)
				result = tryParseImplicationAndEquivalence(text);
			if(result == null)
				result = tryParseAndAndOr(text);
			if(result == null)
				result = tryParseNot(text);
			
		} catch(Exception e) {
			result = null;
		}
		
		return result;
	}
	
	// The formula (p -> q) and p -> q is the same, hence the outer parentheses can be removed
	public String tryRemoveOuterParentheses(String text) throws Exception {
		// Do basic tests first
		if(text.length() < 3)
			return text;
		if(!((text.charAt(0) == '(') && (text.charAt(text.length()-1) == ')')))
			return text;

		// Assume formula has outer parentheses
		boolean hasOuterParenthesis = true;

		// Try to out find where the parentheses that started at char 0 ends
		int numParen = 0;
		for(int i = 0; i < text.length()-1; i++) {		
			if(text.charAt(i) == '(')
				numParen++;
			else if(text.charAt(i) == ')') {				 
				numParen--;
				if(numParen == 0) {
					// First parentheses ended somewhere before the last char
					hasOuterParenthesis = false;
					break;
				}
			}
		}
		
		if(hasOuterParenthesis)
			return text.substring(1, text.length()-1).trim(); 			// Remove parentheses
		else
			return text;										 		// Return original String
	}
	
	public Formula tryParseAsSymbol(String text) throws Exception {
		Formula result = null;
		
		if((text.length() == 1) && (Symbol.charIsEnglishLetter(text.charAt(0))))
				result = new Symbol(text.charAt(0));
		
		return result;
	}
	
	public Formula tryParseAsContradiction(String text) throws Exception {
		if(text.equals("_|_"))
			return new Contradiction();
		else 
			return null;
	}
	
	public Formula tryParseImplicationAndEquivalence(String text) throws Exception {
		Formula result = null;

		// Go thorugh every char in the String, but do not consider things within parentheses
		int numParen = 0;		
		for(int i = 0; i < text.length(); i++) {		
			if(text.charAt(i) == '(')
				numParen++;
			else if(text.charAt(i) == ')') { 
				numParen--;
			}
			
			if(numParen == 0) {
				// Implication (->)
				if((text.charAt(i) == '-') && (i < text.length()-1) && (text.charAt(i+1) == '>')) {
					Formula subformula1 = parse(text.substring(0, i));
					Formula subformula2 = parse(text.substring(i+2));
					if(subformula1 != null && subformula2 != null)
						result = new Implication(subformula1, subformula2);
					break;
				}
				
				// Equivalence (<->)
				if((text.charAt(i) == '<') && (i < text.length()-2) && (text.charAt(i+1) == '-') && (text.charAt(i+2) == '>')) {
					Formula subformula1 = parse(text.substring(0, i));
					Formula subformula2 = parse(text.substring(i+3));
					if(subformula1 != null && subformula2 != null)
						result = new Equivalence(subformula1, subformula2);
					break;					
				}
			}
		}
		
		return result;		
	}
	
	public Formula tryParseAndAndOr(String text) throws Exception {
		Formula result = null;

		// Go thorugh every char in the String, but do not consider things within parentheses
		int numParen = 0;		
		for(int i = 0; i < text.length(); i++) {		
			if(text.charAt(i) == '(')
				numParen++;
			else if(text.charAt(i) == ')') { 
				numParen--;
			}
			
			if(numParen == 0) {
				// And (&)
				if(text.charAt(i) == '&') {
					Formula subformula1 = parse(text.substring(0, i));
					Formula subformula2 = parse(text.substring(i+1));
					if(subformula1 != null && subformula2 != null)
						result = new And(subformula1, subformula2);
					break;
				}
				
				// Or (\/)
				if((text.charAt(i) == '\\') && (i < text.length()-1) && (text.charAt(i+1) == '/')) {
					Formula subformula1 = parse(text.substring(0, i));
					Formula subformula2 = parse(text.substring(i+2));
					if(subformula1 != null && subformula2 != null)
						result = new Or(subformula1, subformula2);
					break;					
				}
			}
		}
		
		return result;
	}
	
	public Formula tryParseNot(String text) throws Exception {
		Formula result = null;

		if(text.length() > 0 && (text.charAt(0) == '~' || text.charAt(0) == '-')) {
			Formula subresult = parse(text.substring(1));
			if(subresult != null)
				result = new Not(subresult);
		}
		
		return result;
	}
}