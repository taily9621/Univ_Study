// EFQ
// ===
// Input:
//		a1,...,an				(j)		[contradiction]
// Output:
//		a1,...,an				(k)		[placeholder]				j EFQ

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;

public class EFQ extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Create the resulting formula
		Formula f = new Placeholder(); 
		
		// Create the resulting dependency
		Dependency d = pla[0].getDependency();
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first formula must be a Contradiction
		if(!(pla[0].getFormula() instanceof Contradiction))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
}