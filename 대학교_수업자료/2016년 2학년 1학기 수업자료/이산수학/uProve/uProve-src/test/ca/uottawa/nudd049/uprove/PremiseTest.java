package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;

import junit.framework.*;

public class PremiseTest extends TestCase {
	public void testCreation() {
		Premise p = new Premise();
	}

	public void testToString() {
		Premise p = new Premise();
		assertEquals("Premise",p.toString());
	}
	
	public void testIsJustification() {
		Premise p = new Premise();
		Justification j = p;
		assertEquals("Premise",j.toString());
	}
}