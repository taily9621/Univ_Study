package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class RuleTesterTest extends TestCase {
	private RuleTester rt;
	
	public void setUp() {
		rt = new RuleTester();
	}
	
	public void testCreation() {
		assertTrue(rt instanceof RuleTester);
	}
	
	public void testNoArguments() {
		ProofLine[] suggestions = rt.test(new ProofLine[0], 1);
		assertEquals(1, suggestions.length);
		ProofLine s1 = suggestions[0];
		assertEquals(1, s1.getLineNumber());
		assertTrue(s1.getJustification() instanceof Assumption);
	}
	
	public void testTwoArguments() {
		// Input:
		// 1   (1) p -> q			Premise
		// 2   (2) p				Assumption
		
		// Excpected output:
		// 1,2 (3) (p -> q) & p		1,2 &I
		// 1,2 (3) p & (p -> q)		2,1 &I
		// 1,2 (3) q				1,2 ->E
		// 1   (3) p -> (p -> q)	2,1 ->I

		ProofLine pl1 = new ProofLine(new Dependency(), 1, new Implication(new Symbol('p'),new Symbol('q')), new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(), 2, new Symbol('p'), new Assumption());
		
		pl1.getDependency().addProofLine(pl1);
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] result = rt.test(pl1.packageAsArray(pl2),3);
		assertEquals(4, result.length);
		
		assertTrue(result[0].getJustification() instanceof AndIntro);
		assertEquals(2, result[0].getDependency().getProofLines().length);
		assertEquals(pl1, result[0].getDependency().getProofLines()[0]);
		assertEquals(pl2, result[0].getDependency().getProofLines()[1]);
		assertEquals(3, result[0].getLineNumber());
		assertEquals("((p -> q) & p)", result[0].getFormula().toString());
		assertEquals(2, ((Rule)result[0].getJustification()).getProofLines().length);
		assertEquals(pl1, ((Rule)result[0].getJustification()).getProofLines()[0]);
		assertEquals(pl2, ((Rule)result[0].getJustification()).getProofLines()[1]);
		
		assertTrue(result[1].getJustification() instanceof AndIntro);
		assertEquals(2, result[1].getDependency().getProofLines().length);
		assertEquals(pl1, result[1].getDependency().getProofLines()[0]);
		assertEquals(pl2, result[1].getDependency().getProofLines()[1]);
		assertEquals(3, result[1].getLineNumber());
		assertEquals("(p & (p -> q))", result[1].getFormula().toString());
		assertEquals(2, ((Rule)result[1].getJustification()).getProofLines().length);
		assertEquals(pl2, ((Rule)result[1].getJustification()).getProofLines()[0]);
		assertEquals(pl1, ((Rule)result[1].getJustification()).getProofLines()[1]);
		
		assertTrue(result[2].getJustification() instanceof ImplicationElim);
		assertEquals(2, result[2].getDependency().getProofLines().length);
		assertEquals(pl1, result[2].getDependency().getProofLines()[0]);
		assertEquals(pl2, result[2].getDependency().getProofLines()[1]);
		assertEquals(3, result[2].getLineNumber());
		assertEquals("q", result[2].getFormula().toString());
		assertEquals(2, ((Rule)result[2].getJustification()).getProofLines().length);
		assertEquals(pl1, ((Rule)result[2].getJustification()).getProofLines()[0]);
		assertEquals(pl2, ((Rule)result[2].getJustification()).getProofLines()[1]);	
		
		assertTrue(result[3].getJustification() instanceof ImplicationIntro);
		assertEquals(1, result[3].getDependency().getProofLines().length);
		assertEquals(pl1, result[3].getDependency().getProofLines()[0]);
		assertEquals(3, result[3].getLineNumber());
		assertEquals("(p -> (p -> q))", result[3].getFormula().toString());
		assertEquals(2, ((Rule)result[3].getJustification()).getProofLines().length);
		assertEquals(pl2, ((Rule)result[3].getJustification()).getProofLines()[0]);
		assertEquals(pl1, ((Rule)result[3].getJustification()).getProofLines()[1]);					
	}
}