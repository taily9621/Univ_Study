package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;

import junit.framework.*;

public class PermutatorTest extends TestCase {
	private Permutator p;
	
	public void testCreate() {
		String[] s = {"1","2","3"};
		p = new Permutator(s);
		assertTrue(p instanceof Permutator);
	}
	
	public void testSmallGetNumber() {
		String[] s = {"1"};
		p = new Permutator(s);
		assertEquals(1,p.getNumberOfPermutations());
	}

	public void testBigGetNumber() {
		String[] s = {"1","2","3","4","5"};
		p = new Permutator(s);
		assertEquals(120,p.getNumberOfPermutations());
	}
	
	public void testNothingGetNumber() {
		String[] s = new String[0];
		p = new Permutator(s);
		assertEquals(0,p.getNumberOfPermutations());
	}
	
	public void testMediumGetPermutation() {
		String[] s = {"1","2","3"};
		p = new Permutator(s);
		assertEquals(6,p.getNumberOfPermutations());
		String[] sResult = {"2","3","1"}; 
		assertTrue(java.util.Arrays.equals(sResult, p.getPermutation(3)));
	}
}