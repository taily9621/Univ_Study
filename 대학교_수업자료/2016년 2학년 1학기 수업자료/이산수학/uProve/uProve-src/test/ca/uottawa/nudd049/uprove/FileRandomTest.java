package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;
import ca.uottawa.nudd049.uprove.persistence.*;
import java.io.*;

import junit.framework.*;

public class FileRandomTest extends TestCase {
	public void testRandom() {
		File file = new File("___temp_test.upr");
		assertFalse("Test file exists -- aborting!", file.exists());
						
		try {
			for(int rounds = 0; rounds < 10; rounds++) {
				Proof oldProof = getRandomProof();
				(new ProofToFile()).write(oldProof, file);
				Proof newProof = (new ProofFromFile()).read(file);
				
				ProofLine[] plaOld = oldProof.getProofLines();
				ProofLine[] plaNew = newProof.getProofLines();
				
				assertEquals(plaOld.length, plaNew.length);
				for(int i = 0; i < plaOld.length; i++) {
					String[] sOld = plaOld[i].toStrings();
					String[] sNew = plaNew[i].toStrings();
					
					assertEquals(sOld[0],sNew[0]);
					assertEquals(sOld[1],sNew[1]);
					assertEquals(sOld[2],sNew[2]);
					assertEquals(sOld[3],sNew[3]);
				}
				
				file.delete();
			}
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}		
	}

	public Proof getRandomProof() {
		Proof p = new Proof();
		
		p.setConclusion(getRandom());

		int numPremises = (int)(Math.random()*5)+1;
		for(int i = 0; i < numPremises; i++)
			p.addPremise(getRandom());
		
		for(int i = 0; i < 20; i++) {
			int numSelected = (int)(Math.random()*6) % p.getNextLineNumber();
			ProofLine[] plaOld = (ProofLine[])p.getProofLines().clone();
			ProofLine[] plaNew = new ProofLine[numSelected];
			int pos = 0;
			while(pos < plaNew.length) {
				int tryIt = (int)(Math.random()*p.getNextLineNumber());
				if(tryIt >= plaOld.length)
					tryIt--;
				if(plaOld[tryIt] != null) {
					plaNew[pos++] = plaOld[tryIt];
					plaOld[tryIt] = null;
				}
			}
			
			ProofLine[] plaSuggestions = (new RuleTester()).test(plaNew, p.getNextLineNumber());
			if(plaSuggestions.length == 0)
				continue;
			
			ProofLine newPl = plaSuggestions[(int)(Math.random() * 100) % plaSuggestions.length];
			if(newPl.hasPlaceholder())
				newPl.changePlaceholder(getRandom());
		
			p.addProofLine(newPl);			
		}

		return p;
	}
	
	public Formula getRandom() {
		return FormulaParserRandomTest.getRandomFormula(5);
	}
}