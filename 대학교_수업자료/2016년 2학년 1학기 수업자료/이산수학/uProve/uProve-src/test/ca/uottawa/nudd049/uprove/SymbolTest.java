package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;

import junit.framework.*;

public class SymbolTest extends TestCase {
	public void testCreation() {
		Symbol p = new Symbol('p');
	}
	
	public void testGetter() {
		Symbol p = new Symbol('p');
		assertEquals(p.getLetter(),'p');
	}
	
	public void testException() {
		try {
			Symbol p = new Symbol('1');
			fail("No exception thrown");
		} catch (Exception e) { }
	}
	
	public void testToString() {
		Symbol q = new Symbol('Q');
		assertEquals(q.toString(),"Q");
		Symbol a = new Symbol('a');
		assertEquals(a.toString(),"a");
	}
	
	public void testFormula() {
		Symbol p = new Symbol('p');
		Formula f = (Formula)p;
	}
}
