package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

import junit.framework.*;

public class EquivalenceTest extends TestCase {
	public void testCreation() {
		Equivalence i = new Equivalence(new Placeholder(),new Placeholder());
	}

	public void testConnective() {
		Equivalence i = new Equivalence(new Placeholder(),new Placeholder());
		Connective c = (Connective)i;
	}
	
	public void testException() {
		try {
			Equivalence i = new Equivalence(null,null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}

	public void testException2() {
		try {
			Equivalence i = new Equivalence(new Symbol('p'),null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testException3() {
		try {
			Equivalence i = new Equivalence(null,new Symbol('Q'));
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testSymbol() {
		Equivalence i = new Equivalence(new Placeholder(),new Placeholder());
		assertEquals("<->",i.symbolToString());
	}
	
	public void testPrint() {
		Equivalence i = new Equivalence(new Symbol('p'),new Symbol('q'));
		assertEquals("p <-> q",i.toString(true));
	}
	
	public void testPrint2() {
		Equivalence i = new Equivalence(new Symbol('p'),new Symbol('q'));
		assertEquals("(p <-> q)",i.toString());
	}
	
	public void testFormula() {
		Equivalence i = new Equivalence(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
	}
	
	public void testPrintAsFormula() {
		Equivalence i = new Equivalence(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
		assertEquals("(p <-> q)",f.toString());
	}
	
	public void testPrintAsFormula2() {
		Equivalence i = new Equivalence(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
		assertEquals("p <-> q",f.toString(true));
	}
	
	public void testPrintNested() {
		Formula f = (Formula)new And(new Symbol('p'),new Symbol('q'));
		Formula f2 = (Formula)new Symbol('r');
		Formula f3 = (Formula)new Equivalence(f,f2);
		assertEquals("(p & q) <-> r",f3.toString(true));
	}
	
	public void testEquals() {
		Formula f = (Formula)new Equivalence(new Symbol('p'),new Symbol('q'));
		Formula f2 = (Formula)new Symbol('r');
		Formula f3 = (Formula)new Implication(f,f2);

		Symbol s1 = new Symbol('p');
		Symbol s2 = new Symbol('q');
		Symbol s3 = new Symbol('r');
		Formula f4 = new Implication(new Equivalence(s1,s2),s3);
		
		assertTrue(f3.equals(f4));
	}
}