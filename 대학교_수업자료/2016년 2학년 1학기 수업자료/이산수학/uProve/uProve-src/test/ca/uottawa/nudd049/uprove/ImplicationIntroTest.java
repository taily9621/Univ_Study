package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class ImplicationIntroTest extends TestCase {
	public void testCreation() {
		Rule rule = new ImplicationIntro();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
//		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('q')),new Premise());
//		pl2.getDependency().addProofLine(pl2);
		
//		ProofLine pl3 = new ProofLine(new Dependency(),3,new Implication(new Symbol('r'),new Symbol('q')),new Premise());
//		pl3.getDependency().addProofLine(pl1);

		Rule rule = new ImplicationIntro();

		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(),4);
		assertNull(plaResult);
	}

	public void testWrongArguments2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		Rule rule = new ImplicationIntro();

		ProofLine[] plaResult = rule.apply(pla,3);
		assertNull(plaResult);
	}
	
	public void testUsual() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		Rule rule = new ImplicationIntro();
		ProofLine[] plaResult = rule.apply(pla,15);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1,2		(3)		p & q					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(1,plDep.length);
		assertEquals(pl2,plDep[0]);
		
		// Line number
		assertEquals(15,plResult.getLineNumber());
		
		// Formula
		Formula fExpected = new Implication(new Symbol('p'),new Symbol('q'));
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}

	public void testDependency() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Assumption());
		pl1.getDependency().addProofLine(pl1);		// Dependency: 1
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('r'),new AndElim());
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('q'),new Premise());
		pl3.getDependency().addProofLine(pl1);
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);		// Dependency: 1,2,3
		
		// Expected resulting dependency: 2,3
		
		ProofLine[] pla = {pl1,pl3};
		
		Rule rule = new ImplicationIntro();
		ProofLine[] plaResult = rule.apply(pla,4);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	2,3		(4)		p -> q					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(2,plDep.length);
		assertEquals(pl2,plDep[0]);
		assertEquals(pl3,plDep[1]);
		
		// Line number
		assertEquals(4,plResult.getLineNumber());
		
		// Formula
		Formula fExpected = new Implication(new Symbol('p'),new Symbol('q'));
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}
}