package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;
import junit.framework.*;

public class RuleTest extends TestCase {
	public void testTryApply() {
		Rule rule = new ImplicationElim();
		
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise());
		pl2.getDependency().addProofLine(pl2);

		// Both these calls should return the same result
		ProofLine[] plaResult1 = rule.tryApply(pl1.packageAsArray(pl2),3);
		ProofLine[] plaResult2 = rule.tryApply(pl2.packageAsArray(pl1),3);
		
		// Each call should return one ProofLine
		assertEquals(1,plaResult1.length);
		assertEquals(1,plaResult2.length);
		
		// Two dependencies
		assertEquals(2,plaResult1[0].getDependency().getProofLines().length);
		assertEquals(2,plaResult2[0].getDependency().getProofLines().length);
		
		// Line number 3
		assertEquals(3,plaResult1[0].getLineNumber());
		assertEquals(3,plaResult2[0].getLineNumber());
		
		// Formula q
		assertEquals(new Symbol('q'),plaResult1[0].getFormula());
		assertEquals(new Symbol('q'),plaResult2[0].getFormula());
		
		// Justification ImplicationElim
		assertTrue(plaResult1[0].getJustification() instanceof ImplicationElim);
		assertTrue(plaResult2[0].getJustification() instanceof ImplicationElim);
	}
	
	public void testTryApply2() {
		// If no line is selected, the rulf of Assumption is applicable

		Rule rule = new Assumption();
		
		ProofLine[] plaResult1 = rule.tryApply(new ProofLine[0],1);
		
		assertEquals(1,plaResult1.length);
		assertEquals(1,plaResult1[0].getDependency().getProofLines().length);
		assertEquals(1,plaResult1[0].getLineNumber());
		assertEquals(new Placeholder(),plaResult1[0].getFormula());
		assertTrue(plaResult1[0].getJustification() instanceof Assumption);
	}
	
	public void testTryApply3() {
		Rule rule = new AndIntro();
		
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl2.getDependency().addProofLine(pl2);

		// This call should return both p&q and q&p
		ProofLine[] plaResult1 = rule.tryApply(pl1.packageAsArray(pl2),3);
		
		assertEquals(2,plaResult1.length);
		assertEquals(2,plaResult1[0].getDependency().getProofLines().length);
		assertEquals(2,plaResult1[1].getDependency().getProofLines().length);
		assertEquals(3,plaResult1[0].getLineNumber());
		assertEquals(3,plaResult1[1].getLineNumber());
		assertEquals(new And(new Symbol('p'),new Symbol('q')),plaResult1[0].getFormula());
		assertEquals(new And(new Symbol('q'),new Symbol('p')),plaResult1[1].getFormula());
		assertTrue(plaResult1[0].getJustification() instanceof AndIntro);
		assertTrue(plaResult1[1].getJustification() instanceof AndIntro);
	}
	
	public void testTryApply4() {
		Rule rule = new NotIntro();
		
		// 1	(1)		p->q				Premise
		// 2	(2)		q->r				Premise
		// 3	(3)		p \/ q				Premise
		// 4	(4)		p					Assumption
		// 1,4	(5)		q					1,4 ->E
		// 1,2,4(6)		r					2,5 ->E
		// 7	(7)		q					Assumption
		// 2,7	(8)		r					2,7 ->E
		// 1,2,3(9)		r					3,4,6,7,8 \/E
		
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('r')),new Premise());
		pl2.getDependency().addProofLine(pl2);
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Or(new Symbol('p'),new Symbol('q')),new Premise());
		pl3.getDependency().addProofLine(pl3);
		
//		ProofLine pl4 = new ProofLine(new Dependency(),3,new Or(new Symbol('p'),new Symbol('q')),new Ass());
//		pl3.getDependency().addProofLine(pl3);
		
	}
}