package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class JustificationTest extends TestCase {
	public void testToString() {
		Rule rule = new AndIntro();
		
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		
		
		rule.apply(pl1.packageAsArray(pl2),3);

		assertEquals("1,2 &I", rule.toString());
	}
	
	public void testToString2() {
		assertEquals("Premise", (new Premise()).toString());
	}
}