package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class EFQTest extends TestCase {
	private Rule rule;
	
	public void setUp() {
		rule = new EFQ();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('q')),new Premise());
		pl2.getDependency().addProofLine(pl2);

		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl2),3);
		assertNull(plaResult);
	}

	public void testWrongArguments2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Not(new Symbol('p')),new Premise());
		pl1.getDependency().addProofLine(pl1);
				
		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(),2);
		assertNull(plaResult);
	}
	
	public void testUsual() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Contradiction(),new Premise());
		pl1.getDependency().addProofLine(pl1);
			
		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(),2);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1		(2)		[placeholder]					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(1,plDep.length);
		assertEquals(pl1,plDep[0]);
		
		// Line number
		assertEquals(2,plResult.getLineNumber());

		// Formula
		Formula fExpected = new Placeholder();
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}
}