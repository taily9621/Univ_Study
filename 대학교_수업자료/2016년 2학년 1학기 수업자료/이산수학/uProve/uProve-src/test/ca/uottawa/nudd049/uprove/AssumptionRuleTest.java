// Originally, both Assumption and AssumptionRule existed 

package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class AssumptionRuleTest extends TestCase {
	private Rule rule;
	
	public void setUp() {
		rule = new Assumption();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('q')),new Premise());
		pl2.getDependency().addProofLine(pl2);

		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl2),3);
		assertNull(plaResult);
	}
	
	public void testUsual() {
		ProofLine[] pla = new ProofLine[0];
		
		ProofLine[] plaResult = rule.apply(pla,1);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1		(1)		[placeholder]					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();
		assertEquals(1,plDep.length);
		assertEquals(plResult,plDep[0]);
		
		// Line number
		assertEquals(1,plResult.getLineNumber());

		// Formula
		Formula fExpected = new Placeholder();
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}
}