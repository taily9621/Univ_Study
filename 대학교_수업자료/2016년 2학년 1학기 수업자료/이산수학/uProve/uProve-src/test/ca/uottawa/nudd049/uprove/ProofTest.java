package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class ProofTest extends TestCase {
	public void testCreation() {
		new Proof();
	}
	
	public void testGetNextLineNumber1() {
		Proof p = new Proof();
		assertEquals(1,p.getNextLineNumber());
	}
	
	public void testAddProofLine1() {
		Proof p = new Proof();
		ProofLine pl = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl.getDependency().addProofLine(pl);
		
		p.addProofLine(pl);
	}
	
	public void testGetProofLine1() {
		Proof p = new Proof();
		ProofLine pl = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl.getDependency().addProofLine(pl);
		
		p.addProofLine(pl);
		
		// Method 1
		ProofLine[] pla1 = p.getProofLines();
		assertEquals(1,pla1.length);
		assertEquals(pl,pla1[0]);
		
		// Method 2
		assertEquals(pl,p.getProofLine(0));
	}
	
	public void testGetNextLineNumber2() {
		Proof p = new Proof();
		ProofLine pl = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl.getDependency().addProofLine(pl);		
		p.addProofLine(pl);
		
		assertEquals(2,p.getNextLineNumber());
	}
	
	public void testGetNextLineNumber3() {
		Proof p = new Proof();

		ProofLine pl = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl.getDependency().addProofLine(pl);		
		p.addProofLine(pl);

		pl = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl.getDependency().addProofLine(pl);		
		p.addProofLine(pl);

		assertEquals(3,p.getNextLineNumber());
	}
	
	public void testGetProofLine2() {
		Proof p = new Proof();
		
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl1.getDependency().addProofLine(pl1);
		p.addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl2.getDependency().addProofLine(pl2);		
		p.addProofLine(pl2);
		
		// Method 1
		ProofLine[] pla1 = p.getProofLines();
		assertEquals(2,pla1.length);
		assertEquals(pl1,pla1[0]);
		assertEquals(pl2,pla1[1]);
		
		// Method 2
		assertEquals(pl1,p.getProofLine(0));
		assertEquals(pl2,p.getProofLine(1));
	}
	
	public void testAddPremise() {
		Proof p = new Proof();
		p.addPremise(new And(new Symbol('p'),new Symbol('q')));
		
		ProofLine[] pla = p.getProofLines();
		assertEquals(1,pla.length);
		
		ProofLine pl = pla[0];
		assertEquals(1,pl.getDependency().getProofLines().length);
		assertEquals(1,pl.getDependency().getProofLines()[0].getLineNumber());
		assertEquals(1,pl.getLineNumber());
		assertEquals("(p & q)", pl.getFormula().toString());
		assertTrue(pl.getJustification() instanceof Premise);
	}

	public void testAddPremise2() {
		Proof p = new Proof();
		p.addPremise(new And(new Symbol('p'),new Symbol('q')));
		p.addPremise(new Not(new Or(new Symbol('p'),new Symbol('q'))));
		
		ProofLine[] pla = p.getProofLines();
		assertEquals(2,pla.length);
		
		ProofLine pl = pla[1];
		assertEquals(1,pl.getDependency().getProofLines().length);
		assertEquals(2,pl.getDependency().getProofLines()[0].getLineNumber());
		assertEquals(2,pl.getLineNumber());
		assertEquals("~(p \\/ q)", pl.getFormula().toString());
		assertTrue(pl.getJustification() instanceof Premise);
	}
	
	public void testAddConclusion() {
		Proof p = new Proof();
		p.setConclusion(new Implication(new Not(new Symbol('A')),new Symbol('B')));
		assertEquals("(~A -> B)", p.getConclusion().toString());
	}
	
	public void testIsProofCompleted() {
		Proof p = new Proof();
		p.setConclusion(new Symbol('q'));
		p.addPremise(new Symbol('p'));
		p.addPremise(new Implication(new Symbol('p'),new Symbol('q')));
		
		assertFalse(p.isProofCompleted());
		
		ImplicationElim ie = new ImplicationElim();
		p.addProofLine(ie.apply(p.getProofLine(1).packageAsArray(p.getProofLine(0)), 3)[0]);
		
		assertTrue(p.isProofCompleted());
	}
}