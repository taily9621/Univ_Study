package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class ImplicationElimTest extends TestCase {
	public void testCreation() {
		Rule rule = new ImplicationElim();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);

		Rule rule = new ImplicationElim();

		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(),2);
		assertNull(plaResult);
	}

	public void testWrongArguments2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('r'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		Rule rule = new ImplicationElim();

		ProofLine[] plaResult = rule.apply(pla,3);
		assertNull(plaResult);
	}
	
	
	public void testWrongArguments3() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new And(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		Rule rule = new ImplicationElim();

		ProofLine[] plaResult = rule.apply(pla,3);
		assertNull(plaResult);
	}

	
	public void testUsual() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		Rule rule = new ImplicationElim();
		ProofLine[] plaResult = rule.apply(pla,3);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1,2		(3)		q					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(2,plDep.length);
		assertEquals(pl1,plDep[0]);
		assertEquals(pl2,plDep[1]);
		
		// Line number
		assertEquals(3,plResult.getLineNumber());
		
		// Formula
		Formula fExpected = new Symbol('q');
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}

	public void testUsualComplex() {
		Formula rightFormula = new And(
				new Symbol('q'),
				new Implication(
						new Symbol('a'), 
						new Equivalence(
								new Symbol('f'),
								new Symbol('g'))
						));
		ProofLine pl1 = new ProofLine(new Dependency(),1,
				new Implication(
						new Symbol('p'),
						rightFormula),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		Rule rule = new ImplicationElim();
		ProofLine[] plaResult = rule.apply(pla,3);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
				
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(2,plDep.length);
		assertEquals(pl1,plDep[0]);
		assertEquals(pl2,plDep[1]);
		
		// Line number
		assertEquals(3,plResult.getLineNumber());

		// Formula
		assertEquals(rightFormula,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}
	public void testTryApply() {
		Rule rule = new ImplicationElim();
		
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise());
		pl2.getDependency().addProofLine(pl2);

		// Both these calls should return the same result		
		ProofLine[] plaResult1 = rule.tryApply(pl1.packageAsArray(pl2),3);
		ProofLine[] plaResult2 = rule.tryApply(pl2.packageAsArray(pl1),3);
		
		// Each call should return one ProofLine
		assertEquals(1,plaResult1.length);
		assertEquals(1,plaResult2.length);
		
		// Two dependencies
		assertEquals(2,plaResult1[0].getDependency().getProofLines().length);
		assertEquals(2,plaResult2[0].getDependency().getProofLines().length);
		
		// Line number 3
		assertEquals(3,plaResult1[0].getLineNumber());
		assertEquals(3,plaResult2[0].getLineNumber());
		
		// Formula q
		assertEquals(new Symbol('q'),plaResult1[0].getFormula());
		assertEquals(new Symbol('q'),plaResult2[0].getFormula());
		
		// Justification ImplicationElim
		assertTrue(plaResult1[0].getJustification() instanceof ImplicationElim);
		assertTrue(plaResult2[0].getJustification() instanceof ImplicationElim);
	}
}