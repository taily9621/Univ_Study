package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class OrIntroTest extends TestCase {
	private Rule rule;
	
	public void setUp() {
		rule = new OrIntro();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('q')),new Premise());
		pl2.getDependency().addProofLine(pl2);

		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl2),3);
		assertNull(plaResult);
	}
	
	public void testUsual() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl1.getDependency().addProofLine(pl1);
			
		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(),2);
		
		assertEquals(2,plaResult.length);
		ProofLine plResult1 = plaResult[0];
		ProofLine plResult2 = plaResult[1];
		
		// Expected result:
		//	1		(2)		p \/ q				"rule"
		//	1		(2)		q \/ p				"rule"
		
		// Dependencies
		Dependency d1 = plResult1.getDependency();
		ProofLine[] plDep1 = d1.getProofLines();		
		assertEquals(1,plDep1.length);
		assertEquals(pl1,plDep1[0]);
		Dependency d2 = plResult2.getDependency();
		ProofLine[] plDep2 = d2.getProofLines();		
		assertEquals(1,plDep2.length);
		assertEquals(pl1,plDep2[0]);
		
		// Line numbers
		assertEquals(2,plResult1.getLineNumber());
		assertEquals(2,plResult2.getLineNumber());
		
		// Formulas
		Formula fExpected = new Or(new Symbol('p'),new Placeholder());
		assertEquals(fExpected,plResult1.getFormula());
		fExpected = new Or(new Placeholder(),new Symbol('p'));
		assertEquals(fExpected,plResult2.getFormula());
		
		// Justifications
		assertEquals(rule,plResult1.getJustification());
		assertEquals(rule,plResult2.getJustification());
	}
}