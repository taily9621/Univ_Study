package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;

import junit.framework.*;

public class PlaceholderTest extends TestCase {
	public void testCreation() {
		Placeholder p = new Placeholder();
	}
	
	public void testToString() {
		Placeholder p = new Placeholder();
		assertEquals(p.toString(),"[?]");
	}
	
	public void testFormula() {
		Placeholder p = new Placeholder();
		Formula f = (Formula)p;
	}
}