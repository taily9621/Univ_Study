package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.And;
import ca.uottawa.nudd049.uprove.logic.rules.AndIntro;

import junit.framework.*;

public class AndIntroTest extends TestCase {
	public void testCreation() {
		AndIntro ai = new AndIntro();
	}
	
	public void testIntro() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] pla = {pl1,pl2};
		
		AndIntro ai = new AndIntro();
		ProofLine[] plaResult = ai.apply(pla,3);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1,2		(3)		p & q					"ai"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(2,plDep.length);
		assertEquals(pl1,plDep[0]);
		assertEquals(pl2,plDep[1]);
		
		// Line number
		assertEquals(3,plResult.getLineNumber());
		
		// Formula
		Formula fExpected = new And(new Symbol('p'),new Symbol('q'));
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(ai,plResult.getJustification());
	}
	
	public void testNoNo() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		AndIntro ai = new AndIntro();
		
		// This should not work, AndIntro needs two arguments
		ProofLine[] plaResult = ai.apply(pl1.packageAsArray(),2);
		assertNull(plaResult);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('r'),new Premise());
		ProofLine[] pla = {pl1,pl2,pl3};
		
		plaResult = ai.apply(pla,4);
		assertNull(plaResult);
	}
}