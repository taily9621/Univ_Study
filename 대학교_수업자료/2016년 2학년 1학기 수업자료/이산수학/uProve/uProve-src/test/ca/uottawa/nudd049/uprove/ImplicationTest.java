package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

import junit.framework.*;

public class ImplicationTest extends TestCase {
	public void testCreation() {
		Implication i = new Implication(new Placeholder(),new Placeholder());
	}
	
	public void testConnective() {
		Implication i = new Implication(new Placeholder(),new Placeholder());
		Connective c = (Connective)i;
	}
	
	public void testException() {
		try {
			Implication i = new Implication(null,null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}

	public void testException2() {
		try {
			Implication i = new Implication(new Symbol('p'),null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testException3() {
		try {
			Implication i = new Implication(null,new Symbol('Q'));
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testImplicationSymbol() {
		Implication i = new Implication(new Placeholder(),new Placeholder());
		assertEquals("->",i.symbolToString());
	}
	
	public void testPrint() {
		Implication i = new Implication(new Symbol('p'),new Symbol('q'));
		assertEquals("p -> q",i.toString(true));
	}
	
	public void testPrint2() {
		Implication i = new Implication(new Symbol('p'),new Symbol('q'));
		assertEquals("(p -> q)",i.toString());
	}
	
	public void testFormula() {
		Implication i = new Implication(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
	}
	
	public void testPrintAsFormula() {
		Implication i = new Implication(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
		assertEquals("(p -> q)",f.toString());
	}
	
	public void testPrintAsFormula2() {
		Implication i = new Implication(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
		assertEquals("p -> q",f.toString(true));
	}
	
	public void testPrintNested() {
		Formula f = (Formula)new Implication(new Symbol('p'),new Symbol('q'));
		Formula f2 = (Formula)new Symbol('r');
		Formula f3 = (Formula)new Implication(f,f2);
		assertEquals("(p -> q) -> r",f3.toString(true));
	}
	
	public void testEquals() {
		Formula f = (Formula)new Implication(new Symbol('p'),new Symbol('q'));
		Formula f2 = (Formula)new Symbol('r');
		Formula f3 = (Formula)new Implication(f,f2);

		Symbol s1 = new Symbol('p');
		Symbol s2 = new Symbol('q');
		Symbol s3 = new Symbol('r');
		Formula f4 = new Implication(new Implication(s1,s2),s3);
		
		assertTrue(f3.equals(f4));
	}
}
