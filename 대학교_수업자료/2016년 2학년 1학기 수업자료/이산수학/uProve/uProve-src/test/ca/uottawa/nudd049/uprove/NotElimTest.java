package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class NotElimTest extends TestCase {
	private Rule rule;
	
	public void setUp() {
		rule = new NotElim();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('q')),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Implication(new Symbol('r'),new Symbol('q')),new Premise());
		pl3.getDependency().addProofLine(pl1);

		ProofLine[] pla = {pl1,pl2,pl3};
		
		ProofLine[] plaResult = rule.apply(pla,4);
		assertNull(plaResult);
	}

	public void testWrongArguments2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('q'),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Not(new Symbol('q')),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl2),3);
		assertNull(plaResult);
	}
	
	public void testUsual() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Not(new Symbol('p')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise());
		pl2.getDependency().addProofLine(pl2);
			
		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl2),3);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1,2		(3)		_|_					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(2,plDep.length);
		assertEquals(pl1,plDep[0]);
		assertEquals(pl2,plDep[1]);
		
		// Line number
		assertEquals(3,plResult.getLineNumber());
		
		// Formula
		Formula fExpected = new Contradiction();
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}

	public void testDependency() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Not(new Symbol('p')),new Assumption());
		pl1.getDependency().addProofLine(pl1);		// Dependency: 1
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('r'),new AndElim());
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('p'),new Premise());
		pl3.getDependency().addProofLine(pl1);
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);		// Dependency: 1,2,3
		
		// Expected resulting dependency: 1,2,3
		
		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl3),4);
		ProofLine plResult = plaResult[0];
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();		
		assertEquals(3,plDep.length);
		assertEquals(pl1,plDep[0]);
		assertEquals(pl2,plDep[1]);
		assertEquals(pl3,plDep[2]);
	}
}