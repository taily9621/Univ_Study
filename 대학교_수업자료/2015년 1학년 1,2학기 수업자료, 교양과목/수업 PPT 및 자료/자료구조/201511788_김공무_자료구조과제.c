
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
int List01[19];
int List02[19];
int Select_number01 = 0, Select_number02 = 0;
int Count = 0;
int main()
{
	void sort();
	void Select();
	void Drowing();
	int Check();

	int i;
	int sum01 = 0, sum02 = 0;
	int player01 = 0, player02 = 0;
	srand((unsigned)time(NULL));
	printf("컴퓨터 과학과 201511788 김공무\n");
	for (i = 0; i < 19; i++)//무작위  숫자 배열에 삽입
	{
		List01[i] = rand() % 1000;
		List02[i] = rand() % 1000;
	}
	printf("\t\t[ 숫자 맞추기 게임 ]\n");
	sort(List01, 19);
	sort(List02, 19);
	Select(List01,1);
	Select(List02,2);
	for (i = 50; i >= 0; i--)
	{
		printf("밀어내는 중......\n\n\n\n");
		Sleep(100);
	}
	printf("Player01이 선택한 대표 번호 : %d\n", Select_number01);
	printf("Player02이 선택한 대표 번호 : %d\n", Select_number02);
	Drowing(1);
	printf("\n\n");
	Drowing(2);
	printf("< 주사위로 순서 정하기 >\n");
	while (1)
	{
		printf("player01\tplayer02\n");
		player01 = rand() % 6;
		printf("%d\t\t", player01);
		player02 = rand() % 6;
		printf("%d\n", player02);
		if (player01 != player02)
			break;
	}
		if (player01 > player02)
		{
			printf("Player01이 먼저!\n");
			while (1)
			{
				printf("Player01 차례!\n");
				Check(List02, 1);
				if (Count != 0)
					break;
				printf("\n");
				Drowing(1);
				printf("\n\n");
				Drowing(2);
				printf("\n");
				printf("Player02 차례!\n");
				Check(List01, 2);
				if (Count != 0)
					break;
				Drowing(1);
				printf("\n\n");
				Drowing(2);
			}

		}
		else
		{
			printf("Player02이 먼저!\n");
			while (1)
			{
				printf("Player02 차례!\n");
				Check(List01, 2);
				if (Count != 0)
					break;
				printf("\n");
				Drowing(1);
				printf("\n\n");
				Drowing(2);
				printf("\n");
				printf("Player01 차례!\n");
				Check(List02, 1);
				if (Count != 0)
					break;
				Drowing(1);
				printf("\n\n");
				Drowing(2);
			}
		}
	return 0;
}
void sort(int arr[], int n)//무작위 숫자 정렬 함수
{
	int i, cnt = 1, tmp;
	while (cnt)
	{
		cnt = 0;
		for (i = 0; i<n - 1; i++)
		{
			if (arr[i] > arr[i + 1])
			{
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				cnt++;
			}
		}
	}
}
void Select(int arr[],int Ar)
{
	int i,checking=0;
	if(Ar==1)
		printf("Player01은 대표 숫자를 정하시오.\n(주의! Player02는 절대 보지 마시오.)\n");
	else
		printf("Player02은 대표 숫자를 정하시오.\n(주의! Player01는 절대 보지 마시오.)\n");
	for (i = 20; i >= 0; i--)
	{
		printf("%d\n\n\n\n", i);
		Sleep(1000);
	}
	if(Ar==1)
	{
		for (i = 0; i < 19; i++)
			printf("%d번째 배열 : %d\n", i + 1, arr[i]);
		while (1)
		{
			scanf_s("%d", &Select_number01);
			for (i = 0; i < 19; i++)
			{
				if (Select_number01 == arr[i])
				{
					++checking;
					break;
				}
			}
			if (checking != 0)
				break;
			printf("다시 입력하세요.\n");
		}
	}
	else
	{
		for (i = 0; i < 19; i++)
			printf("%d번째 배열 : %d\n", i + 1, arr[i]);
		while (1)
		{
			scanf_s("%d", &Select_number02);
			for (i = 0; i < 19; i++)
			{
				if (Select_number02 == arr[i])
				{
					++checking;
					break;
				}
			}
			if (checking != 0)
				break;
			printf("다시 입력하세요.\n");
		}
	}
}
void Drowing(int Br)
{
	if(Br==1)
		printf("\t\t< Player01>\n");
	for (int i = 0; i < 48; i++)
		printf("-");
	printf("\n");
	for (int i = 0; i < 19; i++)
		printf("%d ",i+1);
	printf("\n");
	for (int i = 0; i < 48; i++)
		printf("-");
	printf("\n");
	if (Br==2)
		printf("\t\t< Player02>\n");
}
int Check(int Li[],int Cr)
{
	int EnterNum = 0, Number = 0;
	printf("상대방 팀의 배열 번호 중 하나를 선택하시오.");
	scanf_s("%d", &EnterNum);
	Number = Li[EnterNum-1];
	for (int i = 0; i < 19; i++)
	{
		if (Cr == 1)
		{
			if (Number == Select_number02)
			{
				printf("정답입니다!\n");
				printf("Player01이 승리하였습니다!\n");
			    ++Count;
				return 0;
			}
		}
		else if (Cr == 2)
		{
			if (Number == Select_number01)
			{
				printf("정답입니다!\n");
				printf("Player02가 승리하였습니다!\n");
				++Count;
				return 0;
			}
		}
	}
	printf("틀렸습니다.\n");
	return 0;
}
