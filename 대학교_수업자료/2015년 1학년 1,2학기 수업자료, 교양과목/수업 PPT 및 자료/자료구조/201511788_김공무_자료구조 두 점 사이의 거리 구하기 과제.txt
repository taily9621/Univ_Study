//201511788 김공무
#include<stdio.h>
#include<math.h>//루트를 사용하기 위해 math함수 선언
void get_distance();
typedef struct Point//좌표 x,y를 받기 위한 구조체 Point
{
	int x;
	int y;
}Point;
void main()
{
	struct Point P1 = { 1,2 };//P1은 1,2로 초기화
	struct Point P2 = { 9,8 };//P2는 9,8로 초기화
	get_distance(P1,P2);//두 점 사이 거리 계산 함수 호출
}
void get_distance(struct Point P1,struct Point P2)//두 점 사이 거리 계산 함수
{
	double sum=0.0;
	sum = (P2.x - P1.x)*(P2.x - P1.x) + (P2.y - P1.y)*(P2.y - P1.y);//P1과 P2 사이의 거리 계산
	printf("두 점 사이의 거리는 %.3lf\n", sqrt(sum));//출력
}