// Df 2
// ====
// Input:
//		a1,...,an				(j)		(p <-> q)
// Output:
//		a1,...,an				(k)		(p -> q) & (q -> p)				j Df

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class Df2 extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extract the two formulas p and q
		Formula p = ((Connective)pla[0].getFormula()).getSubformula1();
		Formula q = ((Connective)pla[0].getFormula()).getSubformula2();
		
		// Create the resulting formula
		Formula f = new And(new Implication(p,q),new Implication(q,p)); 
		
		// Create the resulting dependency
		Dependency d = pla[0].getDependency();
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first formula must be an Equivalence
		if(!(pla[0].getFormula() instanceof Equivalence))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
	
	public String toString() {
		return lineNumberToString() + "Df";
	}
}