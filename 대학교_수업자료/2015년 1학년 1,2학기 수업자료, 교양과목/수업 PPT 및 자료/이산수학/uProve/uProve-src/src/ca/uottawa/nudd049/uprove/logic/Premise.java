// Premise
// -------
// This class represents a Premise.

package ca.uottawa.nudd049.uprove.logic;

public class Premise extends Justification {
	public String toString() {
		return "Premise";
	}
}