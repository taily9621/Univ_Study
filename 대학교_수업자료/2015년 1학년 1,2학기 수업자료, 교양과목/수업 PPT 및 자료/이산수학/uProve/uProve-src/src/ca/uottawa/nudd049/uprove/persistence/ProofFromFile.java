// ProofFromFile
// -------------
// Read a File and return a Proof.

package ca.uottawa.nudd049.uprove.persistence;

import ca.uottawa.nudd049.uprove.logic.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;

public class ProofFromFile {
	public Proof read(File file) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(file); 
		Element root = (Element)doc.getChildNodes().item(0);
		
		verifyFormatAndVersion(root);

		// Build the proof
		Proof proof = new Proof();
		Element rebuild = (Element)root.getElementsByTagName("rebuild").item(0);		
		NodeList rebuildStep = rebuild.getChildNodes();		
		for(int i = 0; i < rebuildStep.getLength(); i++) {
			if(rebuildStep.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Formula f = (new FormulaParser()).parse(rebuildStep.item(i).getTextContent());
				if(rebuildStep.item(i).getNodeName().equals("conclusion")) {
					proof.setConclusion(f);
				} else if(rebuildStep.item(i).getNodeName().equals("premise")) {
					proof.addPremise(f);
				} else if(rebuildStep.item(i).getNodeName().equals("rule")) {
					// Get the rule to use 
					String name = rebuildStep.item(i).getAttributes().getNamedItem("name").getNodeValue();				
					Rule rule = (Rule)Class.forName(name).newInstance();

					// Get the list of lines to apply the rule to
					String[] lines = rebuildStep.item(i).getAttributes().getNamedItem("lines").getNodeValue().split(",");
					if(lines.length == 1 && lines[0].length() == 0)
						lines = new String[0];
					
					ProofLine[] pla = new ProofLine[lines.length];
					for(int j = 0; j < lines.length; j++)
							pla[j] = proof.getProofLine(Integer.parseInt(lines[j]) - 1);

					// Apply the rule and get the right result
					ProofLine[] plaS = rule.apply(pla,proof.getNextLineNumber());
					int suggestion = Integer.parseInt(rebuildStep.item(i).getAttributes().getNamedItem("suggestion").getNodeValue());
					ProofLine newToAdd = plaS[suggestion];
					
					// Change the placeholder if any active					
					newToAdd.changePlaceholder(f);
					
					// Add the line to the proof
					proof.addProofLine(newToAdd);
				}
			}
		}
		
		return proof;
	}
	
	private void verifyFormatAndVersion(Element root) throws Exception {
		if(!root.getNodeName().equals("proof"))
			throw new Exception("File is not a valid uProve proof file.");
		
		NamedNodeMap nmn = root.getAttributes();
		if(!nmn.getNamedItem("format").getNodeValue().equals("uprove"))
			throw new Exception("File is not a valid uProve proof file.");

		if(!nmn.getNamedItem("formatversion").getNodeValue().equals("1"))
			throw new Exception("This file is saved in a newer version of uProve and can not be opened in this version. Please download the latest version and try again.");
	}
}
