// Df 1
// ====
// Input:
//		a1,...,an				(j)		(p -> q) & (q -> p)
// Output:
//		a1,...,an				(k)		(p <-> q)				j Df

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class Df1 extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extract the two formulas p and q
		Connective and = (Connective)pla[0].getFormula();
		Formula p = ((Connective)and.getSubformula1()).getSubformula1();
		Formula q = ((Connective)and.getSubformula1()).getSubformula2();
		
		// Create the resulting formula
		Formula f = new Equivalence(p,q); 
		
		// Create the resulting dependency
		Dependency d = pla[0].getDependency();
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first formula must be an And
		Formula f = pla[0].getFormula();
		if(!(f instanceof And))
			return false;
		
		// Each subformula of the And must be Implications
		Formula impLeft = ((Connective)f).getSubformula1();
		Formula impRight = ((Connective)f).getSubformula2();
		if(!(impLeft instanceof Implication))
			return false;
		if(!(impRight instanceof Implication))
			return false;
		
		// The subformulas in the Implications must match
		Formula impLeftLeft = ((Connective)impLeft).getSubformula1();
		Formula impLeftRight = ((Connective)impLeft).getSubformula2();
		Formula impRightLeft = ((Connective)impRight).getSubformula1();
		Formula impRightRight = ((Connective)impRight).getSubformula2();
		if(!(impLeftLeft.equals(impRightRight)))
			return false;
		if(!(impLeftRight.equals(impRightLeft)))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
	
	public String toString() {
		return lineNumberToString() + "Df";
	}
}