// Or Introduction
// ===============
// Input:
//		a1,...,an				(j)		p
// Output:
//		a1,...,an				(k)		p \/ [placeholder]			j \/I
//		a1,...,an				(k)		[placeholder] \/ p			j \/I

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class OrIntro extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extrace the formula from the first line
		Formula f = pla[0].getFormula();
		
		// Create the resulting formulas
		Formula f1 = new Or(f,new Placeholder());
		Formula f2 = new Or(new Placeholder(),f);
		
		// Create the resulting dependencies
		Dependency d = pla[0].getDependency();
		
		// Create the resulting ProofLines
		ProofLine pl1 = new ProofLine(d,nextLineInProof,f1,this,0);
		ProofLine pl2 = new ProofLine(d,nextLineInProof,f2,this,1);
		
		return pl1.packageAsArray(pl2);
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// Anything goes
		return true;
	}
	
	public int getNumberOfArguments() {
		return 1;
	}
	
	public String toString() {
		return lineNumberToString() + "\\/I";
	}
}