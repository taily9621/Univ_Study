// Implication Elimination
// =======================
// Input:
//		a1,...,an			(j)		p -> q
//		b1,...,bu			(k)		p
// Output:
//		a1,...,an,b1,...,bu	(m)		q				j,k ->E

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class ImplicationElim extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extract the two dependencies and put them together
		Dependency d1 = pla[0].getDependency();
		Dependency d2 = pla[1].getDependency();
		Dependency d = new Dependency(d1,d2);
		
		// Extract the new formula
		Formula f = ((Connective)pla[0].getFormula()).getSubformula2();
		
		// Constuct the ProofLine
		ProofLine result = new ProofLine(d,nextLineInProof,f,this);

		return result.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		Formula f1 = pla[0].getFormula();
		Formula f2 = pla[1].getFormula();
		
		// Formula 1 must be an implication
		if(!(f1 instanceof Implication))
			return false;
		
		// The left part of the implication must equals the second formula
		if(!((Connective)f1).getSubformula1().equals(f2))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 2;
	}
	
	public String toString() {
		return lineNumberToString() + "->E";
	}
}