// UIConnector
// -----------
// This class is responsible for supporting the Start proof dialog. 

package ca.uottawa.nudd049.uprove.logic;

import ca.uottawa.nudd049.uprove.ui.GUINew;
import ca.uottawa.nudd049.uprove.ui.FormulaEnterer;

public class UIConnectorNew {
	private UIConnector uic;
	private GUINew guiNew;
	private Proof proof;
	
	public UIConnectorNew(UIConnector uic) {
		this.uic = uic;
		proof = new Proof();
	}
	
	public void registerGUI(GUINew guiNew) {
		this.guiNew = guiNew;
	}
	
	public void addPremise() {
		FormulaEnterer fe = new FormulaEnterer(guiNew);
		Formula f = fe.getFormulaFromUser("Enter premise.");
		if(f != null) {
			proof.addPremise(f);
			guiNew.addPremise(f.toString(true));
		}
	}
	
	public void addConclusion() {
		FormulaEnterer fe = new FormulaEnterer(guiNew);
		Formula f = fe.getFormulaFromUser("Enter conclusion.");
		if(f != null) {
			proof.setConclusion(f);
			guiNew.addConclusion(f.toString(true));
			guiNew.conclusionEntered();
		}
	}
	
	public void okClicked() {
		guiNew.setVisible(false);
		uic.startNewProof(proof);
	}
	
	public void cancelClicked() {
		guiNew.setVisible(false);
	}
}