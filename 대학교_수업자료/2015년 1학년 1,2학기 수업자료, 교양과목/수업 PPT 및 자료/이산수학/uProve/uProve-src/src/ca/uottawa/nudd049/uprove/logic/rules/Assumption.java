// Assumption
// ==========
// Input:
//		[none]		
// Output:
//		j (j)		[placeholder]				Assumption

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;

public class Assumption extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Create the resulting formula
		Formula f = new Placeholder(); 
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(new Dependency(),nextLineInProof,f,this);
		
		// Add the ProofLine itself as its Dependency
		pl.getDependency().addProofLine(pl);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// No arguments to check actually
		return true;
	}
	
	public int getNumberOfArguments() {
		return 0;
	}
	
	public String toString() {
		return "Assumption";
	}
}