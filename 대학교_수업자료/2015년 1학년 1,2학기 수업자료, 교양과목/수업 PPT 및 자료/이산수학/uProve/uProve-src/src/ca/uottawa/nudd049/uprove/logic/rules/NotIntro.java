// Not Introduction
// ================
// Input:
//		j						(j)		p					Assumption
//		a1,...,an				(k)		[contradiction]
// Output:
//		{a1,...,an}/j			(m)		~p					j,k ~I

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class NotIntro extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Extract the first formula and create the resulting formula
		Formula f = new Not(pla[0].getFormula());
		
		// Create the resulting dependency
		Dependency d = new Dependency(pla[1].getDependency(),pla[0]);
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first line must be an Assumption
		if(!(pla[0].getJustification() instanceof Assumption))
			return false;
		
		// The second formula must be a Contradiction 
		if(!(pla[1].getFormula() instanceof Contradiction))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 2;
	}
	
	public String toString() {
		return lineNumberToString() + "~I";
	}
}