// RuleTester
// ----------
// Test all available rules on a set of ProofLines.

package ca.uottawa.nudd049.uprove.logic;

import ca.uottawa.nudd049.uprove.ui.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;
import java.util.Vector;

public class RuleTester {
	public ProofLine[] test(ProofLine[] pla, int nextLineInProof) {
		Vector<ProofLine> result = new Vector<ProofLine>();
		
		// Go through the rules one after another and collect a list of the result from each call
		// to tryApply, which tries to apply the rules to (all permutations of) the ProofLines.
		String[] ruleNames = Main.getRules();
		for(int i = 0; i < ruleNames.length; i++) {
			try {
				Rule r = (Rule)Class.forName(getClass().getPackage().getName() + ".rules." + ruleNames[i]).newInstance();
				ProofLine[] resultThisRule = r.tryApply(pla, nextLineInProof);
				
				if(resultThisRule != null) {
					for(int j = 0; j < resultThisRule.length; j++)
						result.add(resultThisRule[j]);
				}
			} catch(Exception e) {
				System.out.println("Error accessing rule " + ruleNames[i]);
			}
		}
		
		return result.toArray(new ProofLine[result.size()]);
	}
}