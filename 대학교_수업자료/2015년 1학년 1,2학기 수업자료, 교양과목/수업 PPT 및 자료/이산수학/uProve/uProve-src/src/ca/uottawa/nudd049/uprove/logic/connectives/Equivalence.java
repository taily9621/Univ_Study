// Equivalence
// --
// The connective Equivalence.

package ca.uottawa.nudd049.uprove.logic.connectives;

import ca.uottawa.nudd049.uprove.logic.*;

public class Equivalence extends Connective {
	public Equivalence(Formula f1, Formula f2) {
		super(f1,f2);
	}
	
	protected void checkInput(Formula f1, Formula f2) {
		if(f1==null || f2==null)
			throw new IllegalArgumentException("An 'equivalence' requires two formulas");
	}
	
	public String symbolToString() {
		return "<->";
	}
}