// GUINew
// ------
// This class represents a New proof dialog, which allows the user to enter premises and a conclusion.
// It is mainly communication with (an instance of) UIConnectorNew.

package ca.uottawa.nudd049.uprove.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import ca.uottawa.nudd049.uprove.logic.UIConnectorNew;

public class GUINew extends JDialog {
	private UIConnectorNew uicNew;
	private JPanel premise; 
	private JPanel conclusion;
	private JButton addConclusion;
	private JButton buttonOk;
	
	public GUINew(UIConnectorNew uicNew, JFrame owner) {
		super(owner, "New proof", true);
		this.uicNew = uicNew;

		JPanel title = createTitle();
		JPanel centerContent = createCenter();	
		JPanel buttons = createButtons();
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(title, BorderLayout.NORTH);
		panel.add(centerContent, BorderLayout.CENTER);
		panel.add(buttons, BorderLayout.SOUTH);
		
		Container c = getContentPane();
		c.setLayout(new BoxLayout(c,BoxLayout.PAGE_AXIS));
		c.add(panel);
		
		pack();
	}

	private JPanel createCenter() {
		JButton addPremise = new JButton("Add");
		addConclusion = new JButton("Add");		
		premise = new JPanel();		
		conclusion = new JPanel();		
		JPanel pPanel= createFormulaInputArea(premise,"Premises",addPremise);
		JPanel cPanel = createFormulaInputArea(conclusion,"Conclusion",addConclusion);
		
		addActionListenersToAddButtons(addPremise);
		
		JPanel centerContent = new JPanel();
		centerContent.setLayout(new BoxLayout(centerContent, BoxLayout.PAGE_AXIS));
		centerContent.add(pPanel);
		centerContent.add(cPanel);
		return centerContent;
	}

	private void addActionListenersToAddButtons(JButton addPremise) {
		addPremise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPremise();
			}
		});
		addConclusion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addConclusion();
			}
		});
	}
	
	private void addPremise() {
		uicNew.addPremise();
	}
	
	private void addConclusion() {
		uicNew.addConclusion();
	}
	
	private void okClicked() {
		uicNew.okClicked();
	}
	
	private void cancelClicked() {
		uicNew.cancelClicked();
	}
	
	private void helpClicked() {
		showHelp();
	}
	
	private JPanel createFormulaInputArea(JPanel formulaPanel, String title, JButton addButton) {
		formulaPanel.setLayout(new GridLayout(0,1));
		JPanel addPanel = new JPanel(new BorderLayout());
		addPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		addPanel.add(addButton, BorderLayout.WEST);
		
		JPanel result = new JPanel(new BorderLayout());
		result.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createEmptyBorder(5, 0, 5, 0),
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0,0,0)), title),
								BorderFactory.createEmptyBorder(5,5,5,5))));
		result.add(formulaPanel, BorderLayout.WEST);
		result.add(addPanel, BorderLayout.SOUTH);
		
		return result;
	}

	private JPanel createButtons() {
		buttonOk = new JButton("OK");
		buttonOk.setEnabled(false);
		buttonOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okClicked();
			}
		});
		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelClicked();
			}
		});
		JButton buttonHelp = new JButton("Help");
		buttonHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				helpClicked();
			}
		});
		
		JPanel buttons = new JPanel(new GridLayout(1,3));
		buttons.add(buttonOk);
		buttons.add(buttonCancel);
		buttons.add(buttonHelp);
		return buttons;
	}

	private JPanel createTitle() {
		JLabel titleTop = new JLabel();
        titleTop.setFont(new java.awt.Font("Tahoma", 0, 16));
        titleTop.setText("Start a new proof");

        JLabel titleCenter = new JLabel("Enter the premises and the conclusion to start your proof.");
        
        JPanel title = new JPanel();
        title.setLayout(new BorderLayout());
        title.add(titleTop, BorderLayout.NORTH);
        title.add(titleCenter, BorderLayout.CENTER);
		return title;
	}
	
	private void showHelp() {
		JOptionPane.showMessageDialog(this, "Before a proof can be started, the premises and the conclusion must be entered.\nYou enter these formulas by clicking the corresponding \"Add\"-button.\n\nExample of formula:\n     (p & q) -> (p \\// q)");
	}
	
	public void addPremise(String s) {
		premise.add(new JLabel(s));
		validate();
		pack();
	}
	
	public void addConclusion(String s) {
		conclusion.add(new JLabel(s));
		validate();
		pack();		
	}
	
	public void conclusionEntered() {
		addConclusion.setVisible(false);
		buttonOk.setEnabled(true);
	}
}