// Formula
// -------
// This class is a superclass for all formulas.

package ca.uottawa.nudd049.uprove.logic;

public abstract class Formula {
	public String toString(boolean skipParenthesesOnce) {
		return toString();
	}
	
	public boolean equals(Object f) {
		// Compare two formulas by comparing their String representations
		return toString().equals(f.toString());
	}
}