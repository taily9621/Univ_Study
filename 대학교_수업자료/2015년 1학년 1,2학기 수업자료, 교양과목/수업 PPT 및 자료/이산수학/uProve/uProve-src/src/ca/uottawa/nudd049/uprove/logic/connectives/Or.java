// Or
// --
// The connective Or.

package ca.uottawa.nudd049.uprove.logic.connectives;

import ca.uottawa.nudd049.uprove.logic.*;

public class Or extends Connective {
	public Or(Formula f1, Formula f2) {
		super(f1,f2);
	}
	
	protected void checkInput(Formula f1, Formula f2) {
		if(f1==null || f2==null)
			throw new IllegalArgumentException("An 'or' requires two formulas");
	}
	
	public String symbolToString() {
		return "\\/";
	}
}