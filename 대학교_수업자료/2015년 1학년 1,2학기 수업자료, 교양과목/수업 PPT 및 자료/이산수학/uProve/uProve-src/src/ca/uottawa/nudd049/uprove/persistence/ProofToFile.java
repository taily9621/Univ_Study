// ProofToFile
// -----------
// Read a Proof and write it to a File.

package ca.uottawa.nudd049.uprove.persistence;

import ca.uottawa.nudd049.uprove.logic.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*; 
import java.io.*;

public class ProofToFile {
	public void write(Proof proof, File file) throws Exception { 				
		Document doc = createTree(proof);
		writeToFile(doc, file);
	}
	
	private Document createTree(Proof proof) throws Exception {
		// Create the document
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		DOMImplementation dom = builder.getDOMImplementation();
		Document doc = dom.createDocument(null, "proof", null);
		
		// Set attributes and create main elements
		Element root = doc.getDocumentElement();
		root.setAttribute("format", "uprove");
		root.setAttribute("formatversion", "1");
		Element rebuild = doc.createElement("rebuild");
		Element text = doc.createElement("text");
		text.setAttribute("completed", Boolean.toString(proof.isProofCompleted()));

		// Add the conclusion
		Element conclusion = doc.createElement("conclusion");
		conclusion.setTextContent(proof.getConclusion().toString(true));
		rebuild.appendChild(conclusion);
		
		// Add each proof line
		for(int i = 0; i < (proof.getNextLineNumber() - 1); i++) {
			ProofLine pl = proof.getProofLine(i);
			String[] plString = pl.toStrings();
			
			// Add the proof line to the "rebuild" element
			Element stepNode;
			if(pl.getJustification() instanceof Premise) {
				// Add a premise
				stepNode = doc.createElement("premise");
				stepNode.setTextContent(plString[2]);
			} else {
				// Add a rule
				stepNode = doc.createElement("rule");
				stepNode.setAttribute("name", pl.getJustification().getClass().getName());
				stepNode.setAttribute("suggestion", Integer.toString(pl.getSuggestionNumber()));
				stepNode.setAttribute("lines", pl.getJustification().lineNumberToString().trim());
			
				// If the rule application produced a placeholder, the value entered in the placeholder's
				// place must be stored
				Formula placeholder = pl.getPlaceholder();
				if(placeholder != null)
					stepNode.setTextContent(placeholder.toString());				
			}
			
			rebuild.appendChild(stepNode);
			
			// Add the proof line to the "text" element
			Element plNode = doc.createElement("proofline");
			plNode.setAttribute("number", Integer.toString(pl.getLineNumber()));
			plNode.setAttribute("dependencies", plString[0]);
			plNode.setAttribute("justification", plString[3]);
			plNode.setTextContent(plString[2]);						
			text.appendChild(plNode);
		}
		
		root.appendChild(rebuild);
		root.appendChild(text);
		
		return doc;
	}
	
	private void writeToFile(Document doc, File file) throws Exception {
		// Write the document to the file
		DOMSource domSource = new DOMSource(doc);
		StreamResult streamResult = new StreamResult(file); 
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		transformer.transform(domSource, streamResult); 
	}
}