// Not
// --
// The connective Not.

package ca.uottawa.nudd049.uprove.logic.connectives;

import ca.uottawa.nudd049.uprove.logic.*;

public class Not extends Connective {
	public Not(Formula f1, Formula f2) {
		super(f1,f2);
	}
	
	public Not(Formula f1) {
		super(f1,null);
	}
	
	protected void checkInput(Formula f1, Formula f2) {
		if(f1==null || f2!=null)
			throw new IllegalArgumentException("A 'not' requires one formula");
	}
	
	public String symbolToString() {
		return "~";
	}
	
	public String toString(boolean skipParenthesesOnce) {
		// This function is defined in Connective but is overridden here since it won't work
		// for unary connectives
		String s = symbolToString();
		if(formula1 != null)
			s += formula1.toString();
		return s;
	}
}