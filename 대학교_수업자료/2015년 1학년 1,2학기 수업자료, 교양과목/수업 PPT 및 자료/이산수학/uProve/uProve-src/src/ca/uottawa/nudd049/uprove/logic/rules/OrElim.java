// Or Elimination
// ==============
// Input:
//		a1,...,an				(g)		p \/ q
//		h						(h)		p					Assumption
//		b1,...,bu				(i)		r
//		j						(j)		q					Assumption
//		c1,...,cw				(k)		r
// Output:
//		X1,X2,X3				(m)		r					g,h,i,j,k \/E
//			X1 = a1,...,an
//			X2 = {b1,...,bu}/h
//			X3 = {c1,...,cw}/j

package ca.uottawa.nudd049.uprove.logic.rules;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

public class OrElim extends Rule {	
	protected ProofLine[] applyWithoutChecks(ProofLine[] pla, int nextLineInProof) {
		// Create the resulting formula
		Formula f = pla[2].getFormula();
		
		// Create dependency X1 ({a1,...,an})
		Dependency dX1 = pla[0].getDependency();
			
		// Create dependency X2 ({b1,...,bu}/h)
		Dependency dLine3 = pla[2].getDependency();
		Dependency dX2 = new Dependency(dLine3,pla[1]);
		
		// Create dependency X3 ({c1,...,cw}/j)
		Dependency dLine5 = pla[4].getDependency();
		Dependency dX3 = new Dependency(dLine5,pla[3]);
		
		// Create the resulting dependency
		Dependency d = new Dependency(dX1,new Dependency(dX2,dX3));
		
		// Create the resulting ProofLine
		ProofLine pl = new ProofLine(d,nextLineInProof,f,this);
		
		return pl.packageAsArray();
	}
	
	public boolean checkArguments(ProofLine[] pla) {
		// The first formula must be an Or
		if(!(pla[0].getFormula() instanceof Or))
			return false;
		
		// The second line must be an Assumption
		if(!(pla[1].getJustification() instanceof Assumption))
			return false;
			
		// The second formula must be the left part of the first line's Or
		Formula firstFormulaLeft = ((Connective)pla[0].getFormula()).getSubformula1();
		if(!(firstFormulaLeft.equals(pla[1].getFormula())))
			return false;

		// The forth line must be an Assumption
		if(!(pla[3].getJustification() instanceof Assumption))
			return false;
		
		// The forth formula must be the right part of the first line's Or
		Formula firstFormulaRight = ((Connective)pla[0].getFormula()).getSubformula2();
		if(!(firstFormulaRight.equals(pla[3].getFormula())))
			return false;

		// The third and the fifth formula must be the same  
		if(!(pla[2].getFormula().equals(pla[4].getFormula())))
			return false;
		
		return true;
	}
	
	public int getNumberOfArguments() {
		return 5;
	}
	
	public String toString() {
		return lineNumberToString() + "\\/E";
	}
}