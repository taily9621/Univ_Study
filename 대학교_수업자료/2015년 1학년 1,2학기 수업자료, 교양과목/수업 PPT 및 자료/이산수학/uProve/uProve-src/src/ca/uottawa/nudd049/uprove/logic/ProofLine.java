// ProofLine
// ---------
// This class is representing one line in the proof. One such line consists of a list of dependencies,
// a line number, a formula, and a justification.
//
// A proof line in the real world might look like this:
// 3,4 (5)  A&B                        3,4 &I
//
// Explanation:
// 3,4 == Dependencies
// 		5 == Line number
// 			A&B == Formula
// 									   3,4 &I == Justification

package ca.uottawa.nudd049.uprove.logic;

import java.lang.reflect.*;

public class ProofLine {
	private Dependency dependency;
	private int lineNumber;
	private Formula formula;
	private Justification justification;
	
	// Saving the placeholder used (if any) and the internal suggestion number is used for reproducing
	// the proof (i.e., when saving the proof). 
	private Formula placeholder;
	private int suggestionNumber;
	
	public ProofLine(Dependency d, int l, Formula f, Justification j) {
		dependency = (d != null)?d:new Dependency();
		lineNumber = l;
		formula = f;
		justification = j;
		placeholder = null;
		suggestionNumber=0;
	}
	
	public ProofLine(Dependency d, int l, Formula f, Justification j, int s) {
		this(d,l,f,j);
		suggestionNumber = s;
	}
	
	public Dependency getDependency() {
		return dependency;
	}
	
	public int getLineNumber() {
		return lineNumber;
	}

	public Formula getFormula() {
		return formula;
	}
	
	public Justification getJustification() {
		return justification;
	}
	
	public Formula getPlaceholder() {
		return placeholder;
	}
	
	public int getSuggestionNumber() {
		return suggestionNumber;
	}
	
	public ProofLine[] packageAsArray() {
		ProofLine[] result = {this};
		return result;
	}
	
	public ProofLine[] packageAsArray(ProofLine pl) {
		ProofLine[] result = {this,pl};
		return result;
	}
	
	public boolean hasPlaceholder() {
		return hasPlaceholderRecursive(formula);
		
	}
	
	private boolean hasPlaceholderRecursive(Formula f) {
		// If there are no formula, it sure isn't a specific type of formula 
		if(f == null)
			return false;
		
		// If it is placeholder, it is a placeolder (obviously)
		if(f instanceof Placeholder)
			return true;
		
		// If the formula is of type Connective, there might be placeholders as subformulas
		if(f instanceof Connective) {
			if (hasPlaceholderRecursive(((Connective)f).getSubformula1()))
				return true;
			if (hasPlaceholderRecursive(((Connective)f).getSubformula2()))
				return true;
		}
		
		return false;
	}
	
	public void changePlaceholder(Formula newFormula) {
		placeholder = newFormula;
		formula = changePlaceholderRecursive(formula, newFormula);
	}
	
	private Formula changePlaceholderRecursive(Formula f, Formula n) {
		if(f == null)
			return null;
		
		if(f instanceof Placeholder)
			return n;
		
		if(f instanceof Connective) {
			
			Formula f1 = ((Connective)f).getSubformula1();
			Formula f2 = ((Connective)f).getSubformula2();
			
			Formula f1Changed = changePlaceholderRecursive(f1, n);
			Formula f2Changed = changePlaceholderRecursive(f2, n);

			Formula fChanged = f;
			Constructor con = f.getClass().getConstructors()[0];
			Object[] args = {f1Changed,f2Changed};
			try {
				fChanged = (Formula)con.newInstance(args);
			} catch(Exception e) {
				// TODO: Error handling
			}
		
			return fChanged;
		}
		
		return f;
	}
	
	public String[] toStrings() {
		String[] result = new String[4];
		result[0] = dependency.toString();
		result[1] = "(" + lineNumber + ")";
		result[2] = formula.toString(true);
		result[3] = justification.toString();
		
		return result;
	}
	
	public static String getLineNumbersAsString(ProofLine[] pla) {
		String result = new String();
		
		if(pla != null) {
			for(int i = 0; i < pla.length; i++) {
				if(result.length() > 0)
					result += ",";
				result += pla[i].getLineNumber();
			}
		}
		
		return result;
	}
}