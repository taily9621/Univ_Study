// FormulaEnterer
// --------------
// This class is used for retrieving an well-formed formula from the user. 

package ca.uottawa.nudd049.uprove.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import ca.uottawa.nudd049.uprove.logic.*;

public class FormulaEnterer {
	private Window owner;
	
	public FormulaEnterer(Window owner) {
		this.owner = owner;
	}
	
	public Formula getFormulaFromUser(String reason) {
		return getFormulaFromUser(reason, "", "");
	}

	private Formula getFormulaFromUser(String reason, String error, String defaultFormula) {
		Formula result = null;
		
		String reply = (String)JOptionPane.showInputDialog(
		                    owner,
		                    reason + "\n\n" +
		                    error +
		                    "Please enter the formula below. Use symbols (a-Z), and (&), or (\\/), not (~ or -), implication (->), equivalence (<->) and contradiction (_|_).",
		                    "Enter formula",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    null,
		                    defaultFormula);

		if ((reply != null) && (reply.length() > 0)) {
			// This business logic would probably benefit from being moved to the logical layer
			FormulaParser fp = new FormulaParser();
			result = fp.parse(reply);
			if(result == null) {
				// User entered something, but it wasn't a well-formed formula
				result = getFormulaFromUser(reason,"The formula you entered is not a well-formed formula. Please try again!\n\n",reply);
			}
		}

		return result;
	}
}