package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class CopyTest extends TestCase {
	private Rule r;
	
	public void setUp() {
		r = new Copy();
	}
	
	public void testCreation() {
		assertTrue(r instanceof Copy);
	}
	
	public void testUsual() {
		ProofLine pl = new ProofLine(new Dependency(), 4, new Or(new Symbol('p'),new And(new Symbol('q'),new Symbol('r'))), new Premise());
		pl.getDependency().addProofLine(pl);
		
		ProofLine[] pla = r.apply(pl.packageAsArray(), 5);		
		assertEquals(1, pla.length);
		
		ProofLine plOut = pla[0];
		assertEquals(pl.getDependency(), plOut.getDependency());
		assertEquals(5, plOut.getLineNumber());
		assertEquals(pl.getFormula(), plOut.getFormula());
		assertEquals(r, plOut.getJustification());
	}
}