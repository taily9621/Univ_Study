package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

import junit.framework.*;

public class FormulaParserRandomTest extends TestCase {
	private FormulaParser fp;
	
	public void setUp() {
		fp = new FormulaParser();
	}

	public void testRandom() {
		for(int i = 0; i < 100; i++) {
			Formula f = getRandomFormula(0);
			evalIt(f);
		}
	}
	
	public static Formula getRandomFormula(int calls) {
		try {
			int formula = (int)(Math.floor(Math.random() * (4+calls)));
			
			if(formula == 0)
				return new And(getRandomFormula(calls+1), getRandomFormula(calls+1));
			if(formula == 1)
				return new Equivalence(getRandomFormula(calls+1), getRandomFormula(calls+2));
			if(formula == 2)
				return new Implication(getRandomFormula(calls+1), getRandomFormula(calls+1));
			if(formula == 3)
				return new Not(getRandomFormula(calls+1));
			if(formula == 4)
				return new Or(getRandomFormula(calls+1), getRandomFormula(calls+1));
			
			char symbol = (char)(Math.floor(Math.random() * 24) + 65);
			return new Symbol(symbol);
		} catch(Exception e) {
			System.out.println("Exception in random test: " + e.toString());
			return new Symbol('z');
		}
	}
	
	// First transform the Formula to a String, then parse the String
	private void evalIt(Formula formula) {
		Formula formulaActual = fp.parse(formula.toString());
		if(!formulaActual.toString().equals(formula.toString())) {
			System.out.println("Input formula:");
			System.out.println(formula.toString());
			System.out.println("Output formula:");
			System.out.println(formulaActual.toString());
		}
		assertEquals(formula.toString(),formulaActual.toString());
	}
}