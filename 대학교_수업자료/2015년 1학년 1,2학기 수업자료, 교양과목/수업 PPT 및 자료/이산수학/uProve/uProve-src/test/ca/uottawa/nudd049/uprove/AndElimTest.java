package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.And;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class AndElimTest extends TestCase {
	private ProofLine[] plaIn;
	
	public void testCreation() {
		AndElim rule = new AndElim();
	}
	
	private ProofLine[] getPla1() {
		// Create line:
		// 		(1)  p & q				& E
		Formula f = (Formula)new And(new Symbol('p'),new Symbol('q'));  		// p&q
		Justification r = new AndElim();  										// &E
		ProofLine pl = new ProofLine(null,1,f,r);		
		plaIn = new ProofLine[1];
		plaIn[0]=pl;
		
		// Do an and elimination (5 is the next line number to use)
		AndElim rule = new AndElim();
		ProofLine[] plaOut = rule.apply(plaIn,5);
		
		return plaOut;
	}
	
	public void testApply1() {
		getPla1();
	}
	
	public void testIncorrectApply1() {
		AndElim rule = new AndElim();
		assertNull(rule.apply(null,2));
	}

	public void testIncorrectApply2() {
		ProofLine[] pla = new ProofLine[0];
		AndElim rule = new AndElim();
		assertNull(rule.apply(pla,1));
	}

	public void testIncorrectApply3() {
		getPla1(); // Return value not needed, but plaIn initialized
		
		ProofLine[] pla = new ProofLine[2];
		pla[0] = plaIn[0];
		pla[1] = plaIn[0];

		AndElim rule = new AndElim();
		assertNull(rule.apply(pla,1));
	}
	
	public void testApply2() {
		ProofLine[] plaOut = getPla1();
		
		// The apply above should return two lines
		// Line 1:		(5) p			1 &E
		// Line 2:		(5) q			1 &E
		
		// Check line 1
		assertEquals("",plaOut[0].getDependency().toString());
		assertEquals(5,plaOut[0].getLineNumber());
		assertEquals(new Symbol('p'), plaOut[0].getFormula());
		assertEquals(plaIn[0],((Rule)plaOut[0].getJustification()).getProofLines()[0]);
		
		// Check line 2
		assertEquals("",plaOut[1].getDependency().toString());
		assertEquals(5,plaOut[1].getLineNumber());
		assertEquals(new Symbol('q'), plaOut[1].getFormula());
		assertEquals(plaIn[0],((Rule)plaOut[0].getJustification()).getProofLines()[0]);
	}
	
	public void testDependency() {
		ProofLine pl1 = new ProofLine(new Dependency(), 1, new Contradiction(), new Premise());
		ProofLine[] pl1a = {pl1};

		Dependency dep = new Dependency(pl1a);
		ProofLine pl2 = new ProofLine(dep, 2, new And(new Placeholder(),new Placeholder()), new Assumption());
		ProofLine[] pl2a = {pl2};
				
		Rule r = new AndElim();
		ProofLine[] pla = r.apply(pl2a,3);
		
		assertEquals(dep,pla[0].getDependency());

	}
}