package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class ProofLineTest extends TestCase {
	private Formula f;
	private Rule r;
	private Dependency d;
	
	public ProofLine createPl1() {
		f = (Formula)new And(new Symbol('p'),new Symbol('q'));
		d = new Dependency();
		r = new AndElim();
		ProofLine pl = new ProofLine(d,1,f,(Justification)r);
		return pl;
	}
	
	public void testCreation() {
		createPl1();
	}
	
	public void testGetDependency() {
		ProofLine pl = createPl1();
		assertEquals(d,pl.getDependency());
	}
	
	public void testGetLineNumber() {
		ProofLine pl = createPl1();
		assertEquals(1,pl.getLineNumber());
	}
	
	public void testGetFormula() {
		ProofLine pl = createPl1();
		assertEquals(f,pl.getFormula());
	}

	public void testGetJustification() {
		ProofLine pl = createPl1();
		assertEquals(r,pl.getJustification());
	}
	
	public void testSmallProof() {
		ProofLine premise = new ProofLine(new Dependency(), 1, new And(new Symbol('A'),new Symbol('B')), new Premise());
		premise.getDependency().addProofLine(premise);
		
		ProofLine[] premiseArr = {premise};
		
		Rule ae = new AndElim();
		ProofLine[] suggestions = ae.apply(premiseArr,2);
		
		ProofLine conclusion = suggestions[0];
		
		// Expected result:
		//		Dependency = premise's dependency
		//		Line number = 2
		// 		Formula = A
		//		Justification = Rule ae, ProofLine premiseArr
		
		assertEquals(premise.getDependency(), conclusion.getDependency());
		assertEquals(2, conclusion.getLineNumber());
		assertEquals(new Symbol('A'), conclusion.getFormula());
		assertEquals(ae, conclusion.getJustification());
		assertEquals(premiseArr, ((Rule)conclusion.getJustification()).getProofLines());
	}
	
	public void testPackageAsArray() {
		ProofLine pl = new ProofLine(new Dependency(),1,new Placeholder(),new Assumption());
		ProofLine[] pla = pl.packageAsArray();
		
		assertEquals(1,pla.length);
		assertEquals(pl,pla[0]);
	}
	
	public void testPackageAsArrayWithArgument() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Placeholder(),new Assumption());
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('A'),new Premise());
		ProofLine[] pla = pl1.packageAsArray(pl2);
		
		assertEquals(2,pla.length);
		assertEquals(pl1,pla[0]);
		assertEquals(pl2,pla[1]);
	}
	
	public void testToStrings() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		String[] s = pl1.toStrings();
		
		assertEquals("1",s[0]);
		assertEquals("(1)",s[1]);
		assertEquals("a",s[2]);
		assertEquals("Assumption",s[3]);
	}
	
	public void testToStrings2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Assumption());
		ProofLine pl2 = new ProofLine(new Dependency(),2,new And(new Symbol('b'), new Symbol('a')),new Premise());
		pl2.getDependency().addProofLine(pl1);
		pl2.getDependency().addProofLine(pl2);
		String[] s = pl2.toStrings();
		
		assertEquals("1,2",s[0]);
		assertEquals("(2)",s[1]);
		assertEquals("b & a",s[2]);
		assertEquals("Premise",s[3]);
	}
	
	public void testHasPlaceholder() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Implication(new Placeholder(),new Symbol('a')),new Symbol('b')),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Or(new Implication(new Symbol('r'),new Symbol('p')),new Symbol('q')),new Premise());
		
		assertTrue(pl1.hasPlaceholder());
		assertFalse(pl2.hasPlaceholder());
	}
	
	public void testChangePlacholder() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Implication(new Placeholder(),new Symbol('a')),new Symbol('b')),new Premise());
		pl1.changePlaceholder(new Symbol('c'));
		
		assertEquals("((c -> a) \\/ b)", pl1.getFormula().toString());
	}
	
	public void testChangePlaceholder2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Implication(new Symbol('c'),new Symbol('a')),new Symbol('b')),new Premise());
		pl1.changePlaceholder(new Symbol('d'));
		
		assertEquals("((c -> a) \\/ b)", pl1.getFormula().toString());		
	}

	public void testChangePlaceholder3() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Placeholder(),new Premise());
		pl1.changePlaceholder(new Symbol('d'));
		
		assertEquals("d", pl1.getFormula().toString());		
	}
	
	public void testChangePlaceholder4() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new And(new Symbol('a'),new Not(new Placeholder())),new Premise());
		pl1.changePlaceholder(new Symbol('d'));
		
		assertEquals("(a & ~d)", pl1.getFormula().toString());		
	}
}