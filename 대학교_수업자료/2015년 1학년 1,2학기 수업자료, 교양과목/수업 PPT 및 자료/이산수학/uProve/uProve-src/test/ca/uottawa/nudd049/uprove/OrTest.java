package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

import junit.framework.*;

public class OrTest extends TestCase {
	public void testCreation() {
		Or i = new Or(new Placeholder(),new Placeholder());
	}

	public void testConnective() {
		Or i = new Or(new Placeholder(),new Placeholder());
		Connective c = (Connective)i;
	}
	
	public void testException() {
		try {
			Or i = new Or(null,null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}

	public void testException2() {
		try {
			Or i = new Or(new Symbol('p'),null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testException3() {
		try {
			Or i = new Or(null,new Symbol('Q'));
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testSymbol() {
		Or i = new Or(new Placeholder(),new Placeholder());
		assertEquals("\\/",i.symbolToString());
	}
	
	public void testPrint() {
		Or i = new Or(new Symbol('p'),new Symbol('q'));
		assertEquals("p \\/ q",i.toString(true));
	}
	
	public void testPrint2() {
		Or i = new Or(new Symbol('p'),new Symbol('q'));
		assertEquals("(p \\/ q)",i.toString());
	}
	
	public void testFormula() {
		Or i = new Or(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
	}
	
	public void testPrintAsFormula() {
		Or i = new Or(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
		assertEquals("(p \\/ q)",f.toString());
	}
	
	public void testPrintAsFormula2() {
		Or i = new Or(new Symbol('p'),new Symbol('q'));
		Formula f = (Formula)i;
		assertEquals("p \\/ q",f.toString(true));
	}
	
	public void testPrintNested() {
		Formula f = (Formula)new And(new Symbol('p'),new Symbol('q'));
		Formula f2 = (Formula)new Symbol('r');
		Formula f3 = (Formula)new Or(f,f2);
		assertEquals("(p & q) \\/ r",f3.toString(true));
	}
	
	public void testEquals() {
		Formula f = (Formula)new Or(new Symbol('p'),new Symbol('q'));
		Formula f2 = (Formula)new Symbol('r');
		Formula f3 = (Formula)new Implication(f,f2);

		Symbol s1 = new Symbol('p');
		Symbol s2 = new Symbol('q');
		Symbol s3 = new Symbol('r');
		Formula f4 = new Implication(new Or(s1,s2),s3);
		
		assertTrue(f3.equals(f4));
	}
}