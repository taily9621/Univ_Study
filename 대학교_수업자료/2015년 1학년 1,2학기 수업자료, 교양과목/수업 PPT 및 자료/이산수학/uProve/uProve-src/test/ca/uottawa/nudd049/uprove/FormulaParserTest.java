package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

import junit.framework.*;

public class FormulaParserTest extends TestCase {
	private FormulaParser fp;
	
	public void setUp() {
		fp = new FormulaParser();
	}
	
	public void testCreation() {
		assertTrue(fp instanceof FormulaParser);
	}
	
	public void testParseSymbol() {
		Formula f = fp.parse("p");
		assertTrue(f instanceof Symbol);
		assertEquals("p",f.toString());
	}

	public void testParseSymbol2() {
		Formula f = fp.parse("(p)");
		assertTrue(f instanceof Symbol);
		assertEquals("p",f.toString());
	}
	
	public void testParseContradiction() {
		Formula f = fp.parse("_|_");
		assertTrue(f instanceof Contradiction);
	}
	
	public void testParseContradiction2() {
		Formula f = fp.parse("(_|_)");
		assertTrue(f instanceof Contradiction);
	}
	
	public void testParseNot() {
		Formula f = fp.parse("~p");
		assertEquals("~p", f.toString());
	}

	public void testParseNot2() {
		Formula f = fp.parse("~ p");
		assertEquals("~p", f.toString());
	}

	public void testParseOuterParentheses() {
		Formula f = fp.parse("(~ p)");
		assertEquals("~p", f.toString());
	}

	public void testParseOuterParentheses2() {
		Formula f = fp.parse("~ (p)");
		assertEquals("~p", f.toString());
	}

	public void testParseOuterParentheses3() {
		Formula f = fp.parse(" ~( p )");
		assertEquals("~p", f.toString());
	}

	public void testParseAndWithSymbols() {
		Formula f = fp.parse("p & q");
		assertEquals("(p & q)", f.toString());
	}

	public void testParseDoubleAnd() {
		Formula f = fp.parse("p & (q & r)");
		assertEquals("(p & (q & r))", f.toString());
	}

	public void testParseDoubleAndWithNot() {
		Formula f = fp.parse("p & ~(q & r)");
		assertEquals("(p & ~(q & r))", f.toString());
	}
	
	public void testParseNoWhiteSpace() {
		Formula f = fp.parse("p&~(q&r)");
		assertEquals("(p & ~(q & r))", f.toString());
	}
	
	public void testParseExtraWhiteSpace() {
		Formula f = fp.parse("     p         &        ~     (        q   &   r   )    ");
		assertEquals("(p & ~(q & r))", f.toString());
	}

	public void testParseOrWithSymbols() {
		Formula f = fp.parse("p \\/ q");
		assertEquals("(p \\/ q)", f.toString());
	}

	public void testParseAndAndOr() {
		Formula f = fp.parse("((a & b) \\/ (c & (d \\/ e)))");
		assertEquals("((a & b) \\/ (c & (d \\/ e)))", f.toString());
	}

	public void testWrongParseAndAndOr() {
		Formula f = fp.parse("((a & b) \\/ c & (d \\/ e)))");
		assertNull(f);
	}

	public void testPraseImplication() {
		Formula f = fp.parse("p -> q");
		assertEquals("(p -> q)", f.toString());
	}

	public void testPraseImplication2() {
		Formula f = fp.parse("p -> (q -> (a&b))");
		assertEquals("(p -> (q -> (a & b)))", f.toString());
	}
	
	public void testEquivalence() {
		Formula f = fp.parse("p <-> q");
		assertEquals("(p <-> q)", f.toString());
	}
	
	public void testScoping() {
		Formula f = fp.parse("p -> (q -> a&b)");
		assertEquals("(p -> (q -> (a & b)))", f.toString());
	}
	
	public void testMisc() {
		evalIt("p -> q", new Implication(new Symbol('p'),new Symbol('q')));
		evalIt("p & q & r", new And(new Symbol('p'), new And(new Symbol('q'), new Symbol('r'))));
		evalIt("(p&(q->r))", new And(new Symbol('p'), new Implication(new Symbol('q'), new Symbol('r'))));
		evalIt("(a&p <-> (q->r))", new Equivalence(new And(new Symbol('a'),new Symbol('p')), new Implication(new Symbol('q'), new Symbol('r'))));
	}
	
	public void testSuperLong() {
		/*
		
		new Equivalence(
				new Implication(
						new Not(new Symbol('g')),
						new Or(
								new Implication(
										new Symbol('p'),
										new Symbol('B')
								),
								new Symbol('y'))
						),
				new And(
						new Symbol('A'),
						new Symbol('q')
				)
		)
		
		*/

		
		evalIt(new Implication(new Symbol('p'),new Symbol('B')));
		evalIt(new Or(new Implication(new Symbol('p'),new Symbol('B')),new Symbol('y')));
		evalIt(new Not(new Symbol('g')));
		evalIt(new Implication(new Not(new Symbol('g')),new Or(new Implication(new Symbol('p'),new Symbol('B')),new Symbol('y'))));
		evalIt(new And(new Symbol('A'),new Symbol('q')));
		evalIt(new Equivalence(new Implication(new Not(new Symbol('g')),new Or(new Implication(new Symbol('p'),new Symbol('B')),new Symbol('y'))),new And(new Symbol('A'),new Symbol('q'))));
	}
	
	public void testNewNot() {
		// Since there might be a problem entering ~, - is also valid as a not symbol
		Formula f = fp.parse("-p");
		assertEquals("~p", f.toString());
	}

	public void testNewNot2() {
		// Since there might be a problem entering ~, - is also valid as a not symbol
		Formula f = fp.parse("-(p & ~(-r \\/ m))");
		assertEquals("~(p & ~(~r \\/ m))", f.toString());
	}
	
	
	
	// First transform the Formula to a String, then parse the String
	private void evalIt(Formula formula) {
		evalIt(formula.toString(), formula);
	}

	// Parse a String and compare the result with a Formula.
	private void evalIt(String text, Formula formulaExpected) {
		Formula formulaActual = fp.parse(text);
		if(formulaExpected == null)
			assertNull(formulaActual);
		else
			assertEquals(formulaExpected.toString(),formulaActual.toString());
	}
}