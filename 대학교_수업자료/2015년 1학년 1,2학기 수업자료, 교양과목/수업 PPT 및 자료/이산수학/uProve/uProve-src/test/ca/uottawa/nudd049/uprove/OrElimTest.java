package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class OrElimTest extends TestCase {
	private Rule rule;
	
	public void setUp() {
		rule = new OrElim();
	}
	
	public void testWrongArguments1() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Implication(new Symbol('p'),new Symbol('q')),new Assumption());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Implication(new Symbol('q'),new Symbol('q')),new Premise());
		pl2.getDependency().addProofLine(pl2);

		ProofLine[] plaResult = rule.apply(pl1.packageAsArray(pl2),3);
		assertNull(plaResult);
	}
	
	public void testUsual() {
		ProofLine pl0 = new ProofLine(new Dependency(),1,new Contradiction(),new Premise());
		ProofLine pl1 = new ProofLine(new Dependency(),2,new Or(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),3,new Symbol('p'),new Assumption());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),4,new Symbol('r'),new EFQ());
		pl3.getDependency().addProofLine(pl0);
		pl3.getDependency().addProofLine(pl2);
		
		ProofLine pl4 = new ProofLine(new Dependency(),5,new Symbol('q'),new Assumption());
		pl4.getDependency().addProofLine(pl4);
		
		ProofLine pl5 = new ProofLine(new Dependency(),6,new Symbol('r'),new EFQ());
		pl5.getDependency().addProofLine(pl0);
		pl5.getDependency().addProofLine(pl4);
		
		ProofLine[] pla = {pl1,pl2,pl3,pl4,pl5};
			
		ProofLine[] plaResult = rule.apply(pla,7);
		
		assertEquals(1,plaResult.length);
		
		ProofLine plResult = plaResult[0];
		
		// Expected result:
		//	1,2		(7)		r					"rule"
		
		// Dependency
		Dependency d = plResult.getDependency();
		ProofLine[] plDep = d.getProofLines();
		
		assertEquals(2,plDep.length);
		assertEquals(pl0,plDep[0]);
		assertEquals(pl1,plDep[1]);
		
		// Line number
		assertEquals(7,plResult.getLineNumber());
		
		// Formula
		Formula fExpected = new Symbol('r');
		assertEquals(fExpected,plResult.getFormula());
		
		// Justification
		assertEquals(rule,plResult.getJustification());
	}
	
	public void testWrongArguments2() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new And(new Symbol('p'),new Symbol('q')),new Premise()); // !
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Assumption());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('r'),new ImplicationElim());
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);
		
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('q'),new Assumption());
		pl4.getDependency().addProofLine(pl4);
		
		ProofLine pl5 = new ProofLine(new Dependency(),5,new Symbol('r'),new ImplicationElim());
		pl5.getDependency().addProofLine(pl4);
		pl5.getDependency().addProofLine(pl5);
		
		ProofLine[] pla = {pl1,pl2,pl3,pl4,pl5};			
		ProofLine[] plaResult = rule.apply(pla,6);
		
		assertNull(plaResult);
	}
	
	public void testWrongArguments3() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Assumption());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('r'),new ImplicationElim());
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);
		
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('p'),new Assumption());	// !
		pl4.getDependency().addProofLine(pl4);
		
		ProofLine pl5 = new ProofLine(new Dependency(),5,new Symbol('r'),new ImplicationElim());
		pl5.getDependency().addProofLine(pl4);
		pl5.getDependency().addProofLine(pl5);
		
		ProofLine[] pla = {pl1,pl2,pl3,pl4,pl5};			
		ProofLine[] plaResult = rule.apply(pla,6);
		
		assertNull(plaResult);
	}
	
	public void testWrongArguments4() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Premise()); // !
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('r'),new ImplicationElim());
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);
		
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('q'),new Assumption());
		pl4.getDependency().addProofLine(pl4);
		
		ProofLine pl5 = new ProofLine(new Dependency(),5,new Symbol('r'),new ImplicationElim());
		pl5.getDependency().addProofLine(pl4);
		pl5.getDependency().addProofLine(pl5);
		
		ProofLine[] pla = {pl1,pl2,pl3,pl4,pl5};			
		ProofLine[] plaResult = rule.apply(pla,6);
		
		assertNull(plaResult);
	}
	
	public void testWrongArguments5() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Assumption());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('r'),new ImplicationElim());
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);
		
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('q'),new AndElim()); // !
		pl4.getDependency().addProofLine(pl4);
		
		ProofLine pl5 = new ProofLine(new Dependency(),5,new Symbol('r'),new ImplicationElim());
		pl5.getDependency().addProofLine(pl4);
		pl5.getDependency().addProofLine(pl5);
		
		ProofLine[] pla = {pl1,pl2,pl3,pl4,pl5};			
		ProofLine[] plaResult = rule.apply(pla,6);
		
		assertNull(plaResult);
	}

	public void testWrongArguments6() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Or(new Symbol('p'),new Symbol('q')),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('p'),new Assumption());
		pl2.getDependency().addProofLine(pl2);
		
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('r'),new ImplicationElim());
		pl3.getDependency().addProofLine(pl2);
		pl3.getDependency().addProofLine(pl3);
		
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('q'),new Assumption());
		pl4.getDependency().addProofLine(pl4);
		
		ProofLine pl5 = new ProofLine(new Dependency(),5,new Symbol('s'),new ImplicationElim());
		pl5.getDependency().addProofLine(pl4);
		pl5.getDependency().addProofLine(pl5);
		
		ProofLine[] pla = {pl1,pl2,pl3,pl4,pl5};			
		ProofLine[] plaResult = rule.apply(pla,6);
		
		assertNull(plaResult);
	}
}