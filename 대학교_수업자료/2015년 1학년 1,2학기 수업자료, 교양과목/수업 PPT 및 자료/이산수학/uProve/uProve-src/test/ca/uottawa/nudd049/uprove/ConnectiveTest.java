// Most part of the Connective class is tested through the subclasses, e.g. And.  

package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class ConnectiveTest extends TestCase {
	public void testGetSubformula() {
		Symbol p = new Symbol('p');
		Symbol q = new Symbol('q');
		
		Implication i = new Implication(p,q);
		
		assertEquals(p,i.getSubformula1());
		assertEquals(q,i.getSubformula2());
	}
	
}