package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.rules.*;

import junit.framework.*;

public class AssumptionTest extends TestCase {
	public void testCreation() {
		Assumption a = new Assumption();
	}

	public void testToString() {
		Assumption a = new Assumption();
		assertEquals("Assumption",a.toString());
	}
	
	public void testIsJustification() {
		Assumption a = new Assumption();
		Justification j = a;
		assertEquals("Assumption",j.toString());
	}
}