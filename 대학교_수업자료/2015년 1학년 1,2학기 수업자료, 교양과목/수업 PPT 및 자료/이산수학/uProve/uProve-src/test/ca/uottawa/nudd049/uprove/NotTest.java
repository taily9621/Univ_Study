package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.connectives.*;

import junit.framework.*;

public class NotTest extends TestCase {
	public void testCreation() {
		Not a = new Not(new Symbol('p'),null);
	}

	public void testConnective() {
		Not i = new Not(new Placeholder(),null);
		Connective c = (Connective)i;
	}
	
	public void testException() {
		try {
			Not i = new Not(null,null);
			fail("No exception thrown");
		} catch(Exception e) { }
	}

	public void testException2() {
		try {
			Not i = new Not(new Symbol('p'),new Symbol('q'));
			fail("No exception thrown");
		} catch(Exception e) { }
	}
	
	public void testException3() {
		try {
			Not i = new Not(null,new Symbol('Q'));
			fail("No exception thrown");
		} catch(Exception e) { }
	}

	public void testCreation2() {
		Not n = new Not(new Symbol('p'));
	}
	
	public void testSymbol() {
		Not n = new Not(new Symbol('p'));
		assertEquals("~p",n.toString(true));
	}
	
	public void testSymbol2() {
		Not n = new Not(new Symbol('p'));
		assertEquals("~p",n.toString());
	}
	
	public void testCasts() {
		Formula f = (Formula)new Not(new Symbol('p'));
		assertEquals("~p",f.toString());
		Connective c = (Connective)new Not(new Symbol('q'));
		assertEquals("~q",c.toString());
	}

	public void testSymbol3() {
		Formula f = (Formula)new Not(new Not(new Symbol('p')));
		assertEquals("~~p",f.toString());
	}
	
	public void testSymbol4() {
		Formula f = (Formula)new And(new Not(new Implication(new Not(new Symbol('p')),new Symbol('q'))),new Symbol('r'));
		assertEquals("(~(~p -> q) & r)",f.toString());
	}
}