package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;

import junit.framework.*;

public class MainTest extends TestCase {
	public void testGetRules() {
		String[] s = Main.getRules();
		assertEquals(14, s.length);
		assertEquals("AndElim", s[0]);
		assertEquals("AndIntro", s[1]);
		assertEquals("Assumption", s[2]);
		assertEquals("Copy", s[3]);
		assertEquals("Df1", s[4]);
		assertEquals("Df2", s[5]);
		assertEquals("DN", s[6]);
		assertEquals("EFQ", s[7]);
		assertEquals("ImplicationElim", s[8]);
		assertEquals("ImplicationIntro", s[9]);
		assertEquals("NotElim", s[10]);
		assertEquals("NotIntro", s[11]);
		assertEquals("OrElim", s[12]);
		assertEquals("OrIntro", s[13]);
	}
}
