package ca.uottawa.nudd049.uprove;

import ca.uottawa.nudd049.uprove.logic.*;
import ca.uottawa.nudd049.uprove.logic.rules.AndIntro;
import ca.uottawa.nudd049.uprove.logic.rules.Assumption;

import junit.framework.*;

public class DependencyTest extends TestCase {
	public void testCreation() {
		Dependency d = new Dependency();
	}
	
	public void testToString() {
		// Nothing should be printed out if we do not have any dependencies
		Dependency d = new Dependency();
		assertEquals("",d.toString());
	}

	public void testToString2() {
		// Nothing should be printed out if we do not have any dependencies
		ProofLine pl = new ProofLine(null,1,new Contradiction(),new Premise());
		Dependency d = new Dependency(pl.packageAsArray());
		
		assertEquals("1",d.toString());
	}

	public void testToString3() {
		// Nothing should be printed out if we do not have any dependencies
		ProofLine p1 = new ProofLine(null,1,new Contradiction(),new Premise());
		ProofLine p2 = new ProofLine(null,2,new Contradiction(),new Premise());
		ProofLine p3 = new ProofLine(null,3,new Contradiction(),new Premise());
		
		Dependency d = new Dependency();
		d.addProofLine(p1);
		d.addProofLine(p2);
		d.addProofLine(p3);
		
		assertEquals("1,2,3",d.toString());
	}
	
	public void testCreateWithArgument() {
		ProofLine pl = new ProofLine(null,1,new Contradiction(),new Premise());
		ProofLine[] pla = {pl};
		
		Dependency d = new Dependency(pla);
	}
	
	public void testGetProofLines() {
		ProofLine pl = new ProofLine(null,1,new Contradiction(),new Premise());
		ProofLine[] pla = {pl};		
		Dependency d = new Dependency(pla);
		
		assertEquals(pla,d.getProofLines());
	}

	public void testCallAddProofLines() {
		Dependency d = new Dependency();
		ProofLine pl = new ProofLine(d,1,new Contradiction(),new Premise());
		d.addProofLine(pl);
	}
	
	public void testCreationAndAdd() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),1,new Symbol('b'),new Premise());
		ProofLine pl3 = new ProofLine(new Dependency(),1,new Symbol('c'),new Premise());
		ProofLine[] pla123 = {pl1,pl2,pl3};
		
		ProofLine pl4 = new ProofLine(new Dependency(),1,new Symbol('d'),new Premise());
		ProofLine pl5 = new ProofLine(new Dependency(),1,new Symbol('e'),new Premise());
		ProofLine pl6 = new ProofLine(new Dependency(),1,new Symbol('f'),new Premise());
		
		// Create a new dependency with three prooflines
		Dependency d = new Dependency(pla123);
		
		ProofLine[] result1 = d.getProofLines();
		assertEquals(result1.length,3);
		assertEquals(result1[0],pl1);
		assertEquals(result1[1],pl2);
		assertEquals(result1[2],pl3);
		
		// Add new dependencies
		d.addProofLine(pl4);
		d.addProofLine(pl5);
		d.addProofLine(pl6);
		
		ProofLine[] result2 = d.getProofLines();
		assertEquals(result2.length,6);
		assertEquals(result2[0],pl1);
		assertEquals(result2[1],pl2);
		assertEquals(result2[2],pl3);
		assertEquals(result2[3],pl4);
		assertEquals(result2[4],pl5);
		assertEquals(result2[5],pl6);
	}
	
	public void testMerge1() {
		// Create dependency 1
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),3,new Symbol('b'),new Premise());
		Dependency d1 = new Dependency(pl1.packageAsArray(pl2));
		
		// Create dependency 2
		ProofLine pl3 = new ProofLine(new Dependency(),2,new Symbol('c'),new Premise());
		Dependency d2 = new Dependency(pl3.packageAsArray()); 
		
		// Create mixed dependencies and get the ProofLines
		Dependency d = new Dependency(d1,d2);		
		ProofLine[] pla = d.getProofLines();
		
		assertEquals(3,pla.length);
		assertEquals(pl1,pla[0]);
		assertEquals(pl3,pla[1]);
		assertEquals(pl2,pla[2]);
	}

	public void testMerge2() {
		// Create dependency 1
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),3,new Symbol('b'),new Premise());
		Dependency d1 = new Dependency(pl1.packageAsArray(pl2));
		
		// Create dependency 2
		ProofLine pl3 = new ProofLine(new Dependency(),2,new Symbol('c'),new Premise());
		Dependency d2 = new Dependency(pl3.packageAsArray()); 
		
		// Create mixed dependencies and get the ProofLines
		Dependency d = new Dependency(d2,d1);								// Changed order		
		ProofLine[] pla = d.getProofLines();
		
		assertEquals(3,pla.length);
		assertEquals(pl1,pla[0]);
		assertEquals(pl3,pla[1]);
		assertEquals(pl2,pla[2]);
	}

	public void testMerge3EmptyDependency() {
		// Create dependency 1
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),3,new Symbol('b'),new Premise());
		Dependency d1 = new Dependency(pl1.packageAsArray(pl2));
		
		// Create dependency 2
		Dependency d2 = new Dependency(); 

		// Create mixed dependencies and get the ProofLines
		Dependency d = new Dependency(d1,d2);	
		ProofLine[] pla = d.getProofLines();
		
		assertEquals(2,pla.length);
		assertEquals(pl1,pla[0]);
		assertEquals(pl2,pla[1]);
	}
	
	public void testMerge4EmptyDependency() {
		// Create dependency 1
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		Dependency d1 = new Dependency(pl1.packageAsArray());
		
		// Create dependency 2
		Dependency d2 = new Dependency(); 
		
		// Create mixed dependencies and get the ProofLines
		Dependency d = new Dependency(d2,d1);		
		ProofLine[] pla = d.getProofLines();
		
		assertEquals(1,pla.length);
		assertEquals(pl1,pla[0]);
	}

	public void testMerge5Overlap() {
		// Create dependency 1
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),3,new Symbol('b'),new Premise());
		Dependency d1 = new Dependency(pl1.packageAsArray(pl2));
		
		// Create dependency 2
		Dependency d2 = new Dependency(pl1.packageAsArray());  
		
		// Create mixed dependencies and get the ProofLines
		Dependency d = new Dependency(d1,d2);
		ProofLine[] pla = d.getProofLines();
		
		assertEquals(2,pla.length);
		assertEquals(pl1,pla[0]);
		assertEquals(pl2,pla[1]);
	}

	public void testMerge6Overlap() {
		// Create dependency 1
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('b'),new Premise());
		Dependency d1 = new Dependency(pl1.packageAsArray(pl2));
		
		// Create dependency 2
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('c'),new Premise());
		Dependency d2 = new Dependency(pl2.packageAsArray(pl3));  
		
		// Create mixed dependencies and get the ProofLines
		Dependency d = new Dependency(d2,d1);
		ProofLine[] pla = d.getProofLines();
		
		assertEquals(3,pla.length);
		assertEquals(pl1,pla[0]);
		assertEquals(pl2,pla[1]);
		assertEquals(pl3,pla[2]);
	}
	
	
	public void testRemove() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('a'),new Premise());
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('b'),new Premise());
		ProofLine pl3 = new ProofLine(new Dependency(),3,new Symbol('c'),new Premise());
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('d'),new Premise());
		ProofLine[] pla123 = {pl1,pl2,pl3};
				
		Dependency d = new Dependency(pla123);
	
		// Check {pl1,pl2,pl3}/pl1
		ProofLine[] result = (new Dependency(d,pl1)).getProofLines();
		assertEquals(2,result.length);
		assertEquals(pl2,result[0]);
		assertEquals(pl3,result[1]);

		// Check {pl1,pl2,pl3}/pl2
		result = (new Dependency(d,pl2)).getProofLines();
		assertEquals(2,result.length);
		assertEquals(pl1,result[0]);
		assertEquals(pl3,result[1]);		
		
		// Check {pl1,pl2,pl3}/pl3
		result = (new Dependency(d,pl3)).getProofLines();
		assertEquals(2,result.length);
		assertEquals(pl1,result[0]);
		assertEquals(pl2,result[1]);
		
		// Check {pl1,pl2,pl3}/pl4
		result = (new Dependency(d,pl4)).getProofLines();
		assertEquals(3,result.length);
		assertEquals(pl1,result[0]);
		assertEquals(pl2,result[1]);
		assertEquals(pl3,result[2]);		
	}
	
	public void testDependOnlyOnPremises() {
		ProofLine pl1 = new ProofLine(new Dependency(),1,new Symbol('p'),new Premise());
		pl1.getDependency().addProofLine(pl1);
		
		ProofLine pl2 = new ProofLine(new Dependency(),2,new Symbol('q'),new Premise());
		pl2.getDependency().addProofLine(pl2);
		
		AndIntro ai = new AndIntro();
		ProofLine pl3 = ai.apply(pl1.packageAsArray(pl2),3)[0];
		
		assertTrue(pl3.getDependency().containsOnlyPremises());
		
		ProofLine pl4 = new ProofLine(new Dependency(),4,new Symbol('r'),new Assumption());
		pl4.getDependency().addProofLine(pl4);
		
		AndIntro ai2 = new AndIntro();
		ProofLine pl5 = ai2.apply(pl3.packageAsArray(pl4),5)[0];
		
		assertFalse(pl5.getDependency().containsOnlyPremises());
		
	}
}