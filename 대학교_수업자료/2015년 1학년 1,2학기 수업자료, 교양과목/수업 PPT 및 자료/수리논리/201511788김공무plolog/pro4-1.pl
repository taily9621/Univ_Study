man(cronus).
woman(rhea).
is_married_to(cronus, rhea).

is_son_of(hades, cronus).
is_son_of(poseidon, cronus).
is_son_of(zeus, cronus).
is_daughter_of(hestia, cronus).
is_daughter_of(hera, cronus).
is_daughter_of(demeter,cronus).

is_parent_of(Y, X) :- is_son_of(X, Y).
is_parent_of(Y, X) :- is_daughter_of(X, Y).

is_mother_of(Y, X) :- is_parent_of(Z,X), is_married_to(Z,Y).
is_father_of(X, Y) :- is_parent_of(X,Y).

is_son_of(cronus, uranus).
is_married_to(uranus, gaea).

is_married_to(zeus, metis).
is_daughter_of(athena, zeus).

is_married_to(zeus, demeter).
is_daughter_of(persephone, zeus).

is_married_to(zeus, hera).
is_son_of(ares, zeus).
is_son_of(hephaestus, zeus).
is_daughter_of(hebe, zeus).

is_married_to(zeus, dione).
is_daughter_of(aphrodite, zeus).

is_married_to(zeus, leto).
is_son_of(apollo, zeus).
is_daughter_of(artemis, zeus).

is_child_of(X,Y) :- is_son_of(X,Y).
is_child_of(X,Y) :- is_daughter_of(X,Y).
is_descendant_of(X,Y) :- is_child_of(X,Y).
is_descendant_of(X,Y) :- is_child_of(X,Z),is_descendant_of(Z,Y).
parent_child(X,Y,Z) :-is_son_of(Z,X),is_son_of(Z,Y).
parent_child(X,Y,Z) :-is_daughter_of(Z,X),is_daughter_of(Z,Y).
is_daughter_of(persephone,demeter).
is_son_of(ares,hera).
is_son_of(hephaestus,hera).
is_daughter_of(hebe,hera).
is_daughter_of(aphrodite,dione).
is_son_of(apollo,leto).
is_daughter_of(artemis,leto).
