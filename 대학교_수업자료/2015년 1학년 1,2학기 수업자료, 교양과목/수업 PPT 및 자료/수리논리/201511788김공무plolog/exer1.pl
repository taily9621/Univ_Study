direct_line(incheon, singapore).
direct_line(singapore, london).
direct_line(incheon, narita).
direct_line(narita, los_angeles).
direct_line(los_angeles, huston).
direct_line(huston, washingtonDC).

connected(X,Y) :- direct_line(X,Y).
connected(X,Y) :- direct_line(X,A),connected(A,Y).
reconnected(Y,X) :- direct_line(X,A),connected(A,Y).
