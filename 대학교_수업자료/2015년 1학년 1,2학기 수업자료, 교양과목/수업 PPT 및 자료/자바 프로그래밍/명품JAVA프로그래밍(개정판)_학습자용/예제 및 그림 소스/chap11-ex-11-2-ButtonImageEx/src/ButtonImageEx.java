import javax.swing.*;
import java.awt.*;

public class ButtonImageEx extends JFrame {
	Container contentPane;
	ButtonImageEx() {
		setTitle("이미지 버튼 예제");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());

		// 3 개의 이미지를 파일로부터 읽어들인다.
		ImageIcon normalIcon = new ImageIcon("images/normalIcon.gif");// normalIcon용 이미지
		ImageIcon rolloverIcon = new ImageIcon("images/rolloverIcon.gif");// rolloverIcon용 이미지
		ImageIcon pressedIcon = new ImageIcon("images/pressedIcon.gif");// pressedIcon용 이미지		
		
		// normalIcon을 가진 버튼 컴포넌트 생성
		JButton btn = new JButton("call~~", normalIcon);
		btn.setPressedIcon(pressedIcon);// pressedIcon용 이미지 등록
		btn.setRolloverIcon(rolloverIcon);// rolloverIcon용 이미지 등록
		contentPane.add(btn);
		
		setSize(250,150);
		setVisible(true);
	}
	
	public static void main(String [] args) {
		new ButtonImageEx();
	}
} 




