parent(mary,jhon).
parent(mary,jil).
parent(john,caren).
parent(john,isaac).
parent(jil,anna).
parent(caren,billy).
grandparent(X,Z):- parent(X,Y),parent(Y,Z).
siblings(X,Y) :- parent(Z,X),parent(Z,Y),X\=Y.
cousin(X,Y) :- siblings(A,B),parent(A,X),parent(B,Y).
aunt_uncle(X,Y) :- parent(X,Z),cousin(Y,Z).

