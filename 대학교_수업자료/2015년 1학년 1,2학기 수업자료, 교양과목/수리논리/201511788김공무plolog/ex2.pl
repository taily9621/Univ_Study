has_played(peter, titanic).
has_played(peter, hulk).
has_played(donald, titanic).
has_played(willy, matrix).
has_played(susan, titanic).
has_played(susan, matrix).
has_played(jane, highnoon).
played_in_same_film(Y,X,Z) :- has_played(X,Z),has_played(Y,Z),X\=Y.
played_in_the_same_film(susan,X),X\=Y.
same_actor(Y,peter) :- has_played(peter,Y).
same_actor(Y,susan) :- has_played(susan,Y).
same_actor(titanic,matrix,X) :- has_played(X,titanic),has_played(X,matrix).

