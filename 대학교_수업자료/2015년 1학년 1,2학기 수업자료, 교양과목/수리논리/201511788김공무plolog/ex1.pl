father(abraham, isaac).
father(haran, lot).
father(haran, milcah).
father(haran, yiscah).
male(isaac).
male(lot).
female(milcah).
female(yiscah).
son(isaac, Y).
son(lot,Y).
daughter(milcah, Y).
daughter(yiscah,Y).
is_parent_of(Y,X):-is_son_of(X,Y),male(Y).
is_parent_of(Y,X):-is_daughter_of(X,Y),female(Y).
