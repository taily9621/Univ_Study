//201511788 1학년 김공무
#include<stdio.h>
float Total(float, float);//평균값을 구하는 함수
int Grade(float);//학점을 매기는 함수	
void draw(float,float,float,char);//출력하는 함수
float Midterm, Finalexam, average; char input;
void main()//중간고사 점수와 기말고사 점수를 받고 평균 함수와 학점 함수 그리고 풀력 함수를 호출
{

	printf("중간고사와 기말고사 점수로 학점 계산\n\n");
	printf("중간고사 : ");
	scanf_s("%f", &Midterm);
	printf("\n");
	printf("기말고사 : ");
	scanf_s("%f", &Finalexam);
	Total(Midterm, Finalexam);//평균 함수 호출!
	Grade(average);//학점 함수 호출!
	draw(Midterm,Finalexam,average,input);//결과값 출력 함수 호출!
}
float Total(float Midterm, float Finalexam)//평균 함수
{
	average = (Midterm + Finalexam) / 2;
	printf("%d", average);
	return average;
}
int Grade(float average)//학점 함수
{
	if (average >= 90)
		input = 'A';
	else if (average >= 80 && average < 90)
		input = 'B';
	else if (average >= 70 && average < 80)
		input = 'C';
	else if (average >= 60 && average < 70)
		input = 'D';
	else 
		input = 'F';
	return input;//문자형 input에다 학점을 넣는다.
}
void draw(float Midterm,float Finalexam,float average,char input)//출력 함수
{
	printf("중간고사 ; %g, 기말고사 : %g, 평균 : %f, 학점 : %c\n", Midterm, Finalexam,average,input );//각각 중간고사 점수, 기말고사 점수, 평균, 학점
}
//완성!
//2학기 개강후 처음으로 받은 과제인 만큼 열심히 해보게 되었습니다. 처음엔 살짝 헷갈리는 부분이 있었지만 차근차근 해보고 나서는
//제대로 결과가 나올 수 있었습니다. 앞으로 헷갈리지 않게 프로그램을 많이 짜봐야겠습니다.