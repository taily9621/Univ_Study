//과제5번> 201511788 1학년 김공무
#include<stdio.h>
void swapArray(int* a, int* b, int c) //배열을 바꾸는 함수
{
	int storage = 0;

	for (int count = 0; count < c; count++) 
	{
		storage = a[count];
		a[count] = b[count];
		b[count] = storage;
	}
}

void sumAvg(int* a, int b, int *c, float *d) //바뀐 배열의 평균값을 계산하는 함수
{
	int sum = 0;
	float avg = 0;
	for (int i = 0; i < b; i++) {
		sum += a[i];
	}
	*c = sum;
	for (int i = 0; i < b; i++) {
		avg += a[i];
	}
	avg = avg / b;
	*d = avg;

}

int main() 
{
	int first[10] = { 10, 20, 30, 40, 50 , 60 , 70 , 80, 90, 100 };//첫 번째 배열 선언
	int second[10] = { 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };//두 번째 배열 선언

	//필요한 변수 선언
	int i = 0;
	int input;
	int sum = 0;
	float avg = 0;



	//교환할 배열의 크기를 키보드 입력(1~10까지만 입력 가능)
	printf("교환할 배열의 크기를 입력하세요(1~10 사이의 값 중 한개의 값 입력>");
	scanf_s("%d", &input);
	printf("\n\n");



	//함수 호출 전의 배열 a와 배열 b의 배열 값을 화면에 출력
	printf("함수 호출 전\n");
	for (i = 0; i < input; i++) {
		printf("a[%d번째] = %d\t", i + 1, first[i]);
	}
	printf("\n");
	for (i = 0; i < input; i++) {
		printf("b[%d번째] = %d\t", i, second[i]);
	}
	printf("\n\n");

	//배열 a와 배열 b의 배열값들을 서로 바꿈(swapArray 함수 호출)
	swapArray(first, second, input);


	//서로 바뀌어진 배열 a와 배열 b의 배열 값을 화면에 출력
	printf("2. 함수 호출 후");
	printf("\n");
	for (i = 0; i < input; i++) {
		printf("a[%d번째] = %d\t", i + 1, first[i]);
	}
	printf("\n");
	for (i = 0; i < input; i++) {
		printf("b[%d번째] = %d\t", i + 1, second[i]);
	}
	printf("\n\n");



	//배열 a의 합계와 평균값을 계산함(sumAvg 함수 호출)
	sumAvg(first, input, &sum, &avg);
	printf("3. 배열 a의 합계 = %d, 평균 = %f\n\n", sum, avg);

	//배열 b의 합계와 평균값을 계산함(sumAvg 함수 호출)
	sumAvg(second, input, &sum, &avg);
	printf("4. 배열 b의 합계 = %d, 평균 = %f\n\n", sum, avg);

}
//프로그램 완성!
//이번 5번째 과제를 하면서 수업시간에 배웠던 내용이 많은 도움이 되어 코딩을 쉽게 짤 수 있었습니다.
//예전과 다르게 이번에는 함수를 위에서 선언과 정의를 한번에 해보게 되었습니다.(친구의 조언이 큰 영향을 주게 되었습니다)