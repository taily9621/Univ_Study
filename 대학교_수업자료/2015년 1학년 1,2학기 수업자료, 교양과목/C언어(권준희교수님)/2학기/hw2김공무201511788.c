//201511788 1학년 김공무
#include<stdio.h>
int plus(int,int);//덧셈 함수
int minus(int,int);//뺄셈 함수
int multiple(int,int);//곱셈 함수
int division(int,int);//나눗셈 함수
int main()
{
	static int input1, input3;//정수를 input1과 input3에 static int로 선언
	while(1)//무한 반복문
	{
		char input2;//기호를 input2에 char형으로 선언
		printf("수식을 입력하세요 <0 0 0 입력시 종료>:");
		scanf_s("%d",&input1);//정수 입력
		scanf_s("%c", &input2);//기호 입력
		scanf_s("%d", &input3);//정수 입력
		if (input1 == 0 && input3 == 0)
			break;
		switch(input2)//기호에 따른 계산 분별
		{ case '+':
			plus(input1,input3);//input2이 +일때 덧셈 함수를 호출
			break;
		  case '-':
			minus(input1,input3);//input2이 -일때 뺄셈 함수를 호출
			break;
		  case '*':
			multiple(input1,input3);//input2이 *일때 곱셈 함수를 호출
			break;
		  case '/':
	 		division(input1,input3);//input2이 /일때 곱셈 함수를 호출
			break;
		}
	}
	return 0;
}
int plus(int input1,int input3)//덧셈 함수
{
	int result=0;
	static int plus_count = 0;
	++plus_count;
	printf("덧셈은 총 %d번 호출되었습니다.\n", plus_count);
	result=input1+input3;
	printf("연산 결과:%d\n", result);
	return result;
}
int minus(int input1,int input3)//뺄셈 함수
{
	int result=0;
	static int minus_count = 0;
	++minus_count;
	printf("뺄셈은 총 %d번 호출되었습니다.\n", minus_count);
	result = input1 - input3;
	printf("연산 결과:%d\n", result);
	return result;
}
int multiple(int input1,int input3)//곱셈 함수
{
	int result=0;
	static int multiple_count = 0;
	++multiple_count;
	printf("곱셈은 총 %d번 호출되었습니다.\n", multiple_count);
	result = input1 * input3;
	printf("연산 결과:%d\n", result);
	return result;
}
int division(int input1,int input3)//나눗셈 함수
{
	int result=0;
	static int division_count = 0;
	++division_count;
	printf("나눗셈은 총 %d번 호출되었습니다.\n", division_count);
	result = input1 / input3;
	printf("연산 결과:%d\n", result);
	return result;
}
//미완성
//첫번째 과제를 이어서 두번째 과제를 하면서 첫번째 과제보다 많은 생각을 하면서 프로그래밍을 했던것 같습니다.
//정수와 기호를 입력 할 때 000을 누르게 되면 한번 더 입력창이 생기게 되는 부분을 잘 모르겠습니다.
