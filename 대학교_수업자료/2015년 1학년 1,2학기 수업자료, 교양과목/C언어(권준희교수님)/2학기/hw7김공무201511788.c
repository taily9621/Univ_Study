// 과제7> 201511788 1학년 김공무 
#include <stdio.h> 
#include <string.h> 
#pragma warning(disable:4996)//strcpy의 안정성 에러 무시
#define sentence 100//문장 제한
// 찾아 바꾸기 함수
void Find_and_Change(const char *ipt, const char *fnd, const char *cng, char *opt) 
{
	char    *sts;
	sts = strstr(ipt, fnd);
	if (sts == NULL) // 찾는 문자열이 없으면 그대로 복사 
	{
		strcpy(opt, ipt);
	}
	else // 찾는 문자열이 있으면 문자열을 조합 
	{
		strncpy(opt, ipt, sts - ipt);
		opt[sts - ipt] = 0;
		strcpy(opt + strlen(opt), cng);
		strcpy(opt + strlen(opt), sts + strlen(fnd));
	}
}
int main() 
{
	char    ipt[sentence], opt[sentence], fnd[sentence], cng[sentence];
	// 초기 입력 함수, 찾는 문자열 함수, 바꿀 문자열 함수, 출력 함수 선언
	printf("문자열을 입력하시오 : ");//초기 문자열 입력
	gets(ipt);
	ipt[strlen(ipt) - 1] = 0;
	
	printf("찾을 문자열 : ");//찾을 문자열 입력
	gets(fnd);
	fnd[strlen(fnd) - 1] = 0;

	printf("바꿀 문자열 : ");//바꿀 문자열 입력
	gets(cng);
	cng[strlen(cng) - 1] = 0;

	Find_and_Change(ipt, fnd, cng, opt);//찾아 바꾸기 함수 호출
	printf("결과 : %s\n", opt);//결과 출력
	return 0;
}
//7번째 과제 완성!
//이번 일곱 번째 과제를 완성하면서 요번에 배우기 어려웠던 문자와 문자열 챕터여서 좀 어려웠던 부분도 있었지만
//관련된 정보들을 찾아보면서 과제를 완성할 수 있었습니다.
//strcpy의 안정성 오류에 대해서는 인터넷을 통해 알아보고 해결하게 되었습니다.