//201511788 1학년 김공무
#include<stdio.h>
#define student 10//학생수를 10명으로 제한
void main()
{
	int grade[student][3]; int highest[10]; int worst[10]; int total[10];//배열 선언
	int count, count_s, highest1 = 0,input, highest2 = 0, highest3 = 0, worst1 = 101, worst2 = 101, worst3 = 101, total1 = 0, total2 = 0, total3 = 0;//변수 선언
	printf("입력할 학생 수를 입력하세요.");
	scanf_s("%d", &input);//학생 수 입력
	if (input < 0 && input>student)
		return 0;//입력받은 학생수가 0명 미만이랑 10초과가 되면 프로그램 종료
	for (count = 0; count < input; count++)
	{
		printf("<학생 %d의 시험 성적>\n", count + 1);
		printf("1번째 시험 성적:");
		scanf_s("%d\n", &grade[count][0]);
		printf("2번째 시험 성적:");
		scanf_s("%d\n", &grade[count][1]);
		printf("3번째 시험 성적");
		scanf_s("%d\n", &grade[count][2]);
	}//입력받은 학생수 대로 성적 입력
	printf("\n");
	for (count = 0; count < input; count++)
	{
		if (highest1 < grade[count][0])
			highest1 = grade[count][0];
	}
	for (count = 0; count < input; count++)
	{
		if (highest2 < grade[count][1])
			highest2 = grade[count][1];
	}
	for (count = 0; count < input; count++)
	{
		if (highest3 < grade[count][2])
			highest3 = grade[count][2];
	}//시험1 시험2 시험3에 대한 최고점수 구하기
	for (count = 0; count < input; count++)
	{
		if (worst1 > grade[count][0])
			worst1 = grade[count][0];
	}
	for (count = 0; count < input; count++)
	{
		if (worst2 > grade[count][1])
			worst2 = grade[count][1];
	}
	for (count = 0; count < input; count++)
	{
		if (worst3 > grade[count][2])
			worst3 = grade[count][2];
	}//시험1 시험2 시험3에 대한 최저점수 구하기
	for (count = 0; count < input; count++)
		total1 += grade[count][0];
	for (count = 0; count < input; count++)
		total2 += grade[count][1];
	for (count = 0; count < input; count++)
		total3 += grade[count][2];//시험1 시험2 시험3 총합 구하기
	printf("시험 1의 최고점수=%d,최저점수=%d,평균점수=%lf\n", highest1, worst1, (double)(total1 / input));
	printf("시험 2의 최고점수=%d,최저점수=%d,평균점수=%lf\n", highest2, worst2, (double)(total2 / input));
	printf("시험 3의 최고점수=%d,최저점수=%d,평균점수=%lf\n", highest3, worst3, (double)(total3 / input));
	printf("\n");//시험1 시험2 시험3 최고점수 최저점수 평균 출력
	for (count = 0; count < input; count++)
	{
		highest[0] = 0;
		for (count_s = 0; count_s < 3; count_s++)
		{
			if (highest[count] < grade[count][count_s])
				highest[count] = grade[count][count_s];
		}
	}//학생 최고점수 구하기
	for (count = 0; count < input; count++)
	{
		worst[0] = 101;
		for (count_s = 0; count_s < 3; count_s++)
		{
			if (worst[count] > grade[count][count_s])
				worst[count] = grade[count][count_s];
		}
	}//학생 최저점수 구하기
	for (count = 0; count < input; count++)
	{
		for (count_s = 0; count_s < 3; count_s++)
			total[count] += grade[count][count_s];
	}//학생 점수 총합 구하기
	for (count = 0; count < input; count++)
		printf("학생 %d의 최고점수=%d,최저점수=%d,평균점수=%lf", highest[count], worst[count], (double)total[count] / 3);//학생당 최고점수 최저점수 평균 출력
}
//<미완성>
//세번째 과제를 하면서 지난 과제보다 좀 까다롭다고 느껴졌지만 코드를 짜면서 흥미로웠던 과제였습니다.
//디버깅을 실행시키면 학생수를 입력받은 다음 첫번째 학생 점수를 입력하면 바로 두 번째 학생 점수를 입력을 받아야 하는데
//아무것도 없는 입력창이 한번 더 호출이 되면서 임의의 숫자를 입력하게 되면 그때 두 번째 학생 점수를 입력을 받으며 두 번째 학생
//의 점수를 입력을 하게 되면 콘솔창이 더이상 응답을 하지 못하고 종료하게 됩니다.