//과제9>학번:201511788 학년:1학년 이름:김공무
#include<stdio.h>
#include<string.h>
#define SIZE 100//글자수 제한
enum type { Cartoon, SF, Novel, Classic };//책 종류 열거형 선언
typedef struct Book_Information //책 정보 구조체 틀
{
	char title[SIZE];
	char writer[SIZE];
	int position;
	enum type varation;
}BI;//구조체 정의
void Book_input(BI library[],int count)//책 정보 입력 함수(여기서 프로그램 문제!)
{
	int Type;

	fflush(stdin);//입력 버퍼를 지워주는 플러시
	printf("제목:");
	gets(library[count].title);//제목을 입력받지 못함(?)
	printf("저자:");
	gets(library[count].writer);
	printf("위치:");
	scanf_s("%d",&library[count].position);
	printf("장르(0: 만화, 1: 공상소설, 2: 소설, 3: 고전)");
	scanf_s("%d", &Type);
	if (Type >= 0 && Type <= 3)
		library[count].varation = Type;
	else
		library[count].varation = Cartoon;

};
int Book_output(BI library[],int count)//책 정보를 출력하는 함수
{
	int i;
	fflush(stdin);//입력 버퍼를 비워주는 플러시
	for (i = 0; i < count; i++)
	{
		printf("제목 : %s\n", library[i].title);
		printf("저자 : %s\n", library[i].writer);
		printf("위치 : %d\n", library[i].position);
		if (library[i].varation == 0)
			printf("장르 : 코믹\n");
		else if (library[i].varation == 1)
			printf("장르 : 공상과학\n");
		else if (library[i].varation == 2)
			printf("장르 : 소설\n");
		else if (library[i].varation == 3)
			printf("장르 : 고전\n");
	}
};
int Book_search(BI library[], int count)//책 정보를 찾는 함수
{
	int i;
	char Title[SIZE];

	fflush(stdin);
	printf("제목: ");
	gets(Title);

	for (i = 0; i < count; i++)
	{
		if (strcmp(Title, library[i].title) == 0)//저장된 위치를 찾을 때 까지 돌려서 찾는다.
		{
			printf("저장된 위치는 %d\n", library[i].position);
			return;
		}
	}
	printf("찾는 책이 테이블에 없습니다.\n");//없으면 print
};
void main()
{
	BI library[SIZE];//구조체 library 선언
	int count=0,Number;
	while (1)//무한반복
	{
		for (count = 0; count < 24; count++)
			printf("=");
		printf("\n");
		printf("1. 추가\n");
		printf("2. 출력\n");
		printf("3. 검색\n");
		printf("4. 종료\n");
		for (count = 0; count < 24; count++)
			printf("=");
		printf("\n");
		printf("정수값을 압력하시오: ");
		scanf_s("%d", &Number);//출력 및 번호 입력
		switch (Number)
		{
		case 1://책 정보 입력
			Book_input(library, count);
			break;
		case 2://책 정보 출력
			Book_output(library, count);
			break;
		case 3://책 정보 탐색
			Book_search(library, count);
			break;
		case 4://나가기
			return 0;
		}

	}
}
//과제 미완성.
// 이번 아홉 번째 과제를 하게 되면서 "진짜 프로그램"을 짜보게 된 것 같아 매우 흥미롭고 재미있기도 했으며
//또 한편으로는 체감 난이도가 많이 높다는 것을 느낄 수 있었습니다.
//그로므로 과제를 하는 과정에서 어려운 점이 있었지만 다시 한 번 구조체에 대해 공부할 수 있어 많은 도움이 되었습니다.
//1번 함수에서 책 정보를 제대로 받지 않아 2번과 3번 함수도 제대로 돌아가지 않습니다.
//1번 함수에서 책 제목과 저자를 입력받는 문자열 입력 부분에서 제목은 입력 받지 않고 바로 저자로 
//넘어가는 현상이 있습니다.