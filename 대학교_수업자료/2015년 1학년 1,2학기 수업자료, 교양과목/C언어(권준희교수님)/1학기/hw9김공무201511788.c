//hw9김공무201511788 미완성
#include<stdio.h>

void NUMBER(void); // 짝수 홀수 구별 프로그램
void diamond(void); // 다이아몬드 출력프로그램

int main(void)
{
	int choice;
	printf("1. 짝수 홀수 구별\n2. 다이아몬드 출력하기\n0. 종료\n선택 : ");
	do // 선택시 선택 하는 프로그램의 번호와 다를때 재선택을 위한 반복문
	{
		printf("재선택 : ");
	scanf("%d", &choice);
	
	if ( choice == 1 ) // 첫번째 프로그램 실행 if문
	{
		int number;
		NUMBER();
		
	}
	if ( choice == 2 ) // 두번째 프로그램 실행 if문
	{
		diamond();
	}
	if ( choice == 0 ) // 종료문
	{
		return ;
	}
	} while ( choice > 2 || choice < 0 );
}

void NUMBER(void)
{
	// 입력받은 정수가 홀수인지 짝수인지 판별하는 프로그램
	int number;
	printf("정수를 입력하세요 : ");
	scanf("%d", &number);
	if ( number % 2 && 1 ) // 입력받은 정수를 식별
	{
		printf("홀수입니다.\n");
	}
	else
	{
		printf("짝수입니다.\n");
	}
}

void diamond(void)
{
    int i, j, k;
    int inp;
    int alpha;  //피라미드에서 역피라미드로 전환
    printf("정수를 입력하세요 : ");
    scanf("%d", &inp);
    inp = inp/2; // 입력받은 값을 2로 나눔(피라미드와 역피라미드를 나누어서 찍기 위해)
   
    /*  절반으로 나눈 값을 이용, i는 -inp ~ inp까지 범위를 가짐
        (5를 입력한 경우, 5의 절반인 2가 inp이 되어 i는 -2 ~ 2의 범위를 가짐) */

    for(i= -inp; i <= inp; i++){
        //i의 상태에 따른 알파값 설정
        //i의 값이 음수 또는 0인 경우 피라미드
        //i의 값이 양수인 경우 역피라미드
        alpha = (i>0)? i : -i;
        //알파값을 이용하여 공백을 찍는다
        for(j=0; j<alpha; j++){
            printf(" ");
        }
       
        //알파값을 이용하여 별을 찍고 무조건 홀수 단위로 별이 찍히게 끔 +1을 해준다
        for(k=0; k<(inp-alpha)*2+1; k++){
            printf("*");
        }
        printf("\n");
    }
    return 0;
}
//이번 아홉번째 코딩 과제를 하면서 '좀 어려운 과제였다'라고 느꼈던 과제중 하나였던 것 같습니다.
/*c:\users\공무\documents\visual studio 2010\projects\hw9김공무201511788-1\hw9김공무201511788-1\hw9김공무201511788.cpp(15): warning C4996: 'scanf': This function or variable may be unsafe. Consider using scanf_s instead. To disable deprecation, use _CRT_SECURE_NO_WARNINGS. See online help for details.
1>          c:\program files (x86)\microsoft visual studio 10.0\vc\include\stdio.h(304) : 'scanf' 선언을 참조하십시오.
1>c:\users\공무\documents\visual studio 2010\projects\hw9김공무201511788-1\hw9김공무201511788-1\hw9김공무201511788.cpp(30): error C2561: 'main' : 함수는 값을 반환해야 합니다.
1>          c:\users\공무\documents\visual studio 2010\projects\hw9김공무201511788-1\hw9김공무201511788-1\hw9김공무201511788.cpp(7) : 'main' 선언을 참조하십시오.
1>c:\users\공무\documents\visual studio 2010\projects\hw9김공무201511788-1\hw9김공무201511788-1\hw9김공무201511788.cpp(89): error C2562: 'diamond' : 'void' 함수에서 값을 반환하고 있습니다.
1>          c:\users\공무\documents\visual studio 2010\projects\hw9김공무201511788-1\hw9김공무201511788-1\hw9김공무201511788.cpp(5) : 'diamond' 선언을 참조하십시오.*/
//위에 에러가 뜨면서 main 선언과 diamond 선언에서 발생한 에러를 잘 모르겠습니다.