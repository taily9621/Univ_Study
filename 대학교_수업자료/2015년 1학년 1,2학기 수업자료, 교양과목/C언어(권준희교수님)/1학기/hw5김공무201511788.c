//201511788 1학년 김공무 5번째 코딩 숙제
#include<stdio.h>
int main(void)
{
	int age;
	int bus;
	int money;//나이, 버스종류, 금액 상수 설정
	age = 0;
	bus = 0;
	money = 0;//초기화
	printf("**** 버스 요금 정보 시스템에 오신 것을 환영합니다 ****\n");
	printf("승객의 나이를 입력하세요:");
	scanf("%d",&age);//나이 입력
	if(0<=age&&age<=6)
		printf("요금 지불을 하지 않습니다. 프로그램을 종료됩니다.");
	else
		printf("버스 종류에 해당하는 번호를 선택하세요. 1.마을버스 2.일반버스 3.광역버스\n");
	    scanf("%d",&bus);//6살 이하는 요금을 지불하지 않도록 분류
	if(bus==1)
		if(age>=7&&age<=12){
			printf("교통카드 잔액을 입력하세요.");
		    scanf("%d",&money);
			if(money>=350)
				printf("\a삑~ 남은 잔액은 %d원 입니다.\n",money-350);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
		else if(age>=13&&age<=18){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=560)
				printf("\a삑~ 남은 잔액은 %d원 입니다.\n",money-560);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
		else if(age>=19){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=850)
				printf("\a삑~ 남은 잔액을 %d원 입니다.\n",money-850);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
	if(bus==2)
		if(age>=7&&age<=12){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=450)
				printf("\a삑~ 남은 잔액은 %d원 입니다.\n",money-450);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
		else if(age>=13&&age<=18){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=720)
				printf("\a삑~ 남은 잔액은 %d원 입니다.\n",money-720);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
		else if(age>=19){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=1050)
				printf("\a삑~ 남은 잔액을 %d원 입니다.\n",money-1050);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
	if(bus==3)
		if(age>=7&&age<=12){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=1200)
				printf("\a삑~ 남은 잔액은 %d원 입니다.\n",money-1200);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
		else if(age>=13&&age<=18){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=1360)
				printf("\a삑~ 남은 잔액은 %d원 입니다\n",money-1360);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}
		else if(age>=19){
			printf("교통카드 잔액을 입력하세요.\n");
		    scanf("%d",&money);
			if(money>=1850)
				printf("\a삑~ 남은 잔액을 %d원 입니다.\n",money-1850);
			else
				printf("\a\a삐빅~잔액이 부족합니다.!!\n");}//6살 이상인 사람들은 버스 종류에 따라 지불 요금을 분류
	return 0;
}
//이번 다섯번째 코딩 숙제는 버스 요금 정보 시스템을 짜보게 됬는데 지난번 과제였던 4번 과제보다 조금 더 수월하게 느껴져서 어렵지 않게 짜보았습니다.
//다음 과제에서도 지금 느낌 그대로 어렵지 않게 코딩을 하겠습니다!!!