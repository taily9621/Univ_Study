/* 이름 : 고 영빈
   학년 : 1
   학번 : 201511781
   완성여부 : 완성*/

#include<stdio.h>
#define MA1 350		// MA1은 어린이의 마을버스 가격을 나타낸다.
#define MA2 560		// MA2는 청소년의 마을버스 가격을 나타낸다.
#define MA3 850		// MA3는 성인의 마을버스 가격을 나타낸다.
#define ILL1 450	// ILL1은 어린이의 일반버스 가격을 나타낸다.
#define ILL2 720	// ILL2는 청소년의 일반버스 가격을 나타낸다.
#define ILL3 1050	// ILL3는 성인의 일반버스 가격을 나타낸다.
#define Kwang1 1200	// Kwang1은 어린이의 광역버스 가격을 나타낸다.
#define Kwang2 1360	// Kwang2는 청소년의 광역버스 가격을 나타낸다.
#define Kwang3 1850	// Kwang3는 성인의 광역버스 가격을 나타낸다.

void MA(int, int);
void ILL(int, int);
void Kwang(int, int);

void main()
{
	
	int age;	// age 변수는 탑승자의 나이를 나타낸다.
	int number;	// number 변수는 탑승하려는 버스의 종류를 구분하는 변수이다.
	int money;	// money 변수는 자신이 가지고 있는 잔액을 나타낸다.
	
	printf("**** 버스 요금 정보 시스템에 오신 것을 환영합니다****\n\n");
	
	// 승객의 나이를 입력하여 나이 변수를 조정한다.
	printf("1> 승객의 나이를 입력하시오 : ");
	scanf("%d", &age);

	// 나이가 6세 이하이면 어린아이이므로 비용을 지불하지 않는다.
	if ( age <= 6)
	{
		printf("요금 지불을 하지 않습니다. 프로그램을 종료합니다.\n");
		return ;
	}

	// 버스의 종류에 해당하는 번호를 입력하여 스위치 변수를 조정한다.
	printf("2> 버스 종류에 해당하는 번호를 선택하세오. 1.마을버스 2.일반버스 3.광역버스 ");
	scanf("%d", &number);

	// 버스의 종류에 해당하는 번호를 입력하지 아니할때 오류가 발생함을 나타낸다.
	if ( number >= 4 ) 
	{
		printf("\n해당하는 버스 종류 번호를 올바르게 입력하시오.\n");
		return ;
	}

	// 교통카드의 여분 잔액을 입력하여 나의 현재 잔액을 조정한다.
	printf("3> 교통카드 잔액을 입력하시오 : ");
	scanf("%d", &money);
	
	switch(number)
	{
		// 마을버스를 탔을 때 각 나이에 따른 버스 요금 지불 값과 지불 후 남은 값을 나타낸다.
	case 1 :
		{
			MA(money, age);
			break;
		}
	case 2 :
		{
			ILL(money, age);
			break;
		}
	case 3 :
		{
			Kwang(money, age);
			break;
		}
	}
	
		
	return 0 ;
}
	void MA(int money, int age)
	{
		if ( age >= 7 && age <= 12 )
			{
				money -= MA1;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			else if ( age >= 13 && age <=18 )
			{
				money -= MA2;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			else if ( age >= 19 )
			{
				money -= MA3;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			// 가지고 있던 잔액이 지불해야하는 잔액보다 적을 때 나타나는 문구이다.
			if ( money < 0)
					printf("\n삐삑~! 남은 잔액이 부족합니다.\n");
	}

	void ILL(int money, int age)
	{
		// 일반버스를 탔을 때 각 나이에 따른 버스 요금 지불 값과 지불 후 남은 값을 나타낸다.
			if ( age >= 7 && age <= 12 )
			{
				money -= ILL1;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			else if ( age >= 13 && age <=18 )
			{
				money -= ILL2;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			else if ( age >= 19 )
			{
				money -= ILL3;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			// 가지고 있던 잔액이 지불해야하는 잔액보다 적을 때 나타나는 문구이다.
			if ( money < 0)
					printf("\n삐삑~! 남은 잔액이 부족합니다.\n");
	}

	void Kwang(int money, int age)
	{
		// 광역버스를 탔을 때 각 나이에 따른 버스 요금 지불 값과 지불 후 남은 값을 나타낸다.
			if ( age >= 7 && age <= 12 )
			{
				money -= Kwang1;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			else if ( age >= 13 && age <=18 )
			{
				money -= Kwang2;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			else if ( age >= 19 )
			{
				money -= Kwang3;
				
				if ( money >= 0 )
					printf("\n삑~! 남은 잔액은 %d원입니다.\n", money);
			}
			// 가지고 있던 잔액이 지불해야하는 잔액보다 적을 때 나타나는 문구이다.
			if ( money < 0)
					printf("\n삐삑~! 남은 잔액이 부족합니다.\n");
	}
