
#include <iostream>
#include <gl/glut.h>


float g_fAngle = 0;

void SetupRC();
void RenderScene();
void ReshapeScene(int width, int height);
void OnTimer(int id);

int main()
{
	// 초기화 함수들
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);	// 디스플레이 모드 설정
	glutInitWindowPosition(100, 100);				// 윈도우의 위치 지정
	glutInitWindowSize(500, 500);					// 윈도우의 크기 지정
	glutCreateWindow("D.3.2 HW");					// 윈도우 생성 (윈도우 이름)
	glutDisplayFunc(RenderScene);					// 출력 함수의 지정
	glutReshapeFunc(ReshapeScene);					// 윈도우크기변경함수의 지정

	SetupRC();										

	glutMainLoop();

	return 0;
}

void OnTimer(int id)
{
	g_fAngle += 1.0f;
	glutPostRedisplay();
}

void ReshapeScene(int width, int height)
{
	float fRange = 10.0f;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (width <= height) {
		glOrtho( -fRange, fRange, -fRange * height/ width, fRange * height / width, -fRange, fRange );
	} else {
		glOrtho( -fRange * width / height, fRange * width / height, - fRange, fRange, -fRange, fRange );
	}

}

void SetupRC()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// 바탕색을 검은색으로 한다.
	glShadeModel(GL_FLAT);					
	glFrontFace(GL_CCW);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glutTimerFunc(50, OnTimer, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef( 0.0f, 0.0f, -1.0f );

	glPushMatrix();
		glColor3ub(255, 0, 0);
		glutSolidSphere(2.0f, 24, 24);		// 태양 그리기
	glPopMatrix();

	glPushMatrix();
		glColor3ub(0, 0, 200);
		glRotatef( g_fAngle, 0.0f, 1.0f, 0.0f);
		glTranslatef( 6.0f, 0.0f, 0.0f );
		glutSolidSphere(0.5f, 24, 24);		// 지구 그리기

		glPushMatrix();
			glColor3ub(150, 150, 0);
			glRotatef( g_fAngle * 12.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef( 2.0f, 0.0f, 0.0f );
			glutSolidSphere(0.2f, 24, 24);		// 달 그리기
		glPopMatrix();
	glPopMatrix();
	
	glFlush();								// 화면에 출력하기
}