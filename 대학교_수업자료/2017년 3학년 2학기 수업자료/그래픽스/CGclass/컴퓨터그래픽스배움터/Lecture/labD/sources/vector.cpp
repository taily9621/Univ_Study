void CalcNormal(float v[3][3], float out[3]) //법선벡터를 계산하는 함수
{
	float v1[3], v2[3];
	static const int x = 0;
	static const int y = 1;
	static const int z = 2;

	// Calculate two vectors from the three points
	v1[x] = v[0][x] - v[1][x];
	v1[y] = v[0][y] - v[1][y];
	v1[z] = v[0][z] - v[1][z];

	v2[x] = v[1][x] - v[2][x];
	v2[y] = v[1][y] - v[2][y];
	v2[z] = v[1][z] - v[2][z];

	// Take the cross product of the two vectors to get
	// the normal vector which will be stored in out
	out[x] = v1[y] * v2[z] - v1[z] * v2[y];
	out[y] = v1[z] * v2[x] - v1[x] * v2[z];
	out[z] = v1[x] * v2[y] - v1[y] * v2[x];

	// Normalize the vector (shorten length to one)
	ReduceToUnit(out);
}

void ReduceToUnit(float vector[])//단위법선벡터를 계산하는 함수
{
	float fLength;
	
	// Calculate the length of the vector		
	fLength = (float)sqrt((vector[0] * vector[0]) +
			  			  (vector[1] * vector[1]) +
						  (vector[2] * vector[2]));

	// Keep the program from blowing up by providing an exceptable
	// value for vectors that may calculated too close to zero.
	if(fLength == 0.0f)
		fLength = 1.0f;

	// Dividing each element by the length will result in a
	// unit normal vector.
	vector[0] /= fLength;
	vector[1] /= fLength;
	vector[2] /= fLength;
}