var dragobject = null;
var resizeobject = null;
var resizeimage = null;
var resizenote = null;
var tx;
var ty;

function getReal(el) {
	temp = el;

	while ((temp != null) && (temp.tagName != "BODY")) {
		if ((temp.className == "moveme") || (temp.className == "resize") || (temp.className == "handle")){
			el = temp;
			return el;
		}
		temp = temp.parentElement;
	}
	return el;
}

function handle_onmousedown() {
	el = getReal(window.event.srcElement)
	if (el.className == "moveme") {
		dragobject = el;
		ty = (window.event.clientY - dragobject.style.pixelTop);
		tx = (window.event.clientX - dragobject.style.pixelLeft);
		window.event.returnValue = false;
		window.event.cancelBubble = true;
	}
	else if ((el.className == "resize")) {
		tmp = el.getAttribute("for");
		if (tmp != null) {
			el = eval(tmp);
			resizeobject = el;
			tmp2 = eval(tmp + "bar");
			resizeimage = tmp2;
			tmp2 = eval(tmp + "content");
			resizenote = tmp2;

//			ty = (window.event.clientY - resizeobject.style.pixelTop);
//			tx = (window.event.clientX - resizeobject.style.pixelLeft);
			ty = window.event.clientY - resizeobject.style.pixelHeight;
			tx = window.event.clientX - resizeobject.style.pixelWidth;
			window.event.returnValue = false;
			window.event.cancelBubble = true;
		}
	}
	else if ((el.className == "handle")) {
		tmp = el.getAttribute("for");
		if (tmp != null) {
			el = eval(tmp);
			dragobject = el;
			ty = (window.event.clientY - dragobject.style.pixelTop);
			tx = (window.event.clientX - dragobject.style.pixelLeft);
			window.event.returnValue = false;
			window.event.cancelBubble = true;
		}
		else {
			dragobject = null;
			resizeobject = null;
			resizeimage = null;
			resizenote = null;
		}
	}
	else {
		dragobject = null;
		resizeobject = null;
		resizeimage = null;
		resizenote = null;
	}
}

function handle_onmouseup() {
	if(dragobject) {
		dragobject = null;
	}
	else if(resizeobject) {
		resizeobject = null;
		resizeimage = null;
		resizenote = null;
	}
}

function handle_onmousemove() {
	if (resizeobject) {
		if (window.event.clientX >= 0) {
			if (resizeobject.style.pixelWidth >= 50) {
				resizeobject.style.width = window.event.clientX - tx;
				resizeimage.style.width = resizeobject.style.pixelWidth - 59;
			}
			if (resizeobject.style.pixelHeight >= 30) {
				resizeobject.style.height = window.event.clientY - ty;
			}
		}
		window.event.returnValue = false;
		window.event.cancelBubble = true;
	}	
	else if (dragobject) {
		if (window.event.clientX >= 0) {
			dragobject.style.left = window.event.clientX - tx;
			dragobject.style.top = window.event.clientY - ty;
		}
		window.event.returnValue = false;
		window.event.cancelBubble = true;
	}
}

if (document.all) { //This only works in IE4 or better
	document.onmousedown = handle_onmousedown;
	document.onmouseup = handle_onmouseup;
	document.onmousemove = handle_onmousemove;
}

document.write("<style>");
document.write(".moveme		{cursor: move;}");
document.write(".handle		{cursor: move;}");
document.write(".resize		{cursor: move;}");
document.write("</style>");