function showLayer(id)
{
	document.all(id).style.visibility="visible"
}

function hideLayer(id)
{
 	document.all(id).style.visibility="hidden"
}

function toggleLayer(id)
{
	 var str;
	
	 if (document.all(id).style.visibility == "hidden")
	 {
		document.all(id).style.visibility = "visible";
		str = "visible";
	}
 	else
 	{
		document.all(id).style.visibility = "hidden";
		str = "hidden";
	}

	var i = 1;
	while (toggleLayer.arguments[i])
	{
		document.all(toggleLayer.arguments[i]).style.visibility = str;
		i++;
	}
	
}

function moveLayer(id, x, y)
{
	document.all(id).style.left = x;
	document.all(id).style.top = y;
}

function donext(num)
{
	tmp = num + 1;
	hideLayer('layer' + num + '-0');
	hideLayer('content' + num + 'content');
	hideLayer('content' + num + 'footer');
	hideLayer('content' + num);
	showLayer('layer' + tmp + '-0');
	showLayer('content' + tmp);
	showLayer('content' + tmp + 'content');
	showLayer('content' + tmp + 'footer');
}

function doprev(num)
{
	tmp = num - 1;
	hideLayer('layer' + num + '-0');
	hideLayer('content' + num + 'content');
	hideLayer('content' + num + 'footer');
	hideLayer('content' + num);
	showLayer('layer' + tmp + '-0');
	showLayer('content' + tmp);
	showLayer('content' + tmp + 'content');
	showLayer('content' + tmp + 'footer');
}

function donext2(num)
{
	tmp = num + 1;
	hideLayer('content' + num + 'content');
	hideLayer('content' + num + 'footer');
	hideLayer('content' + num);
	showLayer('content' + tmp);
	showLayer('content' + tmp + 'content');
	showLayer('content' + tmp + 'footer');
}

function doprev2(num)
{
	tmp = num - 1;
	hideLayer('content' + num + 'content');
	hideLayer('content' + num + 'footer');
	hideLayer('content' + num);
	showLayer('content' + tmp);
	showLayer('content' + tmp + 'content');
	showLayer('content' + tmp + 'footer');
}

function namosw_exchange_src() 
{ 
  str = namosw_exchange_src.arguments[0];
  str = (navigator.appName == 'Netscape') ? 'document.' + str : 'document.all.' + str;
  img = eval(str);
  if (img) {
    if (img.ori_src == null) {
      img.ori_src = img.src;
      img.src     = namosw_exchange_src.arguments[1];
    } else {
      var temp    = img.src;
      img.src     = img.ori_src;
      img.ori_src = temp;
    }
  } 
}

function namosw_preload_img()
{ 
  var img_list = namosw_preload_img.arguments;
  if (document.preloadlist == null) 
    document.preloadlist = new Array();
  var top = document.preloadlist.length;
  for (var i=0; i < img_list.length; i++) {
    document.preloadlist[top+i]     = new Image;
    document.preloadlist[top+i].src = img_list[i];
  } 
}
