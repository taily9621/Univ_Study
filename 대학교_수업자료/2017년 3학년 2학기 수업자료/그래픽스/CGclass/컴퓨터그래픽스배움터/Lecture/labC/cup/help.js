var HelpX, HelpY, HelpText;
var ToShow, ToClear;

function DoFlyOver()
{
	if( ToClear != -1 ) window.clearTimeout( ToClear );
	FOArea.innerText = HelpText;
	FOArea.style.posLeft = HelpX + 8;
	FOArea.style.posTop = HelpY + 8
	FOArea.style.display = "";
	ToClear = setTimeout( "ClearFlyOver()", 10000, "JAVASCRIPT" );
}

function ClearFlyOver()
{
	FOArea.style.display = "none";
	if (ToShow) clearTimeout(ToShow);
}

function FlyOver(Text )
{
	HelpText = Text;
	HelpX = window.event.clientX;
	HelpY = window.event.clientY;
	ToShow = setTimeout( "DoFlyOver()", 1000, "JAVASCRIPT" );
}

document.write("<DIV ID='FOArea' CLASS='Help' STYLE='display: none; z-index: 99'> </DIV>");