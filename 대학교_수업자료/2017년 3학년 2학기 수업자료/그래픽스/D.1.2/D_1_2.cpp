
#include <iostream>
#include <gl/glut.h>

void SetupRC();
void RenderScene();
void ReshapeScene(int width, int height);

int main()
{
	// 초기화 함수들
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);	// 디스플레이 모드 설정
	glutInitWindowPosition(100, 100);				// 윈도우의 위치 지정
	glutInitWindowSize(500, 500);					// 윈도우의 크기 지정
	glutCreateWindow("Example1");					// 윈도우 생성 (윈도우 이름)
	glutDisplayFunc(RenderScene);					// 출력 함수의 지정
	glutReshapeFunc(ReshapeScene);					// 윈도우크기변경함수의 지정

	SetupRC();										

	glutMainLoop();
	
	return 0;
}

void SetupRC()
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);	// 바탕색검을 파란색으로
}

void ReshapeScene(int width, int height)
{
	float fRange = 100.0f;					// 화면의 크기를 결정하는 변수

	glViewport(0, 0, width, height);		// OpenGL 뷰포트를 윈도우 전체크기에 맞추어 변경한다.
	glMatrixMode(GL_PROJECTION);			// 프로젝션 매트릭스 초기화
	glLoadIdentity();						

	// 직각투영으로 프로젝션 매트릭스를 초기화
	if (width <= height) {
		glOrtho( -fRange, fRange, -fRange * height/ width, fRange * height / width, -fRange, fRange );
	} else {
		glOrtho( -fRange * width / height, fRange * width / height, - fRange, fRange, -fRange, fRange );
	}

}

void RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT);			// 설정한 색으로 전체를 칠하기

	glMatrixMode(GL_MODELVIEW);				// 모델 매트릭스모드로 전환
	glLoadIdentity();

	glFlush();								// 화면에 출력하기
}