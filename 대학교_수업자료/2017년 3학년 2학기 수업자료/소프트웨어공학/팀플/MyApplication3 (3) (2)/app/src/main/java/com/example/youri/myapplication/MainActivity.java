package com.example.youri.myapplication;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TestDataBase";
    public DbOpenHelper mDbOpenHelper;
    public Cursor mCursor;
    public InfoClass mInfoClass;
    public ArrayList<InfoClass> mInfoArr;
    private CustomAdapter mAdapter;

    //UserDatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        //데이터베이스 생성(파라메터 Context) 및 오픈
        mDbOpenHelper = new DbOpenHelper(this);
        try {
            mDbOpenHelper.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mDbOpenHelper.insertColumn("gmkim1996", "1234");
        mDbOpenHelper.insertColumn("danny1224", "1324");

        //ArrayList 초기화
        mInfoArr = new ArrayList<InfoClass>();

        doWhileCursorToArray();

        /*
        try
        {
            if(database == null) {
                //databaseHelper = new UserDatabaseHelper( getApplicationContext(), databaseName, null, 1 );
                //database = databaseHelper.getWritableDatabase();
                Toast.makeText( this, databaseName + "생성", Toast.LENGTH_SHORT ).show();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }


        try
        {
            if(database != null)
            {
                database.execSQL(
                        "CREATE TABLE"  + tableName +"("+
                        "name TEXT PRIMARY KEY," +
                        "password TEXT" +
                        ")"
                );
                Toast.makeText(this, "table:"+tableName+"생성", Toast.LENGTH_SHORT).show();
            }
        } catch(Exception e)
        {
            e.printStackTrace();
        }
        */
    }

    public void MakeAccount(View v)
    {
        Intent intent = new Intent( this, CreateAccount.class );
        startActivity( intent );
    }

    public void LoginAccount(View v)
    {
        try
        {
            if(mInfoArr != null)
            {
                TextView resultAccount = findViewById( R.id.result_login);
                TextView IDResult = findViewById( R.id.ID );
                TextView passwordResult = findViewById( R.id.Password );
                String loginID = IDResult.getText().toString();
                String loginPW = passwordResult.getText().toString();
                if(!loginID.equals( "" ) || !loginPW.equals( "" )) {
                    boolean isCorrect = false;

                    for (InfoClass min : mInfoArr) {
                        if (loginID.equals( min.getId() ) && loginPW.equals( min.getPassword() )) {
                            isCorrect = true;
                            Intent transMain = new Intent( this, Main.class );
                            startActivity( transMain );
                            break;
                        }
                    }

                    if (isCorrect == false)
                        resultAccount.setText( "잘못된 ID 혹은 PW를\n입력하셨습니다." );
                } else
                {
                    resultAccount.setText( "ID 혹은 PW를 입력하세요." );
                }
                IDResult.setText( null );
                passwordResult.setText( null );
            }
        } catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //doWhile문을 이용하여 Cursor에 내용을 다 InfoClass에 입력 후 InfoClass를 ArrayList에 Add
    private void doWhileCursorToArray() {

        mCursor = null;
        //DB에 있는 모든 컬럼을 가져옴
        mCursor = mDbOpenHelper.getAllColumns();
        //컬럼의 갯수 확인
        Log.i(TAG, "Count = " + mCursor.getCount());

        while (mCursor.moveToNext()) {
            //InfoClass에 입력된 값을 압력
            mInfoClass = new InfoClass(
                    mCursor.getInt(mCursor.getColumnIndex("_id")),
                    mCursor.getString(mCursor.getColumnIndex("id")),
                    mCursor.getString(mCursor.getColumnIndex("password"))
            );
            //입력된 값을 가지고 있는 InfoClass를 InfoArray에 add
            mInfoArr.add(mInfoClass);
        }
        //Cursor 닫기
        mCursor.close();
    }

    //액티비티가 종료 될 때 디비를 닫아준다
    @Override
    protected void onDestroy() {
        mDbOpenHelper.close();
        super.onDestroy();
    }
}

/*class UserDatabaseHelper extends SQLiteOpenHelper
{

    public UserDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super( context, name, factory, version );

    }

    public void onOpen(SQLiteDatabase database)
    {
        Toast.makeText(getApplicationContext(),"Helper의 onOpen()호출됨",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Toast.makeText(getApplicationContext(),"Helper의 onCreate()호출됨",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Toast.makeText(getApplicationContext(),"Helper의 onUpgrade()호출됨 :"+oldVersion+" => "+newVersion,Toast.LENGTH_LONG).show();
    }
}*/
