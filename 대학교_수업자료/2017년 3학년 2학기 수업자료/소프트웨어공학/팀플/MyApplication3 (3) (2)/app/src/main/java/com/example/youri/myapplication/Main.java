package com.example.youri.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> arraylist = new ArrayList<String>();
        arraylist.add("글픽        TextMapping  13시 ");
        arraylist.add("인공        팀 프로젝트    15시");
        arraylist.add("소공        팀 프로젝트    24시");

        ArrayAdapter<String> Adapter;
        Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arraylist);

        ListView list = (ListView)findViewById(R.id.list);
        list.setAdapter(Adapter);

    }
    public void openAdd(View view){
        Intent intent = new Intent(this, Add.class);
        startActivity(intent);
    }
    public void openDate(View view){
        Intent intent = new Intent(this, Date.class);
        startActivity(intent);
    }
    public void openSettings(View view){
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

}
