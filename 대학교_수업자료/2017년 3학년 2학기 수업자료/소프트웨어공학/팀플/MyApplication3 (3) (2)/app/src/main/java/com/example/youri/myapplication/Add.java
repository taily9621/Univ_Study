package com.example.youri.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Add extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
    }
    public void openAdd_Schedule(View view){
        Intent intent = new Intent(this, Add_Schedule.class);
        startActivity(intent);
    }
    public void openAdd_Subject(View view){
        Intent intent = new Intent(this, Add_Subject.class);
        startActivity(intent);
    }
    public void BackMain(View view){
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
    }

}