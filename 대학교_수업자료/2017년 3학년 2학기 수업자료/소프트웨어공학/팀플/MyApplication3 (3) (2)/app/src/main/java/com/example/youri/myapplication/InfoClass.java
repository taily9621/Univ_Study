package com.example.youri.myapplication;

/**
 * Created by ChoiMinSu on 2016. 3. 29..
 */
public class InfoClass {

    public int _id;
    public String id;
    public String password;

    //생성자
    public InfoClass(){}

    /**
     * 실질적으로 값을 입력할 때 사용되는 생성자(getter and setter)
     * @param _id       테이블 아이디
     * @param id      아이디
     * @param password   비밀번호
     */
    public InfoClass(int _id, String id, String password) {
        this._id = _id;
        this.id = id;
        this.password = password;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id; }

    public String getPassword() {return password; }

    public void setPassword(String password) {
        this.password = password;
    }
}
