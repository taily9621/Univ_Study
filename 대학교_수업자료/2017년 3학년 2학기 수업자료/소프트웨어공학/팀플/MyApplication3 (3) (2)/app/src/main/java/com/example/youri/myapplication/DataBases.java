package com.example.youri.myapplication;

import android.provider.BaseColumns;

/**
 * 데이터베이스 테이 생성
 * Created by ChoiMinSu on 2016. 3. 29..
 */
public class DataBases {

    //데이터베이스 호출 시 사용될 생성자
    public static final class CreateDB implements BaseColumns {
        public static final String NAME = "person";
        public static final String ID = "id";
        public static final String PASSWD = "password";
        public static final String _TABLENAME = "useraccount";
        public static final String _CREATE =
                "create table " + _TABLENAME + "("
                + _ID + " integer primary key autoincrement, "
                + ID + " text not null , "
                + PASSWD + " text not null );";

    }
}
