package com.example.youri.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> arraylist = new ArrayList<String>();
        arraylist.add("이주환");
        arraylist.add("박성수");
        arraylist.add("조영광");
        arraylist.add("이승호");
        arraylist.add("고광연");
        arraylist.add("김공무");

        ArrayAdapter<String> Adapter;
        Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arraylist);

        ListView list = (ListView)findViewById(R.id.list);
        list.setAdapter(Adapter);

    }
    public void openAdd(View view){
        Intent intent = new Intent(this, Add.class);
        startActivity(intent);
    }
    public void openDate(View view){
        Intent intent = new Intent(this, Date.class);
        startActivity(intent);
    }
    public void openSettings(View view){
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

}
