package com.todolist.gongmukim.login_createaccount_linkdb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by GongmuKim on 2017-12-07.
 */

public class CreateAccount extends MainActivity{
    EditText addID;
    EditText addPasswd;
    String ID;
    String Passwd;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.create_account );

        addID = (EditText)findViewById( R.id.create_ID );
        addPasswd = (EditText)findViewById( R.id.create_passwd );
        ID = addID.getText().toString();
        Passwd = addPasswd.getText().toString();
    }

    public void check_overlap_ID(View v)
    {
        System.out.println("중복확인");
        boolean isOverlap = false;
        try
        {
            if(mInfoArr != null)
            {
                TextView overlapResult = findViewById( R.id.result_view );
                TextView loginCheck = findViewById( R.id.create_ID );
                String loginCK = loginCheck.getText().toString();

                for(InfoClass min : mInfoArr)
                {
                    System.out.println("DB계정 체크");
                    if(loginCK.equals( min.getId()))
                    {
                        System.out.println("중복발생!");
                        isOverlap = true;
                        break;
                    }
                }
                if(isOverlap == false)
                    overlapResult.setText( "이 ID는 사용가능 합니다." );
                else
                    overlapResult.setText( "중복된 ID 입니다." );
            }
        } catch(Exception e)
        {
            System.out.println("ID체크 에러발생!");
            e.printStackTrace();
        }
    }
    public void makeAccount(View v)
    {
        System.out.println("계정 만들기");
        TextView overlapResult = findViewById( R.id.result_view );
        try
        {
            TextView loginCheck = findViewById( R.id.create_ID );
            TextView passwordCheck = findViewById( R.id.create_passwd );
            String checkID = loginCheck.getText().toString();
            String checkPW = passwordCheck.getText().toString();
            if(!checkID.equals( "" ) || !checkPW.equals( "" ))
            {
                mDbOpenHelper.insertColumn( checkID, checkPW );
                System.out.println( "계정 DB에 추가" );
                Intent intent = new Intent( this, MainActivity.class );
                startActivity( intent );
            } else
            {
                overlapResult.setText( "ID 혹은 PW를 입력하세요." );
            }
        } catch(Exception e)
        {
            System.out.println("계정추가 에러!");
            e.printStackTrace();
        }
    }
    public void reject(View v)
    {
        Intent intent = new Intent( this, MainActivity.class );
        startActivity( intent );
    }
}
