package com.example.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Jambo on 14. 2. 5.
 */


public class second extends Activity implements AdapterView.OnItemLongClickListener{

    ArrayList<String> arrlist = null;
    ArrayList<String> arr_id_list = null;
    SQLiteDatabase database;
    String dbName = "test_db_name";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.titlelistview);
        database = openOrCreateDatabase(dbName, MODE_WORLD_WRITEABLE, null);

        arrlist = new ArrayList<String>();
        arr_id_list = new ArrayList<String>();

        selectData();

        ArrayAdapter<String> Adapter;
        Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrlist);
        ListView list = (ListView)findViewById(R.id.l_view);

        list.setAdapter(Adapter);

        list.setOnItemLongClickListener(this);


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.close();
    }

    public void selectData(){
        String sql = "select * from test_table";
        Cursor result = database.rawQuery(sql, null);
        result.moveToFirst();
        while(!result.isAfterLast()){
            arr_id_list.add(result.getString(0));
            arrlist.add(result.getString(1));
            result.moveToNext();
        }
        result.close();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        final Integer selectedPos = i;
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(view.getContext());
        alertDlg.setTitle(R.string.alert_title_question);

        alertDlg.setPositiveButton( R.string.button_yes, new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String position = arr_id_list.get(selectedPos);
                final String sql = "delete from test_table where id = "+ position;
                dialog.dismiss();

                database.execSQL(sql);
            }
        });

        alertDlg.setNegativeButton( R.string.button_no, new DialogInterface.OnClickListener(){

            @Override
            public void onClick( DialogInterface dialog, int which ) {
                String position = arr_id_list.get(selectedPos);
                dialog.dismiss();

                Intent intent = new Intent(second.this, updatedb.class);
                intent.putExtra("p_id", position);

                startActivity(intent);
            }
        });

        alertDlg.setMessage(R.string.alert_msg_delete);
        alertDlg.show();
        return false;

    }
}