package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by ium on 14. 2. 5.
 */
public class updatedb extends Activity implements View.OnClickListener {
    SQLiteDatabase database;
    String title;
    EditText u_text;
    String id;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update);
        database = openOrCreateDatabase("test_db_name", MODE_WORLD_WRITEABLE, null);

        Bundle bundle = getIntent().getExtras();
        id =  bundle.getString("p_id");
        getTitle(id);
        u_text = (EditText) findViewById(R.id.u_text);
        u_text.setText(title);
        Button u_save = (Button) findViewById(R.id.u_save);
        u_save.setOnClickListener((View.OnClickListener) this);

    }

    public void getTitle(String g_id){

        String sql = "select title from test_table where id = " + g_id;
        Cursor result = database.rawQuery(sql, null);
        result.moveToFirst();
        title = result.getString(0);
    }

    @Override
    public void onClick(View v){
        String u_title = u_text.getText().toString();
        String sql = "update test_table set title = '"+u_title+"' where id = "+id;

        database.execSQL(sql);

        Intent intent = new Intent(updatedb.this, second.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.close();
    }
}
