package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class MainActivity extends Activity implements View.OnClickListener{

    SQLiteDatabase database;
    String dbName = "test_db_name";
    String createTable = "create table test_table (id integer primary key , title text);";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createDatabase();
        createTable();

        Button b_save = (Button) findViewById(R.id.b_save);
        Button b_listview = (Button) findViewById(R.id.b_listview);

        b_save.setOnClickListener((View.OnClickListener) this);
        b_listview.setOnClickListener((View.OnClickListener) this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                // Handle action bar item clicks here. The action bar will
                // automatically handle clicks on the Home/Up button, so long
                // as you specify a parent activity in AndroidManifest.xml.
                int id = item.getItemId();
                if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v){
        TextView test = (TextView) findViewById(R.id.t_title);

        switch(v.getId()){
            case R.id.b_save:
                EditText id = (EditText)findViewById(R.id.e_text);
                String t_text = id.getText().toString();
                insertData(t_text);
                test.setText("b_save");
                break;
            case R.id.b_listview:
                Intent intent = new Intent(MainActivity.this, second.class);
                startActivity(intent);
                test.setText("b_listview");
                break;
        }
    }

    public void createDatabase(){
        database = openOrCreateDatabase(dbName, MODE_WORLD_WRITEABLE, null);
    }

    public void createTable(){
        try{
            database.execSQL(createTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertData(String title){

        database.beginTransaction();

        try{
            String sql = "insert into test_table (title) values ('"+ title +"');";
            database.execSQL(sql);
            database.setTransactionSuccessful();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            database.endTransaction();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.close();
    }
}
