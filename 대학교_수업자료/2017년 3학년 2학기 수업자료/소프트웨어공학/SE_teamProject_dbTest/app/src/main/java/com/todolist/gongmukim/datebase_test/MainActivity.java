package com.todolist.gongmukim.datebase_test;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.w3c.dom.Text;

public class MainActivity extends Activity {
    dbHelper helper;
    SQLiteDatabase db;
    EditText edit_name, edit_tel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        helper = new dbHelper(this);
        try {
            db = helper.getWritableDatabase();
            //데이터베이스 객체를 얻기 위하여 getWritableDatabse()를 호출

        } catch (SQLiteException e) {
            db = helper.getReadableDatabase();
        }

        edit_name = (EditText) findViewById( R.id.name );
        edit_tel = (EditText) findViewById( R.id.tel );


        //버튼에 대한 클릭 처리기를 작성.

        //추가 버튼.
        findViewById( R.id.add ).setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = edit_name.getText().toString();
                String tel = edit_tel.getText().toString();

                db.execSQL( "INSERT INTO contact VALUES(null, '" + name + "','" + tel + "');" );

            }
        } );

        //검색 버튼
        findViewById( R.id.query ).setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = edit_name.getText().toString();
                Cursor cursor;
                cursor = db.rawQuery( "SELECT name, tel FROM contact where name='" + name + "';", null );

                while (cursor.moveToNext()) {
                    String tel = cursor.getString( 1 );
                    edit_tel.setText( tel );
                }
            }
        } );

    }

    //현재 상태가 어떻게 되는지 textview에 표현해주는 기능

    private void println(String s) {

        textView.append("\n" + s);

    }


    //데이터베이스를 만들어주는 기능

    private void createDatabase(String name) {

        //현재 상태가 어떻게 되는지 textview에 뿌려줌

        println("creating database [" + name + "].");

        //db를 name값으로 생성해줌 (name값은 위에 databaseName 값을 가져옴)

        db = openOrCreateDatabase(name, MODE_WORLD_WRITEABLE, null);

        databaseCreated = true;

    }


    //테이블을 만들어주는 기능

    private void createTable(String name) {

        //현재 상태가 어떻게 되는지 textview에 뿌려줌

        println("creating table [" + name + "].");

        //위에 tableName값을 name으로 받아와서 테이블을 생성

        db.execSQL("create table "

                + name

                + "("

                + " _id integer PRIMARY KEY autoincrement, "

                + " name text, "

                + " age integer, "

                + " phone text);");


        tableCreated = true;

    }


    //테이블을 검색해주는 기능

    private void searchTable(String searchName) {

        //db객체를 얻어옴(읽기)

        db = helper.getReadableDatabase();

        //db에 들어있는 값들은 Cursor로 받아줘야됨

        //Cursor c = db.query(searchName, null, null, null, null, null, null); <<이렇게 해줘도됨 하지만 밑에게 더 간단함

        Cursor c = db.rawQuery("select * from " + searchName, null);


        //받아와서 뿌려주는 작업은 while문을 이용해야됨

        //while문 안의 조건에 들어가있는 c.moveToNext()는 위에 담아온 Cursor에 다음 값이 있으면 계속 실행

        //만약에 다음값이 없다면 멈춘다는 뜻

        while (c.moveToNext()){

            //각자의 값을 맡는 타입의 변수를 만들어 넣어줌

            String name = c.getString(c.getColumnIndex("name"));

            int age = c.getInt(c.getColumnIndex("age"));

            String phone = c.getString(c.getColumnIndex("phone"));

            //그리고 값을 뿌려줌

            println(searchName+" 테이블의 값 =\n이름 : " + name + " 나이 : " + age + " 번호 : " + phone);

        }


    }

    //테이블에 값을 넘어줌 insert (테이블 검색기능을 테스트해보기 위해서)

    private int insertRecord(){

        println("inserting records.");


        int count = 3;


        db.execSQL("insert into employee(name, age, phone) values ('john', 20, '010-1234-5678');");


        return count;

    }
}

class dbHelper extends SQLiteOpenHelper{


    private static final String DATABASE_NAME = "mycontacts.db";
    private static final int DATABASE_VERSION =1;


    /*
     *먼저 SQLiteOpenHelper클래스를 상속받은 dbHelper클래스가 정의 되어 있다. 데이터베이스 파일 이름은 "mycontacts.db"가되고,
     *데이터베이스 버전은 1로 되어있다. 만약 데이터베이스가 요청되었는데 데이터베이스가 없으면 onCreate()를 호출하여 데이터베이스
     *파일을 생성해준다.
     */

    public dbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE contact (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, tel TEXT);");

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXITS contact");
        onCreate(db);
    }



}