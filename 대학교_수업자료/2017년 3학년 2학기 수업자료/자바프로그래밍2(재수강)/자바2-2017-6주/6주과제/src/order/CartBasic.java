package order;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import item.ItemType;
import item.Item;

public class CartBasic {
	public static void main(String[] args) {
		CartBasic cart = new CartBasic();
		cart.doit();
	}
	Scanner keyin = new Scanner(System.in);
	static ArrayList<Order> ordList = new ArrayList<>();
	static ArrayList<Item> itemList = new ArrayList<>();
	static ArrayList<String> members = new ArrayList<>();
	static {
		members.add("ejlee"); 
		members.add("admin");
	};
	void doit() {
		readAllItems();
		readAllOrders();
		menu();
	}
	void readAllOrders() {
		Scanner orderFile = openFile("orders.txt");
		while (orderFile.hasNext()) {
			processAnOrder(orderFile);
		}
	}
	void processAnOrder(Scanner orderFile) {
		String id = orderFile.next();
		Order ord = null;
		while (true) {
			ord = new Order(id);
			ord.read(orderFile);
			if (ord.item == null)
				break;
			ordList.add(ord);
			ord.print();
		}
	}
	void menu() {
	}
	static boolean isMember(String user) {
		return members.contains(user);
	}
	static int contains(String user, Item item) {
		for (Order ord : ordList) {
			if (ord.userId.equals(user) && ord.item == item) {
				return ord.cc;
			}
		}		
		return 0;		
	}
	public static Scanner openFile(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	public static Item findItem(String kwd) {
		for (Item item : itemList) {
			if (item.compare(kwd))
				return item;
		}
		return null;		
	}
	void readAllItems() {
		Scanner fileIn = openFile("items.txt");
		Item it = null;
		fileIn.nextLine();
		int type = 1;
		while (fileIn.hasNext()) {
			//type = fileIn.nextInt();  // 상속에서 추가되어야 하는 부분
			if (type == 1) it = new Item();
			// ...
			it.read(fileIn);
			it.print();
			System.out.println();
			itemList.add(it);
		}
		fileIn.close();
	}

	void printAllItems() {
		for (Item item : itemList) {
			item.print();
			System.out.println();
		}
	}
	void printAnOrder(String id) {
		int total = 0;
		System.out.println("주문확인 : "+id);
		for (Order ord : ordList) {
			if (ord.userId.equals(id)) {
				ord.print();
				total += ord.getTotal();
			}
		}
		System.out.println(" 합계 : " +  total + "원");
	}
}