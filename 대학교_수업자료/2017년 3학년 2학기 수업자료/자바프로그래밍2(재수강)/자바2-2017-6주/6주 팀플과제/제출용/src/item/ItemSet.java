package item;

import java.util.ArrayList;
import java.util.Scanner;
import order.CartBasic;
import order.Order;

public class ItemSet extends Item {
	String yn;
	int setPrice;
	String set;
	@Override
	public void read(Scanner sc) {
		super.read(sc);
		yn = sc.next();
		setPrice = sc.nextInt();
		set = sc.next();
	}
	@Override
	public void print() {
		super.print();
		printSet();
	}
	@Override
	public void printmenu() {
		super.printmenu();
		printSet();
	}
	@Override
	public void printOrderDiscount(Order ord) { // 상속에서 Overriding
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" [%d개] size:%s ", ord.cc, ord.sz);

		Item it = null;
		Order o = null;
		int minOrd = 0;

		it = CartBasic.makeSetItem(set);
		o = CartBasic.makeSetOrder(it);
		if (ord.count(it) > ord.count(this))
			minOrd = ord.cc;

		else
			minOrd = o.cc;
		
		System.out.printf("소계 : %d원", price);
		finalPrice = price * ord.cc - (price + it.price - setPrice) * minOrd;
		if (yn.equals("y") && ord.isMember && ord.count(it) > 0) {
			System.out.print(" 회원/");
			System.out.printf("묶음할인  %d원", setPrice);
			System.out.printf("<%d개 총 %d원 할인>    \n[%s]", minOrd, (price + it.price - setPrice) * minOrd, set);

		} else if (yn.equals("n") && ord.count(it) > 0) {
			System.out.print(" 전체/");
			System.out.printf("묶음할인  %d원", setPrice);
			System.out.printf("<%d개 총 %d원 할인>    \n[%s]", minOrd, (price + it.price - setPrice) * minOrd, set);
		} else
			finalPrice = price * ord.cc;
	}

	void printSet() {
		if (yn.equals("y"))
			System.out.print(" 회원/");
		else
			System.out.print(" 전체/");
		System.out.printf("묶음할인 [%s]  %d원", set, setPrice);
	}
}