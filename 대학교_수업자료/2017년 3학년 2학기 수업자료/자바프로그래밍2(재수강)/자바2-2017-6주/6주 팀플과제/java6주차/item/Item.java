package item;

import java.util.Scanner;

import order.Order;

public class Item {
	static int count = 1;
	int index = 0;
	public String itemId;
	public int saleType; // 1 일반, 2 특별할인, 3 묶음할인
	String name;
	int price;
	public ItemType category;
	String mf;
	String sizeOption;

	public Item() {
	};


	public void init(String indx, String code, String name, String sz) {
		if (indx.length() == 0)
			index = 0;
		else
			index = Integer.parseInt(indx);
		if (code.length() == 0)
			this.itemId = "";
		else
			this.itemId = code;
		if (name.length() == 0)
			this.name = "";
		else
			this.name = name;
		if (sz.length() == 0)
			this.sizeOption = "";
		else
			this.sizeOption = sz;
	}

	public void read(Scanner sc) {
		index = count++;
		// saleType = sc.nextInt();
		itemId = sc.next();
		category = ItemType.get(sc.nextInt()); // 1~7
		name = sc.next();
		price = sc.nextInt();
		mf = sc.next();
		sizeOption = sc.next();
		// index = count++;

	}

	// T-01 3 후드티_ST1 5000 f s/m/l
	// static String categoryStr[] = {"운동복 ", "운동바지", "후드티 ", "면티 ", "바지 ", "반바지
	// ", "치마 "};
	public void printmenu() {
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" - %-8s ", sizeOption);

	}

	public void print() {
		// TODO Auto-generated method stub
		System.out.printf("%2d)", index);

		this.printOrder();

	}

	public void printOrder(Order ord) {
		System.out.printf("%2d) ", index);
		printOrder();
	}

	public void printOrderDiscount(Order ord) { // 상속에서 Overriding
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" [%d개] size:%s ", ord.cc, ord.sz);

		int finalPrice = 0;
		finalPrice = getPrice() * ord.cc;
		System.out.printf("소계 : %d원", finalPrice);
	}

	protected void printOrder() {
		System.out.printf(" [%s] %s %s\t(%d)", itemId, category.getName(), name, price);
	}

	protected void printMenu() {
		System.out.printf(" [%s] %s %s\t%d원", itemId, category.getName(), name, price);
	}

	public int getPrice() { // 상속에서 Overriding
		return price;
	}

	public ItemType getCategory() {
		return category;
	}

	public boolean compare(String kwd) {
		return (kwd.equals(index + "") || kwd.equals(itemId) || kwd.equals(name));
	}

	public int getDiscount(boolean bMember) {
		return 0;
	}

	public boolean compare(Item it) {
		if (index == it.index)
			return true;
		if ( itemId.equals(it.itemId))
			return true;
		if ( name.equals(it.name))
			return true;
		if (!(it.sizeOption.length()==0)&&sizeOption.contains(it.sizeOption)) {
			if (it.sizeOption.equals("l")) {
				if (sizeOption.indexOf("l") == sizeOption.indexOf("xl") + 1)
					return false;
			} // l이 검색되어야 하는데 l은 없고 xl만 있는 경우
			return true;
		}
		return false;
	}

	public int getIndex() {
		return index;
	}

}
