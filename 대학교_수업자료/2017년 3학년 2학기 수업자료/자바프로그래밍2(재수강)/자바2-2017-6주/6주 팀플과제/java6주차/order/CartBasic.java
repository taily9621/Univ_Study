package order;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import item.ItemType;
import item.setItem;
import item.speItem;
import item.Item;

public class CartBasic {
	public static void main(String[] args) {
		CartBasic cart = new CartBasic();
		cart.doit();
	}

	Scanner keyin = new Scanner(System.in);
	static ArrayList<Order> ordList = new ArrayList<>();
	static ArrayList<Item> itemList = new ArrayList<>();
	static ArrayList<String> members = new ArrayList<>();
	static {
		members.add("ejlee");
		members.add("admin");
	};

	void doit() {
		readAllItems();
		readAllOrders();
		menu();
	}

	void readAllOrders() {
		Scanner orderFile = openFile("orders.txt");
		while (orderFile.hasNext()) {
			processAnOrder(orderFile);
		}
	}

	void processAnOrder(Scanner orderFile) {
		String id = orderFile.next();
		Order ord = null;
		while (true) {
			ord = new Order(id);
			ord.read(orderFile);
			if (ord.item == null)
				break;
			ordList.add(ord);

		}
	}

	void menu() {
		int kwd;
		while (true) {
			kwd = 0;
			System.out.print("(1) 상품 검색 (2) 분류별 상품 리스트 (3) 주문하기 (4) 주문 확인 (5) 상품별 주문자 검색 (0) 종료");
			kwd = keyin.nextInt();
			switch (kwd) {
			case 1:
				searchItem();
				break;
			case 2:
				ItemList();
				break;
			case 3:
				sthOrder();
				break;
			case 4: // 사용자별 주문 내역 확인
				checkOnesOrder();

				break;
			case 5: // 상품별 주문자 (이 상품의 주문자 : ---
				orderbySome();
				break;
			case 0:
				return;

			}
		}

	}

	private void orderbySome() {
		// TODO Auto-generated method stub
		int num = 0;
		boolean comma = false;
		System.out.println("아이템 번호(1~11, 0이면 종료) : ");
		num = keyin.nextInt();
		System.out.print("이 상품의 주문자 : [");
		for (Order ord : ordList) {

			if (ord.getItem().getIndex() == num && !comma) {
				System.out.print(ord.userId);
				comma= true;
			} else if (ord.getItem().getIndex() == num && comma)
				System.out.print("," + ord.userId);
		}

		System.out.println("]");
	}

	void ItemList() {
		// TODO Auto-generated method stub
		int i = 1;
		int num = 0;
		String typeName = null;
		;
		for (ItemType d : ItemType.values()) {
			System.out.printf("[%d] %s %n", i, ItemType.map.get(i).getName());

			i++;
		}
		System.out.print("검색할 분류 번호를 넣으세요...");
		num = keyin.nextInt();
		i = 1;
		for (ItemType d : ItemType.values()) {
			if (d.getNumber() == num)
				typeName = d.getName();
			i++;
		}
		for (Item it : itemList) {
			if (it.category.getName().equals(typeName)) {
				it.print();
				System.out.println();
			}
		}

	}

	void checkOnesOrder() {
		String id;
		System.out.print("사용자 아이디를 넣으세요...");
		id = keyin.next();
		for (Order o : ordList)
			if (o.userId.equals(id)) {
				o.print();
				System.out.println();
			}
	}

	public static Item makeSetItem(String set) {
		Item it = null;
		for (Item i : itemList)
			if (i.itemId.equals(set)) {
				it = i;
				return it;
			}
		return null;
	}

	public static Order makeSetOrder(Item it) {
		Order ord = null;
		for (Order o : ordList)
			if (o.item == it) {
				ord = o;
				return ord;
			}
		return null;
	}

	private void searchItem() {
		// TODO Auto-generated method stub
		String index;

		String code;
		String name;
		String sz;
		keyin.nextLine();
		System.out.print("상품 번호 : ");
		index = keyin.nextLine();

		System.out.print("상품 코드 : ");
		code = keyin.nextLine();

		System.out.print("상품명 : ");
		name = keyin.nextLine();

		System.out.print("사이즈 : ");
		sz = keyin.nextLine();

		Item it = new Item();
		it.init(index, code, name, sz);
		for (Item i : itemList) {
			if (i.compare(it)) {
				i.print();
				System.out.println();
			}
		}

	}

	private void sthOrder() {// 아이디 입력 후 주문하고 주문내역 알려주기
		// TODO Auto-generated method stub
		String userid;
		String tmp;
		Item itemid;
		int cc;
		String sz;

		Order o = null;
		while (true) {
			System.out.print("user id(end면 종료) : ");
			userid = keyin.next();
			if (userid.equals("end"))
				break;
			o = new Order(userid);
			System.out.println("itemid 개수 사이즈(끝내려면 0) : ");
			while (true) {
				System.out.print("... ");
				tmp = keyin.next();
				if (tmp.equals("0"))
					break;
				itemid = findItem(tmp);
				cc = keyin.nextInt();
				sz = keyin.next();
				o.item = itemid;
				o.cc = cc;
				o.sz = sz;
				ordList.add(o);
			}
			System.out.println("printOrders : " + userid);
			int sum = 0;
			for (Order o1 : ordList) {
				if (o1.userId.equals(userid)) {
					o1.printOrders();
					sum += o1.getTotal();
				}
			}
			System.out.printf("합계 : %d원%n", sum);

		}

	}

	static boolean isMember(String user) {
		return members.contains(user);
	}

	static int contains(String user, Item item) {
		for (Order ord : ordList) {
			if (ord.userId.equals(user) && ord.item == item) {
				return ord.cc;
			}
		}
		return 0;
	}

	public static Scanner openFile(String filename) {
		String path = CartBasic.class.getResource("").getPath();
		File f = new File(path + filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	public static Item findItem(String kwd) {
		for (Item item : itemList) {
			if (item.compare(kwd))
				return item;
		}
		return null;
	}

	void readAllItems() {
		Scanner fileIn = openFile("items-i-step4.txt");
		Item it = null;
		fileIn.nextLine();
		int type = 1;
		while (fileIn.hasNext()) {
			type = fileIn.nextInt(); // 상속에서 추가되어야 하는 부분
			if (type == 1)
				it = new Item();
			else if (type == 2)
				it = new speItem();
			else if (type == 3)
				it = new setItem();

			it.saleType = type;
			it.read(fileIn);
			it.printmenu();
			System.out.println();
			itemList.add(it);
		}
		fileIn.close();
	}

	void printAllItems() {
		for (Item item : itemList) {
			item.printmenu();
			System.out.println();
		}
	}

	void printAnOrder(String id) {
		int total = 0;
		System.out.println("주문확인 : " + id);
		for (Order ord : ordList) {
			if (ord.userId.equals(id)) {
				ord.print();
				total += ord.getTotal();
			}
		}
		System.out.println(" 합계 : " + total + "원");
	}
}