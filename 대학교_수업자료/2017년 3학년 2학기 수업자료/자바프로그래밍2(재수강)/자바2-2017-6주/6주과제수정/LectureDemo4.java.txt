

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class LectureDemo {
	Scanner scan = new Scanner(System.in);
	ArrayList<Student> stList = new ArrayList<>();
	static ArrayList<Lecture> lecList = new ArrayList<>();
	//static HashMap<String, String> lectureMap = new HashMap<>();

	public static void main(String[] args) {
		LectureDemo demo = new LectureDemo();
		demo.doit();
	}

	void doit() {
		readAllLectures();
		readAllStudents();
		//printAllStudents();
		menu();
	}
	static Scanner fileOpen(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	void readAllLectures() {
		Scanner fileIn = fileOpen("lecture-list.txt");
		Lecture lec = null;
		while (fileIn.hasNext()) {
			lec = new Lecture();
			lec.read(fileIn);
			lecList.add(lec);
		}
		fileIn.close();
	}

	void readAllStudents() {
		Scanner fileIn = fileOpen("students.txt");
		Student st = null;
		while (fileIn.hasNext()) {
			st = new Student();
			st.read(fileIn);
			stList.add(st);
		}
		fileIn.close();
	}
	static Lecture findLecture(String kwd) {
		for (Lecture l : lecList)
			if (l.compare(kwd))
				return l;
		return null;
	}
	void printAllStudents() {
		int index = 1;
		for (Student s : stList) {
			System.out.printf("(%2d)  ", index++);
			s.print();
		}
	}
	void printAllLectures() {
		int index = 1;
		for (Lecture l : lecList) {
			System.out.printf("(%2d)  ", index++);
			l.print();
		}
	}
	void searchLecture() {
		System.out.print("검색어 : ");
		String kwd = scan.next();
		for (Lecture l : lecList) 
			if (l.compare(kwd))
				l.print();
	}
	void searchLectureStudents() {
		System.out.print("검색어 : ");
		String kwd = scan.next();
		Lecture l = findLecture(kwd);
		System.out.printf("%s 수강생 : ", l.name);
		for (Student s : stList) 
			if (s.enroll(l))
				System.out.print(s.name + " ");
		System.out.println();
	}
	void menu() {
		while (true) {
			System.out.print("(1) 전체학생  (2) 전체과목  (3) 시간표검색   (4) 교수별수강생검색   (0) 종료 ");
			int menu = scan.nextInt();
			if (menu == 0) break;
			switch (menu) {
			case 1:printAllStudents(); break;
			case 2:printAllLectures(); break;
			case 3: searchLecture(); break;
			case 4: searchLectureStudents(); break;
			default:break;
			}
		}
	}
}
class Student {
	String id;
	String name;
	String major;
	int grade;
	String phoneNum;
	ArrayList<Lecture> myClasses = new ArrayList<>();
	void read(Scanner s) {
		id = s.next();
		name = s.next();
		major = s.next();
		grade = s.nextInt();
		phoneNum = s.next();
		String temp = s.nextLine();
		String[] codes = temp.split(" ");
		Lecture lec = null;
		for (String code : codes) {
			lec = LectureDemo.findLecture(code);
			if (lec != null)
				myClasses.add(lec);
		}
		
	}
	void print() {
		System.out.printf("%s %s/%s/%d학년(%s)%n 신청과목 :", 
			id, name, major, grade, phoneNum);
		for (Lecture l : myClasses) 
			System.out.print(l.name + " ");
		System.out.println();
	}
	boolean enroll(Lecture l) {
		return myClasses.contains(l);
	}
}
class Lecture {
	String code;
	String name;
	int year;
	String prof;
	String schedule;
	void read(Scanner in) {
		code = in.next();
		name = in.next();
		year = in.nextInt();
		prof = in.next();
		schedule = in.next();
	}
	void print() {
		System.out.printf("%s %d / %s / %s %-15s %n", code, year, prof, schedule, name);
	}
	boolean compare(String kwd) {
		if (code.equals(kwd))
			return true;
		if (name.contains(kwd))
			return true;
		return false;
	}
}