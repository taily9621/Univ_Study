package great;
import java.util.HashMap;

public enum Dinasty {
	None(0, "없음"), GoChosun(1, "고조선"), SamKuk(2, "삼국"), Silla(3, "통일신라"), Koryo(4, "고려"), Chosun(5, "조선"), Ilje(6,
			"식민지"), KorRep(7, "대한민국");
	private int num;
	private String name;
	static HashMap<Integer, Dinasty> dinastyMap = new HashMap<>();

	private Dinasty(int num, String name) {
		this.num = num;
		this.name = name;
	}

	static {
		for (Dinasty dinasty : Dinasty.values()) 
			dinastyMap.put(dinasty.num, dinasty);
	}

	static Dinasty get(int n) {
		return dinastyMap.get(n);
	}

	static Dinasty get(String str) {
		return dinastyMap.get(str);
	}

	public int getNum() {
		return num;
	}

	public String getName() {
		return name;
	}	
}
