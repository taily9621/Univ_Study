package manager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
	protected ArrayList<Managable> gList = new ArrayList<>();
	
	protected static Scanner openFile(String filename) {
		String path= Manager.class.getResource("").getPath();
		File f = new File(path+filename);
		
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	protected void readAll(String fileName, Factory fac) {
		Scanner scan = openFile(fileName);
		Managable m = null;
		scan.nextLine();
		while (scan.hasNext()) {
			m = fac.create();
			m.read(scan);
			gList.add(m);
		}
		scan.close();
	}


	protected Managable find(String kwd) {
		for (Managable m : gList)
			if (m.compare(kwd))
				return m;
		return null;
	}

	protected void printAll() {
		for (Managable s : gList)
			s.print();
	}
}
