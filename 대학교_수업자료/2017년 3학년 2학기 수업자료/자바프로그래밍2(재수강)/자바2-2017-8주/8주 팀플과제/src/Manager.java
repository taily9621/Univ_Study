import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
	ArrayList<Managable> gList = new ArrayList<>();

	private static Scanner openFile(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	void readAll(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			String temp = fileIn.next();
			if (temp.equals("m")) {
				temp = fileIn.next();
				s = new FamousGreat(temp);
			} else
				s = new Great(temp);
			s.read(fileIn);
			gList.add(s);
		}
		fileIn.close();
	}

	void printAll() {
		for (Managable s : gList)
			s.print();
	}
}
