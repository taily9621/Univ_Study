import java.util.Scanner;

public class GreatDemo extends Manager implements Factory {
	public Managable create() {
		return new Great();
	}

	public void doit() {
		readAll("great-inherit.txt", this);
		printAll();
		search();
	}

	static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	void search() {
		Scanner in = new Scanner(System.in);
		while (true) {
			System.out.printf("검색어를 입력하세요..(종료 y) : ");
			String kwd = in.nextLine();
			kwd = kwd.trim();
			if (kwd.equals("y"))
				break;
			else {
				for (Managable managable : gList) {
					MatchType mt = managable.match(kwd);
					if (managable.compare(kwd))
						managable.printMatch(mt, kwd);
				}
			}
		}
	}

	public static void main(String[] args) {
		GreatDemo demo = new GreatDemo();
		demo.doit();
	}
}
