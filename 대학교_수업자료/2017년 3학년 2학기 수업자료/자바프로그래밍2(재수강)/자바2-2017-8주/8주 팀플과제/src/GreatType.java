import java.util.HashMap;

public enum GreatType {
	Etc(0, "기타"), King(1, "왕"), Millitary(2, "무인"), Politician(3, "정치가"), Artist(4, "예술가"),
	Doctor(5, "의사"), Religionist(6, "종교인");
	private int num;
	private String name;
	static HashMap<Integer, GreatType> greatTypeMap = new HashMap<>();

	private GreatType(int num, String name) {
		this.num = num;
		this.name = name;
	}

	static {
		for (GreatType greatType : GreatType.values())
			greatTypeMap.put(greatType.num, greatType);
	}

	static GreatType get(int n) {
		return greatTypeMap.get(n);
	}

	static GreatType get(String str) {
		return greatTypeMap.get(str);
	}

	public int getNum() {
		return num;
	}

	public String getName() {
		return name;
	}
}