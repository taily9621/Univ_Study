package music;

import java.util.ArrayList;
import java.util.Scanner;

import manager.Managable;

public class User implements Managable  {
	String id;
	String name;
	String age;
	String mf;
	String myMusics;
	String[] myMusicNumbers;
	ArrayList<Integer> intmyMusicNumbers = new ArrayList<>();
	ArrayList<Music> myMusic = new ArrayList<>();
	int tmp;

	public void read(Scanner scan) {
		id = scan.next();
		name = scan.next();
		age = scan.next();
		mf = scan.next();
		myMusics = scan.nextLine();
		myMusics = myMusics.trim();
		myMusicNumbers = myMusics.split(" ");
		for (int i = 0; i < myMusicNumbers.length; i++) {
			tmp = Integer.parseInt(myMusicNumbers[i]);
			intmyMusicNumbers.add(tmp);
		}
		Music mus = null;
		for (int code : intmyMusicNumbers) {
			mus = MusicDemo.findMusic(code);
			if (mus != null)
				myMusic.add(mus);
		}
	}

	public boolean listen(Music m) {
		// TODO Auto-generated method stub
		return myMusic.contains(m);
	}

	public void print() {
		System.out.printf("%s /%s/%s/%s %n", id, name, age, mf);
		for (Music m : myMusic)
			System.out.printf("        [%d] %s - %s (%s)%n", m.musicNum, m.Singer, m.musicName, m.musicYear);
		System.out.println();

	}

	void printSales() {
		System.out.printf("%s /%s/%s/%s %n", id, name, age, mf);
		for (Music m : myMusic)
			System.out.printf("        [%d] %s - %s (%s)%n", m.musicNum, m.Singer, m.musicName, m.musicYear);
		System.out.println();

	}

	@Override
	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		return false;
	}
}
