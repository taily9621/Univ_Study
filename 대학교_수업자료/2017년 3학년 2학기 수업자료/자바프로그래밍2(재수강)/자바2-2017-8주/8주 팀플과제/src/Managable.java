import java.util.Scanner;

interface Managable {
	void read(Scanner scan);
	void print();
	boolean compare(String kwd);
	MatchType match(String kwd);
	void printMatch(MatchType m, String str);
}
