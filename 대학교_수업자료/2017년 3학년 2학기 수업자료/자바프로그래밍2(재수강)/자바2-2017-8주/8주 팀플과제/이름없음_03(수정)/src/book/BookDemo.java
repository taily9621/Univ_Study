package book;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import manager.Manager;

public class BookDemo extends Manager implements Factory {
	//static ArrayList<Book> bookList = new ArrayList<>();
	ArrayList<Person> personList = new ArrayList<>();
	Scanner s = new Scanner(System.in);
	static int cnt = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BookDemo demo = new BookDemo();
		demo.doit();
	}

	void doit() {
		readAllBook("book.txt", this);
		readAllPerson("person.txt", this);
		menu();

	}

	protected void readAllBook(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			s = new Book();
			s.read(fileIn);
			s.print();
			gList.add(s);
		}
		fileIn.close();
	}

	protected void readAllPerson(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Person s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			s = new Person();
			s.read(fileIn);
			personList.add(s);
		}
		fileIn.close();
	}
	private void menu() {
		// TODO Auto-generated method stub
		int select = 0;

		while (true) {
			System.out.println("(1) 전체 책 출력  (2) 책제목 검색 (3) 전체 사용자 출력 (4) 사용자 검색 (5) 대출불가책 (6) 대출 (7) 반납 (0) 종료");
			System.out.print("메뉴 선택 : ");
			select = s.nextInt();
			switch (select) {
			case 1:
				printAllBook();
				//printAll();
				break;
			case 2:
				searchBook();
				break;
			case 3:
				printAllUser();
				break;
			case 4:
				searchPerson();
				break;
			case 5:
				cantBorrow();
				break;
			case 6:
				borrowBook();
				break;
			case 7:
				returnBook();
				break;
			case 0:
				return;
			}
		}

	}

	private void borrowBook() {
		// TODO Auto-generated method stub
		String id = null;
		String yn = null;
		String kwd = null;
		boolean already = false;
		boolean cannotBorrow = false;
		boolean sthBorrow = false;
		int borrowNum = 0;
		System.out.print("대출할 사용자 학번 : ");
		id = s.next();
		System.out.print("대출하시겠습니까? ");
		yn = s.next();
		if (yn.equals("y")) {
			for (Person p : personList)
				if (id.equals(p.id))
					System.out.println(p.name + "님, 안녕하세요!");
			while (true) {
				System.out.print("대출할 책 제목 일부 또는 isbn (끝내려면 end) : ");
				kwd = s.next();
				if (kwd.equals("end"))
					break;
				already = false;
				cannotBorrow = false;
				sthBorrow = false;
				for (Managable b : gList)
					for (Person p : personList)
						if (id.equals(p.id) && b.compare(kwd))
							if (p.loan((Book) b)) {
								already = true;
								break;
							}
				if (already)
					System.out.print("이미 대출한 책입니다...\n");

				for (Managable b : gList) {
					if (b.compare(kwd)) {
						if (((Book)b).cnt == 0) {
							System.out.print("대출불가 책입니다...\n");
							cannotBorrow = true;
							break;
						} else if (!already) {
							((Book)b).cnt--;
							borrowNum++;
							sthBorrow = true;
							System.out.println(((Book)b).name + " 대출합니다.");
							for (Person p : personList)
								if (id.equals(p.id))
									p.myBook.add(((Book)b));
							break;
						}
					}
				}

				if (!already && !cannotBorrow && !sthBorrow)
					System.out.print("없는 책입니다. 다시 입력해주세요...\n");

			}
			for (Person p : personList)
				if (id.equals(p.id))
					System.out.println(p.name + "님, " + borrowNum + "권 대출하셨습니다. 안녕히 가세요.");

		}

	}

	private void returnBook() {
		// TODO Auto-generated method stub
		String id = null;
		String yn = null;
		int returnBooknum = 0;
		System.out.print("반납할 사용자 학번 : ");
		id = s.next();
		for (Person p : personList)
			if (id.equals(p.id))
				p.printBook();
		System.out.print("\n반납하시겠습니까? ");
		yn = s.next();
		if (yn.equals("y")) {
			for (Person p : personList) {
				if (id.equals(p.id)) {
					System.out.print(p.name + "님, ");
					for (Managable b : gList)
						if (p.myBook.contains(((Book)b))) {
							((Book)b).cnt++;
							returnBooknum++;
						}
				}
			}
			System.out.println(returnBooknum + "권 반납되었습니다.");
			for (Person p : personList) {
				if (id.equals(p.id)) {
					for (Managable b : gList)
						if (p.myBook.contains(b)) {
							((Book) b).printexceptNumber();
							p.myBook.remove(b);
						}
				}
			}
		} else
			return;

	}

	private void cantBorrow() {
		// TODO Auto-generated method stub
		int cannotBorrow = 0;
		for (Managable b : gList)
			if (((Book)b).cnt == 0) {
				b.print();
				cannotBorrow++;
			}
		System.out.println(cannotBorrow + "권 대출 불가");
	}

	private void searchBook() {
		// TODO Auto-generated method stub
		System.out.print("책검색 키워드 :");
		String kwd = s.next();
		for (Managable b : gList)
			if (b.compare(kwd))
				b.print();
		System.out.println();
	}

	private void searchPerson() {
		// TODO Auto-generated method stub
		System.out.print("유저검색 키워드 :");
		String kwd = s.next();
		for (Person p : personList)
			if (p.compare(kwd))
				p.print();
		System.out.println();
	}

	private void printAllBook() {
		// TODO Auto-generated method stub
		for (Managable b : gList)
			b.print();
	}

	private void printAllUser() {
		for (Person p : personList)
			p.print();
	}

	// private void readAllBook() {
	// // TODO Auto-generated method stub
	// Scanner fileIn = openFile("book.txt");
	// Book b = null;
	//
	// String trash = fileIn.nextLine();
	// while (fileIn.hasNext()) {
	// b = new Book();
	// b.read(fileIn);
	// bookList.add(b);
	// }
	//
	// fileIn.close();
	// for (Book b1 : bookList) {
	// b1.num = cnt;
	// cnt++;
	// }
	// }
	//
	// private void readAllUser() {
	// // TODO Auto-generated method stub
	// Scanner fileIn = fileOpen("person.txt");
	// Person p = null;
	//
	// String trash = fileIn.nextLine();
	// while (fileIn.hasNext()) {
	// p = new Person();
	// p.read(fileIn);
	// personList.add(p);
	// }
	// fileIn.close();
	// }

	public static Book findBook(String code) {
		// TODO Auto-generated method stub
		for (Managable b : gList)
			if (code.equals(((Book)b).name))
				return (Book) b;
		return null;
	}

	@Override
	public Managable create() {
		// TODO Auto-generated method stub
		return new Book();
	}

}