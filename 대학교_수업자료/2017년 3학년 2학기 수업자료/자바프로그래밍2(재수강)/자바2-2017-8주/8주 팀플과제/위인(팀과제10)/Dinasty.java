package heroSuch;

import java.util.HashMap;

public enum Dinasty {
	None(0, "�뾾�쓬"), GoChosun(1, "怨좎“�꽑"), SamKuk(2, "�궪援�"), Silla(3, "�넻�씪�떊�씪"), Koryo(4, "怨좊젮"), Chosun(5, "議곗꽑"), Ilje(6,
			"�떇誘쇱�"), KorRep(7, "���븳誘쇨뎅");
	private int num;
	private String name;
	static HashMap<Integer, Dinasty> dinastyMap = new HashMap<>();

	private Dinasty(int num, String name) {
		this.num = num;
		this.name = name;
	}

	static {
		for (Dinasty dinasty : Dinasty.values()) 
			dinastyMap.put(dinasty.num, dinasty);
	}

	static Dinasty get(int n) {
		return dinastyMap.get(n);
	}

	static Dinasty get(String str) {
		return dinastyMap.get(str);
	}

	public int getNum() {
		return num;
	}

	public String getName() {
		return name;
	}	
}
