package heroSuch;

import java.util.Scanner;

class Great implements Managable {
	String name;
	boolean gender;
	Dinasty dinasty;
	String greatType;
	int birth;
	int death;
	String work;

	Great() {

	}

	Great(String name) {
		this.name = name;
	}

	public void read(Scanner in) {
		System.out.printf("%s \n", name);
		gender = in.next().equals("M");
		int tmp = in.nextInt();
		dinasty = Dinasty.get(tmp);
		greatType = in.next();
		birth = in.nextInt();
		death = in.nextInt();
		work = in.nextLine();
		work = work.trim();
	}

	public void print() {
		if (birth < 0 && death >= 0)
			System.out.printf("%-5s\t%s\tBC%2d ~ %4d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", -birth, death,
					dinasty.getName(), greatType, work);
		else if (birth >= 0 && death < 0)
			System.out.printf("%-5s\t%s\t%4d ~ BC%2d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", birth, -death,
					dinasty.getName(), greatType, work);
		else if (birth < 0 && death < 0)
			System.out.printf("%-5s\t%s\tBC%2d ~ BC%2d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", -birth, -death,
					dinasty.getName(), greatType, work);
		else
			System.out.printf("%-5s\t%s\t%4d ~ %4d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", birth, death,
					dinasty.getName(), greatType, work);
	}

	/*
	 * void printMovie() {
	 * 
	 * }
	 * 
	 * void printDrama() {
	 * 
	 * }
	 */
	public boolean compare(String kwd) {
		if (kwd.equals(name))
			return true;
		else if (kwd.equals("M"))
			return true;
		else if (kwd.equals("F"))
			return true;
		else if (kwd.equals(dinasty.getName()))
			return true;
		else if (kwd.equals(greatType))
			return true;
		else if (GreatDemo.isNumber(kwd)) {
			int alive = Integer.parseInt(kwd);
			if (alive >= birth && alive <= death)
				return true;
			else
				return false;
		}
		else if (work.contains(kwd))
			return true;
		else
			return false;
	}

	public MatchType match(String kwd) {
		MatchType matchType = MatchType.None;
		if (name.equals(kwd))
			matchType = MatchType.Name;
		else if (kwd.equals("M") && gender)
			matchType = MatchType.Gender;
		else if (kwd.equals("F") && !gender)
			matchType = MatchType.Gender;
		else if (dinasty.getName().equals(kwd))
			matchType = MatchType.Age;
		else if (greatType.equals(kwd))
			matchType = MatchType.Type;
		else if (GreatDemo.isNumber(kwd)) {
			matchType = MatchType.Year;
		} else if (work.contains(kwd))
			matchType = MatchType.Work;
		return matchType;
	}

	public void printMatch(MatchType t, String kwd) {
		if (t == MatchType.Name) // �씠由�
			if (birth < 0 && death >= 0)
				System.out.printf("�씠 �쐞�씤��\t%s\tBC%2d ~ %4d %-8s\t%-3s\t%s\n", gender ? "" : "[F]", -birth, death,
						dinasty.getName(), greatType, work);
			else if (birth >= 0 && death < 0)
				System.out.printf("�씠 �쐞�씤��\t%s\t%4d ~ BC%2d %-8s\t%-3s\t%s\n", gender ? "" : "[F]", birth, -death,
						dinasty.getName(), greatType, work);
			else if (birth < 0 && death < 0)
				System.out.printf("�씠 �쐞�씤��\t%s\tBC%2d ~ BC%2d %-8s\t%-3s\t%s\n", gender ? "" : "[F]", -birth, -death,
						dinasty.getName(), greatType, work);
			else
				System.out.printf("�씠 �쐞�씤��\t%s\t%4d ~ %4d %-8s\t%-3s\t%s\n", gender ? "" : "[F]", birth, death,
						dinasty.getName(), greatType, work);
		if (t == MatchType.Gender) // �꽦蹂�
			if (birth < 0 && death >= 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ %4d %-8s\t%-3s\t%s\n", name, gender ? "" : "", -birth, death,
						dinasty.getName(), greatType, work);
			else if (birth >= 0 && death < 0)
				System.out.printf("%-5s\t%s\t%4d ~ BC%2d %-8s\t%-3s\t%s\n", name, gender ? "" : "", birth, -death,
						dinasty.getName(), greatType, work);
			else if (birth < 0 && death < 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ BC%2d %-8s\t%-3s\t%s\n", name, gender ? "" : "", -birth, -death,
						dinasty.getName(), greatType, work);
			else
				System.out.printf("%-5s\t%s\t%4d ~ %4d %-8s\t%-3s\t%s\n", name, gender ? "" : "", birth, death,
						dinasty.getName(), greatType, work);
		if (t == MatchType.Age) // �떆��
			if (birth < 0 && death >= 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ %4d \t%-3s\t%s\n", name, gender ? "" : "[F]", -birth, death,
						greatType, work);
			else if (birth >= 0 && death < 0)
				System.out.printf("%-5s\t%s\t%4d ~ BC%2d \t%-3s\t%s\n", name, gender ? "" : "[F]", birth, -death,
						greatType, work);
			else if (birth < 0 && death < 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ BC%2d \t%-3s\t%s\n", name, gender ? "" : "[F]", -birth, -death,
						greatType, work);
			else
				System.out.printf("%-5s\t%s\t%4d ~ %4d \t%-3s\t%s\n", name, gender ? "" : "[F]", birth, death,
						greatType, work);
		if (t == MatchType.Type) // Great���엯
			if (birth < 0 && death >= 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ %4d %-8s\t\t%s\n", name, gender ? "" : "[F]", -birth, death,
						dinasty.getName(), work);
			else if (birth >= 0 && death < 0)
				System.out.printf("%-5s\t%s\t%4d ~ BC%2d %-8s\t\t%s\n", name, gender ? "" : "[F]", birth, -death,
						dinasty.getName(), work);
			else if (birth < 0 && death < 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ BC%2d %-8s\t\t%s\n", name, gender ? "" : "[F]", -birth, -death,
						dinasty.getName(), work);
			else
				System.out.printf("%-5s\t%s\t%4d ~ %4d %-8s\t\t%s\n", name, gender ? "" : "[F]", birth, death,
						dinasty.getName(), work);
		if (t == MatchType.Year)
			print();
		if (t == MatchType.Work) {
			StringBuilder sb = new StringBuilder();
			sb.append(work);
			int index = work.indexOf(kwd);
			int length = kwd.length();
			sb.insert(index, "<<");
			sb.insert(index + 4, ">>");
			String temp = sb.toString();
			if (birth < 0 && death >= 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ %4d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", -birth, death,
						dinasty.getName(), greatType, temp);
			else if (birth >= 0 && death < 0)
				System.out.printf("%-5s\t%s\t%4d ~ BC%2d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", birth, -death,
						dinasty.getName(), greatType, temp);
			else if (birth < 0 && death < 0)
				System.out.printf("%-5s\t%s\tBC%2d ~ BC%2d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", -birth, -death,
						dinasty.getName(), greatType, temp);
			else
				System.out.printf("%-5s\t%s\t%4d ~ %4d %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]", birth, death,
						dinasty.getName(), greatType, temp);
		}
	}
}
