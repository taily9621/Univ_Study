package manager;
import java.util.Scanner;
import great.MatchType;

public interface Managable {
	void read(Scanner scan);
	void print();
	boolean compare(String kwd);
}
