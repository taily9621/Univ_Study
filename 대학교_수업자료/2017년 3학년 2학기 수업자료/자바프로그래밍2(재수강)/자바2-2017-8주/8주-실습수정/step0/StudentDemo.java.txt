import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class StudentDemo {
	public static void main(String[] args) {
		StudentDemo demo = new StudentDemo();
		demo.doit();
	}
	void doit() {
		readAll("students.txt");
		readScores();
		printAll();
	}

	void readScores() {
		Scanner keyin = new Scanner(System.in);
		System.out.println("학생 아이디와 점수 입력 (점수 끝은 0)");
		String id;
		Managable st = null;
		while (true) {
			System.out.print("... ");
			id = keyin.next();
			st = find(id);
			if (st == null) break;
			((Student)st).readScores(keyin);;
		}		
	}
	ArrayList<Student> sList = new ArrayList<>();
	void readAll(String fileName) {
		Scanner scan = openFile(fileName);
		Student s = null;
		while (scan.hasNext()) {
			s = new Student();
			s.read(scan);
			sList.add(s);
		}
		scan.close();
	}
	Scanner openFile(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	Student find(String kwd) {
		for (Student s : sList)
			if (s.compare(kwd)) 
				return s;
		return null;
	}
	void printAll() {
		for (Student s : sList)
			s.print();
	}	
}