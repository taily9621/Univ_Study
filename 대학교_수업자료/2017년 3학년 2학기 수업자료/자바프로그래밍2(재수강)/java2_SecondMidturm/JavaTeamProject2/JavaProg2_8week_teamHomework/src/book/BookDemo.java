package book;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import manager.Manager;
// 인터페이스를 이용한 책 관리 프로그램
public class BookDemo extends Manager implements Factory // manager 패키지의 Manager 클래스를 오버라이딩 하고 Factory 인터페이스를 StudentDemo에서 구현
{
	static ArrayList<Book> bookList = new ArrayList<>();
	ArrayList<Person> personList = new ArrayList<>();
	Scanner s = new Scanner(System.in);
	int cnt = 1;

	public static void main(String[] args) // 메인 메소드
	{
		// TODO Auto-generated method stub
		BookDemo demo = new BookDemo();
		demo.doit();
	}

	void doit() // 실행 메소드
	{
		readAll("book.txt", this);
		for (Book b1 : bookList) {
			b1.num = cnt;
			cnt++;
		}
		readAll("person.txt", this);
		menu();

	}

	protected void readAll(String fileName, Factory fac) // 책 정보와 고객 정보가 있는 텍스트 파일을 읽는 메소드
	{
		Scanner fileIn = openFile(fileName);
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			if (fileName.equals("person.txt")) // 고객 정보 텍스트 파일일 경우
			{

				s = new Person();
			} else // 책 정보 텍스트 파일일 경우
				s = new Book();
			s.read(fileIn);
			gList.add(s); // Person() 또는 Book() 정보가 저장된 s를 리스트인 gList에 추가
		}
		fileIn.close();
	}

	private void menu() // 실행 메뉴
	{
		// TODO Auto-generated method stub
		int select = 0;

		while (true) {
			System.out.println("(1) 전체 책 출력  (2) 책제목 검색 (3) 전체 사용자 출력 (4) 사용자 검색 (5) 대출불가책 (6) 대출 (7) 반납 (0) 종료");
			System.out.print("메뉴 선택 : ");
			select = s.nextInt();
			switch (select) {
			case 1:
				printAllBook();
				break;
			case 2:
				searchBook();
				break;
			case 3:
				printAllUser();
				break;
			case 4:
				searchPerson();
				break;
			case 5:
				cantBorrow();
				break;
			case 6:
				borrowBook();
				break;
			case 7:
				returnBook();
				break;
			case 0:
				return;
			}
		}

	}

	private void borrowBook() // 책 대출 메소드
	{
		// TODO Auto-generated method stub
		String id = null;
		String yn = null;
		String kwd = null;
		boolean already = false;
		boolean cannotBorrow = false;
		boolean sthBorrow = false;
		int borrowNum = 0;
		System.out.print("대출할 사용자 학번 : ");
		id = s.next();
		System.out.print("대출하시겠습니까? ");
		yn = s.next();
		if (yn.equals("y")) {
			for (Person p : personList)
				if (id.equals(p.id))
					System.out.println(p.name + "님, 안녕하세요!");
			while (true) {
				System.out.print("대출할 책 제목 일부 또는 isbn (끝내려면 end) : ");
				kwd = s.next();
				if (kwd.equals("end"))
					break;
				already = false;
				cannotBorrow = false;
				sthBorrow = false;
				for (Book b : bookList)
					for (Person p : personList)
						if (id.equals(p.id) && b.compare(kwd))
							if (p.loan(b)) {
								already = true;
								break;
							}
				if (already)
					System.out.print("이미 대출한 책입니다...\n");

				for (Book b : bookList) {
					if (b.compare(kwd)) {
						if (b.cnt == 0) {
							System.out.print("대출불가 책입니다...\n");
							cannotBorrow = true;
							break;
						} else if (!already) {
							b.cnt--;
							borrowNum++;
							sthBorrow = true;
							System.out.println(b.name + " 대출합니다.");
							for (Person p : personList)
								if (id.equals(p.id))
									p.myBook.add(b);
							break;
						}
					}
				}

				if (!already && !cannotBorrow && !sthBorrow)
					System.out.print("없는 책입니다. 다시 입력해주세요...\n");

			}
			for (Person p : personList)
				if (id.equals(p.id))
					System.out.println(p.name + "님, " + borrowNum + "권 대출하셨습니다. 안녕히 가세요.");

		}

	}

	private void returnBook() // 책 반납 메소드
	{
		// TODO Auto-generated method stub
		String id = null;
		String yn = null;
		int returnBooknum = 0;
		System.out.print("반납할 사용자 학번 : ");
		id = s.next();
		for (Person p : personList)
			if (id.equals(p.id))
				p.printBook();
		System.out.print("\n반납하시겠습니까? ");
		yn = s.next();
		if (yn.equals("y")) {
			for (Person p : personList) {
				if (id.equals(p.id)) {
					System.out.print(p.name + "님, ");
					for (Book b : bookList)
						if (p.myBook.contains(b)) {
							b.cnt++;
							returnBooknum++;
						}
				}
			}
			System.out.println(returnBooknum + "권 반납되었습니다.");
			for (Person p : personList) {
				if (id.equals(p.id)) {
					for (Book b : bookList)
						if (p.myBook.contains(b)) {
							b.printexceptNumber();
							p.myBook.remove(b);
						}
				}
			}
		} else
			return;

	}

	private void cantBorrow() // 대출 불가 권수 출력 메소드
	{
		// TODO Auto-generated method stub
		int cannotBorrow = 0;
		for (Book b : bookList)
			if (b.cnt == 0) {
				b.print();
				cannotBorrow++;
			}
		System.out.println(cannotBorrow + "권 대출 불가");
	}

	private void searchBook() // 책 검색 메소드
	{
		// TODO Auto-generated method stub
		System.out.print("책검색 키워드 :");
		String kwd = s.next();
		for (Book b : bookList)
			if (b.compare(kwd))
				b.print();
		System.out.println();
	}

	private void searchPerson() // 고객 검색 메소드
	{
		// TODO Auto-generated method stub
		System.out.print("유저검색 키워드 :");
		String kwd = s.next();
		for (Person p : personList)
			if (p.compare(kwd))
				p.print();
		System.out.println();
	}

	private void printAllBook() // 전체 책 리스트 출력 메소드
	{
		// TODO Auto-generated method stub
		for (Book b : bookList)
			b.print();
	}

	private void printAllUser() // 전체 고객 리스트 출력 메소드
	{
		for (Person p : personList)
			p.print();
	}

	// private void readAllBook() {
	// // TODO Auto-generated method stub
	// Scanner fileIn = openFile("book.txt");
	// Book b = null;
	//
	// String trash = fileIn.nextLine();
	// while (fileIn.hasNext()) {
	// b = new Book();
	// b.read(fileIn);
	// bookList.add(b);
	// }
	//
	// fileIn.close();
	// for (Book b1 : bookList) {
	// b1.num = cnt;
	// cnt++;
	// }
	// }
	//
	// private void readAllUser() {
	// // TODO Auto-generated method stub
	// Scanner fileIn = fileOpen("person.txt");
	// Person p = null;
	//
	// String trash = fileIn.nextLine();
	// while (fileIn.hasNext()) {
	// p = new Person();
	// p.read(fileIn);
	// personList.add(p);
	// }
	// fileIn.close();
	// }

	public static Book findBook(String code) // 코드를 이용한 책 검색 메소드
	{
		// TODO Auto-generated method stub
		for (Book b : bookList)
			if (code.equals(b.name))
				return b;
		return null;
	}

	@Override
	public Managable create() // Factory 인터페이스 구현
	{
		// TODO Auto-generated method stub
		return new Book();
	}

}