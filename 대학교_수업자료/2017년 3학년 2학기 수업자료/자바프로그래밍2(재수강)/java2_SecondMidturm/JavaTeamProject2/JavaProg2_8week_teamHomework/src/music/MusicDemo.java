package music;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import book.Book;
import book.Person;
import manager.Factory;
import manager.Managable;
import manager.Manager;
// 인터페이스를 이용한 음악 관리 프로그램
public class MusicDemo extends Manager implements Factory // manager 패키지의 Manager 클래스를 오버라이딩 하고 Factory 인터페이스를 StudentDemo에서 구현
{
	static ArrayList<Music> musicList = new ArrayList<>(); // 
	ArrayList<User> userList = new ArrayList<>();
	static HashMap<Integer, String> musicMap = new HashMap<>();
	Scanner s = new Scanner(System.in);

	public static void main(String[] args) // 메인 메소드
	{
		// TODO Auto-generated method stub
		MusicDemo demo = new MusicDemo();
		demo.doit();
	}

	private void doit() // 실행 메소드
	{
		// TODO Auto-generated method stub
		readAll("music.txt", this); // 음악 텍스트 파일 읽기
		readAll("link.txt", this); // 유저 정보 텍스트 파일 읽기
		menu(); // 메뉴 실행
	}

	protected void readAll(String fileName, Factory fac) // 음악과 유저 정보가 있는 텍스트 파일 정보를 읽는 메소드
	{
		Scanner fileIn = openFile(fileName);
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			if (fileName.equals("music.txt")) // 음악 텍스트 파일일 경우
			{
				s = new Music();
			} else if(fileName.equals("link.txt")) // 유저 정보 텍스트 파일일 경우
				s = new User();
			s.read(fileIn);
			gList.add(s); // Music() 또는 User() 가 저장된 s를 리스트인 gList에 추가
		}
		fileIn.close();
	}
//	private void readAllUser() {
//		// TODO Auto-generated method stub
//		Scanner fileIn = openFile("link.txt");
//		User u = null;
//		String trash = fileIn.nextLine();
//		while (fileIn.hasNext()) {
//			u = new User();
//			u.read(fileIn);
//			userList.add(u);
//
//		}
//		fileIn.close();
//	}
//
//	void readAllMusic() {
//		Scanner fileIn = openFile("music.txt");
//		Music m = null;
//
//		String trash = fileIn.nextLine();
//		while (fileIn.hasNext()) {
//			m = new Music();
//			m.read(fileIn);
//			musicList.add(m);
//		}
//		fileIn.close();
//
//	}

	void printMusicList() // 리스트에 저장된 음악 리스트를 출력하는 메소드
	{
		for (Music m : musicList)
			m.print();

	}

	void printUserList() // 리스트에 저장된 유저 리스트를 출력하는 메소드
	{
		for (User u : userList)
			u.print();

	}

	private void searchUser() // 유저를 검색하는 메소드
	{
		// TODO Auto-generated method stub
		System.out.print("검색어 :");
		String kwd = s.next();
		Music m = findMusic(kwd);
		System.out.print(m.musicName+"링크 유저: ");
		for (User u : userList)
			if (u.listen(m))
				System.out.print( u.name + " ");
		System.out.println();
	}

	private void searchMusic() // 음악을 검색하는 메소드
	{
		// TODO Auto-generated method stub
		System.out.print("검색어 :");
		String kwd = s.next();
		for (Music m : musicList)
			if (m.musicName.contains(kwd))
				m.print();

	}

	private void menu() // 실행 메뉴 
	{
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int select = 0;
		while (true) 
		{
			System.out.print("(1) 전체곡목록 (2) 전체유저 (3) 곡목검색 "
					+ "(4)곡목별유저 (5) 판매현황 (0) 종료\n");
			System.out.print("select >>");
			select = scan.nextInt();
			switch (select) {
			case 1:
				printMusicList();
				break;
			case 2:
				printUserList();
				break;
			case 3:
				searchMusic();
				break;
			case 4:
				searchUser();
				break;
			case 5:
				checkSale();
				break;
			case 0:
				return;
			}
		}

	}

	private void checkSale() // 음악 판매 현황 체크 메소드
	{
		// TODO Auto-generated method stub
		int maxcnt = 0;
		for (Music m : musicList) {
			for (User u : userList){
				if (u.listen(m))
					m.cnt++;
				if(m.cnt>maxcnt)
					maxcnt = m.cnt;
			}
		}
		for (Music m : musicList) 
			if (m.cnt > 0)
				m.printSales();
		for(Music m: musicList)
			if(m.cnt == maxcnt){
				System.out.print("Best Seller Music [ "+maxcnt+"명 ] :");
				m.print();
			}
		

	}

	public static Music findMusic(int code) // 음악 찾기 메소드 (번호로 찾을 경우)
	{
		// TODO Auto-generated method stub
		for (Music m : musicList)
			if (code == m.musicNum)
				return m;
		return null;
	}

	static Music findMusic(String kwd) // 음악 찾기 메소드 (곡명으로 찾을 경우)
	{
		for (Music m : musicList)
			if (m.compare(kwd))
				return m;
		return null;
	}

	@Override
	public Managable create() // Factory 인터페이스를 구현
	{
		// TODO Auto-generated method stub
		return null;
	}
}