package item;

import java.util.Scanner;

import order.Order;

public class speItem extends Item {
	String yn;
	int dc;

	public void read(Scanner sc) {
		super.read(sc);

		yn = sc.next();
		dc = sc.nextInt();

	}

	public int getPrice() { // 상속에서 Overriding
		return dc;
	}

	public void print() {
		// TODO Auto-generated method stub
		super.print();
		printSpe();

	}

	public void printmenu() {
		super.printmenu();
		printSpe();

	}

	public void printOrderDiscount(Order ord) { // 상속에서 Overriding

		super.printOrderDiscount(ord);
		if (yn.equals("y") && ord.isMember) {
			System.out.print(" 회원/");
			System.out.printf("특별할인  %d원", dc);
			System.out.printf("<%d원 할인>  ", (dc/ ord.cc));
		} else if (yn.equals("n") && !ord.isMember) {
			System.out.print(" 전체/");
			System.out.printf("특별할인  %d원", dc);
			System.out.printf("<%d원 할인>  ", (dc/ ord.cc));
		}
	}

	void printSpe() {
		if (yn.equals("y"))
			System.out.print(" 회원/");
		else
			System.out.print(" 전체/");
		System.out.printf("특별할인  %d원", dc);
	}
}
