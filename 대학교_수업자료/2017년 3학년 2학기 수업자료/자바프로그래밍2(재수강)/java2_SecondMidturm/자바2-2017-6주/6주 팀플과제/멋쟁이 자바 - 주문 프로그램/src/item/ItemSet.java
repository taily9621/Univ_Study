package item;

import java.util.ArrayList;
import java.util.Scanner;
import order.CartBasic;
import order.Order;

public class ItemSet extends Item {
	String yn;
	int setPrice;
	String set;

	@Override
	public void read(Scanner sc) {
		super.read(sc);
		yn = sc.next();
		setPrice = sc.nextInt();
		set = sc.next();
	}

	@Override
	public void print() {
		super.print();
		printSet();
	}

	@Override
	public void printmenu() {
		super.printmenu();
		printSet();
	}

	@Override
	public void printOrderDiscount(Order ord) { // 상속에서 Overriding
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" [%d개] size:%s ", ord.cc, ord.sz);

		Item it = null;
		Order o = null;
		int minOrd = 0;

		it = CartBasic.makeSetItem(set);
		o = CartBasic.makeSetOrder(it);
		if (ord.count(it) > ord.count(this))
			minOrd = ord.cc;

		else
			minOrd = o.cc;
		
		if (!ord.isMember && yn.equals("y")) {
			finalPrice = price * ord.cc;
			System.out.printf("소계 : %5d원", finalPrice);
		}
		else {
			finalPrice = price * ord.cc - (price + it.price - setPrice) * minOrd;
			System.out.printf("소계 : %5d원", finalPrice);
			System.out.printf(" %s/", yn.equals("y") ? "회원" : "전체");
			System.out.printf("묶음할인  %5d원", setPrice);
			System.out.printf("<%d개 총 %d원 할인>    \n[%s]", minOrd, (price + it.price - setPrice) * minOrd, set);
		}
		
	}

	void printSet() {
		System.out.printf(" %s/묶음할인 [%s]  %d원", yn.equals("y") ? "회원" : "전체", set, setPrice);
	}
}