package item;

import java.util.Scanner;
import order.Order;

public class ItemSpe extends Item {
	String yn;
	int dc;

	@Override
	public void read(Scanner sc) {
		super.read(sc);
		yn = sc.next();
		dc = sc.nextInt();
	}

	@Override
	public void print() {
		super.print();
		printSpe();
	}

	@Override
	public void printmenu() {
		super.printmenu();
		printSpe();
	}

	@Override
	public void printOrderDiscount(Order ord) { // 상속에서 Overriding
		System.out.printf("%2d)", index);
		printMenu();
		System.out.printf(" [%d개] size:%s ", ord.cc, ord.sz);

		finalPrice = dc * ord.cc;

		if (yn.equals("y") && ord.isMember) {
			System.out.printf("소계 : %d원", finalPrice);
			System.out.printf(" 회원/");
			System.out.printf("특별할인  %d원", dc);
			System.out.printf("<%d원 할인>  ", (price - dc));
		} else if (yn.equals("n") && !ord.isMember) {
			System.out.printf("소계 : %d원", finalPrice);
			System.out.printf(" 전체/");
			System.out.printf("특별할인  %d원", dc);
			System.out.printf("<%d원 할인>  ", (price - dc));
		} else {
			finalPrice = price * ord.cc;
			System.out.printf("소계 : %d원", finalPrice);
		}
	}

	void printSpe() {
		if (yn.equals("y"))
			System.out.print(" 회원/");
		else
			System.out.print(" 전체/");
		System.out.printf("특별할인  %d원", dc);
	}
}
