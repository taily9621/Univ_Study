package manager;

import java.util.Scanner;

public interface Factory<E extends Managable> {
	E create(Scanner scan);
}
