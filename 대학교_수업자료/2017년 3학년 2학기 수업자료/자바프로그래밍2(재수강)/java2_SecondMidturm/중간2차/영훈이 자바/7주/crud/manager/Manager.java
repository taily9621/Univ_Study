package manager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
	private ArrayList<Managable> mList = new ArrayList<>();
	public ArrayList<Managable> getList() {
		return mList;
	}
	public void readAll(Scanner in, Factory fac) {
		Managable st = null;
		while (true) {
			st = fac.create();
			st.read(in);
			mList.add(st);
		}
	}
	public void register(Managable m) {
		mList.add(m);
	}
	public void readAll(String filename, Factory fac) {
		Scanner fileIn = Manager.fileOpen(filename);
		Managable m = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			m = fac.create();
			m.read(fileIn);
			mList.add(m);
		}
		fileIn.close();
	}
	public void printAll() {
		for (Managable e : mList) {
			e.print();
		}
	}
	public static Scanner fileOpen(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	public Managable findManagable(int n) {
		for (Managable m : mList)
			if (m.compare(n)) 
				return m;
		return null;
	}
	public Managable findManagable(String kwd) {
		for (Managable m : mList)
			if (m.compare(kwd)) 
				return m;
		return null;
	}
}
