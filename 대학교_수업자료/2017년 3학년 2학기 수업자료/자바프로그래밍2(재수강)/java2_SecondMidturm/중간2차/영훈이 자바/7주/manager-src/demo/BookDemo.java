package demo;
import java.util.ArrayList;
import java.util.Scanner;

import manager.Manager;
import manager.Factory;
import manager.Managable;

class BookDemo implements Factory {
	public static void main(String args[]) {
		BookDemo demo = new BookDemo();
		demo.doit();
	}
	static Manager bookMgr = new Manager();
	Manager userMgr = new Manager();
	Scanner keyin = new Scanner(System.in);
	void doit() {
		bookMgr.readAll("books.txt", this);
		bBook = false;
		userMgr.readAll("users.txt", this);
		menu();
	}
	void menu() {
		while (true) {
			System.out.print("(1) 전체책목록  (2) 책검색  (3) 전채유저  (4) 유저검색  (5) 대출가능책  (0) 종료 ");
			int menu = keyin.nextInt();
			if (menu == 0) break;
			switch(menu) {
			case 1: bookMgr.printAll(); break;
			case 2: searchBook(); break;
			case 3: userMgr.printAll(); break;
			case 4: searchUser(); break;
			case 5: listAvailableBooks(); break;
			default: break;
			}
		}
	}
	void searchBook() {
		System.out.print("책검색 키워드 : ");
		String kwd = keyin.next();
		Managable m = bookMgr.findManagable(kwd);
		if (m != null)
			m.print();
	}
	void searchUser() {
		System.out.print("유저검색 키워드 : ");
		String kwd = keyin.next();
		Managable m = userMgr.findManagable(kwd);
		if (m != null)
			m.print();
	}
	void listAvailableBooks() {
		ArrayList<Managable> list = bookMgr.getList();
		Book book = null;
		for (Managable m : list) {
			book = (Book)m;
			if (book.borrower == null)
				book.print();
		}
	}
	boolean bBook = true;
	public Managable create() {
		if (bBook) return new Book();
		return new User();
	}
}
    
class Book implements Managable {
    private String title;
    private String isbn;
    private String author;
    User borrower;
    int index;
    static int count=1;
    public void read(Scanner s) {
    	index = count++;
    	title = s.nextLine();
    	isbn = s.next();
    	author = s.next();
    	if (s.hasNext()) s.nextLine();
    }
    public boolean compare(int index) {
    	return this.index == index;
    }
    public void print() {
    	System.out.printf("[%2d] %s (%s) %s저, ", index, title, isbn, author);
    	if (borrower == null)
    		System.out.println("[대출가능]");
    	else System.out.printf("[대출중-%s]%n", borrower.getName());
    	
    }
    public String getName() {
    	return title;
    }
	@Override
	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		if (title.indexOf(kwd) != -1)
			return true;
		return false;
	}
}

class User implements Managable {
    String userid;
    ArrayList<Managable> borrowList = new ArrayList<>();
    public void read(Scanner s) {
    	String aLine = s.nextLine();
    	aLine = aLine.trim();
    	String toks[] = aLine.split(" ");
    	userid = toks[0];
    	Book bk = null;
    	for (int i = 1; i < toks.length; i++) {
    		bk = (Book)BookDemo.bookMgr.findManagable(Integer.parseInt(toks[i]));
    		if (bk != null) {
    			borrowList.add(bk);
    			bk.borrower = this;
    		}
    		else System.out.println(toks[i] + " no book");
    	}
    }
    public void print() {
    	System.out.print(userid + " - ");
    	for (Managable bk : borrowList)
    		if (bk != null)
    			System.out.print(bk.getName() + " / ");
    	System.out.println(borrowList.size()+"권");
    }
    public boolean compare(int n) {
    	return false;
    }
    public String getName() {
    	return userid;
    }
	@Override
	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		if (userid.equals(kwd))
			return true;
		return false;
	}
}
