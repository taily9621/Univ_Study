package _pasted_code_;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BookDemo {
    private ArrayList<Book> books = new ArrayList<>();

    public static void main(String[] args) {
        BookDemo demo = new BookDemo();
        demo.start();
    }

    private void start() {
        readBooks();
        printBooks();
        search();
    }

    private void readBooks() {
        Scanner scan = openFile("book-inherit.txt");
        Book book = null;
        int tmp = 0;
        while (scan.hasNextLine()) {
            tmp = scan.nextInt();
            if (tmp == 2) {
                book = new TranslatedBook();
            } else {
                book = new Book();
            }
            book.read(scan);
            books.add(book);
        }
    }
    private void printBooks() {
        for (Book book : books)
        	book.print();
    }

    private void search() {
        Scanner scan = new Scanner(System.in);
        String input = null;
        while (true) {
            System.out.print("검색어를 입력하세요: ");
            input = scan.nextLine(); // 책 제목의 단위는 Line
            if (input.equals("exit"))
                break;

            for (Book book : books)
                if (book.search(input))
                    System.out.println(book.getTitle()); // 한 줄에 하나 출력
        }
    }

    private Scanner openFile(String filename) {
        File f = new File(filename);
        Scanner fileIn = null;
        try {
            fileIn = new Scanner(f);
        } catch (FileNotFoundException e) {
            throw new RuntimeException();
        }
        return fileIn;
    }
}
class Book {
    protected String title, ISBN, year, publisher;
    protected String[] writers;

    /*
    명품 자바 프로그래밍
    5678 2013 생능출판사
    2 황기태 김효수
     */
    public void read(Scanner scan) {
        title = scan.nextLine().trim();
        ISBN = scan.next();
        year = scan.next();
        publisher = scan.nextLine();

        int num = Integer.parseInt(scan.next());
        writers = new String[num];
        for (int i = 0; i < num; i++)
            writers[i] = scan.next();
        readAdded(scan);
        if (scan.hasNext())
        	scan.nextLine(); // Buffer Out
    }
    void readAdded(Scanner scan) {
    }
    public String getTitle() {
        return title;
    }
	void print() {
		System.out.printf("%s%n%s %s %s%n%d",  title, ISBN, year, publisher, writers.length);
		for(String writer : writers) {
			System.out.printf(" %s", writer);
		}
		System.out.println();
	}
	
    public boolean search(String input) {
        /* 제목(부분) 검색 */
        if (title.contains(input))
            return true;

        /* 저자 검색 */
        for (String s : writers)
            if (s.contains(input))
                return true;

        return false;
    }
}
class TranslatedBook extends Book {
    private String originalTitle;
    private String[] originalWriters;

    @Override
    public void readAdded(Scanner scan) {
        /* 원서 데이터 읽기 */
        originalTitle = scan.nextLine().trim();
        int num = scan.nextInt();
        originalWriters = new String[num];
        for (int i = 0; i < num; i++)
        	originalWriters[i] = scan.next();
    }
    @Override
	protected void print() {
		super.print(); // 부모 클래스의 print 호출
		
		System.out.printf("%s%n%d",  originalTitle, originalWriters.length);
		for (String writer : writers) {
			System.out.printf(" %s", writer);
		}
	
		System.out.println();
	}
    /*
    	상속 클래스는 번역서, 원서 모두 검색
     */
    @Override
    public boolean search(String input) {
		if (super.search(input))
			return true;
        if (originalTitle.contains(input))
            return true;
        /* 원서 저자 검색 */
        for (String writer : writers)
            if (writer.contains(input))
                return true;

        return false;
    }
}
