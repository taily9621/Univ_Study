package book;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import Manager.Factory;
import Manager.Managable;
import Manager.Manager;

public class BookDemo extends Manager implements Factory {
	public static Manager bookMgr;
	public static Manager userMgr;
	Scanner scan = new Scanner(System.in);

	public Managable create(String fileName) {//create함수하나
		if (fileName.equals("books.txt"))
			return new Book();
		else
			return new Users();
	}

	public static void main(String[] args) {//메인
		BookDemo demo = new BookDemo();
		demo.doit();
	}

	private void doit() {

		//책들 생성, 입력받기
		bookMgr = new Manager();
		bookMgr.readAll("books.txt", this);
		//유저들 생성, 입력받기
		userMgr = new Manager();
		userMgr.readAll("users.txt", this);
		//둘다 프린트
		bookMgr.printAll();
		userMgr.printAll();
		function();
	}

	void function() {
		int kind;
		while (true) {
			System.out.println("(1) 책 전체 출력 (2) 책 키워드 검색 (3) 사용자 전체 출력 (4) 사용자아이디 검색 (5) 대출가능 책목록 (0) 종료");
			kind = scan.nextInt();
			scan.nextLine();
			switch (kind) {
			case 1:
				bookMgr.printAll();//책 전부 프린트
				break;
			case 2:
				searchBook();//책 검색
				break;
			case 3:
				userMgr.printAll();//유저 전부 프린트
				break;
			case 4:
				searchUsers();//유저 검색
				break;
			case 5:
				canBorrowBook();//책 빌릴 수 있는 것들 검색
				break;
			case 0://프로그램 종료
				return;
			}
		}
	}

	private void searchBook() {

		String key = null;
		System.out.print("검색키워드 : ");
		key = scan.nextLine(); // 책 제목의 단위는 Line

		System.out.println("책수" + bookMgr.stList.size() + "|||" + key);
		
		bookMgr.findAndPrintManager(key);//Manager.java참고


	}

	private void searchUsers() {
		String key = null;
		System.out.print("검색키워드 : ");
		key = scan.nextLine();

		System.out.println("유저수" + userMgr.stList.size() + "|||" + key);
		
		userMgr.findAndPrintManager(key);//Manager.java참고
		
	}

	void canBorrowBook() {
		
		for (Managable book : bookMgr.stList) {
			if (   ((Book)book).borrowName.equals("")   ) book.print();//책 아무도 안빌렸으면 ""그대로
		}
	}
}

class Book implements Managable {
	public String title, ISBN, writer;
	static int hIndex = 1;
	int index;
	public String borrowName = "";// 빌린 사람 이름

	public void read(Scanner scan) {
		index = hIndex++;
		title = scan.nextLine();
		ISBN = scan.next();
		writer = scan.nextLine().trim();

	}

	public String getTitle() {
		return title;
	}

	public void print() {//프린트
		System.out.printf("[%2d] %s (%s) %s저, ", index, title, ISBN, writer);
		if (borrowName.equals(""))
			System.out.printf("[대출가능]\n");
		else
			System.out.printf("[대출중-%s]\n", borrowName);
	}

	@Override
	public boolean compare(String kwd) {//kwd가 writer이거나 제목 안에 포함되어 있는지
		if (kwd.equals(writer))
			return true;
		if (title.contains(kwd))
			return true;

		return false;
	}
}

class Users implements Managable {
	String name;
	ArrayList<Integer> borrowBooks = new ArrayList<>();

	@Override
	public void read(Scanner s) {
		// s.nextLine();
		String split[] = s.nextLine().split(" ");
		System.out.println(split[0]);
		name = split[0];
		for (int i = 1; i < split.length; i++) {
			int index = Integer.parseInt(split[i]) - 1;
			borrowBooks.add(index);
			((Book) (BookDemo.bookMgr.stList.get(index))).borrowName = name;
		}

	}

	@Override
	public boolean compare(String kwd) {//kwd가 유저 이름인지
		if(kwd.equals(name)) return true;
		return false;
	}

	@Override
	public void print() {
		System.out.printf("%s - ", name);
		for (Integer index : borrowBooks) {
			System.out.print(((Book) (BookDemo.bookMgr.stList.get(index))).title + " / ");
		}
		System.out.println(borrowBooks.size() + "권");

	}

}
