개별보고서
첫 팀 과제이다보니 어떻게 분담해야하나 해메는 부분이 조금 있긴 했는데 하다 보니 처음치고는 잘 되었다고 생각합니다. 모든것을 혼자서 했으면 오류검사도, 세세한 부분 하나하나 짜는것까지 혼자해야 해서 오래걸렸을 텐데, 팀원 분들과 같이하니까 생각한 것보다는 더 과제를 하는데 수월했습니다.

저는 팀에서 기초적인 클래스와 인터페이스의 틀을 짜고 구조를 조절하는 역할을 맡았습니다.

프로그램이 정상적으로 돌아가는 것은 만족스러웠습니다. 하지만 교수님이 주신 class diagram에 따라 만들지는 못해서 구조가 조금 엉망이 된거 같아 많이 아쉬웠습니다.