package book;
//메인클래스.  Great,Word클래스도 여기 안에 포함되어 있음
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import Manager.Factory;
import Manager.Managable;
import Manager.Manager;
import Question.QuestionType;
import Question.Questionable;
import Question.QuizManager;

public class BookDemo implements Factory {
	public static QuizManager qMgr;
	public static Manager greatMgr;
	public static Manager wordMgr;
	Scanner scan = new Scanner(System.in);

	public Managable create(String fileName) {//create함수하나
		if (fileName.equals("great.txt"))
			return new GreatMan();
		else
			return new Words();
	}

	public static void main(String[] args) {//메인
		BookDemo demo = new BookDemo();
		demo.doit();
	}

	private void doit() {
		
		qMgr=new QuizManager(scan);

		//위인들 생성, 입력받기
		greatMgr = new Manager();
		System.out.println("ss");
		greatMgr.readAll("great.txt", this);
		System.out.println("ee");
		//워드들 생성, 입력받기
		wordMgr = new Manager();
		wordMgr.readAll("word.txt", this);
		//둘다 프린트
		greatMgr.printAll();
		wordMgr.printAll();
		function();
		System.out.println("---끝---");
	}

	void function() {
		int kind,qType;
		while (true) {//메인 돌아가는 곳
			System.out.println("(1) 위인   (2) 단어   (0) 종료");
			kind = scan.nextInt();
			if(kind==0) break;
			scan.nextLine();
			System.out.println("(1) 단답   (2)선택   (3)밑줄잔답   (4)밑줄선택   (0)종료");
			qType=scan.nextInt();
			if(qType==0) break;
			if(kind==1) qMgr.init(greatMgr, QuestionType.get(qType));
			else if(kind==2) qMgr.init(wordMgr, QuestionType.get(qType));
			qMgr.startQuiz();//퀴즈 시작
		}
	}

}

class GreatMan implements Questionable {
	
	
	static int indexPos=0;
	int index;
	String name;
	int maleFemale;
	String []MF=new String[2];
	String []isBC=new String[2];
	int birthDay,deathDay;
	String moreInfo="";
	String []moreInfoSplit;

	String firstInfo;//첫번째줄이 출력될때 나올 것
	String firstLineInfo="";//첫번째줄의 정보
	
	public GreatMan() {
		// TODO Auto-generated constructor stub
		MF[0]="[남]";
		MF[1]="[여]";
		isBC[0]="";
		isBC[1]="";
		index=indexPos++;
	}

	public void read(Scanner scan) {//입력받기
		
		String oneLine=scan.nextLine();//첫번쨰 줄 입력받기
		//System.out.println(oneLine);
		String []sp=oneLine.split(" ");//뛰어쓰기로 쪼개서 저장
		name=sp[0];
		String readMF=sp[1];
		if(readMF.charAt(0)=='M') maleFemale=0;
		else maleFemale=1;
		birthDay=Integer.parseInt(sp[2]);
		deathDay=Integer.parseInt(sp[3]);
		String moreInfoForSplit="";
		
		firstLineInfo+="["+readMF+"]";
		for(int i=4;i<sp.length;i++)
		{
			firstLineInfo+=sp[i]+" ";
		}
		
		moreInfo=scan.nextLine();

		if(birthDay<0){
			birthDay*=-1;
			isBC[0]="BC";
		}
		if(deathDay<0)
		{
			deathDay*=-1;
			isBC[0]="BC";
		}
		//나중에 한 위인의 정보출력 할 때 첫번째 줄
		firstInfo=String.format(Locale.KOREA, "%s %s %s%d ~ %s%d년%n",name,MF[maleFemale],isBC[0],birthDay,isBC[1],deathDay);
		//업적들 검색용으로 단어별로 나누기
		moreInfoSplit=moreInfoForSplit.split(" ");

	}

	public void print() {//프린트
		System.out.print(firstInfo);
		System.out.println(moreInfo);
	}

	@Override
	public boolean compare(String kwd) {//kwd가 writer이거나 제목 안에 포함되어 있는지
		if (kwd.equals(name))
			return true;
		if (moreInfo.contains(kwd))
			return true;

		return false;
	}

	@Override
	public String getQuestionText(QuestionType qType) {//질문 텍스트 받기
		
		String result;
		
		//단답,선택(firstLineInfo하고 비슷)
		if(qType==QuestionType.WRITE||qType==QuestionType.SELECT)
			result=firstLineInfo;
		else//아니면 moreInfo
			result=moreInfo;
		// TODO Auto-generated method stub
		return result;
	}

	@Override
	public String getAnswer() {//밑줄단답,밑줄선택(moreInfo)
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getName() {//Manager상태로 얻어와야 할 때도 있어서 굳이 만들어줌
		return name;
	}

	@Override
	public int getIndex() {
		return index;
	}
}

class Words implements Questionable {

	static int indexPos=0;
	int index;
	
	String wordClass;//형용사 동사 부사 등등
	String word,mean;
	String example;
	
	public Words()
	{
		index=indexPos++;
	}

	@Override
	public void read(Scanner s) {
		word=s.nextLine();
		//System.out.println(word);
		wordClass=s.next();
		mean=s.nextLine();
		//System.out.println(mean);
		example=s.nextLine();
		//System.out.println(example);
		if(s.hasNext()) s.nextLine();
		//System.out.println("---------");
		

	}

	@Override
	public boolean compare(String kwd) {
		if(kwd.equals(word)) return true;
		return false;
	}

	@Override
	public void print() {
		System.out.println(word);
		System.out.println("("+wordClass+")"+mean);
		System.out.println(example+"\n");
	}

	
	@Override
	public String getQuestionText(QuestionType qType) {
		String result;
		
		//단답,선택(mean)
		if(qType==QuestionType.WRITE||qType==QuestionType.SELECT)
			result="("+wordClass+")   "+mean;
		else//아니면 example
			result=example;
		// TODO Auto-generated method stub
		return result;
	}

	@Override
	public String getAnswer() {//답을 얻어오는 것
		return word;
	}

	@Override
	public String getName() {//Manager상태로 얻어와야 할 때도 있어서 굳이 만들어줌
		return word;
	}

	@Override
	public int getIndex() {
		return index;
	}

}
