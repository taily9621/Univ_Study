package Question;
import Manager.Managable;

public interface Questionable extends Managable{
	public String getQuestionText(QuestionType qType);//문제 문자열 얻어오기
	public String getAnswer();//답 얻어오기
}
