package Question;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import Manager.Managable;
import Manager.Manager;

public class QuizManager{//매니저에서 가져와 쓸 것이 없음  그냥 메인클래스하고 비슷
	Scanner sc;
	int questionKind;//위인문제인지 단어문제인지
	QuestionType questionType;//단답 선택 밑줄단답 밑줄선택 중 뭐인지
	
	ArrayList<Question> qList;
	
	
	Manager myMgr;//word인지 great인지 모르는 전체 목록들
	Random rd;
	
	
	public QuizManager(Scanner scan)//생성자
	{
		sc=scan;
		rd=new Random();
		qList=new ArrayList<>();
	}
	
	public void init(Manager infoMgr, QuestionType qType)
	{
		myMgr=infoMgr;
		questionType=qType;
		sc.nextLine();
	}
	
	public void startQuiz()
	{
		String inputText;
		for(int i=0;i<5;i++)
		{
			makeNewQuestion();//퀴즈 만들기
			showQuestion(i);//퀴즈 보여주기
			System.out.print("답:");
			inputText=sc.next();

			Question quest=qList.get(i);//qList에 저장하는 이유는 퀴즈 5문제가 끝나면 5문제전부 맞고 틀린것 결과를 보여주기 위해서
			if(questionType==QuestionType.SELECT||questionType==QuestionType.UNDERLINESELECT)//선택지 문제이면
				quest.check(Integer.parseInt(inputText));//숫자입력받음
			else
				quest.check(inputText);//아니면 그냥 스트링으로
		}
		showResult();//결과 보여주기
		qList.removeAll(qList);//5문제 결과까지 보여준 다음에는 있던 qList목록을 지움
	}
	
	void showQuestion(int index)//
	{
		Question quest=qList.get(index);
		quest.printQuestion();
	}
	void makeNewQuestion()//새로운 문제 추가
	{
		Question quest=new Question();
		
		int quizIndex;
		while (true)//같은 퀴즈 있나 체크
		{
			quizIndex=rd.nextInt(myMgr.stList.size());
			if(isExistQuiz(quizIndex)==false) break;
		}		
		Questionable mgable=(Questionable)myMgr.stList.get(quizIndex);//퀴즈를 낼 수 있는 내용을 가지고 있음.(questionKind같은건 안가지고있음)
		
		quest.initQuestion(questionKind, mgable ,questionType, myMgr);//퀴즈 종류(위인인지 단어인지), 퀴즈 내용, 퀴즈 타입, 위인이든 단어이든 상관없고 퀴즈를 낼 수 있는 내용읨 목록 
		qList.add(quest);
	}
	void showResult()
	{
		int correctCnt=0;//맞은 문제 개수
		Question quest;
		String answer;
		for(int i=0;i<5;i++)
		{
			quest=qList.get(i);
			System.out.print("["+(i+1)+"]");
			if(quest.correct){
				System.out.println(" O");
				correctCnt++;
			}
			else
			{
				quest.printQuestion();//틀린 문제 출력해주기
				if(questionType==QuestionType.SELECT||questionType==QuestionType.UNDERLINESELECT)//선택지면
					answer=Integer.toString(quest.ansIdx+1);//답이었던 번호출력
				else
					answer=quest.question.getAnswer();//아니면 답스트링을 출력
				System.out.println("  =>정답은 "+answer);
			}
		}
		System.out.println("*** 5 문제 중 "+correctCnt+"개 맞췄습니다.\n");
	}
	boolean isExistQuiz(int index)//지금 qList에 index번째의 문제가 있는가
	{
		for(Question quest : qList)
		{
			if(quest.question.getIndex()==index) return true;
		}
		return false;
	}
}
