package Manager;

import java.util.Scanner;

public interface Managable { 
	void print();            
	void read(Scanner s);
	boolean compare(String kwd);
	String getName();
	int getIndex();//인덱스 얻어오기
}

