package Question;

import java.util.Random;

import Manager.Manager;

public class Question {
	int type;//문제 타입
	//int quizIndex;//몇번째 위인이나 단어에 있는 문제인지(몇번째 문제인지 아님)
	Questionable question;//문제(위인,단어 클래스 그 자체)
	String questionText;//문제 텍스트
	Manager myMgr;//word인지 great인지 모름
	
	QuestionType qType;

	boolean correct;//틀렸나 맞았나
	
	int ansIdx;//정답 번호
	
	String []choiceSentence;//선택지 4개
	
	void initQuestion(int type_, Questionable question_, QuestionType qType, Manager myMgr)
	{
		type=type_;
		question=question_;
		this.qType=qType;
		questionText=question.getQuestionText(qType);
		questionText+="\n";
		if(qType==QuestionType.UNDERLINESELECT||qType==QuestionType.UNDERLINEWRITE)//문제가 밑줄있는 문제면
		{
			questionText=questionText.replaceAll(question.getAnswer() , "___");
		}
		if(qType==QuestionType.SELECT||qType==QuestionType.UNDERLINESELECT)//문제가 선택형 문제면
		{
			
			Random rd= new Random();
			ansIdx=rd.nextInt(4);//1,2,3,4번중에 답이 무엇인지 (0~3)
			choiceSentence=new String[4];//선택지 문자 4개
			int id;
			for(int i=0;i<4;i++)
			{
				if(i==ansIdx)//이번꺼가 답이면
					choiceSentence[i] = question.getAnswer();

				else {
					id = rd.nextInt(myMgr.stList.size());
					// 랜덤으로 뽑혀나온 오답이랑 정답이랑 똑같으면
					if (id == question.getIndex() || //문제의 답이면  (문제 답은 ansIdx 위치에 넣어줘야 하고 그것은 위의 if문이 해줌)
							isExistInChoice(myMgr.stList.get(id).getName(), i))//0~i-1번째에 이미있던 보기가 또나오는것 방지
					{
						i--;
						continue;
					}
					choiceSentence[i] = myMgr.stList.get(id).getName();
				}
				questionText+=(i+1)+")"+choiceSentence[i]+"  ";
			}
			questionText+="\n";
		}

	}
	
	public void printQuestion()
	{
		System.out.print(questionText);
	}
	
	
	
	public void check(String inputText)//입력받은것
	{
		if(inputText.equals( question.getAnswer())  ){
			correct=true;
			System.out.println("정답입니다.\n");
		}
		else{
			correct=false;
				System.out.println("틀렸습니다. 정답은 "+question.getAnswer()+"입니다.\n");
		
		}
	}
	public void check(int inputInt)//입력받은것
	{
		if(inputInt==ansIdx+1){
			correct=true;
			System.out.println("정답입니다.\n");
		}
		else{
			correct=false;
			System.out.println("틀렸습니다. 정답은 "+(ansIdx+1)+"입니다.\n");		
		}
	}
	boolean isExistInChoice(String quizStr, int k)
	{
		for(int i=0;i<k;i++)
		{
			if(choiceSentence[i].equals(quizStr))
				return true;
		}
		return false;
	}
	

}
