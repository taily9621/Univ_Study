package call;
import java.util.*;
import manager.*;

public class CallDemo extends Manager implements Factory
{
	public static void main(String[] args)
	{
		CallDemo demo = new CallDemo();
		demo.doit();
	}
	
	void doit()
	{
		readAll("calls.txt", this);
		printAll();
		//search();
	}
	
	public void readAll(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Managable obj = null;
		
		while (fileIn.hasNext()) {
			obj = fac.create();
			obj.read(fileIn);
			objList.add(obj);
		}
		
		fileIn.close();
	}
	
	void search() {
		Scanner in = new Scanner(System.in);
		while (true) {
			System.out.printf("검색 키워드 : ");
			String kwd = in.nextLine();
			kwd = kwd.trim();
			if (kwd.equals("0"))
				break;
			else {
				for (Managable m : objList) {
					if (m.equals(kwd))
						m.print();
				}
			}
		}
	}

	@Override
	public Managable create() {
		// TODO Auto-generated method stub
		return new Call();
	}
}
