package manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import call.Call;

public class Manager{
	protected ArrayList<Managable> objList = new ArrayList<>();
	
	protected Scanner openFile(String fileName) {
		Scanner fileIn = null;
		File f = new File(fileName);
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	public void readAll(String fileName, Factory factory) {
		Scanner fileIn = openFile(fileName);
		Managable obj = null;
		fileIn.nextLine();
		
		while (fileIn.hasNext()) {
			obj = factory.create();
			obj.read(fileIn);
			objList.add(obj);
		}
		
		fileIn.close();
	}
	
	public ArrayList<Managable> findObjects(String kwd) {
		ArrayList<Managable> foundedObjList = new ArrayList<>();
		
		for (Managable obj : objList) {
			if (obj.compare(kwd)) 
				foundedObjList.add(obj);
		}
		return foundedObjList;
	}
	public void printAll() 
	{
		int cnt = 1;
		
		for (Managable obj : objList)
		{
			if(cnt < 10)
				System.out.printf("[ %d] ",cnt);
			else
				System.out.printf("[%d] ",cnt);
			obj.print();
			cnt+=1;
		}
	}
	
	protected Managable find(String kwd) {
		for (Managable m : objList)
			if (m.compare(kwd))
				return m;
		return null;
	}
}
