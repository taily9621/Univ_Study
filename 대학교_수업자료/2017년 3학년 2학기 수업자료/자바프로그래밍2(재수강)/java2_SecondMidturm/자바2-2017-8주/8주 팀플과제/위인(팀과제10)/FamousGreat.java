package heroSuch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

class FamousGreat extends Great {
	ArrayList<String> dramaList = new ArrayList<>();
	int countDrama;
	ArrayList<String> movieList = new ArrayList<>();
	int countMovie;

	FamousGreat(String name) {
		super(name);
	}

	public void read(Scanner in) {
		super.read(in);
		countDrama = countMovie = 0;
		String line = in.nextLine().trim();
		String Lines[] = line.split(" ");
		for (int i = 0; i < Lines.length; i += 2) {
			if (Lines[i].equals("�쁺�솕")) {
				movieList.add(Lines[i + 1]);
				countMovie++;
			} else if (Lines[i].equals("�뱶�씪留�")) {
				dramaList.add(Lines[i + 1]);
				countDrama++;
			}
		}
	}

	public void print() {
		super.print();
		printPlus();
	}
	
	void printPlus() {
		if(countDrama != 0 | countMovie != 0)
			System.out.printf("++ ");
		if (countDrama != 0)
			printDrama();
		if (countMovie != 0)
			printMovie();
		System.out.printf("\n");
	}
	
	void printDrama() {
		System.out.printf("�뱶�씪留� ");
		for (String drama : dramaList)
			System.out.printf("%s ", drama);	
	}

	void printMovie() {
		System.out.printf("�쁺�솕 ");
		for (String movie : movieList)
			System.out.printf("%s ", movie);
	}
	
	public void printMatch(MatchType t, String kwd) {
		super.printMatch(t,kwd);
		printPlus();
	}
}
