import java.util.Scanner;

class Great implements Managable {
	String name;
	boolean gender;
	Dinasty dinasty;
	String greatType;
	int birth;
	int death;
	String work;

	Great() {

	}

	Great(String name) {
		this.name = name;
	}

	public void read(Scanner in) {
		System.out.printf("%s \n", name);
		gender = in.next().equals("M");
		int tmp = in.nextInt();
		dinasty = Dinasty.get(tmp);
		greatType = in.next();
		birth = in.nextInt();
		death = in.nextInt();
		work = in.nextLine();
		work = work.trim();
	}

	public void print() {
		System.out.printf("%-5s\t%s\t%4s ~ %4s %-8s\t%-3s\t%s\n", name, gender ? "" : "[F]",
				birth > 0 ? birth : "BC" + (birth * (-1)), death > 0 ? death : "BC" + (death * (-1)), dinasty.getName(),
				greatType, work);
	}

	/*
	 * void printMovie() {
	 * 
	 * }
	 * 
	 * void printDrama() {
	 * 
	 * }
	 */
	public boolean compare(String kwd) {
		if (kwd.equals(name))
			return true;
		else if (kwd.equals("M"))
			return true;
		else if (kwd.equals("F"))
			return true;
		else if (kwd.equals(dinasty.getName()))
			return true;
		else if (kwd.equals(greatType))
			return true;
		else if (GreatDemo.isNumber(kwd)) {
			int alive = Integer.parseInt(kwd);
			if (alive >= birth && alive <= death)
				return true;
			else
				return false;
		} else if (work.contains(kwd))
			return true;
		else
			return false;
	}

	public MatchType match(String kwd) {
		MatchType matchType = MatchType.None;
		if (name.equals(kwd))
			matchType = MatchType.Name;
		else if (kwd.equals("M") && gender)
			matchType = MatchType.Gender;
		else if (kwd.equals("F") && !gender)
			matchType = MatchType.Gender;
		else if (dinasty.getName().equals(kwd))
			matchType = MatchType.Age;
		else if (greatType.equals(kwd))
			matchType = MatchType.Type;
		else if (GreatDemo.isNumber(kwd))
			if (Integer.parseInt(kwd) >= birth && Integer.parseInt(kwd) <= death)
				matchType = MatchType.Year;
			else if (work.contains(kwd))
				matchType = MatchType.Work;
		return matchType;
	}

	public void printMatch(MatchType t, String kwd) {
		if (t == MatchType.None)
			return;

		if (t == MatchType.Name) // 이름
			System.out.printf("%-10s", "이 위인은");
		else
			System.out.printf("%s\t", name);

		if (t != MatchType.Gender && !gender) // 성별
			System.out.printf("[F]\t");
		else
			System.out.printf("   \t");

		System.out.printf("%4s ~ %4s", birth > 0 ? birth : "BC" + (birth * (-1)),
				death > 0 ? death : "BC" + (death * (-1)));

		if (t != MatchType.Age) // 시대
			System.out.printf(" %s", dinasty.getName());
		else
			System.out.print("");

		if (t != MatchType.Type) // Great타입
			System.out.printf("\t\t%-3s\t", greatType);
		else
			System.out.print("\t\t\t");

		if (t != MatchType.Work) // 업적
			System.out.println(work);
		else {
			StringBuilder sb = new StringBuilder(work);
			int index = work.indexOf(kwd);
			int length = kwd.length();
			sb.insert(index, "<<");
			sb.insert(index + 2 + length, ">>");
			System.out.println(sb);
		}
	}
}
