package great;
import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import manager.Manager;

public class GreatDemo extends Manager implements Factory {
	public Managable create() {
		return new Great();
	}

	public void doit() {
		readAll("great-inherit.txt", this);
		printAll();
		search();
	}

	protected void readAll(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			String temp = fileIn.next();
			if (temp.equals("m")) {
				temp = fileIn.next();
				s = new FamousGreat(temp);
			} else
				s = new Great(temp);
			s.read(fileIn);
			gList.add(s);
		}
		fileIn.close();
	}
	
	static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	void search() {
		Scanner in = new Scanner(System.in);
		while (true) {
			System.out.printf("검색어를 입력하세요..(종료 y) : ");
			String kwd = in.nextLine();
			kwd = kwd.trim();
			if (kwd.equals("y"))
				break;
			else {
				for (Managable m : gList) {
					MatchType mt = ((Great) m).match(kwd);
					if (m.compare(kwd))
						((Great) m).printMatch(mt, kwd);
				}
			}
		}
	}

	public static void main(String[] args) {
		GreatDemo demo = new GreatDemo();
		demo.doit();
	}
}
