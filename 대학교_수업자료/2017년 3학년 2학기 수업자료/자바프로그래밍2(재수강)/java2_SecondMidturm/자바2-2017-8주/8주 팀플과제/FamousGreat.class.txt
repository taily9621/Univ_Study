package great;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

class FamousGreat extends Great {
	ArrayList<String> dramaList = new ArrayList<>();
	int countDrama;
	ArrayList<String> movieList = new ArrayList<>();
	int countMovie;

	FamousGreat(String name) {
		super(name);
	}

	public void read(Scanner in) {
		super.read(in);
		countDrama = countMovie = 0;
		String line = in.nextLine().trim();
		String Lines[] = line.split(" ");
		for (int i = 0; i < Lines.length; i += 2) {
			if (Lines[i].equals("영화")) {
				movieList.add(Lines[i + 1]);
				countMovie++;
			} else if (Lines[i].equals("드라마")) {
				dramaList.add(Lines[i + 1]);
				countDrama++;
			}
		}
	}

	public void print() {
		super.print();
		printPlus();
	}

	void printPlus() {
		if (countDrama != 0 | countMovie != 0) {
			System.out.printf("\t\t\t\t\t++ ");
			if (countDrama != 0)
				printDrama();
			if (countMovie != 0)
				printMovie();
		}
		System.out.printf("\n");
	}

	void printDrama() {
		System.out.printf("드라마 ");
		for (String drama : dramaList)
			System.out.printf("%s ", drama);
	}

	void printMovie() {
		System.out.printf("영화 ");
		for (String movie : movieList)
			System.out.printf("%s ", movie);
	}

	public void printMatch(MatchType t, String kwd) {
		super.printMatch(t, kwd);
		if (t != MatchType.None) {
			if (t == MatchType.Drama || t == MatchType.Movie) { // 검색한 kwd가 드라마나 영화에 들어있을 경우
				System.out.printf("\t\t\t\t\t++ ");
				if (t == MatchType.Drama) {
					System.out.print("드라마 ");
					for (String d : dramaList) {
						StringBuilder sb = new StringBuilder(d);
						if (d.contains(kwd)) {
							int index = d.indexOf(kwd);
							int length = kwd.length();
							sb.insert(index, "<<");
							sb.insert(index + 2 + length, ">>");
						}
						System.out.print(sb + " ");
					}
					if (countMovie != 0)
						printMovie();
				} else if (t == MatchType.Movie) {
						System.out.print("영화 ");
						for (String m : movieList) {
							StringBuilder sb = new StringBuilder(m);
							if (m.contains(kwd)) {
								int index = m.indexOf(kwd);
								int length = kwd.length();
								sb.insert(index, "<<");
								sb.insert(index + 2 + length, ">>");
							}
							System.out.print(sb + " ");
						}
				}
				System.out.println();
			} else
				printPlus();
		}
	}

	public boolean compare(String kwd) {
		boolean tmp = super.compare(kwd);
		for (String d : dramaList)
			if (d.contains(kwd))
				return true;
		for (String m : movieList)
			if (m.contains(kwd))
				return true;
		return tmp;
	}

	public MatchType match(String kwd) {
		MatchType matchType = super.match(kwd);
		if(matchType!=MatchType.None)
			return matchType;
		for (String d : dramaList)
			if (d.contains(kwd))
				return MatchType.Drama;
		for (String m : movieList) {
			if (m.contains(kwd))
				return MatchType.Movie;
		}
		return matchType;
	}
}
