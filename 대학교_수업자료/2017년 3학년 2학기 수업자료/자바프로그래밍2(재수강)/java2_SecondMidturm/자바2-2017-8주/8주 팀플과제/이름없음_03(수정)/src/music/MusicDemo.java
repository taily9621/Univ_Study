package music;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import manager.Manager;

public class MusicDemo extends Manager implements Factory {
	//static ArrayList<Music> musicList = new ArrayList<>();
	ArrayList<User> userList = new ArrayList<>();
	static HashMap<Integer, String> musicMap = new HashMap<>();
	Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		MusicDemo demo = new MusicDemo();
		demo.doit();
	}

	private void doit() {
		readAllMusic("music.txt", this);
		readAllUser("link.txt", this);
		menu();
	}

	protected void readAllMusic(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Managable s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			s = new Music();
			s.read(fileIn);
			gList.add(s);
		}
		fileIn.close();
	}
	
	protected void readAllUser(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		User s = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			s = new User();
			s.read(fileIn);
			userList.add(s);
		}
		fileIn.close();
	}
	
//	private void readAllUser() {
//		Scanner fileIn = openFile("link.txt");
//		User u = null;
//		String trash = fileIn.nextLine();
//		while (fileIn.hasNext()) {
//			u = new User();
//			u.read(fileIn);
//			userList.add(u);
//
//		}
//		fileIn.close();
//	}
//
//	void readAllMusic() {
//		Scanner fileIn = openFile("music.txt");
//		Music m = null;
//
//		String trash = fileIn.nextLine();
//		while (fileIn.hasNext()) {
//			m = new Music();
//			m.read(fileIn);
//			musicList.add(m);
//		}
//		fileIn.close();
//
//	}

	/*void printMusicList() {
		for (Music m : musicList)
			m.print();
	}*/

	void printUserList() {
		for (User u : userList)
			u.print();
	}

	private void searchUser() {
		System.out.print("검색어 :");
		String kwd = s.next();
		Music m = (Music) findMusic(kwd);
		System.out.print(m.musicName+"링크 유저: ");
		for (User u : userList)
			if (u.listen(m))
				System.out.print( u.name + " ");
		System.out.println();
	}

	private void searchMusic() {
		System.out.print("검색어 :");
		String kwd = s.next();
		for (Managable m : gList)
			if (((Music)m).musicName.contains(kwd))
				m.print();

	}

	private void menu() {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int select = 0;
		while (true) {
			System.out.print("(1) 전체곡목록 (2) 전체유저 (3) 곡목검색 "
					+ "(4)곡목별유저 (5) 판매현황 (0) 종료\n");
			System.out.print("select >>");
			select = scan.nextInt();
			switch (select) {
			case 1:
				printAll();
				break;
			case 2:
				printUserList();
				break;
			case 3:
				searchMusic();
				break;
			case 4:
				searchUser();
				break;
			case 5:
				checkSale();
				break;
			case 0:
				return;
			}
		}

	}

	private void checkSale() {
		int maxcnt = 0;
		for (Managable m : gList) {
			for (User u : userList){
				if (u.listen((Music) m))
					((Music)m).cnt++;
				if(((Music)m).cnt>maxcnt)
					maxcnt = ((Music)m).cnt;
			}
		}
		for (Managable m : gList) 
			if (((Music)m).cnt > 0)
				((Music)m).printSales();
		for(Managable m: gList)
			if(((Music)m).cnt == maxcnt){
				System.out.print("Best Seller Music [ "+maxcnt+"명 ] :");
				m.print();
			}
		

	}

	public static Music findMusic(int code) {
		for (Managable m : gList)
			if (code == ((Music) m).musicNum)
				return ((Music) m);
		return null;
	}

	Managable findMusic(String kwd) {
		for (Managable m : gList)
			if (((Music) m).compare(kwd))
				return m;
		return null;
	}

	@Override
	public Managable create() {
		return null;
	}
}