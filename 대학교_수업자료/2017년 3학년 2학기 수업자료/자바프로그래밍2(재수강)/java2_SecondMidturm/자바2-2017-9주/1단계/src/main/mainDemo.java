package main;
import java.util.*;
import manager.*;
public class mainDemo extends Manager implements Factory
{
	static ArrayList<Great> greatList = new ArrayList<>();
	ArrayList<Word> wordList = new ArrayList<>();
	Scanner s = new Scanner(System.in);
	
	public Managable create()
	{
		return new Great();
	}
	public static void main(String[] args)
	{
		mainDemo demo = new mainDemo();
		demo.doit();
	}
	
	public void doit()
	{
		readAll("great.txt", this);
		readAll("word.txt", this);
		menu();
	}
	
	public void readAll(String fileName, Factory fac) // 텍스트 파일 읽기 메소드
	{
		Scanner fileIn = openFile(fileName); // manager 패키지의 Manager 클래스에 있는 openFile() 메소드 실행
		Managable s = null;
		
		while (fileIn.hasNext()) {
			if (fileName.equals("great.txt")) {
			// 구현된 Managable s 를 영화나 드라마에 나온 위인이면(m으로 표식) FamousGreat 클래스로 인스턴스 아닐 경우 Great()클래스로 인스턴스 함
				s = new Great(); // 영화나 드라마에 나온 위인일때
				s.read(fileIn);
				greatList.add((Great)s);
			}else {
				s = new Word(); // 일반 위인일때
				s.read(fileIn); // 해당 클래스의 read() 메소드 실행
				wordList.add((Word)s);
			}
			objList.add(s); // manager 패키지의 Manager 클래스에 있는 리스트 gList에 추가 
		}
		fileIn.close(); // 파일읽기 종료
	}
	
	private void menu() // 실행 메뉴
	{
		// TODO Auto-generated method stub
		int select = 0;

		while (true) {
			System.out.println("(1) 전체 위인 출력  (2) 위인 검색 (3) 전체 단어 출력 (4) 단어 검색 (0) 종료");
			System.out.print("메뉴 선택 : ");
			select = s.nextInt();
			switch (select) {
			case 1:
				printAllGreat();
				break;
			case 2:
				searchGreat();
				break;
			case 3:
				printAllWord();
				break;
			case 4:
				searchWord();
				break;
			}
		}

	}
	
	private void printAllGreat() // 전체 위인 리스트 출력 메소드
	{
		// TODO Auto-generated method stub
		for (Great g : greatList)
			g.print();
	}

	private void printAllWord() // 전체 단어 리스트 출력 메소드
	{
		for (Word w : wordList)
			w.print();
	}
	
	private void searchGreat() // 위인 검색 메소드
	{
		// TODO Auto-generated method stub
		System.out.print("책검색 키워드 :");
		String kwd = s.next();
		for (Great b : greatList)
			if (b.compare(kwd))
				b.print();
		System.out.println();
	}

	private void searchWord() // 단어 검색 메소드
	{
		// TODO Auto-generated method stub
		System.out.print("유저검색 키워드 :");
		String kwd = s.next();
		for (Word p : wordList)
			if (p.compare(kwd))
				p.print();
		System.out.println();
	}
}
