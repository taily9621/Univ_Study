package manager;

import java.util.Scanner;

public interface Managable {
	public void read(Scanner scanner);
	public void print();
	
	public boolean compare(String kwd);
}
