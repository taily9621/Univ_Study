package manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
	protected ArrayList<Managable> objList = new ArrayList<>();
	
	protected Scanner openFile(String fileName) {
		Scanner fileIn = null;
		File f = new File(fileName);
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}

	public void readAll(String fileName, Factory factory) {
		Scanner fileIn = openFile(fileName);
		Managable obj = null;
		
		while (fileIn.hasNext()) {
			obj = factory.create();
			obj.read(fileIn);
			objList.add(obj);
		}
		
		fileIn.close();
	}
	
	public ArrayList<Managable> findObjects(String kwd) {
		ArrayList<Managable> foundedObjList = new ArrayList<>();
		
		for (Managable obj : objList) {
			if (obj.compare(kwd)) 
				foundedObjList.add(obj);
		}
		return foundedObjList;
	}
	public void printAll() {
		for (Managable obj : objList)
			obj.print();
	}
}
