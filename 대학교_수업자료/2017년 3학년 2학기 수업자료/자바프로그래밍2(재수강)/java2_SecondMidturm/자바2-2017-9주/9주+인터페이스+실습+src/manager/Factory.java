package manager;

public interface Factory {
	Managable create();
}