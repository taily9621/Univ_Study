package quiz;

import java.util.Scanner;

import contents.Great;
import manager.Managable;

public class Question{
	protected int type;
	protected Great answer;
	protected String questionText;
	protected boolean bCorrect = false;
	
	public void initQuestion(QuestionType type, Great ans) {
		answer = ans;
		
		if (type == QuestionType.Write)
			questionText = answer.getQuestionText();
		else if (type == QuestionType.UnderlineWrite)
			questionText = answer.getSentence();
	}
	
	public void showQuestion(Scanner sc) {
		System.out.println(questionText);
		check(sc);
		System.out.println("");
	}
	public void check(Scanner sc) {
		System.out.print("   답 : ");
		if (answer.getAnswer().equals(sc.next())) {
			bCorrect = true;
			System.out.println("정답! 맞았습니다.");
		}
		else
			System.out.printf("틀렸습니다. 정답은 %s입니다.\n", answer.getAnswer());
	}
	public void print() {
		if (bCorrect)
			System.out.println("O");
		else {
			System.out.println(questionText);
			System.out.printf("  => 정답은 %s입니다.\n", answer.getAnswer());
		}
	}
	
	public Great getAnswer() { return answer; }
	public boolean checkCorrected() { return bCorrect; }
}

