package quiz;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import contents.Great;
import manager.*;

public class QuizManager extends Manager{
	private ArrayList<Question> list;
	private int numCorrect;
	private Scanner scan = new Scanner(System.in);
	
	public void runQuiz(Scanner scan) {
		this.scan = scan;
		QuestionType type = selectType();
		if (type == null) return ;
		
		numCorrect = 0;
		list = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			Question question = makeNewQuestion(type);
			
			question.showQuestion(scan);
			if (question.checkCorrected())
				numCorrect++;
			
			list.add(question);
		}
		
		showResult();
	}
	
	private QuestionType selectType() {
		System.out.println("(1) 단답   (2) 밑줄   (0) 종료");

		int choice;
		do {
			choice = scan.nextInt();
			scan.nextLine();
		} while (choice < 0 || choice > 2);
		
		return QuestionType.ofValue(choice);
	}
	private Question makeNewQuestion(QuestionType type) {
		Question question = null;
		Great answer = selectRandom();
		
		question = new Question();
		question.initQuestion(type, answer);
		
		return question;
	}
	private Great selectRandom() {
		Random rand = new Random();
		Great obj = null;
		
		select:
		while (true) {
			int listSize = rand.nextInt(objList.size());
			obj = (Great)objList.get(listSize);
			
			for (Question question : list) {
				if (obj == question.getAnswer())
					continue select;
			}
			
			break;
		}
		
		return obj;
	}
	
	private void showResult() {
		for (int i = 0; i < 5; i++) {
			System.out.printf("[%d] ", i + 1);
			list.get(i).print();
		}
		System.out.println("");
		System.out.printf("*** 5 문제 중 %d개 맞췄습니다.\n", numCorrect);
	}
}
