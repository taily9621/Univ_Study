package manager;
import java.util.Scanner;

public interface Managable {
	void read(Scanner scan);
	//boolean isValid();
	String getName();
	void print();
	boolean compare(int n);
	//boolean compare(String kwd);
	boolean compare(String kwd);
}
