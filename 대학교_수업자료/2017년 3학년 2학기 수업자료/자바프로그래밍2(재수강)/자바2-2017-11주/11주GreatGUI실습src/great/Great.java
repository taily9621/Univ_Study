package great;
import java.util.InputMismatchException;
import java.util.Scanner;

import manager.Managable;

public class Great implements Managable {
	private String name;
	private char gender;
	private int from;
	private int to;
	private String work;
	
	Great() {}
	Great(String name) {
		this.name = name;
	}
	@Override
	public void read(Scanner s)
	{
		try {
			name = s.next();
			gender = s.next().charAt(0);
			if (gender != 'M' && gender != 'F')
				throw(new InputMismatchException());
			from = s.nextInt();
			to = s.nextInt();
			work = s.nextLine().trim();
		} catch (InputMismatchException e) {
			System.err.println(name+"에서 입력 파일의 값이 잘못되었습니다.");
			s.nextLine();  // 한 줄 끝까지 읽기
			throw e;
		}
	}
	
	@Override
	public void print()
	{
		System.out.print(name + " [");
		System.out.println(gender + "] " + from + "~" + to + " " + work);
	}
	@Override
	public String toString()
	{
		String result = name + " [";
		result = result + gender + "] " + from + "~" + to + " " + work;
		return result;
	}
	@Override
	public boolean compare(String kwd)
	{
		return kwd.equals(name);
	}

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public boolean compare(int n) {
		return (from <= n && n <= to);
	}
	public void readOnLine(Scanner keyin) {
		System.out.println("이름 : " + name);
		System.out.print("성별(m/f/n) : ");
		String tmp = keyin.next();
		gender = tmp.charAt(0);
		System.out.print("생년  사망년 : ");
		from = keyin.nextInt();
		to = keyin.nextInt();
 		keyin.nextLine();
 		System.out.print("업적 : ");
    	work = keyin.nextLine().trim();	
    	System.out.println(name + " 추가됨");
	}
	public boolean checkRemove() {
		return true;
	}
	public void update(Scanner keyin) {
		//keyin.nextLine();
		System.out.printf("성별 (m/f/n, 엔터는 수정안함) [%s] : ", gender+"");
		String aLine = keyin.nextLine();
		if (aLine.trim().length() > 0) 
			gender = aLine.charAt(0);
		System.out.printf("생년  사망년 [%d %d], 엔터는 수정안함): ", from, to);
		aLine = keyin.nextLine().trim();
		if (aLine.length() > 0) {
			String[] tokens = aLine.split(" ");
			from = Integer.parseInt(tokens[0]);
			to = Integer.parseInt(tokens[1]);
		}
    	System.out.printf("업적 (엔터는 수정안함) [%s] : ", work);
    	aLine = keyin.nextLine().trim();
    	if (aLine.length() > 0)
			work = keyin.nextLine();		
		System.out.println(name + "수정 완료");
	}
	
}
