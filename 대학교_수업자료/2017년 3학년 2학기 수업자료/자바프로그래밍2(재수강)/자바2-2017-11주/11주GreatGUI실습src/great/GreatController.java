package great;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

import manager.Factory;
import manager.Manager;

public class GreatController implements Factory<Great>, Iterable<Great> {
	Scanner keyin = new Scanner(System.in);
	Manager<Great> mgr = null;
	public void init() {
		mgr = new Manager<Great>();
		mgr.readAll("great.txt",  this);
		mgr.printAll();
	}
	public Great create() {
		return new Great();
	}
	public Great readNew(String name) {
		Great g = new Great(name);
		g.readOnLine(keyin);
		return g;
	}
	public void update(Great g) {
		System.out.println(g.getName() + " ����...");		
		g.update(keyin);
		return;
	}
	static ArrayList<String> Names = new ArrayList<>();
	static {
		Names.add("��������");
		Names.add("�հ�");
		Names.add("�����ż�");
	}
	public String remove(Great g) {
		if (Names.contains(g.getName()))
			return g.getName() + "�� ���� �Ұ�";
		mgr.getList().remove(g);
		System.out.println(g.getName() + " ������.");
		return null;
	}
	@Override
	public Iterator<Great> iterator() {
		return mgr.getList().iterator();
	}
}
