package week11;

import java.util.Iterator;
import java.util.Scanner;

public class MyArray implements Iterable {
    Object[] values;  
    MyArray(Object[] arr) {
    	values = arr;
    }
    public Iterator iterator() {
        return new ArrayIterator();
    }
    class ArrayIterator implements Iterator {
        ...
    }
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
    	System.out.print("�Է� : ");
        String aLine = scan.nextLine();
        String wordArr[] = aLine.trim().split(" ");
        MyArray words 
		= new MyArray(wordArr);
        Iterator it = words.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}