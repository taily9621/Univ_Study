package manager;

import java.util.Iterator;

import great.Great;

public interface GreatControll {
	
	public void init();
	public Great create();
	public Great readNew(String name);
	public void update(Great g);
	public String remove(Great g);
	public Iterator<Great> iterator();
}
