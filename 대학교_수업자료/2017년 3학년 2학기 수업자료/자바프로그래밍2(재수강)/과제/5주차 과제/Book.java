package Library;
import java.io.*;
import java.util.*;

public class Book 
{
	Scanner scan = new Scanner(System.in);
	ArrayList<BookDemo> bookList = new ArrayList<>();
	ArrayList<UserDemo> personList = new ArrayList<>();
	
	public static void main(String[] args)
	{
		Book book = new Book();
		book.doit();
	}
	
	void doit()
	{
		bookfileRead();
		userfileRead();
		menu();
	}
	
	void menu()
	{
		while (true)
		{
			System.out.print("(1) 전체 책 출력  (2) 책제목 검색  (3) 전체 사용자 출력   (4) 사용자 검색 (5) 대출불가책 (6) 대출 (7) 반납  (0) 종료 ");
			int menu = scan.nextInt();
			if (menu == 0) break;
			switch (menu)
			{
				case 1:
					int cntB = 1;
					for(BookDemo bk : bookList) 
					{
						if(cntB < 10) System.out.printf("[ %d]",cntB);
						else System.out.printf("[%d]", cntB);
						bk.print();
						cntB+=1;
					}
					break;
				case 2:
					searchLecture();
					break;
				case 3:
					for(UserDemo ud : personList) ud.print();
					break;
				case 4: 
					searchUser();
					break;
				case 5:
					for(BookDemo BD : bookList)
						if(BD.booksupply <= 0) BD.print();
					break;
				case 6:
					borrowBook();
					break;
				case 7:
					returnBook();
					break;
				default:
					break;
			}
		}
	}
	
	void bookfileRead()
	{
		Scanner fileIn = fileOpen("book.txt");
		BookDemo bok = null;
		while(fileIn.hasNextLine())
		{
			bok = new BookDemo();
			bok.read(fileIn);
			if(bok.isBlank == false) 
			{
				//System.out.println("리스트 삽입");
				bookList.add(bok);
			}
		}
	}
	
	void userfileRead()
	{
		Scanner fileIn = fileOpen("person.txt");
		UserDemo Urd = null;
		while(fileIn.hasNextLine())
		{
			Urd = new UserDemo();
			Urd.read(fileIn);
			if(Urd.isBlank == false) 
			{
				//System.out.println("리스트 삽입");
				personList.add(Urd);
			}
		}
	}
	
	void searchLecture()
	{
		System.out.print("검색어 : ");
		String kwd = scan.next();
		for (BookDemo l : bookList) 
			if (l.compare(kwd))
				l.print();
	}
	
	void searchUser()
	{
		System.out.print("검색(학번 또는 이름) : ");
		String kwd = scan.next();
		for (UserDemo m : personList) 
			if (m.compare(kwd))
				m.print();
	}
	
	void borrowBook()
	{
		int cntborrow = 0;
		boolean isEnd = false;
		System.out.print("대출/학번 : ");
		String kwd = scan.next();
		for (UserDemo m : personList) 
			if (m.compare(kwd))
			{
				System.out.printf("%s님 안녕하세요?\n",m.name);
				while(true)
				{
					System.out.print("대출할 책 제목 일부 또는 isbn(끝내려면 end) : ");
					String serchkwd = scan.next();
					if(serchkwd.equals("end")) { isEnd = true; break; }
					for(BookDemo b : bookList)
					{
						if(b.compare(serchkwd))
						{
							if(b.booksupply <= 0) System.out.println("대출불가 책입니다...");
							else if(m.borrow.contains(b.bookname)) System.out.println("이미 대출한 책입니다...");
							else
							{
								b.booksupply -= 1;
								m.borrow.add(b.bookname);
								System.out.printf("%s 대출합니다.\n", b.bookname);
								cntborrow += 1;
							}
						}
					}
				}
				if(isEnd == true)
				{
					System.out.printf("%s님, %d권 대출하셨습니다. 안녕히 가세요.\n",m.name,cntborrow);
					System.out.println();
				}
			}
	}
	
	void returnBook()
	{
		System.out.print("반납/학번 : ");
		String rtnbook = scan.next();
		for(UserDemo m : personList)
		{
			if(m.compare(rtnbook))
			{
				System.out.printf("%s님, 안녕하세요? 반납하시겠습니까?\n", m.name);
				String answerRT = scan.next();
				if(answerRT.equals("y")||answerRT.equals("Y"))
				{
					if(m.borrow.size() <= 0)
						System.out.println("반납할 책이 없습니다...");
					else
					{
						for(String bList : m.borrow)
						{
							for(BookDemo b : bookList)
								if(b.bookname.equals(bList)) b.booksupply+=1;
							System.out.printf("%s 반납완료.\n", bList);
						}
						System.out.printf("%s님, %d권 반납하셨습니다.\n", m.name,m.borrow.size());
						System.out.println();
						m.borrow.clear();
					}
				}
				else 
				{
					System.out.println("안녕히 가세요...");
					System.out.println();
				}
			}
		}
	}
	
	static Scanner fileOpen(String filename) 
	{
		File f = new File(filename);
		Scanner fileIn = null;
		try 
		{
			fileIn = new Scanner(f);
		}catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
}

class BookDemo
{
	ArrayList<String> writer = new ArrayList<>();
	boolean isBlank = false;
	String receiveLine;
	//입력받은 책 정보
	String bookname;
	String publisher;
	int isbn = 0;
	int year = 0;
	String numbering;
	int booksupply = 0;
	String booktype;
	
	void read(Scanner in) 
	{
		boolean writerEndpoint = false;
		boolean isNumbering = false;
		boolean isSupply = false;
		boolean isBooktype = false;
		receiveLine = in.nextLine();
		
		if(receiveLine.equals("")) {
			isBlank = true;
			//System.out.println("공백 제거");
		} else
		{
			String[] splitLine = receiveLine.split(" ");
			isBlank = false;
			//System.out.println("파일입출력 확인 : "+receiveLine);
			for(int i = 0; i < splitLine.length; i++)
			{
				switch(i)
				{
					case 0:
						bookname = splitLine[i];
						break;
					case 1:
						publisher = splitLine[i];
						break;
					case 2:
						isbn = Integer.parseInt(splitLine[i]);
						break;
					case 3:
						year = Integer.parseInt(splitLine[i]);
						break;
					default:
						if(writerEndpoint == false)
						{
							if(splitLine[i].equals("0"))
								writerEndpoint = true;
							else
								writer.add(splitLine[i]);
						}
						else
						{
							if(isNumbering == false) { numbering = splitLine[i]; isNumbering = true; }
							else if(isSupply == false) { booksupply = Integer.parseInt(splitLine[i]); isSupply = true; }
							else if(isBooktype == false) { booktype = splitLine[i]; isBooktype = true; }
						}
						break;
				}
			}
		}
	}
	
	void print()
	{
		int cnt = writer.size();
		System.out.printf("%s %s [%d] %d년도,",bookname,publisher,isbn,year);
		for(String wtr : writer)
		{
			if(cnt <= 1) { System.out.printf(" %s",wtr); cnt -= 1;}
			else if(cnt >= 2) { System.out.printf(" %s,", wtr); cnt -= 1; }
		}
		System.out.printf(" 저. (%s: %s) - ", numbering,booktype);
		if(booksupply <= 0) System.out.print("대출불가");
		else System.out.printf("%d권 대출가능", booksupply);
		System.out.println();
	}
	
	boolean compare(String kwd) 
	{
		if(receiveLine.contains(kwd))
			return true;
		
		return false;
	}
}

class UserDemo
{
	ArrayList<String> borrow = new ArrayList<>();
	String receiveLine;
	boolean isBlank = false;
	boolean compareResult = false;
	//입력받은 유저 정보
	int studentNum = 0;
	String name;
	int age = 0;
	String major;
	int grade = 0;
	int birth = 0;
	boolean bMale;
	
	void read(Scanner in)
	{
		boolean writerEndpoint = false;
		
		receiveLine = in.nextLine();
		if(receiveLine.equals("")) {
			isBlank = true;
			//System.out.println("공백 제거");
		} else
		{
			String[] splitLine = receiveLine.split(" ");
			isBlank = false;
			//System.out.println("파일입출력 확인 : "+receiveLine);
			for(int i = 0; i < splitLine.length; i++)
			{
				switch(i)
				{
					case 0:
						studentNum = Integer.parseInt(splitLine[i]);
						break;
					case 1:
						name = splitLine[i];
						break;
					case 2:
						age = Integer.parseInt(splitLine[i]);
						break;
					case 3:
						major = splitLine[i];
						break;
					case 4:
						grade = Integer.parseInt(splitLine[i]);
						break;
					case 5:
						birth = Integer.parseInt(splitLine[i]);
						break;
					case 6:
						bMale = splitLine[i].equals("M");
						break;
					default:
						if(writerEndpoint == false)
						{
							if(splitLine[i].equals("0"))
								writerEndpoint = true;
							else
								borrow.add(splitLine[i]);
						}
						break;
				}
			}
		}
	}
	
	void print()
	{
			System.out.printf("[%d/%s] %d세, %s %d학년, %d, %s, ",studentNum,name,age,major,grade,birth,bMale?"M":"F");
			for(String brro : borrow)
				System.out.printf("%s / ",brro);
			System.out.printf("%d권",borrow.size());
			System.out.println();
	}
	
	boolean compare(String kwd) 
	{
		if (receiveLine.contains(kwd))
			return true;
		
		return false;
	}
}