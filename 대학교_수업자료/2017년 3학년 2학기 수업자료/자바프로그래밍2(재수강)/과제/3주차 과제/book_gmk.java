package javahomework;
import java.util.*;
import java.io.*;

public class book_gmk 
{
	String title;
	int isbn;
	int year;
	String publisher;
	String author[];
	
	public static void main(String[] args) 
	{
		book_gmk[] list = new book_gmk[10];
		book_gmk[] temp = new book_gmk[10];
		Scanner scan = new Scanner(System.in);
		Scanner filein = null;
		int count = 0;

		//파일 입력부분(File 및 파일Scanner 생성)은 꼭 try-catch로 묶어야 함!!
		try {
			File file = new File("book.txt");
			filein = new Scanner(file);
		} catch(FileNotFoundException e) {
			System.out.println("파일 없음 " + e.toString());
		}
		
		while(filein.hasNext()) {
			list[count] = new book_gmk();
			list[count].read(filein);		
			count++;
		}
		
		int tCount;
		int key;
		
		while(true) 
		{
			System.out.print("검색할 키워드를 입력하세요 : ");
			String keyWd = scan.next();
			if(keyWd.equals("end"))
				break;
			
			tCount = 1;
			for(int i = 0; i < count; i++)
			{
				if(list[i].compare(keyWd))
				{
					temp[tCount] = list[i];
					System.out.print("[" + (tCount) + "] ");
					list[i].sPrint();
					tCount++;
				}
			}

			System.out.print("상세정보 => ");
			key = scan.nextInt();
			if(key > 0 && key < tCount)
			{
				temp[key].print();
			} 
			else 
			{
				System.out.println("입력 에러");
			}
		}
	}
	
	void read(Scanner s) {
		title = s.nextLine();
		//nextLine으로 받으면 전에 읽었던 엔터를 인식하기때문에 조건문 생성
		if(title.length() == 0)
			title = s.nextLine();
		isbn = s.nextInt();
		year = s.nextInt();
		publisher = s.next();
		author = new String[s.nextInt()];
		for(int i = 0; i < author.length; i++)
			author[i] = s.next();
	}
	
	void print() {
		System.out.println("" + title + "" + " (" + year + ") " + publisher + "[" + isbn + "]");
		System.out.printf("\t저자 : ");
		for(int i = 0; i < author.length; i++)
			System.out.print(author[i] + " ");
		System.out.println("(" + author.length + "명)");
	}
	
	void sPrint() {
		System.out.println(title + " (" + isbn + ")");
	}

	boolean compare(String keyWd) {
		//indexOf 전체 문장을 비교하지않고 문자가 포함되어 있으면 인덱스 리턴
		//포함된 값이 없으면 -1 리턴
		if(this.title.indexOf(keyWd) != -1)
			return true;
		else if(this.publisher.equals(keyWd))
			return true;
		//Integer.toString() = 정수형을 문자형으로 변경
		//숫자를 문자로 바꾸어 비교
		//Integer.parseInt() = 문자형을 정수형으로 변경 / 변경 실패시 요류
		else if(Integer.toString(this.year).equals(keyWd))
			return true;
		else if(Integer.toString(this.isbn).equals(keyWd))
			return true;
		for(int i = 0; i < author.length; i++)
			if(this.author[i].equals(keyWd))
				return true;
		
		return false;
	}
}
