package main;

import java.util.Scanner;

import manager.Managable;

public class Word implements Managable {
	private String word, part;
	private String contentKr, contentEng;
	
	@Override
	public void read(Scanner scanner) {
		word = scanner.next();
		scanner.nextLine();
		
		part = scanner.next();
		contentKr = scanner.nextLine();
		contentEng = scanner.nextLine();
	}

	@Override
	public void print() {
		System.out.println(this);
	}

	@Override
	public boolean compare(String kwd) {
		return word.equals(kwd);
	}

	@Override
	public String toString() {
		return String.format("%s\n(%s) %s\n%s\n", word, part, contentKr, contentEng);
	}
}
