package quiz;

import java.util.HashMap;

public enum QuestionType {
	Write(1), UnderlineWrite(2);
	private int value;
	private static HashMap<Integer, QuestionType> map;
	
	static {
		map = new HashMap<>();
		for (QuestionType qt : QuestionType.values())
			map.put(qt.value, qt);
	}
	
	private QuestionType(int value) {
		this.value = value;
	}
	
	public static QuestionType ofValue(int value) {
		return map.get(value);
	}
	public int getValue() { return value; }
}
