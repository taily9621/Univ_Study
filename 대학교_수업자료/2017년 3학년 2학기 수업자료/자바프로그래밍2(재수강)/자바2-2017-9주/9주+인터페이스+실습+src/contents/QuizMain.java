package contents;

import java.util.Scanner;

import manager.Factory;
import manager.Managable;
import quiz.QuizManager;

public class QuizMain {
	private static QuizManager greatMgr = new QuizManager();
	private static QuizManager wordMgr = new QuizManager();
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		QuizMain programDemo = new QuizMain();
		programDemo.doit();
	}
	
	private void doit() {
		greatMgr.readAll("great.txt", new Factory() {
			
			@Override
			public Managable create() {
				return new Great();
			}
		});
		wordMgr.readAll("word.txt", new Factory() {
			
			@Override
			public Managable create() {
				return new Word();
			}
		});
		
		menu:
		while (true) {
			switch (menu()) {
			case 1:	greatMgr.runQuiz(scanner);break;
			case 2: wordMgr.runQuiz(scanner); 	break;
			case 0: break menu;
			}
		}
	}
	
	private int menu() {
		System.out.println("(1) 위인  (2) 단어  (0) 종료");
		
		int choice;
		do {
			choice = scanner.nextInt();
			scanner.nextLine();
		} while (choice < 0 || choice > 2);
		
		return choice;
	}
}
