import javax.swing.*;
import java.awt.*;

public class SwingExam {
	public static void main(String args[]) {
		JFrame frm = new JFrame("스윙 예제");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // 프레임종료되면서 프로그램도 종료
		frm.setSize(400,400);
		Container contentPane = frm.getContentPane();
		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(3,2));
		pane.add(new JTextField(10));
		pane.add(new JPasswordField(10));
		pane.add(new JRadioButton("라디오버튼"));
		pane.add(new JButton("그냥버튼"));

		String data[] = {"one", "two", "three", "four" };
		pane.add(new JList(data));
		pane.add(new JCheckBox("체크박스"));
		contentPane.add(pane,"Center");
		contentPane.add(new JButton("단추"),"South");
		frm.setVisible(true);
	}
}
