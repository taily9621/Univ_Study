package collections;

import java.util.*;
public class CardDeck {
    public static void main(String[] args) {
//        if (args.length < 2) {
//            System.out.println("Usage: Deal hands cards");
//            return;
//        }
        Scanner keyin = new Scanner(System.in);
        System.out.print("플레이어수 : ");
        int numHands = keyin.nextInt();
        System.out.print("한사람당 카드수 : ");
        int cardsPerHand = keyin.nextInt();
        // 정상적인 52장 카드 묶음을 만든다
        String[] suit = new String[] {
            "♠♤", "♥♡", 
            "♦♢", "♣♧" 
        };
        String[] rank = new String[] {
            "ace", "2", "3", "4",
            "5", "6", "7", "8", "9", "10", 
            "jack", "queen", "king" 
        };
        List<String> deck = new ArrayList<String>();
        for (int i = 0; i < suit.length; i++)
            for (int j = 0; j < rank.length; j++)
                deck.add(String.format("%6s%2s", rank[j], suit[i]));
        // 카드 묶음을 섞는다.
        Collections.shuffle(deck);
        if (numHands * cardsPerHand > deck.size()) {
            System.out.println("Not enough cards.");
            return;
        }
        for (int i = 0; i < numHands; i++)
            System.out.println(dealHand(deck, cardsPerHand));
    }
    public static <E> List<E> dealHand(List<E> deck, int n) {
        int deckSize = deck.size();
        List<E> handView = deck.subList(deckSize - n, deckSize);
        List<E> hand = new ArrayList<E>(handView);
        handView.clear();
        return hand;
    }
}
