﻿package test001;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Shuffle {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String aLine = scan.nextLine();
		//String words[] = aLine.trim().split(" ");
		List<String> list = Arrays.asList(aLine.trim().split(" "));
		for (String a : args)
			list.add(a);
		Collections.shuffle(list, new Random());
		System.out.println(list);
	}
}
