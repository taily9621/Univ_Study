﻿package test001;

import java.util.*;
public class CardDeck {
    public static void main(String[] args) {
//        if (args.length < 2) {
//            System.out.println("Usage: Deal hands cards");
//            return;
//        }
        Scanner keyin = new Scanner(System.in);
        System.out.print("?뵆?젅?씠?뼱?닔 : ");
        int numHands = keyin.nextInt();
        System.out.print("?븳?궗?엺?떦 移대뱶?닔 : ");
        int cardsPerHand = keyin.nextInt();
        // ?젙?긽?쟻?씤 52?옣 移대뱶 臾띠쓬?쓣 留뚮뱺?떎
        String[] suit = new String[] {
            "?솧?솮", "?솯?솪", 
            "?솱?솫", "?솭?솲" 
        };
        String[] rank = new String[] {
            "ace", "2", "3", "4",
            "5", "6", "7", "8", "9", "10", 
            "jack", "queen", "king" 
        };
        List<String> deck = new ArrayList<String>();
        for (int i = 0; i < suit.length; i++)
            for (int j = 0; j < rank.length; j++)
                deck.add(String.format("%6s%2s", rank[j], suit[i]));
        // 移대뱶 臾띠쓬?쓣 ?꽎?뒗?떎.
        Collections.shuffle(deck);
        if (numHands * cardsPerHand > deck.size()) {
            System.out.println("Not enough cards.");
            return;
        }
        for (int i = 0; i < numHands; i++)
            System.out.println(dealHand(deck, cardsPerHand));
    }
    public static <E> List<E> dealHand(List<E> deck, int n) {
        int deckSize = deck.size();
        List<E> handView = deck.subList(deckSize - n, deckSize);
        List<E> hand = new ArrayList<E>(handView);
        handView.clear();
        return hand;
    }
}
