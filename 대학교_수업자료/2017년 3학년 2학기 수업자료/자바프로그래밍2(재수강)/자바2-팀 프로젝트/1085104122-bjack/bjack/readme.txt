
jafar.com BlackJack - Java Applet Version

INSTALLATION
------------

To use the applet on your site:

1. Use the provided HTML file: index.html
	
			OR

1. Copy the html tags shown below into your HTML page: Both applet tags 
must be present on the same page for the BlackJack applet to function correctly.

2. Place both .jar files - bjadbanner.jar and bjack.jar in the directory
containing the HTML file.

3. Use a Java capable browser to view the HTML page.


HTML TAGS
---------

<!-- AdBanner Applet tags - required to run BlackJack - do not edit or remove -->
<APPLET ARCHIVE="bjadbanner.jar" 
	CODE="com.jafar.applets.adbanner.BJAdBanner.class" 
	NAME="bjadbanner" 
	WIDTH="468" HEIGHT="60" BORDER="1">
<NOAPPLET>Ad Banner for http://www.jafar.com/</NOAPPLET>
</APPLET>

<FONT FACE="Verdana, Arial, Geneva, sans-serif" SIZE="1pt"><A 
	HREF="http://www.jafar.com/">Click here to visit 
	http://www.jafar.com/</A></FONT>
<!-- end AdBanner Applet tags -->

<!-- BlackJack Applet -->
<APPLET ARCHIVE="bjack.jar" 
	CODE="com.jafar.games.bjack.BlackJack.class" WIDTH="450" HEIGHT="350">
	<NOAPPLET><FONT FACE="Trebuchet MS, Arial, Geneva, sans-serif">You need a 
		Java-enabled browser to play this game.</FONT>
	</NOAPPLET>
</APPLET>
<!-- BlackJack Applet -->


If you have any questions about the applet or its use, please contact 
	support@jafar.com

Thank you.

Jafar Communications
http://www.jafar.com/
