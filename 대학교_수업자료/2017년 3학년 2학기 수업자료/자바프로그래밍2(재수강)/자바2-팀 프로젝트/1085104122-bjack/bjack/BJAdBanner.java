package com.jafar.applets.adbanner;

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

public class BJAdBanner
  extends Applet
  implements Runnable, MouseListener
{
  private int width;
  private int height;
  private int numImages;
  private static final String[] imgStrings = {
    "jcom3.gif", 
    "jipcalc.gif", 
    "jimgmap.gif" };
  private Image[] images;
  private static final String[] urlStrings = {
    "http://www.jafar.com/", 
    "http://www.jafar.com/java/jipcalc/", 
    "http://www.jafar.com/java/jimgmap/" };
  private URL[] urls;
  private MediaTracker tracker;
  private Thread thread;
  private Image curImage;
  private int curImageIndex;
  private int rotateInterval;
  private boolean Active = false;
  private Image im;
  private Graphics imGraphics;
  
  public void init()
  {
    String str = null;
    int i = 0;
    
    str = getParameter("width");
    if (str != null) {
      this.width = Integer.parseInt(str);
    } else {
      this.width = 468;
    }
    str = getParameter("height");
    if (str != null) {
      this.height = Integer.parseInt(str);
    } else {
      this.height = 60;
    }
    this.numImages = imgStrings.length;
    if (this.numImages > 0)
    {
      getDocumentBase();
      this.tracker = new MediaTracker(this);
      this.images = new Image[this.numImages];
      for (i = 0; i < this.numImages; i++)
      {
        this.images[i] = getImageResource("/adbres/images/" + imgStrings[i]);
        this.tracker.addImage(this.images[i], i);
        this.tracker.checkID(i, true);
      }
    }
    this.urls = new URL[this.numImages];
    for (i = 0; i < this.numImages; i++) {
      if (urlStrings[i] != null) {
        try
        {
          this.urls[i] = new URL(urlStrings[i]);
        }
        catch (MalformedURLException localMalformedURLException) {}
      }
    }
    str = getParameter("updateInterval");
    if (str != null) {
      try
      {
        this.rotateInterval = (Integer.parseInt(str) * 1000);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        System.err.println("Invalid update interval: " + str);
      }
    } else {
      this.rotateInterval = 30000;
    }
    addMouseListener(this);
    
    this.Active = true;
    start();
  }
  
  public void start()
  {
    this.thread = new Thread(this);
    this.thread.start();
  }
  
  public void stop()
  {
    this.Active = false;
    if (this.thread != null)
    {
      this.thread.stop();
      this.thread = null;
    }
  }
  
  public void run()
  {
    while (this.Active)
    {
      this.curImageIndex += 1;
      if (this.curImageIndex >= this.numImages) {
        this.curImageIndex = 0;
      }
      repaint();
      try
      {
        Thread.sleep(this.rotateInterval);
      }
      catch (InterruptedException localInterruptedException) {}
    }
  }
  
  public void paint(Graphics paramGraphics)
  {
    if (this.im == null)
    {
      Rectangle localRectangle = getBounds();
      if ((localRectangle.width <= 0) || (localRectangle.height <= 0)) {
        return;
      }
      try
      {
        this.im = createImage(localRectangle.width, localRectangle.height);
        this.imGraphics = this.im.getGraphics();return;
      }
      catch (Exception localException)
      {
        this.imGraphics = null;return;
      }
    }
    if (this.imGraphics != null)
    {
      paintApplet(this.imGraphics);
      paramGraphics.drawImage(this.im, 0, 0, this);
    }
    else
    {
      paintApplet(paramGraphics);
    }
  }
  
  private void paintApplet(Graphics paramGraphics)
  {
    Rectangle localRectangle = getBounds();
    try
    {
      if (this.numImages > 0)
      {
        if (!this.tracker.checkID(this.curImageIndex)) {
          this.tracker.waitForID(this.curImageIndex);
        }
        if (!this.tracker.isErrorID(this.curImageIndex))
        {
          this.curImage = this.images[this.curImageIndex];
          int i = (localRectangle.width - this.curImage.getWidth(this)) / 2;
          int j = (localRectangle.height - this.curImage.getHeight(this)) / 2;
          paramGraphics.drawImage(this.curImage, i, j, this);
        }
      }
    }
    catch (InterruptedException localInterruptedException) {}
  }
  
  public void mouseClicked(MouseEvent paramMouseEvent)
  {
    URL localURL = this.urls[this.curImageIndex];
    if (localURL != null) {
      getAppletContext().showDocument(localURL, "_blank");
    }
  }
  
  public void mouseReleased(MouseEvent paramMouseEvent) {}
  
  public void mousePressed(MouseEvent paramMouseEvent) {}
  
  public void mouseEntered(MouseEvent paramMouseEvent) {}
  
  public void mouseExited(MouseEvent paramMouseEvent) {}
  
  private Image getImageResource(String paramString)
  {
    Image localImage = null;
    try
    {
      InputStream localInputStream = getClass().getResourceAsStream(paramString);
      if (localInputStream == null)
      {
        System.err.println("Image " + paramString + " not found.");
        return null;
      }
      byte[] arrayOfByte = new byte[localInputStream.available()];
      localInputStream.read(arrayOfByte);
      localImage = Toolkit.getDefaultToolkit().createImage(arrayOfByte);
    }
    catch (IOException localIOException)
    {
      System.err.println("Unable to read image: " + paramString);
      localIOException.printStackTrace();
    }
    return localImage;
  }
}
