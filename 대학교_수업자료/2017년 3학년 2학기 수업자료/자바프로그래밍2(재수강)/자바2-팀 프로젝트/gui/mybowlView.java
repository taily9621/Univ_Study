package gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class mybowlView extends JPanel {
	JPanel panel;
	public static JPanel panel2;
	public static JPanel panel1;
	JLabel bowlLabel;
	JLabel mybowlfish;
	ImageIcon mbImage;

	String[] text = { "와인통장식", "삼호바위산" };
	JCheckBox[] deco = new JCheckBox[2];
	Font lbfont = new Font("맑은 고딕", 33, 33);
	JLabel decoImage1, decoImage2;

	public void init() {
		// TODO Auto-generated method stub
		// 어항장바구니

		mbImage = new ImageIcon("src/gui/background2.jpg");
		panel = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(mbImage.getImage(), 0, 0, null);
				setOpaque(false);
			}

			@Override
			public Dimension getPreferredSize() {
				return new Dimension(mbImage.getImage().getWidth(null), mbImage.getImage().getHeight(null));
			}

		};

		panel.setLayout(null);	
		add(panel);
		
		
		
		
		panel2= new JPanel();
		panel2.setLayout(new GridLayout(2,2, 1,1));
		panel2.setBounds(220, 0, 590, 500); //myfish
		panel2.setOpaque(true);
		//panel2.setBackground(getBackground());
		panel.add(panel2);

		ImageIcon samho = new ImageIcon("src/images/삼호바위산.png");
		ImageIcon wine = new ImageIcon("src/images/와인통장식.png");

		
		panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBounds(220,500, 580, 280);
		panel1.setOpaque(false);
		panel.add(panel1);
		panel1.setVisible(true);
		// decoImage
		Image image, newing;
		// samho
		image = samho.getImage();
		newing = resizeImage(image, 300, 300);
		samho.setImage(newing);
		decoImage1 = new JLabel(samho);
		decoImage1.setBounds(0, 0, 300, 300);
		panel1.add(decoImage1);
		
		
		// wine
		image = wine.getImage();
		newing = resizeImage(image, 300, 300);
		wine.setImage(newing);
		decoImage2 = new JLabel(wine);
		decoImage2.setBounds(300, 0, 300, 300);
		 panel1.add(decoImage2);
		 
		decoImage1.setVisible(false);
		decoImage2.setVisible(false);		 

		// checkbox
		for (int i = 0; i < 2; i++) {
			deco[i] = new JCheckBox(text[i]);
			deco[i].setFont(lbfont);
			deco[i].setBounds(1100, 1000, 900, 100);
			add(deco[i]);
			deco[i].addItemListener(new MyItemListener());
		}

	}
	public void putFish() {
		// TODO Auto-generated method stub
		Image image = fishView.fishImage.getImage();// curFishImageimageicon															
		Image newing = resizeImage(image, 300, 300);
		mybowlfish = new JLabel();
		mybowlfish.setIcon(new ImageIcon(newing));
		mybowlView.panel2.add(mybowlfish);
		mybowlfish.setVisible(true);
	}
	Image resizeImage(Image image, int width, int height) {
		Image resizeImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return resizeImage;
	}

	class MyItemListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			// TODO Auto-generated method stub
			int selected = -1;

			if (e.getStateChange() == ItemEvent.SELECTED)
				selected = 1;
			else
				selected = -1;

			if(selected ==1 &&e.getItem() == deco[1])
				decoImage1.setVisible(true);
			else if(selected ==1 &&e.getItem()== deco[0])
				decoImage2.setVisible(true);
			else if(selected ==-1 && e.getItem()== deco[1])
				decoImage1.setVisible(false);
			else if(selected ==-1 && e.getItem()== deco[0])
				decoImage2.setVisible(false);
			

		}

	}

}
