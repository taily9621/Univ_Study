package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import fish.Fish;
import fish.FishDemo;
import fish.FishManager;

public class fishView extends JPanel {
	Color idcolor = new Color(16, 75, 134);
	Font lbfont = new Font("맑은 고딕", 33, 33);
	JLabel exLabel;
	public static JLabel curfishImage;
	public static JTextArea explain;
	Color skyblue = new Color(135, 182, 216);
	public static JScrollPane s;
	public static ImageIcon fishImage;
	void init() {

		Fish f= FishDemo.fishMgr.find("구피");
		
		setLayout(null);
		// curimage
		ImageIcon fishImage = new ImageIcon("src/images/구피.png");
		curfishImage = new JLabel(fishImage);
		Image image = fishImage.getImage();
		Image newing = resizeImage(image, 500, 500);
		fishImage.setImage(newing);
		curfishImage = new JLabel(fishImage);
		curfishImage.setBounds(0, 0, 500, 500);
		add(curfishImage);

		// textArea
		String ex= f.toString();
		explain = new JTextArea(ex, 1, 13);
		explain.setLineWrap(true);
		explain.setFont(lbfont);
		s = new JScrollPane(explain);
		s.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		s.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		s.setBounds(10, 550, 450, 500);
		add(s);

	}

	void changeCurfish(Fish f) {
		
		//Image
		String newname = f.getName(); // 물고기이름
		fishImage = new ImageIcon("src/images/" + newname + ".png");
		Image image = fishImage.getImage();
		Image newing = resizeImage(image, 500, 500);		
		curfishImage.setIcon(new ImageIcon(newing));
		
		//text
		String newExplain =f.toString();
		explain.setText(newExplain);
	
	}

	void initLabel(JLabel lb) {
		lb.setFont(lbfont);
		lb.setOpaque(true);
		lb.setHorizontalAlignment(JLabel.CENTER);
		lb.setLayout(null);
		lb.setVisible(true);
		add(lb);
	}

	Image resizeImage(Image image, int width, int height) {
		Image resizeImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return resizeImage;
	}
}
