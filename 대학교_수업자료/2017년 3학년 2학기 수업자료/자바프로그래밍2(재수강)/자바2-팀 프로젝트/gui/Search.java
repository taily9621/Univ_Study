package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fish.FishDemo;
import fish.FishManager;
import gui.FISHFrame;
import user.User;

public class Search extends JPanel implements BJView {

	private static Search search = null;
	public static User user;
	public static FishManager fm;
	@Override
	public void init() {
		// TODO Auto-generated method stub
		GridLayout grid = new GridLayout(1, 3);	
		grid.setVgap(4);
		setLayout(grid);
		
		fishView fv= new fishView();
		fv.init();		
		add(fv);
		
		listView lv= new listView();
		lv.init();
		add(lv);
		
		mybowlView mv= new mybowlView();
		mv.init();
		add(mv);

		
	}
	public void initLabel(JLabel lb) {
	//	lb.setFont(lbfont);
		lb.setOpaque(true);
		lb.setHorizontalAlignment(JLabel.CENTER);
		lb.setLayout(null);
		lb.setVisible(true);
		add(lb);
	}
}
