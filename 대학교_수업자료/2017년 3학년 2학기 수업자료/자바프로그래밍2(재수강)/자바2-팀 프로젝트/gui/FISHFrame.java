package gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import fish.FishDemo;
import gui.BJView.ViewName;

public class FISHFrame extends JFrame {
	private static FISHFrame bjframe = null;
	Image img = null;

	public static FISHFrame GetBJFrame() {
		if (bjframe == null)
			bjframe = new FISHFrame();
		return bjframe;
	}

	static Toolkit tk = Toolkit.getDefaultToolkit();
	public static int SCREEN_X_SIZE = (int) tk.getScreenSize().getWidth();
	public static int SCREEN_Y_SIZE = (int) tk.getScreenSize().getHeight();

	public static Login login2;
	public static Search search2;
	public static MyBowl mb2;
	public static Container con;
	ImageIcon background;
	static JPanel panel;

	public void init() {
		setTitle("MY FISHBOWL");

		setLocationRelativeTo(null);
		con = getContentPane();

		setPreferredSize(new Dimension(600, 600));
		setLocation(0, 0);

		// login
		Login login = new Login();
		login.init();
		login.setBounds(0, 0, SCREEN_X_SIZE, SCREEN_Y_SIZE);

		add(login);
		login2 = login;

		// search
		Search search = new Search();
		search.setBounds(0, 0, SCREEN_X_SIZE, SCREEN_Y_SIZE);
		search.init();
		add(search);
		search2 = search;

		MyBowl mb = new MyBowl();
		mb.setBounds(0, 0, SCREEN_X_SIZE, SCREEN_Y_SIZE);
		mb.init();
		add(mb);
		mb2 = mb;

		pack();
		setSize(SCREEN_X_SIZE, SCREEN_Y_SIZE);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		changeView(BJView.ViewName.LOGIN);
	}

	protected void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, 600, 600, this);
	}

	public static void changeView(ViewName v) {
		BJView view = null;
		switch (v) { // start
		case LOGIN: // login

			login2.setVisible(true);
			search2.setVisible(false);
			mb2.setVisible(false);
			break;
		case SEARCH:// search

			search2.setVisible(true);

			login2.setVisible(false);
			mb2.setVisible(false);
			break;

		case MYBOWL:
			mb2.setVisible(true);
			search2.setVisible(false);
			login2.setVisible(false);
			break;
		}

	}

	public static void main(String args[]) {
		FISHFrame fish = new FISHFrame();
		fish.init();
	}

}