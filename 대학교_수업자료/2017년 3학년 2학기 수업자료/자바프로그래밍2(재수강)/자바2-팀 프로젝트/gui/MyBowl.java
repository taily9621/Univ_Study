package gui;

import java.awt.Graphics;
import java.awt.Label;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public	class MyBowl extends JPanel implements BJView
{
	ImageIcon background;
	ImageIcon fish;
	JPanel panel;
	JLabel fishLabel;
	JList myfishList;

	public void init() {
		String[] myfish ={"다랑어"};
		background = new ImageIcon("src/gui/background.png");
		panel = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(background.getImage(), 0, 0, null);
				setOpaque(false);
			}
		};
		panel.setLayout(null);

		panel.setBounds(0, 0, FISHFrame.SCREEN_X_SIZE, FISHFrame.SCREEN_Y_SIZE);
		add(panel);		
		setLayout(null);
		myfishList= new JList(myfish);

		myfishList.setBounds(100, 100, 500, 700);		
		myfishList.setFont(Login.lbfont);		
		panel.add(new JScrollPane(myfishList));
		panel.add(myfishList);
		
		fish= new ImageIcon("src/gui/다랑어.jpg");
		fishLabel= new JLabel(new ImageIcon("src/gui/다랑어.jpg"));
		fishLabel.setBounds(1000,  500,  800,  750);
		panel.add(fishLabel);	
	}
}

