package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;

import fish.FishDemo;
import user.User;

public class Login extends JPanel implements BJView {
	private static Login login = null;
	private JPasswordField passText;
	private JTextField idText;
	JLabel[] buttons = new JLabel[2];
	JLabel[] jl = new JLabel[3];
	ImageIcon background;
	static JPanel panel;

	static Font lbfont = new Font("맑은 고딕", 33, 33);
	// JButton btnInit = new JButton("Login");
	JButton btnLogin = new JButton("비밀번호 찾기");
	JButton btnReturn = new JButton("시작화면");
	Border mb = BorderFactory.createMatteBorder(5, 5, 5, 5, Color.DARK_GRAY);

	public static Login getLogin() {
		// TODO Auto-generated method stub
		if (login == null)
			login = new Login();
		return login;
	}

	public void init() {

		setLayout(null);

		background = new ImageIcon("src/gui/background.png");
		panel = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(background.getImage(), 0, 0, null);
				setOpaque(false);
			}
		};
		panel.setLayout(null);
		;
		panel.setBounds(0, 0, FISHFrame.SCREEN_X_SIZE, FISHFrame.SCREEN_Y_SIZE);

		add(panel);
		String[] buttonText = { "ID", "Password" };
		Color idcolor = new Color(16, 75, 134);
		for (int i = 0; i < 2; i++) {
			buttons[i] = new JLabel(buttonText[i]);
			buttons[i].setBounds((int) FISHFrame.SCREEN_X_SIZE / 2 - 202,
					(int) FISHFrame.SCREEN_Y_SIZE / 2 + (i - 1) * 60, 200, 50);
			buttons[i].setFont(lbfont);
			buttons[i].setForeground(Color.white);
			buttons[i].setOpaque(true);
			buttons[i].setHorizontalAlignment(JLabel.CENTER);
			buttons[i].setVisible(true);
			buttons[i].setBackground(idcolor);
			panel.add(buttons[i]);
			setVisible(true);
		}

		idText = new JTextField(20);
		idText.setFont(lbfont);
		;

		idText.setBounds((int) FISHFrame.SCREEN_X_SIZE / 2, (int) FISHFrame.SCREEN_Y_SIZE / 2 - 60, 200, 50);
		idText.setLayout(null);
		idText.setBorder(mb);
		panel.add(idText);

		passText = new JPasswordField("");

		passText.setBounds((int) FISHFrame.SCREEN_X_SIZE / 2, (int) FISHFrame.SCREEN_Y_SIZE / 2, 200, 50);
		passText.setLayout(null);
		passText.setFont(lbfont);
		;
		passText.setBorder(mb);

		add(passText);

		JButton btnInit = new JButton("Login");
		btnInit.setBounds(1180, (int) FISHFrame.SCREEN_Y_SIZE / 2 - 60, 180, 110);
		btnInit.setFont(lbfont);
		btnInit.setOpaque(true);
		btnInit.setLayout(null);
		btnInit.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				//FISHFrame.changeView(BJView.ViewName.SEARCH);
				isLoginCheck();

			}
		});
		panel.add(btnInit);

	}

	private void setContentPane(JLabel jLabel) {
		// TODO Auto-generated method stub

	}

	public void isLoginCheck() {
		if (idCheck()) {
			if (passwordCheck()) {
				FISHFrame.changeView(BJView.ViewName.SEARCH);
			} else {
				System.out.println("비밀번호 틀림");
				JOptionPane.showMessageDialog(null, "틀린 비밀번호입니다.");
			}
		} else {
			System.out.println("없는아이디입니다.");
			JOptionPane.showMessageDialog(null, "없는 아이디입니다. ");
		}
	}

	private boolean idCheck() {
		Search.user = FishDemo.userMgr.find(idText.getText());
		if(Search.user!=null)
			return true;
		else
			return false;
		//return UserDemo.idinfo.contains(idText.getText());
	}

	private boolean passwordCheck() {
		if(String.valueOf(passText.getPassword()).equals(Search.user.getPassword()))
			return true;
		else
			return false;
		// String(passText.getPassword()).equals(UserDemo.info.get(idText.getText()));
	}

}