package blackjack;

public class Dealer extends Person{
	
	public Dealer(){
		super();
	}
	//딜로의 핸드 출력(전부)
	public void printHand(){
		System.out.print("딜러의 핸드 : ");
		super.printHand();
	}
	
	//딜러의 핸드 출력(한장은 비공개)
	public void printHand(Dealer dealer){
		System.out.print("딜러의 핸드 : ");
		Hand.printHand(dealer,dealer.hand);
	}
	
	
	//카드의 총합이 17이상이 될때까지 카드 뽑기
	public void dealerPlay(){
		while(Hand.getNumberCount(this)<=16){
			Hand.receiveCard(this.hand);
			System.out.println("딜러가 카드를 뽑았습니다.");
		}
	}
	
}
