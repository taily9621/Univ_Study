package blackjack;

public class Person {
	Hand hand;
	
	public Person(){
		hand = new Hand();
	}
	//카드 뽑기
	public void draw(Person person){
		Hand.receiveCard(person.hand);
	}
	//핸드 출력
	public void printHand(){
		Hand.printHand(this.hand);
	}
	
	public boolean bustCheck(){
		if(Hand.bustCheck(this.hand)){
			System.out.println("버스트 되었습니다.");
			return true;
		}
		return false;
	}
}
