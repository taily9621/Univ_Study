package blackjack;
import java.util.Scanner;

public class Game {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		while(true){
			System.out.println("\nBlackjack\n\n"
					+ "선택해주세요.\n1.게임 시작\n2.게임규칙\n3.종료");
			choice = sc.nextInt();
			if(choice==1){
				Blackjack.blackjack();
				return;
			}
			else if(choice==2)
				Blackjack.gameDescription();
			else
				return;
		}
	}
}
