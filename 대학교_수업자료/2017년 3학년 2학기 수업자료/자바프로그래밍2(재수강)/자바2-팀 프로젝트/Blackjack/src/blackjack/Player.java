package blackjack;

public class Player extends Person{
	String name;
	int money;
	boolean bettingRangeCheck = false; //베팅한 금액이 소지금보다 큰지 체크할 변수, 베팅<=소지금인 경우 true
	
	
	public Player(String name,int money){
		super();
		this.name = name;
		this.money = money;
	}
	
	//현재 금액
	public int getMoney(){
		return money;
	}
	
	//베팅하기
	public void bettingMoney(int betting){
		if(betting<money&&betting>0){
			System.out.println(betting + "원을 베팅하였습니다.");
			this.money -= betting;
			bettingRangeCheck = true;
		}
		else if(betting==money){
			System.out.println("올인하셨습니다.");
			this.money = 0;
			bettingRangeCheck = true;
		}
		else if(betting>money){
			System.out.println("소지금액보다 많습니다. 다시 입력해 주세요.");
			bettingRangeCheck = false;
		}
		else{
			System.out.println("0원 이하는 입력할 수 없습니다. 다시 입력해 주세요.");
			bettingRangeCheck = false;
		}
	}
	//DoubleDown을 선택했을때 실행
	public int doubleDown(int betting){
		if(money-betting>=0){
			System.out.println(betting + "원을 더 베팅하였습니다.");
			money -= betting;
			return betting*2;
		}
		else{
			System.out.println("소지금이 부족하여 DoubleDown할 수 없습니다.");
			System.out.println("카드를 한장만 뽑습니다.");
			return betting;
		}
	}
	
	//베팅한 금액이 소지금보다 많은지 체크
	public boolean bettingCheck(){
		return this.bettingRangeCheck;
	}

	//플레이어의 핸드 출력
	public void printHand(){
		System.out.print("플레이어1의 핸드 : ");
		super.printHand();
	}
	
	public void moneyGet(int money){
		this.money += money;
	}
	
	public String getName(){
		return this.name;
	}
		
}
