package blackjack;
import java.io.*;
import java.lang.Math;
import java.util.StringTokenizer;

public class Computer extends Player {
	String choice;
	int[][] record;	
	//배열의 각 행은 Ai가 가지고 있는 카드의 합에 따른 가중치를 저장
	//핸드의 총합이 n이라 했을때
	//[n][0]에는 Stand가중치,[n][1]에는 Hit가중치,[n][2]에는 DoubleDown가중치,
	//[n][3]에는 n일때 선택한것을 저장 1=Stand,2=Hit,3=DoubleDown
	//[n][4]에는 가중치 변경을 위해 그 게임에서 나왔으면 1을 저장
	//만약 핸드의 총합이 13이고 Stand를 했으면
	//[13][3]에는 1로 변경되고 [13][4]에 1로 변경
	//그 이후 딜러와 핸드를 비교하여 졌으면 [13][1]의 Hit가중치를 -1함

	public Computer(String name,int money) {
		super(name,money);
		//핸드의 총합이 11~20까지만 기록
		record = new int[10][5];
		readFile();
	}
	
	//파일 읽기
	public void readFile(){
		String fileName = "record.txt";
		
		try{
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String tmp = reader.readLine();
			StringTokenizer st = new StringTokenizer(tmp," ");
			for(int i=0; i<record.length; i++)
				for(int j=0; j<record[i].length; j++)
					record[i][j] = Integer.parseInt(st.nextToken());
		}catch(IOException e){
			System.out.println("파일 열기에 실패했습니다.");
			resetRecord();
		}
	}
	
	//파일 저장
	public void saveFile(){
		String fileName = "record.txt";
		try{
			BufferedWriter fw = new BufferedWriter(new FileWriter(fileName,false));
			for(int i=0; i<record.length; i++)
				for(int j=0; j<record[i].length; j++)
					fw.write(String.valueOf(record[i][j]) + " ");
			fw.flush();
			
			fw.close();
		}catch(IOException e){
			System.out.println("파일쓰기 에러가 발생했습니다.");
		}
		
	}
	
	//컴퓨터가 베팅할 금액을 랜덤으로 선택
	public int computerBetting(){
		int bet;
		do{
			bet = (int)Math.round(Math.random()*20)*100;
		}while(bet>super.money);
		return bet;
	}
	//Hit할지 Stand할지 판단
	public String hitOrStand(){
		int handNumberCount = this.hand.cardNumberCount; //현재 핸드의 총합
		int randomNumber; //난수
		int weightSum = 0; //가중치의 총합
		
		
		if(handNumberCount<=10){  //핸드 총합이 10이하면 Hit를 선택
			choice = "Hit";
		}
		else if(handNumberCount>=21)  //핸드 총합이 21이상이면 뽑을 필요가 없으므로 Stand를 선택
			choice = "Stand";
		else{
			
			for(int i=0; i<3; i++)
				weightSum += record[handNumberCount-11][i]; //Hit,Stand,DoubleDown의 가중치를 전부 더함
			
			randomNumber = (int)Math.round(Math.random()*weightSum); //0~가중치의 합 범위 내에서 난수 생성
			
			//생성된 난수가 Stand의 범위내라면 4열을 1(Stand)로 바꾸고 Stand를 선택
			if(randomNumber <= record[handNumberCount-11][0]){
				choice = "Stand";
				record[handNumberCount-11][3] = 1;
			}
			//생성된 난수가 Hit의 범위내라면 4열을 2(Hit)로 바꾸고 Hit를 선택
			else if(randomNumber <= (record[handNumberCount-11][0]+record[handNumberCount-11][1])){
				choice = "Hit";
				record[handNumberCount-11][3] = 2;
			}
			//생성된 난수가 DoubleDown의 범위내라면 4열을 3(DoubleDown)로 바꾸고 DoubleDown를 선택
			else{
				choice = "DoubleDown";
				record[handNumberCount-11][3] = 3;
			}
			//이번 게임에서 나왔던 총합인지 확인을 위해 1을 저장
			record[handNumberCount-11][4] = 1;
		}
		
		//선택한것을 반환
		return choice;
	}
	//게임의 결과를 참고하여 가중치를 변경
	public void gameRecord(String winner){
		
		//플레이어가 이겼을경우 record의 가중치를 올림
		if(winner.equals("player")){
			for(int i=0;i<record.length;i++){
				if(record[i][4]==1){  //이번 게임에서 나왔던 핸드의 총합이면
					if(record[i][3]==1){	     //Stand를 선택하고
						if(record[i][0]<50) //가중치가 50이하면
							record[i][0]+=2;  //가중치+1
					}
					else if(record[i][3]==2){ //Hit를 선택하고
						if(record[i][1]<50) //가중치가 50이하면
							record[i][1]+=2;  //가중치+1
					}
					else if(record[i][3]==3){ //DoubleDown를 선택하고
						if(record[i][2]<50) //가중치가 50이하면
							record[i][2]+=2;  //가중치+1
					}
					else
						System.out.println("에러");
				}
			}
		}
		
		//플레이어가 졌을 경우 record의 가중치를 내림
		else if(winner.equals("dealer")){
			for(int i=0;i<record.length;i++){
				if(record[i][4]==1){   //이번 게임에서 나왔던 핸드의 총합이면
					if(record[i][3]==1){    //Stand를 선택하고
						if(record[i][0]>5)  //가중치가 5이상이면
							record[i][0]--; //가중치-1
					}
					else if(record[i][3]==2){//Hit를 선택하고
						if(record[i][1]>5)  //가중치가 5이상이면
							record[i][1]--; //가중치-1
					}
					else if(record[i][3]==3){//DoubleDown를 선택하고
						if(record[i][2]>5)  //가중치가 5이상이면
							record[i][2]--; //가중치-1
					}
					else
						System.out.println("에러");
				}
			}
		}
		else;  //비겼을 경우 가중치 변경 없음
		
		for(int i=0;i<record.length;i++){
			record[i][3] = 0;   //3열과 4열을 0으로 초기화
			record[i][4] = 0;
		}
		
	}
	
	public void printRecord(){
		for(int i=0;i<record.length;i++){
			for(int j=0;j<record[i].length;j++)
				System.out.print(record[i][j] + " ");
			System.out.println();
		}
		
	}
	
	//record의 각 가중치를 10으로 초기화
	public void resetRecord(){
		for(int i=0; i<record.length; i++){
			for(int j=0; j<5; j++)
				if(j<3)   //Hit,Stand,DoubleDown가중치를 25으로 초기화
					record[i][j] = 25;
				else
					record[i][j] = 25;
		}
	}
	
	public void printHand(){
		System.out.print("플레이어2의 핸드 : ");
		Hand.printHand(this.hand);
	}
}
