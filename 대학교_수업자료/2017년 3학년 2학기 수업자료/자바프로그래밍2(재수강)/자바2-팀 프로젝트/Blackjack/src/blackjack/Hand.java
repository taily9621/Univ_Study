package blackjack;

public class Hand {
	static final int MAXHAND = 12;
	Card[] hand = new Card[MAXHAND]; //손패
	int cardCount = 0;			//카드의 총 장수를 카운트하기 위한 변수 3장이면 cardCount=3가 되어있음
	int cardNumberCount = 0;	//카드의 숫자의 합을 저장하기 위한 변수
	boolean bustCheck = false;  //카드의 숫자의 합이 버스트가 되는지 확인할 변수 버스트이면 true
	boolean blackjack = false;  //핸드가 블랙잭(A+10,J,Q,K)일때 true
	int haveAce = 0;			//a를 몇장 가지고 있는지 저장 할 변수
	
	
	public static Hand receiveCard(Hand hand){
		if(!hand.bustCheck){
			hand.hand[hand.cardCount] = Deck.cardDraw();
			bustCheck(hand);
			Hand.cardNumberCount(hand);
			hand.cardCount++;
			if(hand.cardCount==2 && hand.cardNumberCount==21)
				hand.blackjack = true;
		}
		return hand;
	}
	
	public static void handReset(Hand hand){
		hand.cardCount = 0;
		hand.cardNumberCount = 0;
		hand.haveAce = 0;
		hand.bustCheck = false;
		hand.blackjack = false;
	}
	//핸드 출력
	public static void printHand(Hand hand){
		for(int i=0; i<hand.cardCount; i++)
			System.out.print(hand.hand[i]);
		System.out.println();
	}
	//딜러의 핸드 출력
	public static void printHand(Dealer dealer,Hand hand){
		System.out.print("?? ");
		for(int i=1; i<hand.cardCount; i++)
			System.out.print(hand.hand[i]);
		System.out.println();
	}
	//버스트가 되는지 체크하고 버스트가 될경우 bustCheck에 true를 저장하고 리턴
	public static boolean bustCheck(Hand hand){
		checkAce(hand);			 //Ace카드가 있는지 확인해서 있을경우 Ace카드를 1로계산
		if(hand.cardNumberCount > 21)	//카드의 총합을 확인
			hand.bustCheck = true;
		else
			hand.bustCheck = false;
		return hand.bustCheck;
	}
	
	//카드의 총합을 리턴
	public static int getNumberCount(Hand hand){
		return hand.cardNumberCount;
	}
	
	//카드의 총합 계산
	private static void cardNumberCount(Hand hand){
		//K,Q,J 중 하나일 경우 10으로 계산
		if(Card.getCardNumber(hand.hand[hand.cardCount]).equals("K") || Card.getCardNumber(hand.hand[hand.cardCount]).equals("Q") || Card.getCardNumber(hand.hand[hand.cardCount]).equals("J"))
			hand.cardNumberCount += 10;
		//A일 경우 11을 더하고 haveAce를 1증가시켜 A를 가진 장수를 카운트함
		else if(Card.getCardNumber(hand.hand[hand.cardCount]).equals("A")){
			hand.cardNumberCount += 11;
			hand.haveAce++;
		} else
			hand.cardNumberCount += Integer.parseInt(Card.getCardNumber(hand.hand[hand.cardCount]));
		checkAce(hand);
	}
	//Ace카드가 있는지 체크
	private static void checkAce(Hand hand){
		while(hand.cardNumberCount > 22){
			if(hand.haveAce <= 0){    //Ace카드가 한장도 없는 경우 반복문 탈출
				hand.bustCheck=true;
				break;
			}
			hand.cardNumberCount -= 10;	//Ace카드를 1장이상 가지고 있고 버스트상태이면 A를 1로 계산하기 위해 손패의 합에서 10을 뺌
			hand.haveAce--;
		}
	}
	
	public static int getNumberCount(Person person){
		return person.hand.cardNumberCount;
	}


}
