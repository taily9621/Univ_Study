package blackjack;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Blackjack {
	public static void blackjack(){
		Dealer dealer = new Dealer();  //딜러
		Player player = new Player("플레이어1",20000); //플레이어1,기본 소지금 1만원
		Computer computer = null;
			computer = new Computer("플레이어2",20000); //플레이어2(cpu),기본 소지금 1만원
		Scanner sc = new Scanner(System.in);
		Deck deck = new Deck();
		int gamePlayCount = 4;  //덱을 셔플하고 플레이한 횟수,맨 처음 셔플하기 위해 4로 초기화
		String winner1;  //플레이어1과 딜러중 승자를 저장
		String winner2;  //플레이어2와 딜러중 승자를 저장
		int bet1 = 1000; //플레이어1의 베팅금액
		int bet2;        //플레이어2의 베팅금액
		String draw;  //Hit,Stand,Double Down중 하나를 선택한것을 저장할 변수
		
		System.out.println("블랙잭 게임을 시작합니다.\n"
				+ "먼저 60000원을 만들면 승리하고 0원이 될 경우 패배합니다.\n");
		
		//소지금이 0원 이하가 될때까지 반복
		while(player.getMoney()>0){
			Hand.handReset(dealer.hand);
			Hand.handReset(player.hand);
			Hand.handReset(computer.hand);
			gamePlayCount++;
			//게임 플레이 횟수가 4회 이상이면 덱을 셔플
			if(gamePlayCount>3){
				Deck.shuffle();
				gamePlayCount = 0;
			}
			do{
				try {
					System.out.println("플레이어1의 소지금 : "+player.getMoney());
					System.out.println("플레이어2의 소지금 : "+computer.getMoney());
					System.out.print("베팅 할 금액을 입력해 주세요. ");
					bet1 = sc.nextInt();
					System.out.print(player.getName() + "이(가) ");
					player.bettingMoney(bet1);
				} catch (InputMismatchException e){
					System.out.println("자연수를 입력해 주세요.");
					sc.nextLine();
				}
			} while(!player.bettingCheck());
			bet2 = computer.computerBetting();
			System.out.print(computer.getName() + "이(가) ");
			computer.bettingMoney(bet2);
			System.out.println();
			
			//맨 처음 딜러-플레이어1-플레이어2 순으로 2번씩 카드를 뽑음
			System.out.println("모두 두장씩 카드를 뽑습니다.");
			for(int i = 0; i<2; i++){
				dealer.draw(dealer);
				player.draw(player);
				computer.draw(computer);
			}
			printHand(dealer,player,computer);
			
			pause();
			System.out.println(player.getName() + "이(가) 카드를 뽑을 차례입니다.");

			 while(true){
				System.out.println("현재 카드의 총합은 " + Hand.getNumberCount(player) + "입니다.");
				System.out.println("카드를 뽑으시겠습니까? 원할 경우 h(Hit)를, 더이상 뽑길 원하지 않는 경우 s(Stand)를 입력해 주세요.");
				System.out.println("만약 d(Double Down)를 입력할 경우 두배로 베팅을 하고 단 한장만 더 받습니다.");
				draw=sc.next();
				if(draw.equals("d")){		//Double Down을 했을 경우
					bet1 = player.doubleDown(bet1);
				}
				if(draw.equals("s"))  //s를 입력하면 반복문 탈출;
					break;
				
				player.draw(player);
				System.out.println(player.getName() + "이(가) 카드를 뽑았습니다.");
				printHand(dealer,player,computer);
				if((player.bustCheck()||draw.equals("d"))) //카드를 뽑고 버스트가 되었거나 d를 입력하면 반복문 탈출
					break;
				
			}
			sc.nextLine();
			pause();
			System.out.println(computer.getName() + "이(가) 카드를 뽑을 차례입니다.");
			String cpuChoice;
			while(true){
				cpuChoice = computer.hitOrStand();
				System.out.println(computer.getName() + "이(가) " + cpuChoice + "를 선택하였습니다.");
				if(cpuChoice.equals("DoubleDown")){	//Double Down을 했을 경우
					computer.doubleDown(bet2);
				}
				if(cpuChoice.equals("Stand"))  //Stand를 입력하면 반복문 탈출;
					break;
				
				computer.draw(computer);
				printHand(dealer,player,computer);
				System.out.println();
				if((computer.bustCheck()||cpuChoice.equals("DoubleDown"))) //카드를 뽑고 버스트가 되었거나 DoublwDown를 입력하면 반복문 탈출
					break;
			}
			pause();
			System.out.println("딜러의 차례를 진행합니다. \n");
			dealer.dealerPlay();
			
			System.out.println("결과를 공개합니다.");
			pause();
			sc.nextLine();
			printAllHand(dealer,player,computer);
			System.out.println("--------------------------------------");
			System.out.println("결과\n딜러 : " + Hand.getNumberCount(dealer) + ", 플레이어1 : " + Hand.getNumberCount(player) + ", 플레이어2 : " + Hand.getNumberCount(computer));
			System.out.println("--------------------------------------");
			
			winner1 = compareHand(dealer,player);
			winner2 = compareHand(dealer,computer);

			winner(winner1,player,bet1);
			winner(winner2,computer,bet2);
			
			computer.gameRecord(winner2);
			
			System.out.println("종료하시려면 q를 눌러주세요.");
			String tmp;
			tmp = sc.nextLine();
			try {
				computer.saveFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(tmp.equals("q"))
				return;
			else if(player.money>=60000){
				System.out.println(player.getName() + "이(가) 60000원이 되어 승리하였습니다.");
			}
			else if(computer.money>=60000){
				System.out.println(computer.getName() + "이(가) 60000원이 되어 승리하였습니다.");
			}
			else if(player.money<=0){
				System.out.println(player.getName() + "이(가) 돈을 모두 잃어 패배하였습니다.\n"
						+ computer.getName() + "이(가) 승리하였습니다.");
				return;
			}
			else if(computer.money<=0){
				System.out.println(computer.getName() + "이(가) 돈을 모두 잃어 패배하였습니다.\n"
						+ player.getName() + "이(가) 승리하였습니다.");
				return;
			}
			else;
		}
	}
	
	
	//양쪽의 핸드를 출력(딜러의 경우 한장은 미공개)
	public static void printHand(Dealer dealer,Player player,Computer computer){
		dealer.printHand(dealer);
		player.printHand();
		computer.printHand();
	}
	
	//양쪽의 핸드를 모두 출력
	public static void printAllHand(Dealer dealer,Player player,Computer computer){
		dealer.printHand();
		player.printHand();
		computer.printHand();
	}
	
	//핸드를 비교하여 승자를 리턴
	public static String compareHand(Dealer dealer,Player player){
		int dealerNumberCount = Hand.getNumberCount(dealer); //딜러의 핸드 총합을 저장
		int playerNumberCount = Hand.getNumberCount(player); //플레이어의 핸드 총합을 저장
		//양쪽다 버스트 되었을 경우
		if(Hand.bustCheck(dealer.hand)&&Hand.bustCheck(player.hand))
			return "draw";
		
		//딜러만 버스트 되었을 경우
		else if(Hand.bustCheck(dealer.hand))
			return "player";
		
		//플레이어만 버스트 되었을 경우
		else if(Hand.bustCheck(player.hand))
			return "dealer";
		
		//양쪽다 버스트 되지 않고
		else{
			//플레이어의 핸드총합이 더 클 경우
			if(playerNumberCount>dealerNumberCount)
				return "player";
			
			//딜러의 핸드 총합이 더 클 경우
			else if(playerNumberCount<dealerNumberCount)
				return "dealer";
			
			//양쪽다 블랙잭인 경우
			else if(player.hand.blackjack&&dealer.hand.blackjack)
				return "draw";
			//플레이어만 블랙잭인 경우
			else if(player.hand.blackjack)
				return "player";
			//딜러만 블랙잭인 경우
			else if(dealer.hand.blackjack)
				return "dealer";
			//양쪽 다 블랙잭이 아니고 총합이 같은 경우
			else
				return "draw";
		}
	}
	//승자가 결정되고 처리
	public static void winner(String winner,Player player,int betting){
		if(winner.equals("dealer")){
			System.out.println("딜러가 이겼습니다. \n" + player.getName() + "이(가) " + betting + "원을 잃었습니다.");
			betting=0;
		}
		else if(winner.equals("player")){
			if(player.hand.blackjack){  //블랙잭을 가지고 이기면 돈을 3배로 받음
				System.out.println("블랙잭으로 " + player.getName() + "이(가) 이겼습니다. \n" + player.getName() + "이(가) "  + betting*2 + "원을 얻었습니다.");
				player.moneyGet(betting*3);
			}
			else{
				System.out.println(player.getName() + "이(가) 이겼습니다. \n" + player.getName() + "이(가) "  + betting + "원을 얻었습니다.");
				player.moneyGet(betting*2);
			}
			betting=0;
		}
		else{
			System.out.println(player.getName() + "이(가) 비겼습니다. \n" + player.getName() + "이(가) " + betting + "원을 돌려받았습니다.");
			player.moneyGet(betting);
			betting=0;
		}
	}
	//블랙잭 규칙
	public static void gameDescription(){
		System.out.println("블랙잭 카드게임의 규칙입니다.\n"
				+ "1.베팅을 한다.\n"
				+ "2.딜러가 자신을 포함한 참가자 전원에게 카드 두 장을 나누어 주는데,\n    딜러의 카드 한 장은 상대에게 보이지 않는다.\n"
				+ "3.카드의 합이 딜러보다 먼저 21이 되거나 딜러보다 21에 가깝게 되면\n    이기고, 카드를 더 받았는데 21을 초과하면 버스트(Bust)된다.\n"
				+ "4.먼저 받은 카드 두 장의 합이 21에 못 미치면 히트(Hit)를 선택하면 \n    한 장씩 더 받을 수 있고, 멈추려면 스탠드(Stand)를 선택한다.\n"
				+ "5.딜러는 카드의 합이 16 이하면 무조건 한 장을 더 받아야 하고, \n  17이상의 경우에는 멈추어야 한다.\n"
				+ "6.딜러의 카드와 합이 같으면 비긴 것이 된다.\n"
				+ "7.에이스 카드는 1이나 11로 취급할 수 있고, 10, J, Q, K는 모두\n  10으로 계산한다.\n"
				+ "8.처음 받은 카드 두 장이 에이스와 10, J, Q, K 중의 하나로 합이\n  21이 되면 블랙잭(Blackjack)이 되고, 베팅한 금액의 2배의 돈을 받는다.");
	}
	//일시 정지
	public static void pause() {
		try {
			System.in.read();
		} catch (IOException e) { }
	}
}
