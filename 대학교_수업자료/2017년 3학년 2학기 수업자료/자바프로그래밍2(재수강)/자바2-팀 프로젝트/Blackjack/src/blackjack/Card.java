package blackjack;

public class Card {
	private String cardPattern;
	private String cardNumber;
	
	Card(String pattern, String number){
		this.cardPattern=pattern;
		this.cardNumber=number;
	}
	
	static String getCardPattern(Card card){
		return card.cardPattern;
	}
	
	static String getCardNumber(Card card){
		return card.cardNumber;
	}
	
	public String toString(){
		return cardPattern + cardNumber +" ";
	}
}