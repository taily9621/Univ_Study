package user;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import fish.Fish;
import fish.FishDemo;
import manager.Factory;
import manager.Manager;

public class UserManager extends Manager<User> implements Factory<User>, Iterable<User> {
	Scanner keyin = new Scanner(System.in);

	public void init() {
		mList.clear();
		readAll("src/manager/user.txt", new UserManager());
		for (User u : mList) {
			FishDemo.idList.put(u.getId(), u.getPassword());
		}
	}

	@Override
	public Iterator<User> iterator() {
		return getList().iterator();
	}

	public void remove(User user) {
		getList().remove(user);
	}

	public User readNew() {
		User u = new User();
		u.readNewUser(keyin);
		return u;
	}

	@Override
	public User create() {
		return new User();
	}

	public void update(User u) {
		System.out.println(u.getName() + "수정...");
		u.update(keyin);
		return;
	}

	public boolean idCheck(String id) {
		for (User u : mList)
			if (u.getName().equals(id))
				return true;
		return false;
	}

	public boolean passCheack(String id, String pass) {
		User user = null;
		for (User u : mList)
			if (u.getName().equals(id))
				user = u;
		if (user == null)
			return false;
		if (user.getPassword().equals(pass))
			return true;
		else
			return false;
	}

	// 악세서리의 내용을 txt파일로 저장
	public void writeFile() {
		try {
			File file = new File("txt/user.txt");
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));

			if (file.isFile() && file.canWrite()) {
				bufferedWriter.write("아이디 비밀번호 물고기 0 악세서리");
				bufferedWriter.newLine();
				for (User u : mList) {
					bufferedWriter.write(u.toLine());
					bufferedWriter.newLine();
				}
			}
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
