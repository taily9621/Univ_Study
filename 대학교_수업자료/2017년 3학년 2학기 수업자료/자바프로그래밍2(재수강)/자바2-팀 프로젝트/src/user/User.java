package user;

import java.util.ArrayList;
import java.util.Scanner;

import accessory.Accessory;
import accessory.AccessoryManager;
import manager.Managable;
import fish.Fish;
import fish.FishDemo;
import fish.FishManager;

public class User implements Managable {

	private String id; // 아이디
	private String password; // 패스워드
	private ArrayList<Fish> fList = new ArrayList<>(); // 물고기 목록
	private ArrayList<Accessory> aList = new ArrayList<>();// 어항용품 목록

	public void setId(String id) {
		this.id = id;
	}

	// 아이디 반환
	public String getId() {
		return this.id;
	}

	// 아이디 반환
	public String getName() {
		return this.id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// 패스워드 반환
	public String getPassword() {
		return this.password;
	}

	// 유저정보 저장
	public void read(Scanner fileIn) {
		this.id = fileIn.next();
		this.password = fileIn.next();

		String tmp;
		Fish fish;
		tmp = fileIn.nextLine();
		tmp.trim();
		String str[] = tmp.split(" ");
		int i = 0;
		for (; i < str.length; i++) {
			if (str[i].equals("0"))
				break;
			if (str[i].equals(""))
				continue;
			fish = FishDemo.fishMgr.find(str[i]);
			if (fish != null)
				fList.add(fish);
		}
		i++;
		Accessory acce;
		for (; i < str.length; i++) {
			if (str[i].equals(""))
				continue;
			acce = FishDemo.acceMgr.find(str[i]);
			if (acce != null)
				aList.add(acce);
		}
	}

	// 유저 정보 출력
	public void print() {
		System.out.println("ID : " + id + "\nPASSWORD : " + password);
		for (Fish f : fList)
			f.print();
		for (Accessory a : aList)
			a.print();
		System.out.println();
	}

	// id 일부가 맞는지 비교
	@Override
	public boolean compare(String kwd) {
		return this.id.equals(kwd);
	}

	// fList에서 물고기 추가
	public void add(Fish fish) {
		fList.add(fish);
	}

	// fList에서 해당하는 물고기 삭제
	public void remove(Fish fish) {
		fList.remove(fish);
	}

	// aList에 어항용춤 추가
	public void add(Accessory acce) {
		aList.add(acce);
	}

	// aList에서 해당하는 어항용품 삭제
	public void remove(Accessory acce) {
		aList.remove(acce);
	}

	// 새로운 유저 추가
	public void readNewUser(Scanner keyin) {
		System.out.print("ID : ");
		this.id = keyin.next();
		System.out.print("PASSWORD : ");
		this.password = keyin.next();
	}

	// 유저 정보 수정
	public void update(Scanner keyin) {
		System.out.print("ID : ");
		this.id = keyin.next();
		System.out.print("PASSWORD : ");
		this.password = keyin.next();
	}

	public ArrayList<Fish> getFishList() {
		return this.fList;
	}

	public ArrayList<Accessory> getAcceList() {
		return this.aList;
	}

	public String toLine() {
		StringBuffer fishAcce = new StringBuffer();
		for (Fish f : fList) {
			fishAcce.append(f.getName());
			fishAcce.append(" ");
		}
		fishAcce.append("0 ");
		for (Accessory a : aList) {
			fishAcce.append(a.getName());
			fishAcce.append(" ");
		}

		return id + " " + password + " " + fishAcce;
	}
}
