package accessory;

import java.util.Scanner;

import manager.Managable;

public class Accessory implements Managable {
	static int count = 0;
	int number; // 악세서리 고유 번호
	String name; // 악세서리 이름

	@Override
	public void read(Scanner scan) {
		name = scan.next();
		// price = scan.nextInt();
		number = ++count;
	}

	@Override
	public void print() {
		System.out.println("품명 : "+this.name);
	}

	@Override
	public boolean compare(String kwd) {
		if(name.contains(kwd))
			return true;
		return false;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getNumber() {
		return this.number;
	}
	public String toLine() {
		return name;
	}
}
