package accessory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import fish.FishDemo;
import manager.Factory;
import manager.Manager;

public class AccessoryManager extends Manager<Accessory> implements Factory<Accessory>, Iterable<Accessory> {
	Scanner keyin = new Scanner(System.in);

	public void init() {
		mList.clear();
		readAll("src/manager/accessory.txt", new AccessoryManager());
		for (Accessory a : mList)
			FishDemo.acceName.add(a.getName());
	}

	@Override
	public Iterator<Accessory> iterator() {
		return getList().iterator();
	}

	public void remove(Accessory accessory) {
		getList().remove(accessory);
	}

	@Override
	public Accessory create() {
		return new Accessory();
	}

	// 악세서리의 내용을 txt파일로 저장
	public void writeFile() {
		try {
			File file = new File("txt/accessory.txt");
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));

			if (file.isFile() && file.canWrite()) {
				bufferedWriter.newLine();
				for (Accessory a : mList) {
					bufferedWriter.write(a.toLine());
					bufferedWriter.newLine();
				}
			}
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
