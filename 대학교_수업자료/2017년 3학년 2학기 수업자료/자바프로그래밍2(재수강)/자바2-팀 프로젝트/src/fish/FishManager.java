package fish;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import manager.Factory;
import manager.Manager;

public class FishManager extends Manager<Fish> implements Factory<Fish>, Iterable<Fish> {
	Scanner keyin = new Scanner(System.in);

	public void init() {
		mList.clear();
		readAll("src/manager/fish.txt", this);
		for (Fish f : getList()) {
			FishDemo.fishName.add(f.getName());
		}
	}

	@Override
	public Iterator<Fish> iterator() {
		return getList().iterator();
	}

	public void remove(Fish fish) {
		getList().remove(fish);
	}

	@Override
	public Fish create() {
		return new Fish();
	}

	// 태그와 kwd가 맞는 물고기를 찾아서 반환
	public ArrayList<Fish> findAll(int tag, String kwd) { // 태그 1: 이름, 2:수온, 3:길이, 4:가격
		ArrayList<Fish> list = new ArrayList<>();
		for (Fish m : mList)
			if (m.tagCompare(tag, kwd))
				list.add(m);
		return list;
	}

	public void writeFile() {
		try {
			File file = new File("txt/fish.txt");
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));

			if (file.isFile() && file.canWrite()) {
				bufferedWriter.write("이름 최소온도 최고온도 길이 수명 특징");
				bufferedWriter.newLine();
				for(Fish f:mList) {
					bufferedWriter.write(f.toLine());
					bufferedWriter.newLine();
				}
			}
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
