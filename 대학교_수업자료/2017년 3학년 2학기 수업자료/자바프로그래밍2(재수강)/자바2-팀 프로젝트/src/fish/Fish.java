package fish;

import java.util.Scanner;

import manager.Managable;

public class Fish implements Managable {
	static int count = 1;
	int number; // 물고기 고유번호
	String name; // 이름
	int temperatureLow; // 최저수온
	int temperatureHigh; // 최고수온
	int length; // 길이
	int lifespan; // 수명
	String feature;

	public void read(Scanner in) {
		number = count++;
		name = in.next();
		temperatureLow = in.nextInt();
		temperatureHigh = in.nextInt();
		length = in.nextInt();
		lifespan = in.nextInt();
		feature = in.nextLine();
		feature = feature.trim();
	}

	public void print() {
		System.out.printf("%2d] %-5s\t수명 : %d년 적정 수온 : %2d ~ %2d %2dcm 특징 : %s\n", number, name, lifespan,
				temperatureLow, temperatureHigh, length, feature);
	}

	public boolean compare(String kwd) {
		if (name.equals(kwd))
			return true;
		else if (isNumber(kwd)) {
			int tp = Integer.parseInt(kwd);
			if (tp >= temperatureLow && tp <= temperatureHigh)
				return true;
			else
				return false;
		} else if (feature.contains(kwd))
			return true;
		else
			return false;
	}

	static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	public String getName() {
		return this.name;
	}

	public int getNumber() {
		return this.number;
	}

	public int getLength() {
		return this.length;
	}

	public int getLifesapn() {
		return this.lifespan;
	}

	public int getTemperatureLow() {
		return this.temperatureLow;
	}

	public int getTemperatureHigh() {
		return this.temperatureHigh;
	}

	public String getFeature() {
		return this.feature;
	}

	public boolean tagCompare(int tag, String kwd) { // 태그 1: 이름, 2:수온, 3:길이, 4:가격
		switch (tag) {
		case 1:
			if (name.equals(kwd))
				return true;
			return false;
		case 2:
			if (isNumber(kwd))
				if (temperatureLow <= Integer.parseInt(kwd) && temperatureHigh >= Integer.parseInt(kwd))
					return true;
			return false;
		case 3:
			if (isNumber(kwd))
				if (length <= Integer.parseInt(kwd))
					return true;
			return false;
		/*
		 * case 4: if(isNumber(kwd)) if(price>=Integer.parseInt(kwd)) return true;
		 * return false;
		 */
		default:
			return false;
		}
	}

	public String toString() {
		return "이름 : " + name + "\n" + 
			   "수명 : " + lifespan + "년 \t길이 : " + length + "cm\n" + 
			   "적정 수온 : " + temperatureLow + "°C ~ " + temperatureHigh + "°C\n" + 
			   "특징 : \n" + feature;
	}

	public String toLine() {
		return name + " " + temperatureLow + " " + temperatureHigh + " " + length + " " + lifespan + " " + feature;
	}
}
