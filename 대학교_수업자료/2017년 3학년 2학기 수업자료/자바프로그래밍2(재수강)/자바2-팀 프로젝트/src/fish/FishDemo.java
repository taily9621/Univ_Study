package fish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import accessory.AccessoryManager;
import user.UserManager;

public class FishDemo {
	public static UserManager userMgr = new UserManager();
	public static FishManager fishMgr = new FishManager();
	public static AccessoryManager acceMgr = new AccessoryManager();
	public static HashMap<String,String> idList = new HashMap<>();
	public static ArrayList<String> fishName = new ArrayList<>();
	public static ArrayList<String> acceName = new ArrayList<>();
	static {
		fishMgr.init();
		acceMgr.init();
		userMgr.init();
	}

	public void doit() {
		
	}

	public static void search(String s) {
		Scanner in = new Scanner(System.in);
		ArrayList<Fish> fish;
		while (true) {
			System.out.printf("검색어를 입력하세요..(종료 y) : ");
			String kwd = in.nextLine();
			kwd = kwd.trim();
			if (kwd.equals("y"))
				break;
			else {
				fish = fishMgr.findAll(kwd);
				for (Fish f : fish) {
					f.print();
				}
			}
		}
		in.close();
	}
	//모든 Mgr에 저장되어 있는 파일을 txt파일로 저장
	public static void writeAllFile() {
		fishMgr.writeFile();
		acceMgr.writeFile();
		userMgr.writeFile();
	}

	public static void main(String[] args) {
		FishDemo demo = new FishDemo();
		demo.doit();
		fishMgr.printAll();
		acceMgr.printAll();
		userMgr.printAll();
		writeAllFile();
		ArrayList<Fish> u = fishMgr.findAll(2,"28");
		for(Fish f:u)
			f.print();
	}
}
