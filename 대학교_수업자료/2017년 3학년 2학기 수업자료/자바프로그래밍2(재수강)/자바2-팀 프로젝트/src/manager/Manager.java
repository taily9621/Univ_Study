package manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager<E extends Managable> {
	protected ArrayList<E> mList = new ArrayList<>();

	// 리스트를 반환
	public ArrayList<E> getList() {
		return mList;
	}

	// 파일 열어서 스캐너로 반환
	public static Scanner openFile(String filename) {
		String path= Manager.class.getResource("").getPath();
		File f = new File(path+filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
			throw new RuntimeException();
		}
		return fileIn;
	}

	// 스캐너로 받아온 txt파일의 내용을 읽어서 리스트에 추가
	public void readAll(Scanner in, Factory<E> fac) {
		E st = null;
		while (true) {
			st = fac.create();
			st.read(in);
			mList.add(st);
		}
	}

	// 읽어올 txt의 파일명을 받아서 리스트에 추가
	public void readAll(String filename, Factory<E> fac) {
		Scanner fileIn = openFile(filename);
		E m = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			m = fac.create();
			m.read(fileIn);
			mList.add(m);
		}
		fileIn.close();
	}

	// 리스트의 내용을 출력
	public void printAll() {
		for (E e : mList) {
			e.print();
		}
	}

	// kwd에 해당하는 객체를 찾아서 반환
	public E find(String kwd) {
		for (E m : mList)
			if (m.compare(kwd))
				return m;
		return null;
	}

	// kwd에 해당하는 객체를 모두 찾아서 ArrayList로 반환
	public ArrayList<E> findAll(String kwd) {
		ArrayList<E> list = new ArrayList<>();
		for (E m : mList)
			if (m.compare(kwd))
				list.add(m);
		return list;
	}
}
