package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import fish.Fish;
import fish.FishDemo;
import fish.FishManager;

public class listView extends JPanel implements MouseListener {
	public static int fishNum = FishDemo.fishName.size();
	public static int tag = 1;
	public static int fishcnt =0;
	public static fishView fv = new fishView();
	public static mybowlView mv = new mybowlView();
	int wd, ht;
	Font lbfont = new Font("맑은 고딕", 33, 33);
	JRadioButton name;
	JRadioButton temp;
	JRadioButton length;
	String[] text = { "이름", "수온", "길이" };
	JRadioButton[] radio = new JRadioButton[3];
	JPanel radioPanel = new JPanel();
	JTextField kwd;
	JLabel search;
	JLabel purchase;
	JLabel reset; // 되돌리기
	JLabel mybowlfish;
	ArrayList<Fish> searchedlist;
	Color skyblue = new Color(135, 182, 216);
	String searchword;
	public static Fish curfish = null;
	ArrayList<Fish> selectedFish = new ArrayList<>();
	JLabel[] fishlist = new JLabel[fishNum];

	public void init() {
		// TODO Auto-generated method stub
		setLayout(null);
		// list
		JPanel imageList = new JPanel();
		ImageIcon fishImage[] = new ImageIcon[fishNum];
		imageList.setLayout(new GridLayout(8, 5));
		Image image;
		Image newing;
		imageList.setBackground(new Color(135, 182, 216));

		for (int i = 0; i < FishDemo.fishName.size(); i++) {

			System.out.println(FishDemo.fishName.get(i));
			fishImage[i] = new ImageIcon("src/images/" + FishDemo.fishName.get(i) + ".png");
			image = fishImage[i].getImage();
			newing = resizeImage(image, 100, 100);
			fishImage[i].setImage(newing);
			fishlist[i] = new JLabel(fishImage[i]);
			fishlist[i].addMouseListener(this);
			initLabel(fishlist[i]);
			fishlist[i].setBackground(Color.white);
			fishlist[i].setName(FishDemo.fishName.get(i));
			imageList.add(fishlist[i]);

		}

		imageList.setBounds(0, 0, 650, 800);
		add(imageList);

		// radiobutton

		radioPanel.setBackground(Color.GRAY);
		radioPanel.setBounds(0, 800, 100, 300);

		add(radioPanel);
		ButtonGroup g = new ButtonGroup();
		for (int i = 0; i < radio.length; i++) {
			radio[i] = new JRadioButton(text[i]);
			g.add(radio[i]);
			radioPanel.add(radio[i]);
			radio[i].addItemListener(new MyItemListener());
			radio[i].setFont(lbfont);

		}
		radio[0].setSelected(true);
		// kwd
		kwd = new JTextField(10);
		kwd.setFont(lbfont);
		kwd.setBounds(200, 850, 300, 50);
		add(kwd);
		// search
		search = new JLabel("검색");
		search.setBounds(500, 850, 100, 50);
		search.setBackground(skyblue);
		initLabel(search);

		search.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				searchword = kwd.getText();
				System.out.println(tag);
				System.out.println(searchword);
				selectedFish = FishDemo.fishMgr.findAll(tag, searchword); // 검색에매치된fishlist
				changeFishList();
			}
		});
		add(search);
		// purchase
		purchase = new JLabel("어항에 담기");
		purchase.setBounds(300, 910, 300, 50);
		purchase.setBackground(skyblue);
		;
		initLabel(purchase);

		purchase.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				// 어항에 담기
				if(fishcnt<4)
				mv.putFish(null);
				fishcnt++;

			}

		});
		add(purchase);

		// reset
		reset = new JLabel("되돌리기");
		reset.setBounds(300, 970, 200, 50);
		reset.setBackground(skyblue);
		;
		initLabel(reset);

		reset.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				// 라벨리셋
				labelReset();
			}
		});
		add(reset);

	}

	public void changeFishList() {
		// TODO Auto-generated method stub
		for (int i = 0; i < FishDemo.fishName.size(); i++) {
			if (selectedFish.size() > 0) {
				for (int j = 0; j < selectedFish.size(); j++)
					if (!fishlist[i].getName().equals(selectedFish.get(j).getName())) {
						fishlist[i].setVisible(false);
					}

			}
		}

	}

//	public void putFish() {
//		// TODO Auto-generated method stub
//		Image image = fishView.fishImage.getImage();// curFishImageimageicon															
//		Image newing = resizeImage(image, 300, 300);
//		mybowlfish = new JLabel();
//		mybowlfish.setIcon(new ImageIcon(newing));
//		mybowlView.panel2.add(mybowlfish);
//		mybowlfish.setVisible(true);
//	}

	class MyItemListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			// TODO Auto-generated method stub
			if (e.getStateChange() == ItemEvent.DESELECTED)
				return;
			if (radio[0].isSelected()) {
				tag = 1;
			} else if (radio[1].isSelected()) {
				tag = 2;
			} else {
				tag = 3;
			}

		}

	}

	void reSize() {
		wd = getWidth();
		ht = getHeight();

	}

	Image resizeImage(Image image, int width, int height) {
		Image resizeImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return resizeImage;
	}

	private void labelReset() {
		for( int i=0; i<fishlist.length; i++){
			fishlist[i].setVisible(true);
		}

	}

	public void initLabel(JLabel lb) {
		lb.setFont(lbfont);
		lb.setHorizontalAlignment(JLabel.CENTER);
		lb.setLayout(null);
		lb.setOpaque(true);
		lb.setVisible(true);
		add(lb);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

		JLabel l = (JLabel) e.getSource();
		l.setBackground(skyblue);
		System.out.println(l.getName());
		curfish = FishDemo.fishMgr.find(l.getName());// 현재물고기
		fv.changeCurfish(curfish);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		JLabel l = (JLabel) e.getSource();
		l.setBackground(skyblue);

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		JLabel l = (JLabel) e.getSource();
		l.setBackground(Color.white);

	}

}