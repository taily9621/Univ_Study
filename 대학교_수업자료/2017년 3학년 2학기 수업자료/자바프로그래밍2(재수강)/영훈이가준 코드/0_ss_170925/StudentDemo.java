package jjj;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

class Student {
	String id;
	String name;
	//int age;
	int grade;
	//boolean bMale;
	int bMale;
	String readMF;
	String []maleFemale=new String[2];

	ArrayList<Integer> scores = new ArrayList<>();
	int sum=0;
	double avg=0;
	
	public Student() {//생성자
		// TODO Auto-generated constructor stub
		maleFemale[0]="(남)";
		maleFemale[1]="(여)";
	}

	void read(Scanner s) {//입력받기

		id = s.next();
		if (id.equals("0"))
			return;
		name = s.next();
		//age = s.nextInt();
		grade=s.nextInt();
		readMF = s.next();
		if (readMF.charAt(0) == 'm')bMale = 0;
		 else bMale = 1;
	}
	
	void readScore(Scanner s)//해당 id의 스코어 입력받기
	{
		while(true)
		{
			int score=s.nextInt();
			if(score==0) break;
			sum+=score;
			scores.add(score);
		}
		avg=(double)sum/(double)scores.size();//평균구하기
	}

	
	public String toString() {//출력할것을 문자열로 변환
		String result = null;
		int id = Integer.parseInt(this.id);
		result = String.format(Locale.KOREA, "[%d] %s %d학년 %s ", id, name, grade, maleFemale[bMale]);
		if(scores.size()>0)
		{
			String result2 = null;
			result2 = String.format(Locale.KOREA, "평균 : %.0f점",avg);
			result+=result2;
		}
		result+="\n";//%n으로는 줄바꿈이 되지 않습니다.
		
		return result;
	}
	
	boolean isEqualId(String id)
	{
		return this.id.equals(id);
	}

}

public class StudentDemo {
	Scanner s = new Scanner(System.in);
	ArrayList<Student> al = new ArrayList<>();
	public static void main(String[] args) {//메인
		StudentDemo demo = new StudentDemo();
		demo.doit();
	}
	
	void doit()
	{
		readAll();//일반정보, 점수 정보 읽기
		checkAll();//중복이름체크
		printAll();//전부 출력
	}
	void readAll()
	{
		while (true) {//일반 정보 입력받기
			Student st=new Student();
			st.read(s);
			if (st.isEqualId("0"))
				break;
			al.add(st);
		}
		System.out.println("학생정보 입력 끝");
		while(true)//점수입력받기
		{
			String id=s.next();
			if(id.equals("0")) break;
			int idIndex=findById(id);
			if(idIndex==-1) continue;//입력받은 아이디가 없을 경우 어차피 점수 못받음
			al.get(idIndex).readScore(s);//해당 id에 점수입력받음
		}
		System.out.println("점수 입력 끝");
	}
	void checkAll()
	{
		for(int i=0;i<al.size();i++)
		{
			getUniqueName(al.get(i).name);//이름 겹치는거 있으면 (1) 이런식으로 붙여주기
			//나중에 중복된거 검사 해도 lee(1)이런식으로 검사해서 안걸림
		}
	}
	void printAll()
	{
		for (int i = 0; i < al.size(); i++) {
			System.out.print(al.get(i).toString());//전부프린트
			//al.get(i).print();
		}
	}
	
	void getUniqueName(String name)
	{
		int k=-1;
		for(int i=0; i<al.size();i++)//모든걸 검사
		{
			if(al.get(i).name.equals(name)){
				k++;
				if(k>0){//k가 0이면 첫번쨰로 걸린것이고 그건 자기자신
					al.get(i).name+="("+k+")";
				}
			}
		}
	}
	int findById(String id)//id찾기
	{
		for(int i=0;i<al.size();i++)
		{
			if(al.get(i).isEqualId(id)) return i;
		}
		return -1;//id가 학생중에 없으면 -1 리턴
	}
}
