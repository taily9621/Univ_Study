﻿package test01;

import java.util.HashMap;

public enum Day {
	SUNDAY(0, "Sun", "일"), MONDAY(1, "Mon", "월"), 
	TUESDAY(2, "Tue", "화"), WEDNESDAY(3, "Wed", "수"), 
	THURSDAY(4, "Thu", "목"), 
	FRIDAY(5, "Fri", "금"), SATURDAY(6, "Sat", "토");
	private String value, name, korName;
	private static HashMap<String, Day> strMap = new HashMap<>();
	private static HashMap<String, Day> korMap = new HashMap<>();

	private Day(int n, String name, String kor) {
		value = n;
		this.name = name;
		korName = kor;
	}
	String getKorName() {
		return korName;
	}
	static {
		for (Day d : Day.values()) {
			strMap.put(d.name, d);
			korMap.put(d.korName, d);
		}
	}
	static Day get(String str) {
		return strMap.get(str);
	}
}



