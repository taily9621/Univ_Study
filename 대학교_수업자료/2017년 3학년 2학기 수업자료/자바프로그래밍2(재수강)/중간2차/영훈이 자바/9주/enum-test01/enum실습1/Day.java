﻿package test01;

import java.util.HashMap;

public enum Day {
	SUNDAY(0), MONDAY(1), 
	TUESDAY(2), WEDNESDAY(3), 
	THURSDAY(4), FRIDAY(5), SATURDAY(6);
	private int value;
	private static HashMap<Integer, Day> map = new HashMap<>();

	private Day(int n) {
		value = n;
	}
	String getValue() {
		return value;
	}
	static {
		for (Day d : Day.values()) {
			map.put(d.value, d);
		}
	}
	static Day get(int n) {
		return map.get(n);
	}
}



