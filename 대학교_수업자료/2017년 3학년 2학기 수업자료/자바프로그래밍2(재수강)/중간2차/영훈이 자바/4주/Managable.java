package Manager;

import java.util.Scanner;

public interface Managable { 
	void print();            
	void read(Scanner s);
	boolean compare(String kwd);
}

