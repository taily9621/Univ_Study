package j3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import javax.print.attribute.standard.MediaSize.Other;


class Item
{
	int kind123;//일반인지 보통할인인지 묶음할인인지
	String []kindName={"운동복","운동바지","후드티","면티","바지","반바지","치마"};
	
	int index;
	String id;
	int kind;
	String name;
	int price;
	String maleFemale;
	String sizeKind;
	String kName;
	
	Item(int index_)
	{
		index=index_;
		kind123=1;
	}
	
	void read(Scanner s)
	{
		id=s.next();
		kind=s.nextInt();
		name=s.next();
		price=s.nextInt();
		maleFemale=s.next();
		sizeKind=s.next();
		kName=kindName[kind-1];
	}
	public String toString()
	{
		String result;
		result=String.format(Locale.KOREA, "%d) [%s] %s %s   (%d) - %s",index,id,kName,name,price,sizeKind);
		return result;
	}
}

class GoodItem extends Item
{
	String goodKindStr;
	String goodKind;//	//n:전체      y:회원 할인
	int goodPrice;
	int discountPrice;
	GoodItem(int index_)
	{
		super(index_);
		kind123=2;
	}
	void read(Scanner s)
	{
		super.read(s);
	
		goodKind=s.next();
		goodPrice=s.nextInt();
		discountPrice=price-goodPrice;
		if(goodKind.equals("n")) goodKindStr="전체/특별할인";
		else if(goodKind.equals("y")) goodKindStr="회원/특별할인";
	}
	public String toString()
	{
		String result=super.toString();
		result+=String.format(Locale.KOREA, "%s  %d원",goodKindStr,goodPrice);
		return result;
	}
}
class PackageItem extends Item
{
	String goodKindStr;
	String goodKind;//	//n:전체      y:회원 할인
	int goodPrice;//할인된 가격(둘이 합쳐져서 할인된가격임 )
	int discountPrice;//깎은 가격
	Item myPackageItem;
	
	PackageItem (int index_)
	{
		super(index_);
		kind123=3;
	}
	void read(Scanner s)
	{
		super.read(s);
		
		goodKind=s.next();
		goodPrice=s.nextInt();
		if(goodKind.equals("n")) goodKindStr="전체/묶음할인";
		else if(goodKind.equals("y")) goodKindStr="회원/묶음할인";
		String myPackageItemID=s.next();
		myPackageItem = ItemDemo.findItemByID(myPackageItemID);
		discountPrice=price+myPackageItem.price-goodPrice;
	
	}
	public String toString()
	{
		String result=super.toString();
		result+=String.format(Locale.KOREA, "%s  [%s]  %d원",goodKindStr,myPackageItem.id,
				goodPrice);
		return result;
	}
}

class OrderItem
{
	String userId;
	Item item;
	int cnt;
	String mySize;
	int allPrice;
	int minusPack=0;
	
	OrderItem(String userId_)
	{
		userId=userId_;
	}
	
	boolean read(Scanner s)
	{
		String name=s.next();
		if(name.equals("0")) return false;
		cnt=s.nextInt();
		mySize=s.next();
		
		item=ItemDemo.findItemByID(name);
		return true;
	}
	public String toString()//급해서 여기넣었지만 계산들은 따로 함수 만들어서 다른곳에서 하는게 좋겠음
	{
		int allPriceReal = 0;
		String result;
		String discountString="";//할인이 영어로 이제생각남
		if(item.kind123==2){
			if(  ((GoodItem)item).goodKind.equals("n")||//전체할인이거나
					ItemDemo.joinID.contains(userId)//이놈이 회원이거나
					)
			{
				discountString=String.format(Locale.KOREA, "%s  %d원(%d원 할인)",
						((GoodItem)item).goodKindStr,
						((GoodItem)item).goodPrice,
						((GoodItem)item).discountPrice);
				allPrice=((GoodItem)item).goodPrice*cnt;
			}
			else allPrice=item.price*cnt;
		}
		else if(item.kind123==3){//현재 오더받은 아이템이 패키지 아이템일 때
			OrderItem packItemOrder=ItemDemo.findItemByIdInOrderList(  ((PackageItem)item).myPackageItem.id  );
			if(  (((PackageItem)item).goodKind.equals("n")||//전체할인이거나
					ItemDemo.joinID.contains(userId))//이놈이 회원이거나
					&&(packItemOrder!=null)//패키지아이템이 산것중에 아예 없거나
					)
			{
				discountString=String.format(Locale.KOREA, "%s  %d원(%d원 할인) [%s]",
						((PackageItem)item).goodKindStr,
						((PackageItem)item).goodPrice,
						((PackageItem)item).discountPrice,
						((PackageItem)item).myPackageItem.id);
				
				if( cnt>packItemOrder.cnt ){
					
					allPrice=packItemOrder.cnt* (item.price-((PackageItem)item).discountPrice)+
							(cnt-packItemOrder.cnt)*item.price;
					packItemOrder.minusPack=packItemOrder.cnt;
				}
				else{//cnt<=pa.cnt
					allPrice=cnt*(item.price-((PackageItem)item).discountPrice);
					packItemOrder.minusPack=cnt;
				}
				
			}
			else allPrice=item.price*cnt;
		}
		else{
			allPrice=item.price*cnt;
			//allPriceReal=item.price*(cnt-minusPack);
		}
		
		result=String.format(Locale.KOREA, "%s [%s] %s  %s (%d)  %s  [%d개] size:%s %d",
				userId,
				item.id,
				item.kName
				,item.name
				,item.price,
				discountString
				,cnt
				,mySize,
				allPrice);
		
		//if(item.kind123==1) allPrice=allPriceReal;
		return result;
	}
}

public class ItemDemo {
	
	static ArrayList<String> joinID=new ArrayList<>();
	
	static ArrayList<Item> itemList=new ArrayList<>();
	static ArrayList<OrderItem> orderList=new ArrayList<>();
	Scanner sc = new Scanner(System.in);
	static
	{
		joinID.add("ejlee");
		joinID.add("admin");
	}
	
	Scanner openFile(String fileName) {
		Scanner fileIn = null;
		File f = new File(fileName);
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	
	public static void main(String[] args) {
		ItemDemo demo = new ItemDemo();
		demo.doit();
	}
		
	void doit()
	{
		readAll();
		printAll();
		getOrder();
	}
	void readAll()
	{
		Scanner fileIn = openFile("items.txt");
		Item item = null;
		fileIn.nextLine();
	
		//System.out.println("hh");
		int index=0;
		while (fileIn.hasNext()) {
			index++;
			System.out.println("시작");
			int kind= fileIn.nextInt();//1:그냥 2:할인 3:묶음할인
			System.out.println("kind:"+kind);
			
			switch(kind){
			case 1:
				item=new Item(index);//일반책
				break;
			case 2:
				item=new GoodItem(index);//원서책
				break;				
			case 3:
				item=new PackageItem(index);//원서책
				break;
			}
			
			item.read(fileIn);//읽기
			itemList.add(item);
		}
		//System.out.println("ee");
		fileIn.close();
	}
	void printAll()
	{
		for(Item item : itemList)
		{
			System.out.println(item);
		}
	}
	void getOrder()
	{
		String userId;
		OrderItem orderItem;
		while(true)
		{
			System.out.print("user id(end면 종료) : ");
			userId=sc.next();
			System.out.println("itemid 개수 사이즈 (끝내려면0) : ");
			while(true)
			{
				System.out.print("... ");
				orderItem=new OrderItem(userId);
				if(orderItem.read(sc)==false) break;
				orderList.add(orderItem);
			}
			printOrderList(userId);
			orderList.removeAll(orderList);
		}
	}
	
	void printOrderList(String userId)
	{
		int priceSum=0;
		System.out.println("printOrders : "+userId);
		for(OrderItem orders : orderList)
		{
			System.out.println(orders);
			//System.out.println(orders.allPrice);
			priceSum+=orders.allPrice;
		}
		System.out.println("합계 : "+priceSum+"원");
	}
	static Item findItemByID(String id)
	{
		for(Item item : itemList)
		{
			if(item.id.equals(id))
			{
				return item;
			}
		}
		return null;
	}

	static OrderItem findItemByIdInOrderList(String findId)
	{
		for(OrderItem orderItem : orderList)
		{
			if(orderItem.item.id.equals(findId)){
				return orderItem;
			}
		}
		return null;
	}
}
