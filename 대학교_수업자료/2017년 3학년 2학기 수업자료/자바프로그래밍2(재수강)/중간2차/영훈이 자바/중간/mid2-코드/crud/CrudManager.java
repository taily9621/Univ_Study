package crud;

import java.util.ArrayList;
import java.util.Scanner;
import manager.Managable;
import manager.Manager;

public class CrudManager<E extends Crudable & Managable> extends Manager<E> {
	Scanner keyin = null;
	public CrudManager(Scanner keyin) {
		this.keyin = keyin;
	}
	public void addOne(E e) {
		e.readOnLine(keyin);
		register(e);
	}
	E selectElem() {
		ArrayList<E> arr = null;
		int indx = 1;
		System.out.print("검색 키워드 : ");
		String kwd = keyin.next();
		keyin.nextLine();
		arr = findAll(kwd);
		if (arr.size() == 0)
			return null;
		if (arr.size() == 1)
			return arr.get(indx-1);
		indx = 1;
		for (E m : arr) {
			System.out.printf("[%d] ", indx++);
			m.print();
		}
		do {
			System.out.print("번호를 고르세요.. ");
			indx = keyin.nextInt();
		} while (indx >= 0 && indx < arr.size());
		E e = arr.get(indx-1);
		return e;
	}
	public void removeOne() {
		E e = selectElem();
		if (e == null) 
			return;
		e.print();
		System.out.print("정말 삭제하시겠습니까? ");
		String yn = keyin.next();
		if (!yn.equals("y"))
			return;
		boolean check = e.checkRemove();
		if (check) {
			getList().remove(e);
			System.out.println(e.getName() + " 삭제되었습니다.");
		} else {
			System.out.println("삭제 실패");
		}
	}
	public void updateOne() {
		E e = selectElem();
		if (e == null) 
			return;
		e.update(keyin);
		System.out.println("수정되었습니다.");
		e.print();
	}
}
