package manager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Manager<E extends Managable> {
	private ArrayList<E> mList = new ArrayList<>();
	public ArrayList<E> getList() {
		return mList;
	}
	public void readAll(Scanner in, Factory<E> fac) {
		E st = null;
		while (true) {
			st = fac.create(in);
			st.read(in);
			mList.add(st);
		}
	}
	public void register(E m) {
		mList.add(m);
	}
	public void readAll(String filename, Factory<E> fac) {
		Scanner fileIn = Manager.fileOpen(filename);
		E m = null;
		fileIn.nextLine();
		while (fileIn.hasNext()) {
			m = fac.create(fileIn);
			m.read(fileIn);
			mList.add(m);
		}
		fileIn.close();
	}
	public void printAll() {
		for (E e : mList) {
			e.print();
		}
	}
	public static Scanner fileOpen(String filename) {
		File f = new File(filename);
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	public E find(int n) {
		for (E m : mList)
			if (m.compare(n)) 
				return m;
		return null;
	}
	public E find(String kwd) {
		for (E m : mList)
			if (m.compare(kwd)) 
				return m;
		return null;
	}
	public ArrayList<E> findAll(String kwd) {
		ArrayList<E> list = new ArrayList<>();
		for (E m : mList)
			if (m.compare(kwd)) 
				list.add(m);
		return list;
	}
}
