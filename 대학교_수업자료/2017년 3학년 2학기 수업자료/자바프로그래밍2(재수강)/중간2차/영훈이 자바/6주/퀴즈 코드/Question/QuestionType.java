package Question;

import java.util.HashMap;

public enum QuestionType {//enum
	WRITE(1), SELECT(2), UNDERLINEWRITE(3), UNDERLINESELECT(4), EXIT(0);
	private int value;
	private static HashMap<Integer, QuestionType> map = new HashMap<>();
	private QuestionType(int value) { this.value = value; }
	static {
		for (QuestionType d : QuestionType.values()) {
			map.put(d.value, d); 
		}
	}
	public static QuestionType get(int n){
		return map.get(n);
	}
}
