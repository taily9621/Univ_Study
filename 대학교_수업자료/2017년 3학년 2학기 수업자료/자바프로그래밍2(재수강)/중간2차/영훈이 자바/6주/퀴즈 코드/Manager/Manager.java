package Manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;



public class Manager
{
	public ArrayList<Managable> stList = new ArrayList<>();
	Scanner openFile(String fileName) {
		Scanner fileIn = null;
		File f = new File(fileName);
		try {
			fileIn = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return fileIn;
	}
	public void readAll(String fileName, Factory fac) {
		Scanner fileIn = openFile(fileName);
		Managable st = null;
	
		fileIn.nextLine();//맨처음 앞줄 받고 버리기
		while (fileIn.hasNext()) {
			
			st = fac.create(fileName);//create()함수는 하나만 써야하니 txt파일 이름으로 구분
			st.read(fileIn);
			stList.add(st);
		}
		fileIn.close();
	}
	public void printAll() {
		for (Managable st : stList)
			st.print();
	}
	
	public Managable findManager(String kwd) {//kwd로 한개 찾고 그것 리턴
		for (Managable st : stList)
			if (st.compare(kwd)) 
				return st;
		return null;
	}
	public void findAndPrintManager(String kwd){//조건 만족하는 것들 전부 찾아서 출력
		for (Managable st : stList)
			if (st.compare(kwd)) 
				st.print();
	}
	
}