package crud;

import java.util.Scanner;

import manager.Managable;

public interface Crudable extends Managable {
	void readOnLine(Scanner keyin);
	boolean checkRemove();
	void update(Scanner keyin);
}
