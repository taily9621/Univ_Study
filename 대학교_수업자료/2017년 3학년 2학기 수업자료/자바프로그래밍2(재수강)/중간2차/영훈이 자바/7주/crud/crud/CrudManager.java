package crud;

import java.util.ArrayList;
import java.util.Scanner;
import manager.Managable;
import manager.Manager;

public class CrudManager extends Manager {
	public Manager mgr = null;
	Scanner keyin = null;
	public CrudManager(Scanner keyin) {
		this.keyin = keyin;
	}
	public void addOne(Managable m) {
		Crudable c = (Crudable)m;
		c.readOnLine(keyin);
		mgr.register(c);
	}
	Crudable selectElem() {
		ArrayList<Managable> arr = new ArrayList<>();
		int indx = 1;
		while (true) {
			arr.clear();
			System.out.print("검색 키워드 : ");
			String kwd = keyin.next();
			keyin.nextLine();
			for (Managable m : mgr.getList()) {
				if (m.compare(kwd)) {
					arr.add(m);
				}
			}
			indx = 1;
			for (Managable m : arr) {
				System.out.printf("[%d] ", indx++);
				m.print();
			}
			if (arr.size() == 0)
				continue;
			if (arr.size() == 1)
				indx = 1;
			else {
				System.out.print("번호를 고르세요.. ");
				indx = keyin.nextInt();
				if (indx >= arr.size())
					continue;
			}
			break;
		}
		Managable e = arr.get(indx-1);
		return (Crudable)e;
	}
	public void removeOne() {
		Crudable c = selectElem();
		System.out.println("정말 삭제하시겠습니까? ");
		String yn = keyin.next();
		if (!yn.equals("y"))
			return;
		boolean check = c.checkRemove();
		if (check) {
			mgr.getList().remove(c);
			System.out.println(c.getName() + "삭제되었습니다.");
		} else {
			System.out.println("삭제 실패");
		}
	}
	public void updateOne() {
		Crudable c = selectElem();
		c.update(keyin);
		System.out.println("수정되었습니다.");
		c.print();
	}
}
