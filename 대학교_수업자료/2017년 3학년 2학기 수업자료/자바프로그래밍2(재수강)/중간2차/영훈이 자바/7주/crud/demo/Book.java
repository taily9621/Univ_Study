package demo;

import java.util.Scanner;

import crud.Crudable;
import manager.Managable;

class Book implements Crudable {
    private String title;
    private String isbn;
    private String author;
    User borrower;
    int index;
    static int count=1;
    public void read(Scanner s) {
    	index = count++;
    	title = s.nextLine();
    	isbn = s.next();
    	author = s.next();
    	if (s.hasNext()) s.nextLine();
    }
    public boolean compare(int index) {
    	return this.index == index;
    }
    public void print() {
    	System.out.printf("[%2d] %s (%s) %s저, ", index, title, isbn, author);
    	if (borrower == null)
    		System.out.println("[대출가능]");
    	else System.out.printf("[대출중-%s]%n", borrower.getName());
    	
    }
    public String getName() {
    	return title;
    }
	@Override
	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		if (title.indexOf(kwd) != -1)
			return true;
		return false;
	}
	@Override
	public void readOnLine(Scanner keyin) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean checkRemove() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void update(Scanner keyin) {
		// TODO Auto-generated method stub
		
	}
}

