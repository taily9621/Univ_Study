package demo;

import java.util.ArrayList;
import java.util.Scanner;

import crud.Crudable;
import manager.Managable;

class User implements Crudable {
    String userid;
    ArrayList<Managable> borrowList = new ArrayList<>();
    public void read(Scanner s) {
    	String aLine = s.nextLine();
    	aLine = aLine.trim();
    	String toks[] = aLine.split(" ");
    	userid = toks[0];
    	Book bk = null;
    	for (int i = 1; i < toks.length; i++) {
    		bk = (Book)BookDemo.bookMgr.findManagable(Integer.parseInt(toks[i]));
    		if (bk != null) {
    			borrowList.add(bk);
    			bk.borrower = this;
    		}
    		else System.out.println(toks[i] + " no book");
    	}
    }
    public void print() {
    	System.out.print(userid + " - ");
    	for (Managable bk : borrowList)
    		if (bk != null)
    			System.out.print(bk.getName() + " / ");
    	System.out.println(borrowList.size()+"권");
    }
    public boolean compare(int n) {
    	return false;
    }
    public String getName() {
    	return userid;
    }
	@Override
	public boolean compare(String kwd) {
		// TODO Auto-generated method stub
		if (userid.equals(kwd))
			return true;
		return false;
	}
	@Override
	public void readOnLine(Scanner keyin) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean checkRemove() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void update(Scanner keyin) {
		// TODO Auto-generated method stub
	}
}
