package demo;
import java.util.ArrayList;
import java.util.Scanner;

import crud.CrudManager;
import manager.Factory;
import manager.Managable;

class BookDemo implements Factory {
	public static void main(String args[]) {
		BookDemo demo = new BookDemo();
		demo.doit();
	}
	static Scanner keyin = new Scanner(System.in);
	static CrudManager bookMgr = new CrudManager(keyin);
	static CrudManager userMgr = new CrudManager(keyin);
	void doit() {
		bookMgr.readAll("books.txt", this);
		bBook = false;
		userMgr.readAll("users.txt", this);
		int sel = 0;
		while (true) {
		System.out.println("(1) 사용자메뉴   (2) 관리자메뉴  (0) 종료");
		sel = keyin.nextInt();
		keyin.nextLine();
		if (sel == 1)
			menu();
		else if (sel == 2)
			doCrud();
		else break;
		}
		System.out.println("Bye!");
	}
	void menu() {
		while (true) {
			System.out.print("(1) 전체책목록  (2) 책검색  (3) 전채유저  (4) 유저검색  (5) 대출가능책  (0) 종료 ");
			int menu = keyin.nextInt();
			if (menu == 0) break;
			switch(menu) {
			case 1: bookMgr.printAll(); break;
			case 2: searchBook(); break;
			case 3: userMgr.printAll(); break;
			case 4: searchUser(); break;
			case 5: listAvailableBooks(); break;
			default: break;
			}
		}
	}
	void searchBook() {
		System.out.print("책검색 키워드 : ");
		String kwd = keyin.next();
		Managable m = bookMgr.findManagable(kwd);
		if (m != null)
			m.print();
	}
	void searchUser() {
		System.out.print("유저검색 키워드 : ");
		String kwd = keyin.next();
		Managable m = userMgr.findManagable(kwd);
		if (m != null)
			m.print();
	}
	void listAvailableBooks() {
		ArrayList<Managable> list = bookMgr.getList();
		Book book = null;
		for (Managable m : list) {
			book = (Book)m;
			if (book.borrower == null)
				book.print();
		}
	}
	boolean bBook = true;
	public Managable create() {
		if (bBook) return new Book();
		return new User();
	}
	public void doCrud() {
		int m = 0;
		while (true) {
			System.out.println("(1) 책추가   (2) 책삭제  (3) 책수정   (4) 사용자추가  (5) 사용자삭제  (6) 사용자수정  (0) 종료");
			m = keyin.nextInt();
			keyin.nextLine();
			if (m == 0) break;
			if (m == 1) {
				Book book = new Book();
				bookMgr.addOne(book);
			} else if (m == 2) {
				bookMgr.removeOne();
			} else if (m == 3) {
				bookMgr.updateOne();
			} else if (m == 4) {
				User p = new User();
				userMgr.addOne(p);
			} else if (m == 5) {
				userMgr.removeOne();
			} else if (m == 6) {
				userMgr.updateOne();
			}
		}
	}
}