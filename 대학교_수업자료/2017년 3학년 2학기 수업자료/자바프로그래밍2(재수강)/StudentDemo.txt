import java.util.ArrayList;
import java.util.Scanner;

public class StudentDemo {

	static ArrayList<Student> stList = new ArrayList<>();
	Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		StudentDemo demo = new StudentDemo();
		demo.doit();
	}
	void doit() {
		// TODO Auto-generated method stub
		Student st = null;
		String id = null;
		while (true) {
			id = in.next();
			if (id.equals("0"))
				break;
			st = new Student(id);
			st.read(in);
			stList.add(st);
		}
//		String id = null;
//		while (true) {
//			id = s.next();
//			st = findStudent(id);
//			if (st == null) break;
//			st.readScores(s);;
//		}
		addNewStudents();
		for (Student e : stList) {
			System.out.print(e);
		}
	}
	void addNewStudents() {
		Student st = null;
		st = new Student(null, stList.get(stList.size()-1).name);
		stList.add(st);
		st = new Student(stList.get(0));
		stList.add(st);
		String name, mf;
		int year;
		while (true) {
			System.out.print("학생을 추가하시겠습니까? (y/n)");
			name = in.next();
			if (name.equals("n")) break;
			System.out.print("이름, 학년, 성별 입력 : ");
			name = in.next();
			year = in.nextInt();
			mf = in.next();
			st = new Student(name, year, mf.equals("m"));
			stList.add(st);
		}
	}
}
class Student {
	
	String id;
	String name;
	int year;
	boolean bMale;
	static int sIndex = 1;
	Student() {
		this("...", 0, false);
	}
	Student(String id) {
		this("...", 0, false);
		this.id = id;
	}
	Student(String id, String name) {
		this(name, 0, false);
		if (id != null)
			this.id = id;
	}
	Student(Student st) {
		this(st.name, st.year, st.bMale);
	}
	Student(String name, int year, boolean mf) {
		this.name = name;
		this.year = year;
		this.bMale = mf;
		id = "s" +  sIndex++;
		System.out.printf("%s (%s) 학생이 추가되었습니다.%n", id, name);
	}
	void read(Scanner in) {
		name = in.next();
		year = in.nextInt();
		bMale = in.next().equals("m");
	}
		
	@Override
	public String toString() {
		String result = String.format("%s %s %2d %s%n", 
						id, name, year, bMale?"남":"녀");
		return result;
	}
}