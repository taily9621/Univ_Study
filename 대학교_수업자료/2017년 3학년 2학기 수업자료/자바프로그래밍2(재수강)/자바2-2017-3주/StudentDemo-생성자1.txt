package test01;
import java.util.ArrayList;
import java.util.Scanner;

public class StudentDemo {

	static ArrayList<Student> stList = new ArrayList<>();
	Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		StudentDemo demo = new StudentDemo();
		demo.doit();
	}
	void doit() {
		// TODO Auto-generated method stub
		Student st = null;
		String id = null;
		while (true) {
			id = in.next();
			if (id.equals("0"))
				break;
			st = new Student(id);
			st.read(in);
			stList.add(st);
		}
		addNewStudents();
		for (Student e : stList) {
			e.print();
		}
	}
	void addNewStudents() {
		Student st = null;
		st = new Student(null, stList.get(stList.size()-1).name);
		stList.add(st);
		st = new Student(stList.get(0));
		stList.add(st);
	}
}
class Student {
	
	String id;
	String name;
	int year;
	boolean bMale;
	static int sIndex = 1;
	Student() {
		this("...", 0, false);
	}
	Student(String id) {
		this("...", 0, false);
		this.id = id;
	}
	Student(String id, String name) {
		this(name, 0, false);
		if (id != null)
			this.id = id;
	}
	Student(Student st) {
		this(st.name, st.year, st.bMale);
	}
	Student(String name, int year, boolean mf) {
		this.name = name;
		this.year = year;
		this.bMale = mf;
		id = "s" +  sIndex++;
		System.out.printf("%s (%s) 학생이 추가되었습니다.%n", id, name);
	}
	void read(Scanner in) {
		name = in.next();
		year = in.nextInt();
		bMale = in.next().equals("m");
	}
		
	void print() {
		System.out.printf("%s %s %2d %s%n", 
						id, name, year, bMale?"남":"녀");
	}
}