package loginB;

public interface AccountDB_face 
{
	public void addAccount(loginBean createdb);
	public loginBean getAccount(String userid);
	public boolean checkAccount(loginBean checkdb);
	public boolean checkUser(loginBean checkuser);
}
