package loginB;
import java.sql.*;
import java.util.*;

public class AccountDB implements AccountDB_face
{
	private String jdbc_driver = "com.mysql.jdbc.Driver";
	private String jdbc_url = "jdbc:mysql://localhost:3306/userid?autoReconnect=true&useSSL=false";
	private Connection conn;
	private Statement stmt;
	
	private void connect()
	{
		try
		{
			Class.forName(jdbc_driver);
			conn = DriverManager.getConnection(jdbc_url, "root", "kgu123");
			stmt = conn.createStatement();
		} catch(Exception e)
		{
			
		}
	}
	private void disconnect() 
	{
		try
		{
			stmt.close();
			conn.close();
		} catch (Exception e)
		{
			
		}
	}
	
	@Override
	public void addAccount(loginBean createdb) 
	{
		// TODO Auto-generated method stub
		String sql = "insert into userid values('" 
		+ createdb.getUserid() + "', '"
		+ createdb.getPasswd() + "', '"
		+ createdb.getUsername() + "')";
		try
		{
			connect();
			stmt.executeUpdate(sql);
			disconnect();
		} catch (Exception e){
		}
	}
	
	@Override
	public loginBean getAccount(String userid) {
		// TODO Auto-generated method stub
		String sql = "select id, passwd from event where id="+userid;
		loginBean user = new loginBean();
		try
		{
			connect();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			user.setUsername(rs.getString("username"));
			user.setUserid(rs.getString("userid"));
			user.setPasswd(rs.getString("passwd"));
			rs.close();
			disconnect();
		} catch(Exception e) {
			
		}
		return user;
	}
	
	@Override
	public boolean checkAccount(loginBean checkdb) {
		// TODO Auto-generated method stub
		String sql = "select name, id, passwd from userid";
		String inputId;
		String inputPasswd;
		int boolchk = 1;
		
		ArrayList<loginBean> check = new ArrayList<loginBean>();
		System.out.println("계정 생성시 중복이 있는지 검사");
		try
		{
			System.out.println("MySQL 데이터 불러오기");
			connect();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				loginBean chkAccount = new loginBean();
				chkAccount.setUsername(rs.getString("name"));
				chkAccount.setUserid(rs.getString("id"));
				chkAccount.setPasswd(rs.getString("passwd"));
				check.add(chkAccount);
			}
			rs.close();
			disconnect();
		} catch(Exception e)
		{
			System.out.println("데이터 불러오기 오류!");
		}
		
		for(loginBean lib : check)
		{
			inputId = checkdb.getUserid();
			inputPasswd = checkdb.getPasswd();
			
			System.out.println("입력 확인");
			System.out.println("DB에 있는 아이디 비번 : "+lib.getUserid()+", "+lib.getPasswd());
			System.out.println("현재 입력한 아이디 비번 : "+inputId+", "+inputPasswd);
			
			if(inputId.equals(lib.getUserid()) || inputPasswd.equals(lib.getPasswd()))
			{
				boolchk = 0;
				System.out.println("계정 중복 발생");
				break;
			}
		}
		
		if(boolchk == 0)
			return false;
		else
		{
			System.out.println("계정 중복 없음");
			return true;
		}
	}
	@Override
	public boolean checkUser(loginBean checkuser) {
		// TODO Auto-generated method stub
		String sql = "select name, id, passwd from userid";
		String inputId;
		String inputPasswd;
		int boolchk = 0;
		
		ArrayList<loginBean> check = new ArrayList<loginBean>();
		System.out.println("로그인시 DB에 있는 계정인지 검사");
		try
		{
			System.out.println("MySQL 데이터 불러오기");
			connect();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				loginBean chkAccount = new loginBean();
				chkAccount.setUsername(rs.getString("name"));
				chkAccount.setUserid(rs.getString("id"));
				chkAccount.setPasswd(rs.getString("passwd"));
				check.add(chkAccount);
			}
			rs.close();
			disconnect();
		} catch(Exception e)
		{
			System.out.println("데이터 불러오기 오류!");
		}
		
		for(loginBean lib : check)
		{
			inputId = checkuser.getUserid();
			inputPasswd = checkuser.getPasswd();
			
			System.out.println("입력 확인");
			//System.out.println("DB에 있는 아이디 비번 : "+lib.getUserid()+", "+lib.getPasswd());
			//System.out.println("현재 입력한 아이디 비번 : "+inputId+", "+inputPasswd);
			
			if(inputId.equals(lib.getUserid()) || inputPasswd.equals(lib.getPasswd()))
			{
				boolchk = 1;
				System.out.println("계정 확인");
				break;
			}
		}
		
		if(boolchk == 0)
			return false;
		else
		{
			System.out.println("로그인 승인");
			return true;
		}
	}

}
