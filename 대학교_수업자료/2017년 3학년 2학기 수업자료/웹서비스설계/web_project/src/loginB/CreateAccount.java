package loginB;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreateAccount
 */
@WebServlet("/CreateAccount")
public class CreateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		loginBean create = new loginBean();
		AccountDB_face dbDao = new AccountDB();
		String address;
		
		create.setUsername(request.getParameter("username"));
		create.setUserid(request.getParameter("userid"));
		create.setPasswd(request.getParameter("passwd"));
		
		loginBean setting = new loginBean();
		setting.setUsername(request.getParameter("username"));
		setting.setUserid(request.getParameter("userid"));
		setting.setPasswd(request.getParameter("passwd"));
		
		System.out.println("계정 중복 체크 시작");
		if(dbDao.checkAccount(setting))
		{
			dbDao.addAccount(setting);
			address = "/Front/login_form.html";
			System.out.println("계정 체크 이상 없음");
		} else
		{
			address = "/Front/create_fail.jsp";
			create.setError("Overlap userid/password");
			System.out.println("계정 생성 에러!");
		}
		request.setAttribute("userinfo", create);
		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}
}
