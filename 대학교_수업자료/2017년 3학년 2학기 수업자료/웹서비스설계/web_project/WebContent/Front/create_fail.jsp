<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="userinfo" class="loginB.loginBean" scope="request" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	body {text-align:center;}
</style>
<title>Insert title here</title>
</head>
<body>
	<H2> Account Error!</H2>
		<HR>
		사용자 아이디: <jsp:getProperty name="userinfo" property="userid" />
		<BR>
		사용자 패스워드 <jsp:getProperty name="userinfo" property="passwd" />
		<BR>
		에러 코드: <jsp:getProperty name="userinfo" property="error" />
		<form method="post" action="/Front/create_account.html">
			<table width="250" border="0" align = "center">
				<tr>
					<td></td>
				</tr>
				<tr>
					<td colspan="6">
						<input type="submit" name="Submit" value="Create again"></td>
				</tr>
			</table>
		</form>
</body>
</html>