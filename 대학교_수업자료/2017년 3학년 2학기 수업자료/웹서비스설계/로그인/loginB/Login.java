package loginB;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Login")
public class Login extends HttpServlet 
{	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loginBean login = new loginBean();
		AccountDB_face chkAct = new AccountDB();
		String address;
		
		login.setUserid(request.getParameter("userid"));
		login.setPasswd(request.getParameter("passwd"));
		if(chkAct.checkUser(login))
			address = "/Front/login_success.jsp";
		else
		{
			address = "/Front/login_fail.jsp";
			login.setError("Invalid userid/passwd");
		}
		request.setAttribute("userinfo", login);
		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

}
