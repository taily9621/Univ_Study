package loginB;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ChatLogin
 */
@WebServlet("/ChatLogin")
public class ChatLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		loginBean chkId = new loginBean();
		AccountDB_face chkAct = new AccountDB();
		String matchId;
		String address = null;
		//request.setCharacterEncoding("UTF-8");
		//response.setContentType("text/html;charset=UTF-8");
		
		chkId.setUserid(request.getParameter("userid"));
		matchId = chkId.getUserid();
		
		if(matchId.equals(null) || matchId.trim().length() == 0)
		{
			address = "/Front/login_fail.jsp";
			return;
		} else
		{
			//out.println("<html>");
			//out.println("<frameset rows='85")
		}
		
		request.setAttribute("userinfo", chkId);
		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}

}
