CSV to SQL converter
http://tools.perceptus.ca/text-wiz.php?ops=7

공급정보
오더넘버 / 제품이름 / 색상 / 가격 / 송장번호

검수정보
시리얼번호 / 사용가능여부 / 오더넘버

반송정보
오더넘버 / 반송송장번호

판매정보
오더넘버 / 구매번호 / 제품이름

구매자정보
구매번호 / 성함 / 연락처 / 주소