public static void selfTest() {
	Lib.debug(dbgThread, "Enter KThread.selfTest");
	KThread LowThread = new KThread(new PingTest(0)); 
	KThread MiddleThread = new KThread(new PingTest(1));
	KThread HighThread = new KThread(new PingTest(2));
	
	boolean intStatus = Machine.interrupt().disable();  
	
	ThreadedKernel.scheduler.setPriority(LowThread, 1);
	ThreadedKernel.scheduler.setPriority(MiddleThread, 4);
	ThreadedKernel.scheduler.setPriority(HighThread, 7);
	Machine.interrupt().restore(intStatus); 
	LowThread.fork(); 
	MiddleThread.fork();  
	HighThread.fork();
	
	new KThread(new CommunicatorTest()).fork();
	//new KThread(new PingTest(1)).setName("forked thread").fork();
	//new PingTest(0).run();
    }
    
    private static class CommunicatorTest implements Runnable { 
        private static class Speaker implements Runnable { 
            public void run() { 
                int x = (new Random()).nextInt(); 
                System.out.println("Sending " + x + "..."); 
                c.speak(x); 
                System.out.println("Word " + x + " sent."); 
            } 
        } 
 
        private static class Listener implements Runnable { 
            public void run() { 
                System.out.println("Listening..."); 
                int x = c.listen(); 
                System.out.println("Received " + x + "."); 
            } 
        } 
 
        public void run() { 
        	/* //단일 Speaker & Listener
        	(new KThread(new Speaker())).fork();
        	(new KThread(new Listener())).fork(); 
        	*/
            /* //복수개의 Speaker와 Listener
        	(new KThread(new Speaker())).fork();
        	(new KThread(new Speaker())).fork();
        	(new KThread(new Speaker())).fork();
        	(new KThread(new Listener())).fork();
        	(new KThread(new Listener())).fork();
        	(new KThread(new Listener())).fork();
        	*/
        } 
 
        public static Communicator c = new Communicator(); 
    }