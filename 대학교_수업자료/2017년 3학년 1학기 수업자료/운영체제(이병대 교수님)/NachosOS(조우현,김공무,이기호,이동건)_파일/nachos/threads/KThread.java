package nachos.threads;

import nachos.machine.*;

/**
 * A KThread is a thread that can be used to execute Nachos kernel code. Nachos
 * allows multiple threads to run concurrently.
 *
 * To create a new thread of execution, first declare a class that implements
 * the <tt>Runnable</tt> interface. That class then implements the <tt>run</tt>
 * method. An instance of the class can then be allocated, passed as an
 * argument when creating <tt>KThread</tt>, and forked. For example, a thread
 * that computes pi could be written as follows:
 *
 * <p><blockquote><pre>
 * class PiRun implements Runnable {
 *     public void run() {
 *         // compute pi
 *         ...
 *     }
 * }
 * </pre></blockquote>
 * <p>The following code would then create a thread and start it running:
 *
 * <p><blockquote><pre>
 * PiRun p = new PiRun();
 * new KThread(p).fork();
 * </pre></blockquote>
 */
public class KThread {
    /**
     * Get the current thread.
     *
     * @return	the current thread.
     */
    public static KThread currentThread() {
	Lib.assertTrue(currentThread != null);
	return currentThread;
    }
    
    /**
     * Allocate a new <tt>KThread</tt>. If this is the first <tt>KThread</tt>,
     * create an idle thread as well.
     */
    public KThread() {
	if (currentThread != null) {
	    tcb = new TCB();
	}	    
	else {
	    readyQueue = ThreadedKernel.scheduler.newThreadQueue(false);
	    readyQueue.acquire(this);	    

	    currentThread = this;
	    tcb = TCB.currentTCB();
	    name = "main";
	    restoreState();

	    createIdleThread();
	}
    }

    /**
     * Allocate a new KThread.
     *
     * @param	target	the object whose <tt>run</tt> method is called.
     */
    public KThread(Runnable target) {
	this();
	this.target = target;
    }

    /**
     * Set the target of this thread.
     *
     * @param	target	the object whose <tt>run</tt> method is called.
     * @return	this thread.
     */
    public KThread setTarget(Runnable target) {
	Lib.assertTrue(status == statusNew);
	
	this.target = target;
	return this;
    }

    /**
     * Set the name of this thread. This name is used for debugging purposes
     * only.
     *
     * @param	name	the name to give to this thread.
     * @return	this thread.
     */
    public KThread setName(String name) {
	this.name = name;
	return this;
    }

    /**
     * Get the name of this thread. This name is used for debugging purposes
     * only.
     *
     * @return	the name given to this thread.
     */     
    public String getName() {
	return name;
    }

    /**
     * Get the full name of this thread. This includes its name along with its
     * numerical ID. This name is used for debugging purposes only.
     *
     * @return	the full name given to this thread.
     */
    public String toString() {
	return (name + " (#" + id + ")");
    }

    /**
     * Deterministically and consistently compare this thread to another
     * thread.
     */
    public int compareTo(Object o) {
	KThread thread = (KThread) o;

	if (id < thread.id)
	    return -1;
	else if (id > thread.id)
	    return 1;
	else
	    return 0;
    }

    /**
     * Causes this thread to begin execution. The result is that two threads
     * are running concurrently: the current thread (which returns from the
     * call to the <tt>fork</tt> method) and the other thread (which executes
     * its target's <tt>run</tt> method).
     */
    public void fork()/*과제 1*/ {
	Lib.assertTrue(status == statusNew);
	Lib.assertTrue(target != null);
	
	Lib.debug(dbgThread,
		  "Forking thread: " + toString() + " Runnable: " + target);

	boolean intStatus = Machine.interrupt().disable();

	tcb.start(new Runnable() {
		public void run() {
		    runThread();
		}
	    });

	ready();
	
	Machine.interrupt().restore(intStatus);
    }
/*
 * fork 함수는 새로운 프로세스를 생성한 다음 cpu의 할당 요청으로 쓰레드를 실행 시켜서 준비 상태로 만들어 쓰레드를 언제든지 사용 할 수 있도록 만든다.
 *  "status == statusNew" 부분에서 새로운 status를 생성한 다음 타겟 값을 null로 초기화 한후상태를 disable로 설정 한다.
 *  그런 다음 쓰레드를 실행 시킨 다음 대기 상태로 넣어준다.
 */
    private void runThread()/*과제2*/ {
	begin();
	target.run();
	finish();
    }
/*
 *스케줄러가 쓰레드를 준비 상태에서 실행 상태로 바꿔준다. 그리고 해당 쓰레드는 일을 수행을 하고 일이 끝나게 되면 쓰레드를 반납하고 종료한다.
 * target은PingTest고 run은 아래 pingTest.run을 실행 시키는 것
 */
    private void begin() {
	Lib.debug(dbgThread, "Beginning thread: " + toString());
	
	Lib.assertTrue(this == currentThread);

	restoreState();

	Machine.interrupt().enable();
    }

    /**
     * Finish the current thread and schedule it to be destroyed when it is
     * safe to do so. This method is automatically called when a thread's
     * <tt>run</tt> method returns, but it may also be called directly.
     *
     * The current thread cannot be immediately destroyed because its stack and
     * other execution state are still in use. Instead, this thread will be
     * destroyed automatically by the next thread to run, when it is safe to
     * delete this thread.
     */
    public static void finish() {
	Lib.debug(dbgThread, "Finishing thread: " + currentThread.toString());
	
	Machine.interrupt().disable();

	Machine.autoGrader().finishingCurrentThread();

	Lib.assertTrue(toBeDestroyed == null);
	toBeDestroyed = currentThread;


	currentThread.status = statusFinished;
	
	sleep();
    }

    /**
     * Relinquish the CPU if any other thread is ready to run. If so, put the
     * current thread on the ready queue, so that it will eventually be
     * rescheuled.
     *
     * <p>
     * Returns immediately if no other thread is ready to run. Otherwise
     * returns when the current thread is chosen to run again by
     * <tt>readyQueue.nextThread()</tt>.
     *
     * <p>
     * Interrupts are disabled, so that the current thread can atomically add
     * itself to the ready queue and switch to the next thread. On return,
     * restores interrupts to the previous state, in case <tt>yield()</tt> was
     * called with interrupts disabled.
     */
    public static void yield()/*과제3*/ {
	Lib.debug(dbgThread, "Yielding thread: " + currentThread.toString());
	
	Lib.assertTrue(currentThread.status == statusRunning);
	
	boolean intStatus = Machine.interrupt().disable();

	currentThread.ready();

	runNextThread();
	
	Machine.interrupt().restore(intStatus);
    }
/*
 * 이전 쓰레드가 실행 중인 상태에서 준비 중인 생태로 설정한 다음 다음 쓰레드를 실행 시킨다. 이 함수는 이전 쓰레드에서 다음 쓰레드로 넘어가려고 할 때 실행되는 함수 이다.
 * 쓰레드를 ready큐로 넣는 것
 */
    /**
     * Relinquish the CPU, because the current thread has either finished or it
     * is blocked. This thread must be the current thread.
     *
     * <p>
     * If the current thread is blocked (on a synchronization primitive, i.e.
     * a <tt>Semaphore</tt>, <tt>Lock</tt>, or <tt>Condition</tt>), eventually
     * some thread will wake this thread up, putting it back on the ready queue
     * so that it can be rescheduled. Otherwise, <tt>finish()</tt> should have
     * scheduled this thread to be destroyed by the next thread to run.
     */
    public static void sleep()/*과제4*/ {
	Lib.debug(dbgThread, "Sleeping thread: " + currentThread.toString());
	
	Lib.assertTrue(Machine.interrupt().disabled());

	if (currentThread.status != statusFinished)
	    currentThread.status = statusBlocked;

	runNextThread();
    }
/*
 * 쓰레드가 끝나지 않았을때 쓰레드에 상태 블록을 건 다음 다음 쓰레드를 실행시킨다. 이는 현재 프로세스가 실행중에 다른 프로세스에게 cpu점유를 
 * 넘겨줘야 하는 상황에 발생하게 된다. 따라서 지금 실행하고 있는 프로세스의 쓰레드를 "stauesBlocked"를 걸어 일시 정지를 한 다음
 * 다음 쓰레드를 실행 시켜 다른 프로세스를 실행하는 상태이다. 일종의 다음 쓰레드에서 실행 할 수 있도록 이전 쓰레드를 진행을 하지 못하도록 일시적으로 블락을 건 것이다.(내가 이해한 바로는)
 */
    /**
     * Moves this thread to the ready state and adds this to the scheduler's
     * ready queue.
     */
    public void ready() {
	Lib.debug(dbgThread, "Ready thread: " + toString());
	
	Lib.assertTrue(Machine.interrupt().disabled());
	Lib.assertTrue(status != statusReady);
	
	status = statusReady;
	if (this != idleThread)
	    readyQueue.waitForAccess(this);
	
	Machine.autoGrader().readyThread(this);
    }

    /**
     * Waits for this thread to finish. If this thread is already finished,
     * return immediately. This method must only be called once; the second
     * call is not guaranteed to return. This thread must not be the current
     * thread.
     */
    public void join()/*과제5*/ {
	Lib.debug(dbgThread, "Joining to thread: " + toString());

	Lib.assertTrue(this != currentThread);

    }
/*
 * 이전 쓰레드에서 다음 새로운 쓰레드로 넘어와서 프로세싱을 하려고 할 때 이전 쓰레드가 아닌 새로운 쓰레드가 들어왔다는 것을 확인하는 함수이다.(나의 생각)
 * 두 개의 쓰레드가 있으면 한쪽 쓰레드가 프로세싱이 끝나게 되면 아직 끝나지 않은 다른 한 쪽 쓰레드는 프로세싱을 하게 되는 동안 끝난 쓰레드는 기다렸다가 다른 한 쪽의 쓰레드가 프로세싱이 끝나게 되면 같이 동시에 끝나게 해주는 함수.
 * 이 것은 좀비 쓰레드를 방지하기 위해 쓰임
 */
    /**
     * Create the idle thread. Whenever there are no threads ready to be run,
     * and <tt>runNextThread()</tt> is called, it will run the idle thread. The
     * idle thread must never block, and it will only be allowed to run when
     * all other threads are blocked.
     *
     * <p>
     * Note that <tt>ready()</tt> never adds the idle thread to the ready set.
     */
    private static void createIdleThread() {
	Lib.assertTrue(idleThread == null);
	
	idleThread = new KThread(new Runnable() {
	    public void run() { while (true) yield(); }
	});
	idleThread.setName("idle");

	Machine.autoGrader().setIdleThread(idleThread);
	
	idleThread.fork();
    }
    
    /**
     * Determine the next thread to run, then dispatch the CPU to the thread
     * using <tt>run()</tt>.
     */
    private static void runNextThread()/*과제6*/ {
	KThread nextThread = readyQueue.nextThread();
	if (nextThread == null)
	    nextThread = idleThread;

	nextThread.run();
    }
/*
 * 레디 큐에 있는 nextThread에 다음 쓰레드가 지정되지 않았을 때 새로운 쓰래드를 할당 받은 다음에 할당 받은 새로운 쓰래드로 프로세스를 실행한다.
 * 이 부분은 선언 후 다음 새 쓰레드를 넘겨 받은 다음에 새로운 쓰레드를 실행한다.
 */
    /**
     * Dispatch the CPU to this thread. Save the state of the current thread,
     * switch to the new thread by calling <tt>TCB.contextSwitch()</tt>, and
     * load the state of the new thread. The new thread becomes the current
     * thread.
     *
     * <p>
     * If the new thread and the old thread are the same, this method must
     * still call <tt>saveState()</tt>, <tt>contextSwitch()</tt>, and
     * <tt>restoreState()</tt>.
     *
     * <p>
     * The state of the previously running thread must already have been
     * changed from running to blocked or ready (depending on whether the
     * thread is sleeping or yielding).
     *
     * @param	finishing	<tt>true</tt> if the current thread is
     *				finished, and should be destroyed by the new
     *				thread.
     */
    private void run()/*과제7*/ {
	Lib.assertTrue(Machine.interrupt().disabled());

	Machine.yield();

	currentThread.saveState();

	Lib.debug(dbgThread, "Switching from: " + currentThread.toString()
		  + " to: " + toString());

	currentThread = this;

	tcb.contextSwitch();

	currentThread.restoreState();
    }
/*
 * 쓰레드를 받아 프로세스를 실행하는 함수 부분, yield함수를 호출하여 다음 쓰레드를 불러온 다음 이전 쓰레드까지 실행한 프로세스 내용을 저장한다. 그리고 이전 프로세스로 지정된 currentThread를
 * 새로 바뀐 쓰레드로 재 지정한 다음에 이전 쓰레드에서 새로 들어온 쓰레드로 switch를 하고 새로 들어온 쓰레드 상태를 재정의 해준다.
 */
    /**
     * Prepare this thread to be run. Set <tt>status</tt> to
     * <tt>statusRunning</tt> and check <tt>toBeDestroyed</tt>.
     */
    protected void restoreState()/*과제8*/ {
	Lib.debug(dbgThread, "Running thread: " + currentThread.toString());
	
	Lib.assertTrue(Machine.interrupt().disabled());
	Lib.assertTrue(this == currentThread);
	Lib.assertTrue(tcb == TCB.currentTCB());

	Machine.autoGrader().runningThread(this);
	
	status = statusRunning;

	if (toBeDestroyed != null) {
	    toBeDestroyed.tcb.destroy();
	    toBeDestroyed.tcb = null;
	    toBeDestroyed = null;
	}
    }
/*
 * 쓰레드의 상태를 재정의하는 함수이다. 바뀐 쓰레드를 실행한 다음에 상태를 statusRunning으로 활성화 한 다음 저장한다. 만약 지금 쓰레드가 파괴되지 않았으면 쓰레드를 파괴한 다음 toBeDestroyed.tcb값과
 * toBeDestroyed의 값을 null로 초기화 해준다. 이는 이전 쓰레드에서 다음 새로운 쓰레드로 넘어간 후에 상태 변화를 다시 재정의 해줘야 하기 때문에 새로운 쓰레드의 상태를 Running 상태로 바꾸게 된다.
 * Destroyed의 if문 부분은 tcb에 있는 값을 초기화 해주므로써 tcb에 쓰레드 값이 남아있어 후에 프로그램이 이 때문에 문제가 생기게 되는 것을 막는다(이 부분은 잘 몰라서 추측해봄)
 */
    /**
     * Prepare this thread to give up the processor. Kernel threads do not
     * need to do anything here.
     */
    protected void saveState()/*과제9*/ {
	Lib.assertTrue(Machine.interrupt().disabled());
	Lib.assertTrue(this == currentThread);
    }
/*
 * 이전 쓰레드의 프로세스 정보를 저장하기 위한 함수이다.(tcb) 이전 쓰레드에서 다음 새로운 쓰레드로 넘어가려고 할 때 이전 쓰레드에 중단된 지점까지의 프로세스 정보 지점을 저장한다.
 * 그래야 새로운 쓰레드가 일을 끝난 다음 이전 쓰레드에서 다시 일을 하려고 할 때 중단된 지점 부터 실행 할 수 있다.
 */
    public static class PingTest implements Runnable {
	PingTest(int which) {
	    this.which = which;
	}
/*
 * 운영체제 팀플 과제
 * 문제 1번
 */
	public void run() {
	    for (int i=0; i<10; i++)/*5회에서 10회로 조정*/ {
		System.out.println("*** thread " + which + " looped "
				   + i + " times");
		currentThread.yield();
	    }
	}
/*
 * 반복문에서 횟수를 5에서 10으로 고치면 된다.
 */
	private int which;
    }

    /**
     * Tests whether this module is working.
     */
    
    public static void selfTest() {
	Lib.debug(dbgThread, "Enter KThread.selfTest");
	
	new KThread(new PingTest(1)).setName("forked thread").fork();
	new PingTest(0).run();
    }
/*
 * 핑 테스트에 쓰레드를 생성하여 실행을 시킨다.
 * 쓰레드 작동 테스트를 위해 쓰레드를 생성한 것
 */
    private static final char dbgThread = 't';

    /**
     * Additional state used by schedulers.
     *
     * @see	nachos.threads.PriorityScheduler.ThreadState
     */
    public Object schedulingState = null;

    private static final int statusNew = 0;
    private static final int statusReady = 1;
    private static final int statusRunning = 2;
    private static final int statusBlocked = 3;
    private static final int statusFinished = 4;

    /**
     * The status of this thread. A thread can either be new (not yet forked),
     * ready (on the ready queue but not running), running, or blocked (not
     * on the ready queue and not running).
     */
    private int status = statusNew;
    private String name = "(unnamed thread)";
    private Runnable target;
    private TCB tcb;

    /**
     * Unique identifer for this thread. Used to deterministically compare
     * threads.
     */
    private int id = numCreated++;
    /** Number of times the KThread constructor was called. */
    private static int numCreated = 0;

    private static ThreadQueue readyQueue = null;
    private static KThread currentThread = null;
    private static KThread toBeDestroyed = null;
    private static KThread idleThread = null;
}
