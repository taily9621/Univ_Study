package nachos.threads;

import nachos.machine.*;
import nachos.threads.KThread.PingTest;

/**
 * A multi-threaded OS kernel.
 */
public class ThreadedKernel extends Kernel {
    /**
     * Allocate a new multi-threaded kernel.
     */
    public ThreadedKernel() {
	super();
    }

    /**
     * Initialize this kernel. Creates a scheduler, the first thread, and an
     * alarm, and enables interrupts. Creates a file system if necessary.   
     */
    public void initialize(String[] args) {
	// set scheduler
	String schedulerName = Config.getString("ThreadedKernel.scheduler");
	scheduler = (Scheduler) Lib.constructObject(schedulerName);

	// set fileSystem
	String fileSystemName = Config.getString("ThreadedKernel.fileSystem");
	if (fileSystemName != null)
	    fileSystem = (FileSystem) Lib.constructObject(fileSystemName);
	else if (Machine.stubFileSystem() != null)
	    fileSystem = Machine.stubFileSystem();
	else
	    fileSystem = null;

	// start threading
	new KThread(null);

	alarm  = new Alarm();

	Machine.interrupt().enable();
    }

    /**
     * Test this kernel. Test the <tt>KThread</tt>, <tt>Semaphore</tt>,
     * <tt>SynchList</tt>, and <tt>ElevatorBank</tt> classes. Note that the
     * autograder never calls this method, so it is safe to put additional
     * tests here.
     */
    /*------3번 과제------*/
    /* PingTest 클래스를 새롭게 추가함*/
    private static class PingTest implements Runnable {  
    	PingTest(int which) {      
    		this.which = which;  
    		}    
    	public void run() {      
    		for (int i=0; i<10; i++) {   
    			System.out.println("*** thread " + which + " looped "        + i + " times");   
    			KThread.currentThread().yield();      
    			}  
    		} 
    
    private int which; 
    }
    //문제 3번
    //기존의 selfTest() 부분은 모두 주석으로 처리하고 PingTest thread 부분을 새롭게 추가
    public void selfTest() {
    new KThread(new PingTest(1)).setName("forked thread").fork();
	/*KThread.selfTest();
	Semaphore.selfTest();
	SynchList.selfTest();
	if (Machine.bank() != null) {
	  ElevatorBank.selfTest();
	}*/
	
    }
/*
 * 실행을 하면 핑 테스트 쓰레드를 생성했지만 실제로는 동작하지 않았다. 왜?
 * 현재 ThreadedKernel에 PingTest함수를 추가한 다음 selfTest함수 부분을 모두 주석처리를 한 다음에 KThread 부분을 새로 추가했다.
 * 그럼에도 실행을 하게 되면 핑 테스트 쓰레드는 생성이 되었지만 실제로 동작은 하지 않는다.
 * 그 이유는 말 그대로 "생성"만 하고 동작을 하는 함수가 현재 주석처리가 되어 있으므로 만들어 논 쓰레드를 사용하지 않아 동작을 하지 않은 것이다.
 * "new KThread(new PingTest(1)).setName("forked thread").fork();" 에서 fork()를 통해 쓰레드를 생성한 뒤 쓰레드를 대기 큐로 보낸다.
 * 여기서 주석 처리를 한 selfTest 부분에 new PingTest(0).run();을 임의로 넣은 다음 실행하게 되면은 동작하지 않던 쓰레드가 다시 동작을 하게 된다.
 * 이 뜻은 대기 큐에 있는 쓰레드가 new PingTest(0).run(); 으로 인해 실행이 되었다는 뜻이다.
 * 따라서 쓰레드가 생성이 되었지만 동작을 하지 않은 이유는 쓰레드를 생성한 다음 대기 큐에 있게 되지만 new PingTest(0).run() 같은 실행문이 없었기 때문에
 * 동작을 하지 않은 것이다.
 */
    /**
     * A threaded kernel does not run user programs, so this method does
     * nothing.
     */
    public void run() {
    }

    /**
     * Terminate this kernel. Never returns.
     */
    public void terminate() {
	Machine.halt();
    }

    /** Globally accessible reference to the scheduler. */
    public static Scheduler scheduler = null;
    /** Globally accessible reference to the alarm. */
    public static Alarm alarm = null;
    /** Globally accessible reference to the file system. */
    public static FileSystem fileSystem = null;

    // dummy variables to make javac smarter
    private static RoundRobinScheduler dummy1 = null;
    private static PriorityScheduler dummy2 = null;
    private static LotteryScheduler dummy3 = null;
    private static Condition2 dummy4 = null;
    private static Communicator dummy5 = null;
    private static Rider dummy6 = null;
    private static ElevatorController dummy7 = null;
}
