#include<netinet/in.h>
#include<stdio.h>
union{
	struct sockaddr_in s;
	uint8_t c[16];
} un;
void main()
{
	int num = 0;
	char addr[255], port[10];
	printf("port: ");
	scanf("%s",port);
	printf("address: ");
	scanf("%s",addr);
	un.s.sin_family = AF_INET;
	un.s.sin_port = htons(atoi(port));
	inet_pton(AF_INET,addr,&un.s.sin_addr);
	printf("%s %s",addr,port);
	for(num = 0; num<16; num++)
	{
		if(num%4 == 0)
			printf("\n");
		printf("%02x\t",un.c[num]);
	}
	printf("\n");
}
