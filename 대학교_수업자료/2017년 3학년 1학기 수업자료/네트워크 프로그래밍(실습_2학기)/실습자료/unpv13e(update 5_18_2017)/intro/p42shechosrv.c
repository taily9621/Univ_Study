#include        "unp.h"
#include        <time.h>
#include	<sys/socket.h> 
#include	<unistd.h>
#include	<stdio.h>
void
sig_chld(int signo)
{
	pid_t	pid;
	int	stat;
	
	pid = wait(&stat);
	printf("child %d terminated\n",pid);
	return;
}

void
str_echo(int sockfd)
{
	ssize_t		n;
	char	buf[MAXLINE];

       again:
	while((n = read(sockfd, buf, MAXLINE)) > 0)
		Writen(sockfd, buf, n);

	if(n < 0 && errno == EINTR)
		goto again;
	else if (n < 0)
		err_sys("str_echo: read error");
}

int
main(int argc, char **argv)
{
        int                                     listenfd, connfd;
	//int portnum = atoi(argv[1]);
	pid_t	childpid;
	socklen_t clilen;
        struct sockaddr_in      servaddr;
	struct sockaddr_in	cliaddr;
	//if(argc != 2)
	//	err_quit("Input port number!");
	
        listenfd = Socket(AF_INET, SOCK_STREAM, 0);

        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family      = AF_INET;
        servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servaddr.sin_port        = htons(SERV_PORT);   /* daytime server */

        Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

        Listen(listenfd, LISTENQ);

        for ( ; ; ) {
		clilen = sizeof(cliaddr);
                connfd = Accept(listenfd, (SA *) &cliaddr, &clilen);
		if((childpid = Fork()) == 0)
		{
			Close(listenfd);
			str_echo(connfd);
			exit(0);
		}	

                Close(connfd);
        }
}

