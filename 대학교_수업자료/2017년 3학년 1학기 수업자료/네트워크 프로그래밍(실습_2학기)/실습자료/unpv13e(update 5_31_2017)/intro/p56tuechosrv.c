#include	"unp.h"
void
sig_chld(int signo)
{
	pid_t pid;
	int stat;
	
	pid = wait(&stat);
	printf("child %d terminated\n",pid);
	return;
}

int
main(int argc, char **argv)
{
	int	listenfd, connfd, udpfd,nready, maxfdp1,maxfd,maxi,sockfd;
	int	i;
	char	mesg[MAXLINE];
	char	buf[MAXLINE];
	int	client[FD_SETSIZE];
	pid_t	childpid;
	fd_set	rset, allset;
	ssize_t	n;
	socklen_t	len, clilen;
	const int on = 1;
	struct sockaddr_in cliaddr, servaddr;
	void	sig_chld(int);

	listenfd = Socket(AF_INET, SOCK_STREAM, 0);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port        = htons(SERV_PORT);	/* daytime server */
	
	Setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
	Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

	Listen(listenfd, LISTENQ);

	udpfd = Socket(AF_INET, SOCK_DGRAM, 0);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port        = htons(SERV_PORT);	/* daytime server */
	
	Bind(udpfd, (SA *) &servaddr, sizeof(servaddr));
	
	Signal(SIGCHLD, sig_chld);
	
	FD_ZERO(&rset);
	maxfdp1 = max(listenfd, udpfd) + 1;
	maxfd = listenfd;
	maxi = -1;
	for(i = 0; i < FD_SETSIZE; i++)
		client[i] = -1;
	FD_ZERO(&allset);
	FD_SET(listenfd, &allset);
	for ( ; ; ) 
	{
		rset = allset;
		FD_SET(listenfd, &rset);
		FD_SET(udpfd, &rset);
		if((nready = select(maxfdp1, &rset, NULL, NULL, NULL)) < 0)
		{
			if(errno == EINTR)
				continue;
			else
				err_sys("select error");
		}

		if(FD_ISSET(listenfd, &rset))
		{
			clilen = sizeof(cliaddr);
			connfd = Accept(listenfd ,(SA *) &cliaddr, &len);
			
			for(i = 0; i < FD_SETSIZE; i++)
				if(client[i] < 0)
				{
					client[i] = connfd;
					break;
				}
			if(i == FD_SETSIZE)
				err_quit("too many clients");
			FD_SET(connfd, &allset);
			if(connfd > maxfd)
				maxfd = connfd;
			if(i > maxi)
				maxi = 1;
			if(--nready <= 0)
				continue;
		}
		for(i = 0; i <= maxi; i++)
		{
			if((sockfd = client[i]) == 0)
				continue;
			if(FD_ISSET(sockfd, &rset))
			{
				if((n = Read(sockfd, buf,MAXLINE)) == 0)
				{
					Close(sockfd);
					FD_CLR(sockfd, &allset);
					client[i] = -1;
				}
				else
					Writen(sockfd, buf, n);

				if(--nready <= 0)
					break;
			}
		}
		if(FD_ISSET(udpfd, &rset))
		{
			len = sizeof(cliaddr);
			n = Recvfrom(udpfd, mesg, MAXLINE, 0, (SA *) &cliaddr, &len);

			Sendto(udpfd, mesg, n, 0, (SA *) &cliaddr, len);
		}
	}
}
