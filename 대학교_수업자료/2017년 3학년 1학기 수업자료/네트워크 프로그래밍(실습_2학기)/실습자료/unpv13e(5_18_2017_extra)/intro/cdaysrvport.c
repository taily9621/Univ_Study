#include        "unp.h"
#include        <time.h>
#include	<sys/socket.h> 
#include	<unistd.h>
int
main(int argc, char **argv)
{
        int                                     listenfd, connfd;
        struct sockaddr_in      servaddr;
	struct sockaddr_in	cliaddr;
	struct sockaddr_in	tmpaddr;
        char                            buff[MAXLINE];
        time_t                          ticks;
        int portnum = atoi(argv[1]);
	int len;
	pid_t				pid;
        if(argc != 2)
                err_quit("Input port number!");
	/*AF_INET, SOCK_STREAM,*/
        listenfd = Socket(AF_INET, SOCK_STREAM, 0);

        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family      = AF_INET;
        servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servaddr.sin_port        = htons(atoi(argv[1]));   /* daytime server */

        Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

        Listen(listenfd, LISTENQ);

        for ( ; ; ) {
		len = sizeof(cliaddr);
		/*(SA *) &cliaddr, &len,*/
                connfd = Accept(listenfd, (SA *) &cliaddr, &len);
	if((pid = Fork()) == 0)
	{
		Close(listenfd);
		ticks = time(NULL);
		snprintf(buff, sizeof(buff), "%.24s\r\n", ctime(&ticks));
		Write(connfd, buff, strlen(buff));
		getpeername(connfd, (SA *) &tmpaddr, &len);
		printf("connection from %s port %d\n", Inet_ntop(AF_INET, &cliaddr.sin_addr, buff, sizeof(&ticks)), ntohs(cliaddr.sin_port));
		Close(connfd);
		exit(0);
	}
        Close(connfd);
    }
}

