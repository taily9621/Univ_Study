#include "unp.h"

void game(FILE *fp, int sockfd) 
{
	int maxfdp1;
	fd_set rset;
	uint32_t sendNum, recvNum, crntNum = 0;
	FD_ZERO(&rset);
	maxfdp1 = max(fileno(fp), sockfd) + 1;

	for( : : ) {

		FD_SET(fileno(fp), &rset);
		FD_SET(sockfd, &rset);
		
		Select(maxfdp1, &rset, NULL, NULL, NULL);

		if( FD_ISSET(sockfd, &rset) )
	       {
			if( Readn(sockfd, &recvNum, sizeof(recvNum)) == 0 )
				return;
			
			recvNum = ntohl(recvNum); 
			crntNum += recvNum;

			if(crntNum > 31) 
			{
				printf("<%d> WIN\n", crntNum);
				break;
			}
		}	
		
		if( FD_ISSET(fileno(fp), &rset) ) 
		{
			scanf("%d", &sendNum);
			if(sendNum >=10 ||sendNum<1) 
			{
				printf("input error: 1~9\n");
				return;
			}

			crntNum += sendNum;
			sendNum = htonl(sendNum);
	
			Writen(sockfd, &sendNum, sizeof(sendNum));
			if(crntNum > 31) 
			{
				printf("<%d> LOSE\n", crntNum);
				break;
			}
		}
		
		printf("<%d> ", crntNum);
		fflush(stdout);
	}

}


int main(int argc, char **argv) 
{
	int listenfd, connfd;
	socklen_t clilen;
	struct sockaddr_in cliaddr, servaddr;

	listenfd = Socket(AF_INET, SOCK_STREAM, 0);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(atoi(argv[1]));

	Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));
	Listen(listenfd, LISTENQ);

	while(1) {
		clilen =  sizeof(cliaddr);
		printf("[waiting for an opponent]\n");
		if( (connfd = accept(listenfd, (SA *) &cliaddr, &clilen)) < 0 ) {
			if( errno == EINTR)
				continue;
			else
				err_sys("accept error");
		}
		printf("[connected to %s:%d]\n", inet_ntoa(cliaddr.sin_addr), ntohs(cliaddr.sin_port)); 
		game(stdin, connfd);	 

		Close(connfd);
	}
return o;
}
