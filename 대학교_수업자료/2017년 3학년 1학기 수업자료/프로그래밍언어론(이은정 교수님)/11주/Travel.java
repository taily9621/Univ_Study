﻿package mgr;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import week11.EatNStay;
import week11.Note;

public class Travel {
	ArrayList<Note> notes = new ArrayList<>();
	String title;
	int year, month, dayFrom, dayTo;
	int days;
	String tripType;
	int numMembers;
	
	public int getDayFrom() {
		return dayFrom;
	}
	void readAFile(String filen) {
		Note n = null;
		String token = null;
		File f = new File(filen);
		Scanner s = null;
		try {
			s = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		title = s.next();
		year = s.nextInt();
		month = s.nextInt();
		dayFrom = s.nextInt();
		dayTo = s.nextInt();
		days = s.nextInt();
		tripType = s.next();
		numMembers = s.nextInt();
		while (s.hasNext()) {
			token = s.next();
			switch(token) {
			case "집합":
			case "도착":
			case "이동":
			case "자유":
				n = new Note(token, this);
				break;
			case "숙박":
			case "식사":
				n = new EatNStay(token, this);
				break;
			default:break;
			}
			notes.add(n);
			n.read(s);
//			n.output();
//			System.out.println();
		}
	}
	void showAll() {
		int i = 1; 
		for (Note note : notes) {
			System.out.printf("%2d) ", i++);
			note.output();
			System.out.println();
		}
	}
	public Note find(String kwd)
	{
		for(Note n : notes) {
			if(n.compare(kwd))
				return n;
		}
		return null;
	}
	public void search(Scanner keyin) {
		String kwd = null;
		Note tmp = null;
		while (true) {
			System.out.print(">> ");	
			kwd = keyin.next();
			if (kwd.equals("end")) break;
			tmp = find(kwd);
			if (tmp == null) 
				System.out.print("검색결과 - 없음");
			else tmp.output();
			System.out.println();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Travel travel = new Travel();
		travel.readAFile("tongyoung0.txt");
		travel.showAll();
		Scanner keyin = new Scanner(System.in);
		travel.search(keyin);
	}
}
