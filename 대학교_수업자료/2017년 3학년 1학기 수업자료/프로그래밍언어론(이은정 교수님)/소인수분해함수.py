def isprime(n):
    i=0;
    for k in range(2, int(n**0.5)+1):
        if n%k == 0:
            return False
    else:
        return True

def nextPrime(n):
    while True:
        n+=1
        if isprime(n):
            break
    return n

def prime(n):
    result=[]
    p=2
    while True:
        if n==1:
            break;
        if n%p == 0:
            result.append(p)
            n/=p
        else:
            p = nextPrime(p)
    return result

while True:
    n = int(input("number : "))
    
    result = str(n) +" = 1"
    for i in prime(n):
        result += " x "+str(i)
    print(result)
