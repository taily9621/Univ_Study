import random
secret = []
while len(secret) < 4:
    a = random.randint(1,9)
    if a in secret:
        continue
    secret.append(a)
print(secret)
tries = []
while True:
    guess = []
    while len(guess) < 4:
        a = int(input("=>1~9 "))
        if (a in guess):
            continue
        guess.append(a)
    strikes = 0
    balls = 0
    for i in range(4):
        if guess[i] == secret[i]:
            strikes += 1
        elif guess[i] in secret:
            balls += 1
    print("{} is {} strikes, {} balls.".format(guess, strikes, balls))
    tries.append(guess)
    if strikes == 4:
        break
for a in tries:
    print(a)
print("Bye!")
