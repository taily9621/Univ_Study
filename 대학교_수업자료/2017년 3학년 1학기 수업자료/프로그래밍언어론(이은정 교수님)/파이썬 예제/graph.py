# 2차원 함수의 그래프 그리기
# 테스트값 : a = 0.5, b = -1.0, c = -20
a = 0.5
b = -1.0
c = -20.0
while True:
    a = float(input('a=>'))
    b = float(input('b=>'))
    c = float(input('c=>'))
    print('y = {}x^2 + {}x + {}'.format(a, b, c))
    for x in range(-10, 10):
        y = a*x*x + b*x+c
        delta = y + 30
        if (delta < 0):
            continue
        space = ' '* int(delta)
        if (x == 0):
            space = '-'*int(delta)
        print('{0:4.1f}{1}{2}  ({3})'.format(x, space, '*', y), end='')
        if (x == 0):
            print(space)
        else:
            print()
