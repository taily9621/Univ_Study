// matchGreat 함수에서 통합 검색 기능 구현
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
struct great {
	char name[20];
	char cMF;
	int bYear, dYear;
	char work[256];
};
void readGreat(struct great* gp);
void printGreat(struct great* gp);
int matchGreat(struct great* gp, char* kwd);
FILE *fp = NULL;
void main()
{
	struct great list[50];
	int count = 0, i, n;
	char keyword[20];
	fp = fopen("great.txt", "r");
	if (fp == NULL) {
		printf("파일이 없음\n");
		return;
	}
	while (!feof(fp)) {
		readGreat(&list[count]);
		count++;
	}
	for (i=0; i < count; i++) {
		printf("[%2d] ", i+1);
		printGreat(&list[i]);
	}
	while (1) {
		printf("검색할 키워드를 입력하세요..");
		scanf("%s", keyword);
		n=0;
		for (i=0; i< count; i++)
			if (matchGreat(&list[i], keyword)) { //"1000"
				printGreat(&list[i]);
				n++;
			}
		if (n == 0)
			printf("없는 이름입니다.\n");
	}
}
int matchGreat(struct great* gp, char kwd[])
{
	int year;
	if (isascii(kwd[0]) && isdigit(kwd[0])) {
		year = atoi(kwd);
		if (gp->bYear <= year && gp->dYear >= year)
			return 1;
	} else if (strcmp(kwd, "M")==0 || strcmp(kwd, "F")==0) {
		if (kwd[0] == gp->cMF)
			return 1;
	} else if (strcmp(gp->name, kwd)==0)
		return 1;
	else if (strstr(gp->work, kwd)!=NULL)
		return 1;
	return 0;
}
void readGreat(struct great* gp)
{
	char buf[20];
	fscanf(fp, "%s %s", gp->name, buf);
	gp->cMF = buf[0];
	fscanf(fp, "%d %d", &gp->bYear, &gp->dYear);
	fgets(gp->work, 256, fp);
}
void printGreat(struct great* gp)
{
	printf("%-10s ", gp->name);
	if (gp->cMF == 'F') printf("[F]");
	else printf("   ");
	if (gp->bYear < 0) printf(" BC%2d ~ ", -gp->bYear);
	else printf(" %4d ~ ", gp->bYear);
	if (gp->dYear < 0) printf("BC%2d", -gp->dYear);
	else printf("%4d", gp->dYear);
	printf("\t%s", gp->work);
}