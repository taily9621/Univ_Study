#include "great.h"
char inputString[MAX_SIZE];
void great::read( great *g )
{
	char buf[20];
	ifstream inFile("great.txt");
	if (inFile.fail()) {
		cout << "파일열기실패" << endl;
		exit(1);
	}
	while (!inFile.eof()) 
	{
		inFile >> g->list[g->count].name;
		inFile >> buf;
		g->list[g->count].cMF = buf[0];
		inFile >> g->list[g->count].bYear;
		inFile >> g->list[g->count].dYear;
		inFile.getline(g->list[g->count].work, 256);
		g->count++;
	}

	inFile.close();

}

	void great::print(great gp, int i) {
	cout << setw(10) << left << gp.list[i].name << " ";
	if (gp.list[i].cMF == 'F') cout << "[F]";
	else cout << "   ";
	if (gp.list[i].bYear < 0) cout << right << " BC" << setw(2) << -gp.list[i].bYear << " ~ ";
	else cout << " " << setw(4) << right << gp.list[i].bYear << " ~ ";
	if (gp.list[i].dYear < 0) cout << "BC" << setw(2) << right << -gp.list[i].dYear;
	else cout << setw(4) << right << gp.list[i].dYear;
	cout << "\t" << gp.list[i].work << endl;
}

int great::compare(class great gp, char kwd[], int i)
{
	int year;
	if (isascii(kwd[0]) && isdigit(kwd[0])) {
		year = atoi(kwd);
		if (gp.list[i].bYear <= year && gp.list[i].dYear >= year)
			return 1;
	}
	else if (strcmp(kwd, "M") == 0 || strcmp(kwd, "F") == 0) {
		if (kwd[0] == gp.list[i].cMF)
			return 1;
	}
	else if (strcmp(gp.list[i].name, kwd) == 0)
		return 1;
	else if (strstr(gp.list[i].work, kwd) != NULL)
		return 1;
	return 0;
}

int great:: return_count(great g)
{
	return g.count;
}
/*
int great:: matchGreat(struct great* gp, char kwd[])
{
	int year;
	if (isascii(kwd[0]) && isdigit(kwd[0])) {
		year = atoi(kwd);
		if (gp->bYear <= year && gp->dYear >= year)
			return 1;
	}
	else if (strcmp(kwd, "M") == 0 || strcmp(kwd, "F") == 0) {
		if (kwd[0] == gp->cMF)
			return 1;
	}
	else if (strcmp(gp->name, kwd) == 0)
		return 1;
	else if (strstr(gp->work, kwd) != NULL)
		return 1;
	return 0;
}

void readGreat(struct great* gp)
{
	char buf[20];
	fscanf(fp, "%s %s", gp->name, buf);
	gp->cMF = buf[0];
	fscanf(fp, "%d %d", &gp->bYear, &gp->dYear);
	fgets(gp->work, 256, fp);
}

void printGreat(struct great* gp)
{
	printf("%-10s ", gp->name);
	if (gp->cMF == 'F') printf("[F]");
	else printf("   ");
	if (gp->bYear < 0) printf(" BC%2d ~ ", -gp->bYear);
	else printf(" %4d ~ ", gp->bYear);
	if (gp->dYear < 0) printf("BC%2d", -gp->dYear);
	else printf("%4d", gp->dYear);
	printf("\t%s", gp->work);
}
*/