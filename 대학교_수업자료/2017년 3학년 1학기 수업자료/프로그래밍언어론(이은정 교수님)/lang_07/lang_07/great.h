//- 클래스 헤더는 클래스 선언부를 가지도록 작성함
//- 클래스는 멤버변수를 모두 private으로 선언하고 멤버함수는 read, print, compare를 가지도록 구성
#ifndef __GREAT_H__
#define __GREAT_H__
#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

#define MAX_SIZE 1000

struct Great {
	char name[20];
	char cMF;
	int bYear, dYear;
	char work[256];
};

class great {
private:
	struct Great list[50];
	int count = 0;
	
public:

	void read(great * g);

	void print( great gp, int i);

	int compare(great gp, char kwd[], int i);

	int return_count(great g);
};

#endif // !__GREAT_H__