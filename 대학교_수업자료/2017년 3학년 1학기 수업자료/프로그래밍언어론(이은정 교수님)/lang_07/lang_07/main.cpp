//- great.h, great.cpp, main.cpp 세 개의 파일로 구성
#include "great.h"

void main() {

	int count, i, n;
	char keyword[20];
	great g;
	g.read(&g);
	count = g.return_count(g);
	for (i = 0; i < count; i++) {
		cout << "[" << setw(2) << right << i + 1 << "] ";
		g.print(g,i);
	}
	while (1) {
		cout << "검색할 키워드를 입력하세요..";
		cin >> keyword;
		n = 0;
		for (i = 0; i < count; i++) {
			if (g.compare(g,keyword, i)) { //"1000"
				g.print(g,i);
				n++;
			}
		}
			
		if (n == 0)
			cout << "없는 이름입니다." << endl;

	}
}