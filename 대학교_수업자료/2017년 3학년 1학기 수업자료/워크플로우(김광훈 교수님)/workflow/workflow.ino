#include<LiquidCrystal.h>

LiquidCrystal lcd(8,9,10,11,12,13);
#define SW1 2
#define SW2 3
#define SW3 4
#define SW4 5
#define SW5 6
#define SW6 7

int count = 0;
int password[] =   {5,6,5,3,2,4};
int inputNum[6] =  {0,0,0,0,0,0};
char Name[20];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(SW1, INPUT_PULLUP);
  pinMode(SW2, INPUT_PULLUP);
  pinMode(SW3, INPUT_PULLUP);
  pinMode(SW4, INPUT_PULLUP);
  pinMode(SW5, INPUT_PULLUP);
  pinMode(SW6, INPUT_PULLUP);
  lcd.begin(16,2);
  
  lcd.setCursor(0,0);
  lcd.print("Waiting Name");
  lcd.setCursor(0,1);
  lcd.print("Input...");
  //char temp[20];
  //read name from serial
  while(1){
    if(Serial.available()){
      byte leng = Serial.readBytes(Name, 20);
      
      Serial.print("Input data Lenght : ");
      Serial.println(leng);
      Serial.flush();
      
      if(leng>2){
      
        //erase enter character
        /*
        Name[leng+1]=Name[leng+2];
        Name[leng]=Name[leng+2];
        */

        //print Name
        Serial.print("Your Input : ");
        Serial.println(Name);
        Serial.flush();
        Serial.print("Success");
        Serial.println();
        break;
      }else if(leng>0){
        Serial.println("Name must over 2 letters");
        Serial.flush();
      }
    }
  }
}

void loop() {
  int i, check, state, num;
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Hi! ");
  lcd.print(Name);
  lcd.setCursor(0,1);
  lcd.print("PASSWORD:");

  while(count<6){
    num=0;
    do{ //check butten status
      if(digitalRead(SW1) == LOW){
        num=1;
      }
      if(digitalRead(SW2) == LOW){
        num=2;
      }
      if(digitalRead(SW3) == LOW){
        num=3;
      }
      if(digitalRead(SW4) == LOW){
        num=4;
      }
      if(digitalRead(SW5) == LOW){
        num=5;
      }
      if(digitalRead(SW6) == LOW){
        num=6;
      }
    }while(num==0);

    //save and print number
    inputNum[count] = num;
    lcd.print(num);
    Serial.print(num);
    count++;
  
    delay(300);
  }
  Serial.println();

  //check password
  check=0;
  for(i=0;i<6;i++){
    if(password[i]!=inputNum[i]){
      check++;
    }
  }

  lcd.clear();

  //print password is right
  if(check==0){
    lcd.print("True");
    state=1;
  }else{
    lcd.print("False");
    state=0;
  }
  

  Serial.println(state);

  //reset save
  for(i=0;i<6;i++){
    inputNum[i]='-';
  }
  count=0;
  
  delay(1000);
}
