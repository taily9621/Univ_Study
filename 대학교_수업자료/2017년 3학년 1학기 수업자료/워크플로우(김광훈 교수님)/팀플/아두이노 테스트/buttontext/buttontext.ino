void setup() {
  // put your setup code here, to run once:
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(11,OUTPUT);
  pinMode(12,OUTPUT);
  pinMode(13,OUTPUT);
  pinMode(0,INPUT_PULLUP);
  pinMode(1,INPUT_PULLUP);
  pinMode(2,INPUT_PULLUP);
  pinMode(3,INPUT_PULLUP);
  pinMode(4,INPUT_PULLUP);
  pinMode(5,INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  int button1 = digitalRead(0);
  int button2 = digitalRead(1);
  int button3 = digitalRead(2);
  int button4 = digitalRead(3);
  int button5 = digitalRead(4);
  int button6 = digitalRead(5);
  if(button1 == 0) digitalWrite(8,1);
  else digitalWrite(8,0);
  if(button2 == 0) digitalWrite(9,1);
  else digitalWrite(9,0);
  if(button3 == 0) digitalWrite(10,1);
  else digitalWrite(10,0);
  if(button4 == 0) digitalWrite(11,1);
  else digitalWrite(11,0);
  if(button5 == 0) digitalWrite(12,1);
  else digitalWrite(12,0);
  if(button6 == 0) digitalWrite(13,1);
  else digitalWrite(13,0);
}
