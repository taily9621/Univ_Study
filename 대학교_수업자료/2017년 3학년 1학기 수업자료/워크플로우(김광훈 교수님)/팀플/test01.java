package Homeworktest;
import java.util. *;

public class test01 
{
	public final static class Algorithm
	{
		//2번 문제, 3번 문제
		
		public static <T> T max(T x,T  y)//2번 문제
		{
			return x > y ? x : y;
		}
		
		public static <T> void swap(T[] a, int i, int j)//3번 문제
		{
			
			T temp = a[i];
			a[i] = a[j];
			a[j] = temp;
		}
	}
	//8번 문제
	public static <T extends Object & Comparable<? super T>>
    T max(List<? extends T> list, int begin, int end) 
	{
	//T는 Comparable 인터페이스를 구현한 객체여야 하며
	//Comparable을 구현한 T를 상속한 객체여도 상관없다.

    T maxElem = list.get(begin); 

    for (++begin; begin < end; ++begin)
        if (maxElem.compareTo(list.get(begin)) < 0)
            maxElem = list.get(begin);
	//시작값부터 끝값 범위 비교, 결과값 리턴
    return maxElem;
	}
	//9번 문제
	public class Singleton<T> {

	    public static T getInstance() {
	        if (instance == null)
	            instance = new Singleton<T>();

	        return instance;
	    }

	    private static T instance = null;
	}
	//10번 문제 
	class Shape 
	{
		Scanner xWay = new Scanner(System.in);
		Scanner yWay = new Scanner(System.in);
		
		int X = xWay.nextInt();
		int Y = yWay.nextInt();
		
	}
	class Circle extends Shape 
	{ 
		Node<Double> checkCnt1 = new Node<Double>();
		double CircleArea = 0;
		double Pi = 3.14;
		public double Area1()
		{
			CircleArea = Pi * (X*X);
			checkCnt1.countNum(CircleArea);
			return CircleArea;
		}
	}
	class Rectangle extends Shape 
	{ 
		Node<Integer> checkCnt2 = new Node<Integer>();
		int RectangleArea = 0;
		
		public int Area2()
		{
			RectangleArea = X * Y;
			checkCnt2.countNum(RectangleArea);
			return RectangleArea;
		}
	}

	class Node<T> 
	{ 
		int cnt = 0;
		Object chk;
		
		public void countNum(T chk)
		{
			if(cnt == 10)
			{
				System.out.println("End");
				return;
			}
			else
			{
				System.out.println("result is "+ chk);
				cnt++;
			}
		}
	}
	//확인용 main 클래스
	public static void main(String[] args)
	{	//2본 문제 : 제너릭 비교 연산자 구하기(컴파일 되지 않음!)
				//System.out.println("result : " + Algorithm.max(2, 3));
		//3번 문제 : 배열에 있는 두 개의 다른 요소의 위치를 교환하는 메인 프로그램
				Integer[] Arraynum = {1,2,3,4};
				Algorithm.swap(Arraynum, 2, 3);
				System.out.print("바꾼 배열의 결과 : ");
				for(int k=0; k<4; k++)
					System.out.print(Arraynum[k] + " ");
				System.out.println();
		
		//8번 문제 : 리스트에서 비교하여 최대값 출력해주는 메인 프로그램
				List listA = new ArrayList();
				Scanner s = new Scanner(System.in);
				System.out.print("숫자 4개 입력 : ");
				
				for(int i=0;i<4;i++)
				{
					int a = s.nextInt();
					listA.add(a);
				}
				
				System.out.println("제일 큰 숫자 : " + max(listA,0,4));
		//9번 문제 : 타입 파라미터 T의 정적필드(컴파일 되지 않음!)
				/*
				Singleton <First> firstTon = new Singleton <> ();
				Singleton <Second> secondTon = new Singleton <> ();
				Singleton <third> thirdTon = new Singleton <> ();
				*/
		//10번 문제 : (컴파일 되지 않음!)
				/*
				Node<Circle> nc = new Node<>();
				Node<Rectangle> nr = new Node<>();
				Node<Shape> ns = (Node<Shape>)nc;
				*/
	}
}
