package extendServiceTask;

import org.activiti.designer.integration.annotation.Property;
import org.activiti.designer.integration.annotation.Runtime;
import org.activiti.designer.integration.servicetask.AbstractCustomServiceTask;
import org.activiti.designer.integration.servicetask.PropertyType;

@Runtime(javaDelegateClass="extendServiceTaskExecution") //extendServiceTask 실행파일 경로

public class extendServiceTaskForm  extends AbstractCustomServiceTask //AbstractCustomerServiceTask 확장
{
	@Property(type = PropertyType.TEXT, displayName = "text", required = true)
	private String text;
	
	@Override
	public String contributeToPaletteDrawer() //paletteGroup에 extendServiceTask 추가
	{
		return "PaletteGroupName";
	}
	public String getName() //extendServiceTask 이름
	{
		return "extendServiceTask";
	}
	
	@Override
	public String getSmallIconPath() //extendServiceTask 아이콘 이미지 추가
	{
		return "sensingIcon2.png";
	}
}
