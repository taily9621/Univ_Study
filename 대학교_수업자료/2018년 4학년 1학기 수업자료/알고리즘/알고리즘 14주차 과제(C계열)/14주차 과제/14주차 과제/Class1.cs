﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14주차_과제
{
    class Class1
    {

        // A utility function that returns
        // maximum of two integers
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }

        // Returns the maximum value that can 
        // be put in a knapsack of capacity W
        static int knapSack(int W, int[] wt,int[] val, int n)
        {

            // Base Case
            if (n == 0 || W == 0)
                return 0;

            // If weight of the nth item is 
            // more than Knapsack capacity W,
            // then this item cannot be 
            // included in the optimal solution
            if (wt[n - 1] > W)
                return knapSack(W, wt, val, n - 1);

            // Return the maximum of two cases: 
            // (1) nth item included 
            // (2) not included
            else return max(val[n - 1] +
                knapSack(W - wt[n - 1], wt, val, n - 1),
                       knapSack(W, wt, val, n - 1));
        }

        // Driver function
        public static void Main()
        {
            Random r = new Random();
            int cnt;
            string s_cnt;

            Console.Write("=>");
            s_cnt = Console.ReadLine();
            cnt = Convert.ToInt32(s_cnt);
            //int[] val = new int[] { 60, 100, 120 };
            //int[] wt = new int[] { 10, 20, 30 };
            int[] val = new int[] { };
            int[] wt = new int[] { };
            for (int i = 0; i < cnt; i++)
            {
                val.Add(r.Next(60, 121));
                wt[i] = r.Next(10, 31);
            }
            
            int W = 50;
            int n = val.Length;

            Console.WriteLine(knapSack(W, wt, val, n));
        }
    }

    // This code is contributed by Sam007

}
