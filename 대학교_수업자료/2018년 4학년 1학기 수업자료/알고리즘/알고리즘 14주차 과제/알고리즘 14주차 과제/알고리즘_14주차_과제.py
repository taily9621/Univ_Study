"""
import random
import time

# 1초 안으로 실행되는 최대 값 : 940
#start_time = time.time()
def init_list(post_stamp):
    stamp_point = 0
    for x in range(2):
        for y in range(len(post_stamp[x])):
            if post_stamp[x][y] != '':
                if x == 0:
                    if y <= len(post_stamp[x])-2 and post_stamp[x][y+1] != '':
                        stamp_point += post_stamp[x][y+1]
                    if post_stamp[x+1][y] != '':
                        stamp_point += post_stamp[x+1][y]
                    if y > 0 and post_stamp[x][y-1] != '':
                        stamp_point += post_stamp[x][y-1]
                    point_list[x].append(stamp_point)
                    stamp_point = 0
                else:
                    if y <= len(post_stamp[x])-2 and post_stamp[x][y+1] != '':
                        stamp_point += post_stamp[x][y+1]
                    if post_stamp[x-1][y] != '':
                        stamp_point += post_stamp[x-1][y]
                    if y > 0 and post_stamp[x][y-1] != '':
                        stamp_point += post_stamp[x][y-1]
                    point_list[x].append(stamp_point)
                    stamp_point = 0
            else:
                point_list[x].append('')
                continue

def select_pull(post_stamp, point_list, earn_point):
    min_point = []
    min_value = 9999
    for num1,value1 in enumerate(point_list):
        #print(num1,", ",value1)
        for num2, value2 in enumerate(value1):
            #print("[",num1,",",num2,"] = ",value2)
            if value2 == '':
                continue
            if min_value > value2:
                min_value = value2
                min_point = [num1,num2]

    #print("Minimum point, value : ",min_point," = ",min_value)

    earn_point.append(post_stamp[min_point[0]][min_point[1]])
    #print("획득 값 : ",earn_point)
    post_stamp[min_point[0]][min_point[1]] = ''

    if min_point[1]+1 < len(post_stamp[min_point[0]]) and post_stamp[min_point[0]][min_point[1]+1] != '':
        post_stamp[min_point[0]][min_point[1]+1] = ''
    if min_point[1]-1 >= 0 and post_stamp[min_point[0]][min_point[1]-1] != '':
        post_stamp[min_point[0]][min_point[1]-1] = ''
    if min_point[0] == 0 and post_stamp[min_point[0]+1][min_point[1]] != '':
        post_stamp[min_point[0]+1][min_point[1]] = ''
    if min_point[0] == 1 and post_stamp[min_point[0]-1][min_point[1]] != '':
        post_stamp[min_point[0]-1][min_point[1]] = ''
    return earn_point

if __name__ == "__main__":
    
    line_N = int(input('N = '))

    start_time = time.time()
    post_stamp = []
    point_list = [[],[]]
    earn_point = []

    for x in range(2):
        post_stamp.append([random.randrange(10,200,10) for _ in range(line_N)])

    while True:
        #print(any(post_stamp[0]))
        #print(any(post_stamp[1]))
        if any(post_stamp[0]) == False and any(post_stamp[1]) == False:
            break

        #print(post_stamp)
        init_list(post_stamp)
        #print(point_list)
        select_pull(post_stamp, point_list, earn_point)
        #print(post_stamp)
        point_list = [[],[]]

    print("총 가중치 값 : ",sum(earn_point))
print("%s seconds"%(time.time() - start_time))
"""
# 1초 안으로 실행되는 최대 값 : 28000~29000
import time
import random

# A Dynamic Programming based Python Program for 0-1 Knapsack problem
# Returns the maximum value that can be put in a knapsack of capacity W

def knapSack(W, wt, val, n):
    K = [[0 for x in range(W+1)] for x in range(n+1)]
 
    # Build table K[][] in bottom up manner
    for i in range(n+1):
        for w in range(W+1):
            if i==0 or w==0:
                K[i][w] = 0
            elif wt[i-1] <= w:
                K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]],  K[i-1][w])
            else:
                K[i][w] = K[i-1][w]
 
    return K[n][W]

# Driver program to test above function

cnt = int(input('=>'))

start_time = time.time()
val = [random.randrange(60,121) for _ in range(cnt)]
wt = [random.randrange(10,31) for _ in range(cnt)]
#val = [60, 100, 120, 70, 80, 50, 110, 90]
#wt = [10, 20, 30, 15, 25, 5, 20, 20]
W = 50
n = len(val)
print(knapSack(W, wt, val, n))
print("%s seconds"%(time.time() - start_time))
 
# This code is contributed by Bhavya Jain

"""
# 1초 안으로 실행되는 최대 값 : 1248
# Dynamic programming implementation of LCS problem
 
# Returns length of LCS for X[0..m-1], Y[0..n-1] 
import random
import string
import time

def lcs(X, Y, m, n):
    L = [[0 for x in range(n+1)] for x in range(m+1)]
 
    # Following steps build L[m+1][n+1] in bottom up fashion. Note
    # that L[i][j] contains length of LCS of X[0..i-1] and Y[0..j-1] 
    for i in range(m+1):
        for j in range(n+1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif X[i-1] == Y[j-1]:
                L[i][j] = L[i-1][j-1] + 1
            else:
                L[i][j] = max(L[i-1][j], L[i][j-1])
 
    # Following code is used to print LCS
    index = L[m][n]
 
    # Create a character array to store the lcs string
    lcs = [""] * (index+1)
    lcs[index] = ""
 
    # Start from the right-most-bottom-most corner and
    # one by one store characters in lcs[]
    i = m
    j = n
    res = ''
    while i > 0 and j > 0:
 
        # If current character in X[] and Y are same, then
        # current character is part of LCS
        if X[i-1] == Y[j-1]:
            lcs[index-1] = X[i-1]
            i-=1
            j-=1
            index-=1
 
        # If not same, then find the larger of two and
        # go in the direction of larger value
        elif L[i-1][j] > L[i][j-1]:
            i-=1
        else:
            j-=1
    for x in lcs:
        res += x

    print(res)
    print("LCS of " + X + " and " + Y + " is " + ""+res) 
 
# Driver program
cnt = int(input('=>'))

start_time = time.time()
X = ''
Y = ''
x = [random.choice(string.ascii_uppercase) for _ in range(cnt)]
y = [random.choice(string.ascii_uppercase) for _ in range(cnt)]
for g in x:
    X += g
for h in y:
    Y += h

#print(X)
#print(Y)
#X = "AGGTAB"
#Y = "GXTXAYB"
m = len(X)
n = len(Y)
lcs(X, Y, m, n)
print("%s seconds"%(time.time() - start_time))

# This code is contributed by BHAVYA JAIN
"""