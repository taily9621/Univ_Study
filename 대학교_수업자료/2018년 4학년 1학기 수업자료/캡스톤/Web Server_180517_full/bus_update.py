import wget, os, threading, datetime

def start_download():
	threading.Timer(10.0, start_download).start()
	download_all()
	now = datetime.datetime.now()
	nowDatetime = now.strftime('%Y-%m-%d %H:%M:%S')
	print 'updated - ' + nowDatetime

def download_all():
	bus_ids = (100100593,100100610,100100592,100100586,100100591,100100585,100100589,100100588,115000008)

	url = 'http://ws.bus.go.kr/api/rest/buspos/getBusPosByRtid?serviceKey=lbqpxW1PJ42nTZquSbBUwBxIbr24LXSTb02RFhHIqJFNynvQvErLAp%2FlGdStvZzJMFrOhASXpJOgg2HOa3guyQ%3D%3D&busRouteId='
	file_header = 'getBusPosByRtid'

	for id in bus_ids:
		file_name = file_header + str(id) + '.xml'
		if os.path.exists(file_name):
			os.remove(file_name)
			print 'removed\t' + file_name
		working = wget.download(url + str(id), file_name, None)
		print 'downloaded\t' + working
		
if __name__ == "__main__":
	start_download()