var map = null;
//var bus_ids = new Array(100100593,100100610,100100592,100100586,100100591,100100585,100100589,100100588,115000008);
var bus_dict = {100100593:'N13',100100610:'N15',100100592:'N16',100100586:'N26',100100591:'N30',100100585:'N37',100100589:'N61',100100588:'N62',115000008:'N65'};
var bus_ids = Object.keys(bus_dict);

var line_color = new Array('#FF607F', '#CD1039', '#FF7F50', '#FFAE00', '#64CD3C', '#3296D7', '#00008C', '#7B68EE', '#000000');
//http://www.hipenpal.com/tool/html-color-charts-rgb-color-table-in-korean.php
//http://www.n2n.pe.kr/lev-1/color.htm
//https://mwultong.blogspot.com/2007/10/pastel-color.html


var start = new Date(); // 페이지 첫 로딩 시점

function main(){
	var i;
	try{
		viewMap();
		for (i=0;i<bus_ids.length; i++){
			markers[i] = new Array();
			customOverlay[i] = new Array();
			loadXml_line(i);
			loadXml_pos(i);
			setInterval("loadXml_pos(" + i + ")", 10000);
		}
		//loadXml2(100100096);
		//setInterval("loadXml2(100100096)", 10000);
	}catch(err){
		document.getElementById("error").innerHTML = err.message;
	}
}

function viewMap(){
	var container = document.getElementById('map');
	var options = {
		center: new daum.maps.LatLng(37.5268874,126.9872876),
		level: 7
	};
	
	map = new daum.maps.Map(container, options);
}


function showGPS(){
	// HTML5의 geolocation으로 사용할 수 있는지 확인합니다 
	if (navigator.geolocation) {
		
		// GeoLocation을 이용해서 접속 위치를 얻어옵니다
		navigator.geolocation.getCurrentPosition(function(position) {
			
			var lat = position.coords.latitude, // 위도
				lon = position.coords.longitude; // 경도
			
			var locPosition = new daum.maps.LatLng(lat, lon), // 마커가 표시될 위치를 geolocation으로 얻어온 좌표로 생성합니다
				message = '<div style="padding:5px;">' + lat + ', ' + lon + '</div>'; // 인포윈도우에 표시될 내용입니다
			
			// 마커와 인포윈도우를 표시합니다
			displayGPS(locPosition, message);
			
			getText();
			document.getElementById("enableGPS").innerHTML ='<button onclick="nearBus(' + lat + ', ' lon + ')">가까운 정류장 표시</button>';
		  });
		
	} else { // HTML5의 GeoLocation을 사용할 수 없을때 마커 표시 위치와 인포윈도우 내용을 설정합니다
		alert("geolocation을 사용할 수 없습니다.");
	}
}

var gps_marker = null, gps_infowindow = null;

// 지도에 마커와 인포윈도우를 표시하는 함수입니다
function displayGPS(locPosition, message) {
	
	// 기존에 마커가 있는 경우, 마커를 제거합니다.
	if (gps_marker != null) {
		gps_marker.setMap(null);
		gps_infowindow.open(null, null);
	}
	
	// 마커를 생성합니다
	gps_marker = new daum.maps.Marker({  
		position: locPosition,
		map: map
	}); 
	
	var iwContent = message, // 인포윈도우에 표시할 내용
		iwRemoveable = true;

	// 인포윈도우를 생성합니다
	gps_infowindow = new daum.maps.InfoWindow({
		content : iwContent,
		removable : iwRemoveable
	});
	
	// 인포윈도우를 마커위에 표시합니다 
	gps_infowindow.open(map, gps_marker);
	
	// 지도 중심좌표를 접속위치로 변경합니다
	map.setCenter(locPosition);      
}    

function loadXml_line(busno){
	var xhr = new XMLHttpRequest();
	var url = 'getRoutePath';
	var full_url = url + bus_ids[busno] + '.xml';
	
	xhr.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			//document.getElementById("output").innerHTML = 'Status: '+this.status+'</br>  Headers: '+JSON.stringify(this.getAllResponseHeaders())+'</br>  Body: '+this.responseText;
			loadData_line(this,busno);
		}
	};
	xhr.open('GET', full_url, true);
	xhr.send();
}

function loadData_line(xml, busno){
	var i;
	var xmlDoc = xml.responseXML;
	var linePath = new Array();
	
	var gpsx = xmlDoc.getElementsByTagName("gpsX");
	var gpsy = xmlDoc.getElementsByTagName("gpsY");
	
	try{
		for (i = 0; i < gpsx.length; i++){
			linePath[i] = new daum.maps.LatLng(gpsy[i].firstChild.data, gpsx[i].firstChild.data);
		}
	}catch(err){
		document.getElementById("error").innerHTML = err.message;
	}
	
	drawLine(busno, linePath);
}

function drawLine(busno, linePath){
	var polyline = new daum.maps.Polyline({ // 지도에 표시할 선을 생성합니다
		path: linePath, // 선을 구성하는 좌표배열 입니다
		strokeWeight: 10, // 선의 두께 입니다
		strokeColor: line_color[busno], // 선의 색깔입니다
		strokeOpacity: 0.7, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
		strokeStyle: 'solid' // 선의 스타일입니다
	});
	
	polyline.setMap(map); // 지도에 선을 표시합니다 
	
	document.getElementById("load").innerHTML = new Date() - start + 'ms';
}

function loadXml_pos(busno){
	var xhr = new XMLHttpRequest();
	var url = 'getBusPosByRtid';
	var bus_id = bus_ids[busno];
	var full_url = url + bus_id + '.xml';
	
	xhr.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			//document.getElementById("infos").innerHTML = 'Status: '+this.status+'</br>  Headers: '+JSON.stringify(this.getAllResponseHeaders())+'</br>  Body: '+this.responseText;
			loadData_pos(this,bus_id, busno);
		}
	};
	xhr.open('GET', full_url, true);
	xhr.send();
}

function loadData_pos(xml, bus_id, busno){
	var i;
	var xmlDoc = xml.responseXML;
	var position = new Array();
	var infos = new Array();
	
	var gpsx = xmlDoc.getElementsByTagName("gpsX");
	var gpsy = xmlDoc.getElementsByTagName("gpsY");
	var next = xmlDoc.getElementsByTagName("lastStnId");
	var plain = xmlDoc.getElementsByTagName("plainNo");
	
	try{
		for (i = 0; i < gpsx.length; i++){
			position[i] = new daum.maps.LatLng(gpsy[i].firstChild.data, gpsx[i].firstChild.data);
			infos[i] = [next[i].firstChild.data, plain[i].firstChild.data];
		}
	}catch(err){
		document.getElementById("error").innerHTML = err.message;
	}
	
	drawOverlay(busno, position, infos);
}


var imageSrc = 'bus2.png'; // 마커이미지의 주소입니다
var markers = new Array(); // 마커배열
var customOverlay = new Array(); // 오버레이 배열

function drawOverlay(busno, position, infos){
	try{
		var i;
		for (i=0;i<markers[busno].length; i++){
			markers[busno][i].setMap(null); // 마커를 제거합니다.
			customOverlay[busno][i].setMap(null); //오버레이를 제거합니다.
		}
	}catch(err){
		document.getElementById("error").innerHTML = err.message + 'remove error';
	}
	
	// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
	var imageSize = new daum.maps.Size(32, 43); // 마커이미지의 크기입니다
	var imageOption = {offset: new daum.maps.Point(16, 43)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
	var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize, imageOption);
	
	try{
		var i;
		for (i=0;i<position.length; i++){
			markers[busno][i] = new daum.maps.Marker({ // 마커를 생성합니다
			  position: position[i],
			  image: markerImage // 마커이미지 설정 
			});
			markers[busno][i].setMap(map);  // 마커가 지도 위에 표시되도록 설정합니다
		}
	}catch(err){
		document.getElementById("error").innerHTML = err.message;
	}
	
	try{
		var i;
		var bus_name = bus_dict[bus_ids[busno]];
		for (i=0;i<position.length; i++){
			// 커스텀 오버레이에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
			var content = '<div class="customoverlay">' +
				'    <span class="title">' + bus_name + '버스 (' + infos[i][0] + ' 방면)</br> ' +
				infos[i][1]  + '</span>' +
				'</div>';

			// 커스텀 오버레이가 표시될 위치입니다 
			//var position = new daum.maps.LatLng(37.54699, 127.09598);  

			// 커스텀 오버레이를 생성합니다
			customOverlay[busno][i] = new daum.maps.CustomOverlay({
				map: map,
				position: position[i],
				content: content,
				yAnchor: 1 
			});
		}
	}catch(err){
		document.getElementById("error").innerHTML = err.message + ' making error ' + infos[0];
	}

	document.getElementById("refresh").innerHTML = 'refreshed ' + server_time();
}

function server_time()
{
	var now = new Date();
	//now.setSeconds(now.getSeconds()+1);
 
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	var date = now.getDate();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds();
 
	if (month < 10){
		month = "0" + month;
	}
 
	if (date < 10){
		date = "0" + date;
	}
 
	if (hours < 10){
		hours = "0" + hours;
	}
 
	if (minutes < 10){
		minutes = "0" + minutes;
	}
 
	if (seconds < 10){
		seconds = "0" + seconds;
	}
 
	return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
}