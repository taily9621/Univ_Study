import urllib.request
import os, threading, datetime
bus_ids = (100100593,100100610,100100592,100100586,100100591,100100585,100100589,100100588,115000008)
i = 0
limit = 20

def start_download():
	global i
	download_pos(i)
	download_arr(i)
	i = i + 1
	now = datetime.datetime.now()
	nowDatetime = now.strftime('%Y-%m-%d %H:%M:%S')
	print ('[{}] updated - '.format(i),nowDatetime)
	if i < limit :
		threading.Timer(600.0, start_download).start() #1time/10min. 20times

def download_pos(n):
	url = 'http://ws.bus.go.kr/api/rest/buspos/getBusPosByRtid?serviceKey=lbqpxW1PJ42nTZquSbBUwBxIbr24LXSTb02RFhHIqJFNynvQvErLAp%2FlGdStvZzJMFrOhASXpJOgg2HOa3guyQ%3D%3D&busRouteId='
	file_header = 'getBusPosByRtid'

	for id in bus_ids:
		file_name = file_header + str(id) + '_' + str(n) + '.xml'
		if os.path.exists(file_name): #remove file when it exist
			os.remove(file_name)
			print ('removed\t\t' + file_name)
		#working = wget.download(url + str(id), file_name, None)
		urllib.request.urlretrieve(url+str(id), filename=file_name)
		print ('downloaded\t' + file_name)
		
def download_arr(n):
	url = 'http://ws.bus.go.kr/api/rest/arrive/getArrInfoByRouteAll?serviceKey=lbqpxW1PJ42nTZquSbBUwBxIbr24LXSTb02RFhHIqJFNynvQvErLAp%2FlGdStvZzJMFrOhASXpJOgg2HOa3guyQ%3D%3D&busRouteId='
	file_header = 'getArrInfoByRouteAllList'

	for id in bus_ids:
		file_name = file_header + str(id) + '_' + str(n) + '.xml'
		if os.path.exists(file_name): #remove file when it exist
			os.remove(file_name)
			print ('removed\t\t' + file_name)
		#working = wget.download(url + str(id), file_name, None)
		urllib.request.urlretrieve(url+str(id), filename=file_name)
		print ('downloaded\t' + file_name)
		
if __name__ == "__main__":
	start_download()
    #download_all()
