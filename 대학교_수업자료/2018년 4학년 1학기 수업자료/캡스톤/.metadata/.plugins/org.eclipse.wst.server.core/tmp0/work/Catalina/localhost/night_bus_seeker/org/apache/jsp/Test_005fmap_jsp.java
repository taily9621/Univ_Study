/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/8.5.30
 * Generated at: 2018-04-25 15:45:24 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Test_005fmap_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<p id=\"result\"></p>\t\t\t\t\t\r\n");
      out.write("<script>\r\n");
      out.write("// map ìì±\r\n");
      out.write("// Tmap.mapì ì´ì©íì¬, ì§ëê° ë¤ì´ê° div, ëì´, ëì´ë¥¼ ì¤ì í©ëë¤.\t\t\t\t\t\t\t\t\r\n");
      out.write("map = new Tmap.Map({\r\n");
      out.write("\t\t\t\tdiv : 'map_div',// mapì íìí´ì¤ div\r\n");
      out.write("\t\t\t\twidth : \"100%\",// mapì width ì¤ì \r\n");
      out.write("\t\t\t\theight : \"400px\",// mapì height ì¤ì \r\n");
      out.write("\t\t\t});\r\n");
      out.write("map.setCenter(new Tmap.LonLat(\"126.986072\", \"37.570028\").transform(\"EPSG:4326\", \"EPSG:3857\"), 15);//ì¤ì í ì¢íë¥¼ \"EPSG:3857\"ë¡ ì¢íë³íí ì¢íê°ì¼ë¡ ì¦ì¬ì ì ì¤ì í©ëë¤.\t\t\t\t\t\t\r\n");
      out.write("\r\n");
      out.write("// ìì\r\n");
      out.write("var markerStartLayer = new Tmap.Layer.Markers(\"start\");//ë§ì»¤ ë ì´ì´ ìì±\r\n");
      out.write("map.addLayer(markerStartLayer);//mapì ë§ì»¤ ë ì´ì´ ì¶ê°\r\n");
      out.write("\r\n");
      out.write("var size = new Tmap.Size(24, 38);//ìì´ì½ í¬ê¸° ì¤ì \r\n");
      out.write("var offset = new Tmap.Pixel(-(size.w / 2), -size.h);//ìì´ì½ ì¤ì¬ì  ì¤ì \r\n");
      out.write("var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_r_m_s.png />', size, offset);//ë§ì»¤ ìì´ì½ ì¤ì \r\n");
      out.write("var marker_s = new Tmap.Marker(new Tmap.LonLat(\"126.977022\", \"37.569758\").transform(\"EPSG:4326\", \"EPSG:3857\"), icon);//ì¤ì í ì¢íë¥¼ \"EPSG:3857\"ë¡ ì¢íë³íí ì¢íê°ì¼ë¡ ì¤ì í©ëë¤.\r\n");
      out.write("markerStartLayer.addMarker(marker_s);//ë§ì»¤ ë ì´ì´ì ë§ì»¤ ì¶ê°\r\n");
      out.write("\r\n");
      out.write("// ëì°©\r\n");
      out.write("var markerEndLayer = new Tmap.Layer.Markers(\"end\");//ë§ì»¤ ë ì´ì´ ìì±\r\n");
      out.write("map.addLayer(markerEndLayer);//mapì ë§ì»¤ ë ì´ì´ ì¶ê°\r\n");
      out.write("\r\n");
      out.write("var size = new Tmap.Size(24, 38);//ìì´ì½ í¬ê¸° ì¤ì \r\n");
      out.write("var offset = new Tmap.Pixel(-(size.w / 2), -size.h);//ìì´ì½ ì¤ì¬ì  ì¤ì \r\n");
      out.write("var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_r_m_e.png />', size, offset);//ë§ì»¤ ìì´ì½ ì¤ì \r\n");
      out.write("var marker_e = new Tmap.Marker(new Tmap.LonLat(\"126.997589\", \"37.570594\").transform(\"EPSG:4326\", \"EPSG:3857\"), icon);//ì¤ì í ì¢íë¥¼ \"EPSG:3857\"ë¡ ì¢íë³íí ì¢íê°ì¼ë¡ ì¤ì í©ëë¤.\r\n");
      out.write("markerEndLayer.addMarker(marker_e);//ë§ì»¤ ë ì´ì´ì ë§ì»¤ ì¶ê°\r\n");
      out.write("\r\n");
      out.write("// ê²½ì ì§ ì¬ë³¼ ì°ê¸°\r\n");
      out.write("var markerWaypointLayer = new Tmap.Layer.Markers(\"waypoint\");//ë§ì»¤ ë ì´ì´ ìì±\r\n");
      out.write("map.addLayer(markerWaypointLayer);//mapì ë§ì»¤ ë ì´ì´ ì¶ê°\r\n");
      out.write("\r\n");
      out.write("var size = new Tmap.Size(24, 38);//ìì´ì½ í¬ê¸° ì¤ì \r\n");
      out.write("var offset = new Tmap.Pixel(-(size.w / 2), -size.h);//ìì´ì½ ì¤ì¬ì  ì¤ì \r\n");
      out.write("var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_p.png />', size, offset);//ë§ì»¤ ìì´ì½ ì¤ì \r\n");
      out.write("var marker = new Tmap.Marker(new Tmap.LonLat(\"126.983072\", \"37.573028\").transform(\"EPSG:4326\", \"EPSG:3857\"), icon);//ì¤ì í ì¢íë¥¼ \"EPSG:3857\"ë¡ ì¢íë³íí ì¢íê°ì¼ë¡ ì¤ì í©ëë¤.\r\n");
      out.write("markerWaypointLayer.addMarker(marker);//ë§ì»¤ ë ì´ì´ì ë§ì»¤ ì¶ê°\r\n");
      out.write("\r\n");
      out.write("var size = new Tmap.Size(24, 38);//ìì´ì½ í¬ê¸° ì¤ì \r\n");
      out.write("var offset = new Tmap.Pixel(-(size.w / 2), -size.h);//ìì´ì½ ì¤ì¬ì  ì¤ì \r\n");
      out.write("var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_p.png />', size, offset);//ë§ì»¤ ìì´ì½ ì¤ì \r\n");
      out.write("var marker = new Tmap.Marker(new Tmap.LonLat(\"126.987319\", \"37.565778\").transform(\"EPSG:4326\", \"EPSG:3857\"), icon);//ì¤ì í ì¢íë¥¼ \"EPSG:3857\"ë¡ ì¢íë³íí ì¢íê°ì¼ë¡ ì¤ì í©ëë¤.\r\n");
      out.write("markerWaypointLayer.addMarker(marker);//ë§ì»¤ ë ì´ì´ì ë§ì»¤ ì¶ê°\r\n");
      out.write("\t\t\t\t\t\t\t\r\n");
      out.write("var headers = {}; \r\n");
      out.write("headers[\"appKey\"]=\"ba9f84ee-24a8-4b25-bde3-c13c480bc28c\";//ì¤íì ìí í¤ ìëë¤. ë°ê¸ë°ì¼ì  AppKeyë¥¼ ìë ¥íì¸ì.\r\n");
      out.write("$.ajax({\r\n");
      out.write("\tmethod:\"POST\",\r\n");
      out.write("\theaders : headers,\r\n");
      out.write("\turl:\"https://api2.sktelecom.com/tmap/routes/pedestrian?version=1&format=xml\",//ë³´íì ê²½ë¡ìë´ api ìì²­ urlìëë¤.\r\n");
      out.write("\tasync:false,\r\n");
      out.write("\tdata:{\r\n");
      out.write("\t\t//ì¶ë°ì§ ìê²½ë ì¢íìëë¤.\r\n");
      out.write("\t\tstartX : \"126.977022\",\r\n");
      out.write("\t\tstartY : \"37.569758\",\r\n");
      out.write("\t\t//ëª©ì ì§ ìê²½ë ì¢íìëë¤.\r\n");
      out.write("\t\tendX : \"126.997589\",\r\n");
      out.write("\t\tendY : \"37.570594\",\r\n");
      out.write("\t\t//ê²½ì ì§ì ì¢íìëë¤.\r\n");
      out.write("\t\tpassList : \"126.987319,37.565778_126.983072,37.573028\",\r\n");
      out.write("\t\t//ì¶ë°ì§, ê²½ì ì§, ëª©ì ì§ ì¢íê³ ì íì ì§ì í©ëë¤.\r\n");
      out.write("\t\treqCoordType : \"WGS84GEO\",\r\n");
      out.write("\t\tresCoordType : \"EPSG3857\",\r\n");
      out.write("\t\t//ê°ëìëë¤.\r\n");
      out.write("\t\tangle : \"172\",\r\n");
      out.write("\t\t//ì¶ë°ì§ ëªì¹­ìëë¤.\r\n");
      out.write("\t\tstartName : \"ì¶ë°ì§\",\r\n");
      out.write("\t\t//ëª©ì ì§ ëªì¹­ìëë¤.\r\n");
      out.write("\t\tendName : \"ëì°©ì§\"\r\n");
      out.write("\t},\r\n");
      out.write("\t//ë°ì´í° ë¡ëê° ì±ê³µì ì¼ë¡ ìë£ëìì ë ë°ìíë í¨ììëë¤.\r\n");
      out.write("\tsuccess:function(response){\r\n");
      out.write("\t\tprtcl = response;\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t// ê²°ê³¼ ì¶ë ¥\r\n");
      out.write("\t\tvar innerHtml =\"\";\r\n");
      out.write("\t\tvar prtclString = new XMLSerializer().serializeToString(prtcl);//xml to String\t\r\n");
      out.write("\t    xmlDoc = $.parseXML( prtclString ),\r\n");
      out.write("\t    $xml = $( xmlDoc ),\r\n");
      out.write("    \t$intRate = $xml.find(\"Document\");\r\n");
      out.write("    \t\r\n");
      out.write("    \tvar tDistance = \"ì´ ê±°ë¦¬ : \"+($intRate[0].getElementsByTagName(\"tmap:totalDistance\")[0].childNodes[0].nodeValue/1000).toFixed(1)+\"km,\";\r\n");
      out.write("    \tvar tTime = \" ì´ ìê° : \"+($intRate[0].getElementsByTagName(\"tmap:totalTime\")[0].childNodes[0].nodeValue/60).toFixed(0)+\"ë¶\";\t\r\n");
      out.write("\r\n");
      out.write("    \t$(\"#result\").text(tDistance+tTime);\r\n");
      out.write("\t\t\r\n");
      out.write("\t\tprtcl=new Tmap.Format.KML({extractStyles:true, extractAttributes:true}).read(prtcl);//ë°ì´í°(prtcl)ë¥¼ ì½ê³ , ë²¡í° ëí(feature) ëª©ë¡ì ë¦¬í´í©ëë¤.\r\n");
      out.write("\t\tvar routeLayer = new Tmap.Layer.Vector(\"route\");// ë°±í° ë ì´ì´ ìì±\r\n");
      out.write("\t\t//íì¤ ë°ì´í° í¬ë§·ì¸ KMLì Read/Write íë í´ëì¤ ìëë¤.\r\n");
      out.write("\t\t//ë²¡í° ëí(Feature)ì´ ì¶ê°ëê¸° ì§ì ì ì´ë²¤í¸ê° ë°ìí©ëë¤.\r\n");
      out.write("\t\trouteLayer.events.register(\"beforefeatureadded\", routeLayer, onBeforeFeatureAdded);\r\n");
      out.write("\t\t        function onBeforeFeatureAdded(e) {\r\n");
      out.write("\t\t\t        \tvar style = {};\r\n");
      out.write("\t\t\t        \tswitch (e.feature.attributes.styleUrl) {\r\n");
      out.write("\t\t\t        \tcase \"#pointStyle\":\r\n");
      out.write("\t\t\t\t        \tstyle.externalGraphic = \"http://topopen.tmap.co.kr/imgs/point.png\"; //ë ëë§ í¬ì¸í¸ì ì¬ì©ë  ì¸ë¶ ì´ë¯¸ì§ íì¼ì urlìëë¤.\r\n");
      out.write("\t\t\t\t        \tstyle.graphicHeight = 16;//ì¸ë¶ ì´ë¯¸ì§ íì¼ì í¬ê¸° ì¤ì ì ìí í½ì ëì´ìëë¤.\r\n");
      out.write("\t\t\t\t        \tstyle.graphicOpacity = 1;//ì¸ë¶ ì´ë¯¸ì§ íì¼ì í¬ëªë (0-1)ìëë¤.\r\n");
      out.write("\t\t\t\t        \tstyle.graphicWidth = 16;//ì¸ë¶ ì´ë¯¸ì§ íì¼ì í¬ê¸° ì¤ì ì ìí í½ì í­ìëë¤.\r\n");
      out.write("\t\t\t        \tbreak;\r\n");
      out.write("\t\t\t        \tdefault:\r\n");
      out.write("\t\t\t\t        \tstyle.strokeColor = \"#ff0000\";//strokeì ì ì©ë  16ì§ì color\r\n");
      out.write("\t\t\t\t        \tstyle.strokeOpacity = \"1\";//strokeì í¬ëªë(0~1)\r\n");
      out.write("\t\t\t\t        \tstyle.strokeWidth = \"5\";//strokeì ëì´(pixel ë¨ì)\r\n");
      out.write("\t\t\t        \t};\r\n");
      out.write("\t\t        \te.feature.style = style;\r\n");
      out.write("\t\t        }\r\n");
      out.write("\t\t\r\n");
      out.write("\t\trouteLayer.addFeatures(prtcl);//ë ì´ì´ì ëíì ë±ë¡í©ëë¤.\r\n");
      out.write("\t\tmap.addLayer(routeLayer);//ë§µì ë ì´ì´ ì¶ê°\r\n");
      out.write("\t\t\r\n");
      out.write("\t\tmap.zoomToExtent(routeLayer.getDataExtent());//mapì zoomì routeLayerì ìì­ì ë§ê² ë³ê²½í©ëë¤.\r\n");
      out.write("\t},\r\n");
      out.write("\t//ìì²­ ì¤í¨ì ì½ìì°½ìì ìë¬ ë´ì©ì íì¸í  ì ììµëë¤.\r\n");
      out.write("\terror:function(request,status,error){\r\n");
      out.write("\t\tconsole.log(\"code:\"+request.status+\"\\n\"+\"message:\"+request.responseText+\"\\n\"+\"error:\"+error);\r\n");
      out.write("\t}\r\n");
      out.write("});\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("\t\t");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
