0. 가까운정류소 선택 / 

1. 현재위치 가까운 정류소 확인
-> 노선번호 확인

2. 목적위치 가까운 정류소 확인
-> 노선번호 확인

3. 동일 노선번호로 확인시
3.1. 현재 -> 목적 방향 확인
3.2 해당 노선 방향 정류소 list return

4. 다른 노선번호로 확인시
- 모든버스노선은 1회 환승시 서울역환승센터에 도착할 수 있다.
- 모든버스노선은 왕복루트가 동일하다.

- 버스노선별 환승가능노선 저장 
4.1 1회환승가능시
4.1.1 현재 노선 방향 / 환승정류소 id / 최종 노선 방향 return
4.1.2 노선방향의 정류소들로부터 가까운 정류소 선택

4.2 1회이상 환승필요시
- 일단 에러표시!


console.log(obj)

--------------------------------------------------------
1. 버스정류소로부터 버스노선 확인
input station_id / output bus_id

2. 버스노선 배열 저장
input xml output dict
input busRouteInfo/getStaionByRoute

bus = {bus_id<busRouteId> : {
	name : <busRouteNm>
	trans_num : <transYn>Y</transYn>인 <seq> / default = 999
	trans_id : <trnstnid>
	trans_name : <seq>1</seq> 인 <direction>
	end_num : last <seq>
	end_name : <seq>(trans_num+1)</seq>인 <direction>
	stations :{
		num : <seq>
		id : <station>
		mid : <stationNo>
		gpsx : <gpsX>
		gpsy : <gpsY>
		gotrans : bus.bus_id.trans_num == 999 -> Y else N
	}
}

3. 버스 환승 정류소 저장
bus = {bus_id:{
	tran_stations:{
		nums = [일치num]
		ids = [일치id]
		t_b_id = [해당정류장id운행버스id]
	}
}

< 수정사항 >
1. nbc_core.js : 668~671 라인 빠른 길 찾기 버튼 디자인 수정완료, 줄 띄기 추가
2. index.html : 155 ~ 195 라인 버튼 디자인 CSS 추가
3. nbc_core.js : 157~159, 1430~1432 라인 버튼 디자인 수정완료
4. index.html : 198 ~ 238 라인 링크 디자인 CSS 추가
5. nbc_core.js : 900 라인 버튼 디자인 수정완료
6. nbc.core.js : 1226~1228 라인 디자인 수정완료
7. nbc.core.js : 1455~1457 라인 디자인 수정완료
8. nbc.core,js : 1255~1263 라인 줄간격 및 글자 크기, 색깔 수정완료
9. nbc.core.js : 1260~1264 라인 방면 출력 수정