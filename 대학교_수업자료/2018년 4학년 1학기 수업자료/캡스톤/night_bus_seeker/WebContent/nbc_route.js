var bus_list = [];

function getText(){
    // read text from URL location
    var request = new XMLHttpRequest();
	var url = '전체(정류소 번호순).txt';
	//var url = 'https://cdmasmart.asuscomm.com:3333/nbc_station_list_all.txt';
    request.open('GET', url, true);
    request.send(null);
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            var type = request.getResponseHeader('Content-Type');
            if (type.indexOf("text") !== 1) {
				var text = request.responseText;
				var text_list = text.split('\n');
				for(var i = 0; ;i++){
					var unit_list = text_list[i];
					//console.log(unit_list)
					var separate_list = unit_list.split('\t');
					bus_list.push(separate_list);
					if(separate_list[0] == ''){
						break;
					}
				}
            }
        }
    }
}

function serch_best_location(start_g_x, start_g_y){
	console.log('함수 돌입')
	var close_in = 1.0;
	var array_cnt = 0;
	var close_position = new Array();
	var close_busstop = 0;
	var close_km = 0.0;
	var close_x = 0;
	var close_y = 0;       

	for(var i = 0; ;i++){
		if(bus_list[i][0] == '') break;
		//if(i == 10) break;
		//console.log(start_g_x+' - '+bus_list[i][1]);
		//console.log(start_g_y+' - '+bus_list[i][2]);
		var distance_long = Math.pow(Math.abs(start_g_x - bus_list[i][1]),2) + Math.pow(Math.abs(start_g_y - bus_list[i][2]),2);
		console.log('직선거리:'+Math.sqrt(distance_long).toFixed(4)*100.0+'km');

		if(Math.sqrt(distance_long).toFixed(4)*100.0 < close_in){
			close_km = Math.sqrt(distance_long).toFixed(4)*100.0+'km';
			close_busstop = bus_list[i][0];
			close_x = bus_list[i][1];
			close_y = bus_list[i][2];
			var close_set = [close_km, close_busstop, close_x, close_y];
			close_position[array_cnt] = close_set;
			array_cnt++;
		}
	}

	//직선거리가 제일 작은 순으로 정렬
	close_position.sort();

	//var inner_min = document.getElementById('min_bus');
	//var station_no = document.getElementById('station_no');
	//var station_gps_x = document.getElementById('station_gps_x');
	//var station_gps_y = document.getElementById('station_gps_y');
	//var route_l = document.getElementById('route_l');
	//var route_m = document.getElementById('route_m');
	
	
	if(close_position.length == 0){
		console.log('반경 1km 이내에 N버스 정류소가 없습니다.');
		//inner_min.innerText = '반경 1km 이내에 N버스 정류소가 없습니다.';
		alert('반경 1km 이내에 N버스 정류소가 없습니다.');
		} else {

		//초당 처리 건수 초과로 인해 에러발생이 발생하므로 주석처리
		var total_result = new Array();
		for(var x=0; x < 2; x++){
			//여기서 부터 resultTmap 코드 끌어옴
			//거리 시간 계산 => 시작지점 : start_x, start_y 도착지점 : end_x, end_y
			var total_result
			var headers = {}; 
			headers["appKey"]="ba9f84ee-24a8-4b25-bde3-c13c480bc28c";//실행을 위한 키 입니다. 발급받으신 AppKey를 입력하세요.
			$.ajax({
				method:"POST",
				//headers : headers,
				url:"https://api2.sktelecom.com/tmap/routes/pedestrian?version=1&format=xml&appKey=ba9f84ee-24a8-4b25-bde3-c13c480bc28c",//보행자 경로안내 api 요청 url입니다.
				async:false,
				data:{
					//출발지 위경도 좌표입니다.
					startX : start_g_x,
					startY : start_g_y,
					//목적지 위경도 좌표입니다.
					endX : Number(close_position[x][2]),
					endY : Number(close_position[x][3]),
					//경유지의 좌표입니다.(사용 안함)
					//passList : "126.987319,37.565778_126.983072,37.573028",
					//출발지, 경유지, 목적지 좌표계 유형을 지정합니다.
					reqCoordType : "WGS84GEO",
					resCoordType : "EPSG3857",
					//각도입니다.
					angle : "172",
					//출발지 명칭입니다.
					startName : "출발지",
					//목적지 명칭입니다.
					endName : "도착지"
				},
				//데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
				success:function(response){
					prtcl = response;
							
					// 결과 출력
					var innerHtml ="";
					var prtclString = new XMLSerializer().serializeToString(prtcl);//xml to String	
					xmlDoc = $.parseXML( prtclString ),
					$xml = $( xmlDoc ),
					$intRate = $xml.find("Document");
							
					var tDistance = "총 거리 : "+($intRate[0].getElementsByTagName("tmap:totalDistance")[0].childNodes[0].nodeValue/1000).toFixed(1)+"km,";
					var tTime = " 총 시간 : "+($intRate[0].getElementsByTagName("tmap:totalTime")[0].childNodes[0].nodeValue/60).toFixed(0)+"분";
					total_result[x] = ($intRate[0].getElementsByTagName("tmap:totalDistance")[0].childNodes[0].nodeValue/1000).toFixed(1)+' '+($intRate[0].getElementsByTagName("tmap:totalTime")[0].childNodes[0].nodeValue/60).toFixed(0);
	    			console.log(total_result);    					   			    			   			    	
				},
				//요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
				error:function(request,status,error){
					console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				}
			});    			
			//여기까지 resultTmap
		}
		
		var split_result = [];
		split_result[0] = total_result[0].split(' ');
		split_result[1] = total_result[1].split(' ');
		console.log('총 거리, 시간:'+split_result);	   
		   		
		var min_busstop = 0;	    		
		if(split_result[0][0] < split_result[1][0] || split_result[0][1] < split_result[1][1]){
			console.log(split_result[0]);
			//console.log(split_result[0][1]);
			min_busstop = 0;
		} else{
			console.log(split_result[1]);
			//console.log(split_result[1][1]);
			min_busstop = 1;
		}

		console.log(close_position[min_busstop]);
		//inner_min.innerText = '[ 제일 가까운 정류장 지점 ]\n거리:'+close_position[min_busstop][0]+' 정류소 번호:'+close_position[min_busstop][1]+' x축,y축:'+close_position[min_busstop][2]+', '+close_position[min_busstop][3];
		
		//station_no.innerText = close_position[min_busstop][1];
		//station_gps_x.innerText = close_position[min_busstop][2];
		//station_gps_y.innerText = close_position[min_busstop][3];
		//route_l.innerText = split_result[min_busstop][0];
		//route_m.innerText = split_result[min_busstop][1];
		
		station_info = '<div style="padding:5px;">' + close_position[min_busstop][1] + ' 정류장';
		station_info += '</br>' + split_result[min_busstop][0] + ' km ( ' + split_result[min_busstop][1] + '분 )</div>';
		station_pos = new daum.maps.LatLng(close_position[min_busstop][2], close_position[min_busstop][3]);
		
		displayStation(station_pos, station_info);
	}
}

var station_marker = null, station_infowindow = null;

// 지도에 마커와 인포윈도우를 표시하는 함수입니다
function displayStation(locPosition, message) {
	
	// 기존에 마커가 있는 경우, 마커를 제거합니다.
	if (station_marker != null) {
		station_marker.setMap(null);
		station_infowindow.open(null, null);
	}
	
	// 마커를 생성합니다
	station_marker = new daum.maps.Marker({  
		position: locPosition,
		map: map
	}); 
	
	var iwContent = message, // 인포윈도우에 표시할 내용
		iwRemoveable = true;

	// 인포윈도우를 생성합니다
	station_infowindow = new daum.maps.InfoWindow({
		content : iwContent,
		removable : iwRemoveable
	});
	
	// 인포윈도우를 마커위에 표시합니다 
	station_infowindow.open(map, station_marker);
	
	// 지도 중심좌표를 접속위치로 변경합니다
	map.setCenter(locPosition);      
}    