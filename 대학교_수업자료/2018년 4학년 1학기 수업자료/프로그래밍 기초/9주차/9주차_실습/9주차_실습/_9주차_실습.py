import codecs

class Lecture:
    def __init__(self):
        self.code = ''
        self.classname = ''
        self.grade = ''
        
    def read(self,line):
        self.line = line.split()
        self.code = self.line[0]
        self.classname = self.line[1]
        self.grade = self.line[2]

    def __str__(self):
        return '{} {} - {}'.format(self.code,self.classname,self.grade)

file = codecs.open("lecture_list.txt",'r',encoding='utf-8')
line_list = []

for read_list in file.readlines():
    rd = Lecture()
    rd.read(read_list)
    line_list.append(rd)

for ou in line_list:
    print(ou)