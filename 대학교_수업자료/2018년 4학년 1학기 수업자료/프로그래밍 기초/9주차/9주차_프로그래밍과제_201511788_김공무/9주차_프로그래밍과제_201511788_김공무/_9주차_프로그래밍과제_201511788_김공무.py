# 9주차 강의목록 파일 프로그램 과제 201511788 김공무
import codecs

class Lecture:
    def __init__(self): # 초기화
        self.code = ''
        self.classname = ''
        self.grade = ''
        self.credit = ''
        self.professor = ''
        self.date = ''
        self.place = ''
        self.maxstudents = ''
        self.classtype = ''
        
    def read(self,line): # 파일읽기
        self.line = line.split()
        self.code = self.line[0]
        self.classname = self.line[1]
        self.grade = self.line[2]
        self.credit = self.line[3]
        self.professor = self.line[4]
        self.date = self.line[5]
        self.place = self.line[6]
        self.maxstudents = self.line[7]
        self.classtype = self.line[8]

    def serch(self,findclass): # 리스트안에 해당 키워드가 있는지 검색
        for srhLine in self.line:
            isThere = srhLine.find(findclass)
            if isThere >= 0:
                return True
        return False

    def result(self,setting): # 검색 결과 출력(과목명이 같으면 다음 출력에 과목명은 생략)
        if setting == 0:
            return '{0}\n    {1} {2}학년 {3}학점 [{4}] {5}/{6} ({7}명) {8}'.format(self.classname,self.code,self.grade,self.credit,self.professor,self.date,self.place,self.maxstudents,self.classtype)
        else:
            return '    {0} {1}학년 {2}학점 [{3}] {4}/{5} ({6}명) {7}'.format(self.code,self.grade,self.credit,self.professor,self.date,self.place,self.maxstudents,self.classtype)

    def __str__(self): # 읽어들인 리스트 출력
        return '{0}\n    {1} {2}학년 {3}학점 [{4}] {5}/{6} ({7}명) {8}'.format(self.classname,self.code,self.grade,self.credit,self.professor,self.date,self.place,self.maxstudents,self.classtype)

file = codecs.open("lecture_list.txt",'r',encoding='utf-8') # 파일열기

# 선언문
line_list = []
blink_cnt = 0
issame_classname = ''

#파일 정보를 리스트에 읽어들임
for read_list in file.readlines():
    list_split = read_list.split()
    if blink_cnt == 0:
        blink_cnt += 1
        continue
    rd = Lecture()
    rd.read(read_list) #한 줄 정보를 쪼개서 리스트로 저장
    line_list.append(rd) #한 줄 리스트 정보를 가진 클래스를 리스트에 저장

# 리스트 내용 전체출력
for prtList in line_list:
    print(str(prtList))

# 키워드 검색
while True:
    findclassname = input('=> ')
    for srhList in line_list:
        isExist = srhList.serch(findclassname)
        if isExist:
            if issame_classname == srhList.classname:
                print(srhList.result(1)) # 과목명이 같으면 과목명을 생략하고 출력
            else:
                print(srhList.result(0)) # 전체 출력
            issame_classname = srhList.classname # 과목명 비교를 위해 이전 과목명을 저장
            
