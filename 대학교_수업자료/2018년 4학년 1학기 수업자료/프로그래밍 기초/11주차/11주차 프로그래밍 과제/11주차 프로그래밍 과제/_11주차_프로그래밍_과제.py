import sys
import random
# 다른 컴퓨터에서 압축을 풀고 실행 시 경로 설정을 압축을 푼 곳으로 수정 후 실행해야 할 것
sys.path.append('C:\\Users\\GongmuKim\\Desktop\\2018년 1학기 수업\\프로그래밍 기초\\11주차\\11주차 프로그래밍 과제\\11주차 프로그래밍 과제\\homework_ccy')

from homework_ccy import Ccy
from datetime import datetime #계좌 생성시 날짜 미입력시 현재 날짜로 입력이 들어갈 수 있게 datetime 함수 호출

class Account():
    now_dateinfo = datetime.now()
    trade_log = []

    def __init__(self, account_name, create_date = '%s-%s-%s'%(now_dateinfo.year, now_dateinfo.month, now_dateinfo.day), set_money = 0):
        self.name = account_name
        self.date = create_date
        account_money = str(set_money).split(' ')
        self.money = float(account_money[0])
        #account_money.insert(0,[account_money[0],account_money[1]])
        account_money.append(self.money)
        account_money.append(1)
        Account.trade_log.append(account_money)

    def __str__(self):
        result_str = ''
        list_cnt = 1

        result_str += '총 %s건, 전체 거래내역\n'%(len(Account.trade_log))
        for log in Account.trade_log:
            if list_cnt == 1:
                list_cnt += 1
                continue
            if all(log):
                result_str += '[{}]'.format(list_cnt) + '{0:>10} {1}'.format(log[0],log[1]) + ' '*(len(log[0])+len(log[1])+3) + '\t{0:>17}원 \n'.format(log[2])
            else:
                result_str += '[{}]'.format(list_cnt) + ' '*(len(log[0])+len(log[1])+1) + '{0:>14} {1}'.format('- '+log[0],log[1]) + '\t{0}원 \n'.format(log[2])
            if list_cnt > 2:
                break
            list_cnt += 1
        return result_str

    def show_detail(self):
        result_str = ''
        cnt_log = ''
        list_cnt = 1

        for cnt in range(len(Account.trade_log)):
            cnt_log += '{}.. '.format(cnt+1)
        cnt_log += '\n'
        print(cnt_log)

        result_str += '총 %s건, 전체 거래내역\n'%(len(Account.trade_log))      
        for log in Account.trade_log:
            if all(log):
                result_str += '[{}]'.format(list_cnt) + '{0:>10} {1}'.format(log[0],log[1]) + ' '*(len(log[0])+len(log[1])+3) + '\t{0:>17}원 \n'.format(log[2])
            else:
                result_str += '[{}]'.format(list_cnt) + ' '*(len(log[0])+len(log[1])+1) + '{0:>14} {1}'.format('- '+log[0],log[1]) + '\t{0:>9}원 \n'.format(log[2])
            list_cnt += 1
        result_str += '예금주 {0} / 잔고 {1:.2f} KRW'.format(self.name,self.money)
        print(result_str)

    def __add__(self, add_money):
         if type(add_money) == Ccy or type(add_money) == int:
            if type(add_money) == int:
                add_money = Ccy(float(add_money), 'KRW')
            input_addmoney = str(add_money).split(' ')
            Ccy.changeTo(add_money,'KRW')
            convert_money = str(add_money).split(' ')
            self.money += float(convert_money[0])
            input_addmoney.append(self.money)
            input_addmoney.append(1)
            #Account.trade_log.append(convert_money)
            Account.trade_log.append(input_addmoney)
            return self
         else:
            raise TypeError("unsupported operand type(s) for *: 'Ccy' and " + type(add_money).__name__)
    
    def __iadd__(self, add_money):
        
        if type(add_money) == Ccy or type(add_money) == int:
            if type(add_money) == int:
                add_money = Ccy(float(add_money), 'KRW')
            input_addmoney = str(add_money).split(' ')
            Ccy.changeTo(add_money,'KRW')
            convert_money = str(add_money).split(' ')
            self.money += float(convert_money[0])
            input_addmoney.append(self.money)
            input_addmoney.append(1)
            #Account.trade_log.append(convert_money)
            Account.trade_log.append(input_addmoney)
        else:
            raise TypeError("unsupported operand type(s) for *: 'Ccy' and " + type(add_money).__name__)
        
        return self

    def __radd__(self, add_money):
        return Account.__add__(self, add_money)

    def __sub__(self, sub_money):
        if type(sub_money) == Ccy or type(sub_money) == int:
            if type(sub_money) == int:
                sub_money = Ccy(float(sub_money), 'KRW')
            input_submoney = str(sub_money).split(' ')
            Ccy.changeTo(sub_money,'KRW')
            convert_money = str(sub_money).split(' ')
            self.money -= float(convert_money[0])           
            input_submoney.append(self.money)
            input_submoney.append(0)
            #Account.trade_log.append(convert_money)
            Account.trade_log.append(input_submoney)
        else:
             raise TypeError("unsupported operand type(s) for *: 'Ccy' and " + type(sub_money).__name__)
    
    
    def __isub__(self, sub_money):
        
        if type(sub_money) == Ccy or type(sub_money) == int:
            if type(sub_money) == int:
                sub_money = Ccy(float(sub_money), 'KRW')
            input_submoney = str(sub_money).split(' ')
            Ccy.changeTo(sub_money,'KRW')
            convert_money = str(sub_money).split(' ')
            self.money -= float(convert_money[0])
            input_submoney.append(self.money)
            input_submoney.append(0)
            #Account.trade_log.append(convert_money)
            Account.trade_log.append(input_submoney)
        else:
             raise TypeError("unsupported operand type(s) for *: 'Ccy' and " + type(sub_money).__name__)
        
        return self

    def __rsub__(self, sub_ccy):
        return Account.__sub__(self, sub_ccy)

if __name__ == '__main__':
    print('[ 201511788_김공무_11주차 프로그래밍 과제 ]')
    a = Account('lee', '2018-05-10', Ccy(10000.0, 'KRW'))
    a += Ccy(100.00, 'USD')
    a -= 2000
    a += Ccy(500, 'JPY')
    Ccy.add_currency('HKD', 138.86)
    a -= Ccy(1000, 'HKD')
    #print(str(a))
    a.show_detail()