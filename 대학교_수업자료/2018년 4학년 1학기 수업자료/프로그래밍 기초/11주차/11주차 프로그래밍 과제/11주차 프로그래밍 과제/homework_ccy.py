"""

The class "Ccy" can be used to define money values in various currencies. A Ccy instance has the string attributes 'unit' (e.g. 'CHF', 'CAD' od 'EUR' and the 'value' as a float. 
A currency object consists of a value and the corresponding unit.



"""
import sys
# 다른 컴퓨터에서 압축을 풀고 실행 시 경로 설정을 압축을 푼 곳으로 수정 후 실행해야 할 것
sys.path.append('C:\\Users\\GongmuKim\\Desktop\\2018년 1학기 수업\\프로그래밍 기초\\11주차\\11주차 프로그래밍 과제\\11주차 프로그래밍 과제\\exchange_rates')
from exchange_rates import get_currency_rates

class Ccy:
    
    # 실시간으로 통화 정보를 받아옴
    #currencies = get_currency_rates()

    # 정적으로 통화 정보를 받아옴
    currencies =  {'KRW':1, 
                   'USD':1081.00,
 		           'JPY':984.97,
                   'EUR':1281.96, 
 		           'CNY':169.9}
    
    
    def __init__(self, value, unit="EUR"):
        self.value = value
        self.unit = unit

    def __str__(self):
        return "{0:5.2f}".format(self.value) + " " + self.unit

    def __repr__(self):
        return 'Ccy(' + str(self.value) + ', "' + self.unit + '")'

    def changeTo(self, new_unit):
        """
        An Ccy object is transformed from the unit "self.unit" to "new_unit"
        """
        self.value = (self.value * Ccy.currencies[self.unit] / Ccy.currencies[new_unit])
        self.unit = new_unit
        
    def __add__(self, other):
        """
        Defines the '+' operator.
        If other is a CCy object the currency values 
        are added and the result will be the unit of 
        self. If other is an int or a float, other will
        be treated as a Euro value. 
        """
        if type(other) == int or type(other) == float:
            x = (other * Ccy.currencies[self.unit])
        else:
            x = (other.value / Ccy.currencies[other.unit] * Ccy.currencies[self.unit]) 
        return Ccy(x + self.value, self.unit)


    def __iadd__(self, other):
        """
        Similar to __add__
        """
        if type(other) == int or type(other) == float:
            x = (other * Ccy.currencies[self.unit])
        else:
            x = (other.value / Ccy.currencies[other.unit] * Ccy.currencies[self.unit])
        self.value += x
        return self

    def __radd__(self, other):
        res = self + other
        if self.unit != "EUR":
            res.changeTo("EUR")
        return res
    
    # __sub__, __isub__ and __rsub__ can be defined analogue
    

    def __mul__(self, other):
        """
        Multiplication is only defined as a scalar multiplication, 
        i.e. a money value can be multiplied by an int or a float.
        It is not possible to multiply to money values
        """
        if type(other)==int or type(other)==float:
            return Ccy(self.value * other, self.unit)
        else:
            raise TypeError("unsupported operand type(s) for *: 'Ccy' and " + type(other).__name__)  
        
    def __rmul__(self, other):
        return self.__mul__(other)
    
    def __imul__(self, other):
        if type(other)==int or type(other)==float:
            self.value *= other
            return self
        else:
            raise TypeError("unsupported operand type(s) for *: 'Ccy' and " + type(other).__name__)
    
    #통화 정보 추가    
    @staticmethod
    def add_currency(a, b):
        Ccy.currencies[a] = b
        