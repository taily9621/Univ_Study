﻿class Length:

    __metric = {"mm" : 0.001, "cm" : 0.01, "m" : 1, "km" : 1000,
                "in" : 0.0254, "ft" : 0.3048, "yd" : 0.9144,
                "mi" : 1609.344 }
    
    def __init__(self, value, unit = "m" ):
        self.value = value
        self.__unit = unit

    def Converse2Metres(self):
        return self.value * Length.__metric[self.unit]

    @staticmethod
    def add(a, b):
        Length.__metric[a] = b
    
    def __str__(self):
        return str(self.value)
    
    def __repr__(self):
        return "Length(" + str(self.value) + ", '" + self.unit + "')"

    def __iadd__(self, other):
        l = self.Converse2Metres() + other.Converse2Metres()
        self.value = l / Length.__metric[self.unit]
        return self

    def __add__(self, other):
        if type(other) == int or type(other) == float:
            l = self.Converse2Metres() + other
        else:
            l = self.Converse2Metres() + other.Converse2Metres()
        return Length(l / Length.__metric[self.unit], self.unit )

    def __iadd__(self, other):
        if type(other) == int or type(other) == float:
            l = self.Converse2Metres() + other
        else:
            l = self.Converse2Metres() + other.Converse2Metres()
        self.value = l / Length.__metric[self.unit]
        return self

    def __radd__(self, other):
        return Length.__add__(self,other)

    def __mul__(self, other):
        if type(other) == int or type(other) == float:
           return Length(self.value * other, self.unit)
        else:
           raise TypeError("unsupportedoperand type(s) for *: 'Ccy' and " + type(other).__name__)

    def __imul__(self, other):
        if type(other) == int or type(other) == float:
           self.value = (self.value * other)
        else:
           raise TypeError("unsupportedoperand type(s) for *: 'Ccy' and " + type(other).__name__)
    
    @property
    def unit(self):
        return self.__unit

    @unit.setter
    def unit(self, un):
        if Length.__metric[un] == None:
            print('Error'+un+'is not support')
        else:
            self.value = self.Converse2Metres() / Length.__metric[un]
            self.__unit = un

    
if __name__ == "__main__":
    x = Length(4)
    print(x)
    y = eval(repr(x))

    z = Length(4.5, "yd") + Length(1)
    print(repr(z))
    print(z)