import string
import random

#            012345678 10
grade_score='F   D C B A'

def bubble_sort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if not compare_students(alist[i],alist[i+1]):
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp
                
def compare_students(s1, s2):#false면 a1보다 a2가 더 작은것
    a1 = s1[3]
    a2 = s2[3]
    if (a1 > a2):#평균으로정렬
        return True
    if (a1 == a2):#평균이같으면
        if (s1[4] > s2[4]):#전체합계
            return True
    return False   



my_dic = []

student_cnt = int(input("학생수 =>"))
def my_grade(alist):
    myList=[]
    for n, score in enumerate(alist):
        if score >= 90:
            myList.append('A')
        elif score >= 80:
            myList.append('B')
        elif score >= 70:
            myList.append('C')
        elif score >= 60:
            myList.append('D')
        else:
            myList.append('F')
    return myList

def isF(alist):
    return [n for n, x in enumerate(alist) if x <60]


for name in  random.sample( string.ascii_lowercase, student_cnt):
    my_score = [random.randrange(50, 101) for _ in range(3)]#국영수 3개같은것
    s = [name,my_score,my_grade(my_score)]

    avg = (grade_score.index(s[2][0])+grade_score.index(s[2][1])+grade_score.index(s[2][2]))/3
    s.append(avg)
    s.append(sum(my_score))
    
    my_dic.append(s)

bubble_sort(my_dic)

subject = ['국어','영어','수학']
forF=[]
for n, student in enumerate(my_dic):
    print('[{0}] {1} {2} {3} gpa:{4:5.3} total{5:3d}'.format(n+1, *student) )
    if 'F' in student[2]:
        index = student[2].index('F')
        forF.append([student, isF(student[1])  ])


print()

for x in forF:
    print(x[0][0],' ',end='')
    for k in x[1]:
        print('{0}({1})'.format(subject[k], x[0][1][k] ),end='')
    print()

    
