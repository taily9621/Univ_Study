import string
import random


fcards=['AKQJ98765432','CSHD']
def sort_by(alist, k):#0~3 만 정렬   k가 0이면 수로 1이면 모양으로
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if fcards[k].index(alist[i][k]) > fcards[k].index(alist[i+1][k]):
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp
    return alist

def print_list(alist, my_sep):
    for x in alist:
        print(x,my_sep,end='',sep='')


def myCnt(alist, k):
    mmm = [alist.count(x) for x in fcards[k]]
    if mmm.count(2) == 2:
        return -1
    return max(mmm)
    
def my_release_str(alist):
    my_str=''
    for s in alist:
        my_str += s
    return my_str

def isGood(su, shape):



    if my_release_str(su) == 'AKQJ' and myCnt(shape, 1) == 4:
        print('로티플', end='')
    elif myCnt(shape, 1) == 4 and myCnt(su, 0) == 4:
        print('스티플', end='')
    elif myCnt(su, 0) == 4:
        print('포카드', end='')
    elif myCnt(shape, 1) == 4:
        print('flush', end='')
    elif my_release_str(su) in fcards[0]:
        print('스트레이트', end='')
    elif myCnt(su, 0) == 3:
        print('three card', end='')
    elif myCnt(su, 0) == -1:
        print('two pair', end='')
    elif myCnt(su,0) == 2:
        print('one pair', end='')



while True:
    my_card=[x+y for x in fcards[0] for y in fcards[1]]
    random.shuffle(my_card)


    player=[my_card[i*4:(i+1)*4] for i in range(12)]

    sorted_su=[sort_by(x.copy(),0) for x in player]
    sorted_shape=[sort_by(x.copy(),1) for x in player]

    for i in range(12):
        print_list(sorted_su[i],' ')
        print('/ ',end='')
        print_list(sorted_shape[i],' ')

        by_su=[x[0] for x in sorted_su[i]]
        by_shape=[x[1] for x in sorted_shape[i]]

        print('   ', end='')
        print_list(by_su,'')
        print('  ',end='')
        print_list(by_shape,'')
        print(' --- ',end='')
        isGood(by_su,by_shape)


        print()
    input()




