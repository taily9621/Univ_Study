import string
import random

class Student:
    def __init__(self, name=None,grade=[]):
        self.name = name
        self.grade = grade
    def compute(self):
        self.score = [random.randint(50,100) for _ in range(3)]
        self.avg = sum(self.score)/3.0
    def print(self):
        if not self.score :
            print('{0} : {1} 점수가 없습니다.'.format(self.name,self.score,self.avg))
        else:
            print('{0} : {1} 평균:{2:0.2f}'.format(self.name,self.score,self.avg))
    
stu_list = [Student(x) for x in 'abc']

for x in stu_list:
    x.compute()
    x.print()
