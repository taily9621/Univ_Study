import string
import random

class cardFunc:
    def __init__(player_list, cards = []):
        player_list.cards = cards

    def sort_card(player_list):
        compare_cardnum = [c for c in 'AKQJ98765432']
        compare_cardshape = [p for p in 'CSHD']
        num_sort = []
        shape_sort = []
        num_result = []
        shape_result = []

        for ply in player_list.cards:
            for x in range(len(ply)-1):
                if compare_cardnum.index(ply[x][0]) > compare_cardnum.index(ply[x+1][0]):
                    num_sort.append(ply[x])
                else:
                    num_sort.append(ply[x+1])
                if compare_cardshape.index(ply[x][1]) > compare_cardshape.index(ply[x+1][1]):
                    shape_sort.append(ply[x])
                else:
                    shape_sort.append(ply[x+1])
            num_result.append(num_sort)
            shape_result.append(shape_sort)

        return num_result,shape_result
    
    

cardnum_list = [c for c in 'AKQJ98765432']
cardshape_list = [p for p in 'CSHD']
set_cards = [num+shape for num in cardnum_list for shape in cardshape_list]
random.shuffle(set_cards)
player_cards= [set_cards[i*4:(i+1)*4] for i in range(12)]

for list in player_cards:
    print(' '.join(list))
    
result_player = cardFunc(player_cards)
sorting_list = result_player.sort_card()

