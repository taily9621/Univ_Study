import random
import string

def bubble_sort(alist): # 버블정렬
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if not compare_students(alist[i],alist[i+1]):
                # False는 뒷 사람 평균이 더 클 경우이므로 위치를 바꿈
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp

def compare_students(s1, s2):
    a1 = s1[5]
    a2 = s2[5]
    if (a1 > a2): # 앞 사람 평점이 더 클 경우
        return True
    if (a1 == a2): # 평점이 같을 경우
        return sum(s1[1:4]) > sum(s2[1:4])

    return False # 뒷 사람 평점이 더 클 경우    

def grade_check(std):
    summery = []
    rank = []
    gpa = []
    for g in std:
        if g >= 90:
            rank.append('A')
            gpa.append(10)
        elif g >= 80:
            rank.append('B')
            gpa.append(8)
        elif g >= 70:
            rank.append('C')
            gpa.append(6)
        elif g >= 60:
            rank.append('D')
            gpa.append(4)
        else:
            rank.append('F')
            gpa.append(0)

    summery.append(rank)
    gpa_avg = sum(gpa) / 3
    summery.append(gpa_avg)
    return summery

grade = ['A','B','C','D','F']
student_list = []
total_grade = 0
result = []
rank_cnt = 1

for cnt in range(20):
    student_info = []
    student_info.append(random.choice(string.ascii_lowercase))
    student_info.extend([random.randint(50,100) for _ in range(3)])
    result = grade_check(student_info[1:])
    student_info.extend(result)
    student_list.append(student_info)

bubble_sort(student_list)

for std in student_list:
    print('[{0}] {1} {2} {3} gpa: {4:0.2f} total:{5}'.format(rank_cnt,std[0],std[1:4],std[4],std[5],sum(std[1:4])))
    rank_cnt += 1

