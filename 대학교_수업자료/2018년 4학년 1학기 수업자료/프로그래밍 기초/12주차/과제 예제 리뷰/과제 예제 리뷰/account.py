﻿from ccy import Ccy
from transaction import Transaction


class Account:
    def __init__(self, name, day, ccy=Ccy(10000, 'KRW')):
        self.name = name
        self.day = day
        self.balance = Ccy(ccy.value, ccy.unit)
        self.transactions = [Transaction(ccy, True, self.balance.value)]

    def show_detail(self):
        for i, t in enumerate(self.transactions, 1):
            print("[{}] {}".format(i, t))
        print("예금주 {} / 잔고 {}".format(self.name, self.balance))

    def __iadd__(self, other):
        if type(other) == int:
            other = Ccy(other)
        self.balance += other
        self.transactions.append(Transaction(other, True, self.balance.value))
        return self

    def __isub__(self, other):
        if type(other) == int:
            other = Ccy(other)
        self.balance -= other
        self.transactions.append(Transaction(other, False, self.balance.value))
        return self

            
if __name__ == '__main__':
    a = Account('lee', '2018-05-10', Ccy(10000, 'KRW'))
    a += Ccy(100, 'USD')
    a -= 2000
    a += Ccy(3500, 'JPY')
    a += Ccy(1500, 'JPY')
    a += 2500000
    a -= Ccy(800, 'USD')
    Ccy.add_currency('HKD', 138.86)
    a -= Ccy(1000, 'HKD')
    a.show_detail()
