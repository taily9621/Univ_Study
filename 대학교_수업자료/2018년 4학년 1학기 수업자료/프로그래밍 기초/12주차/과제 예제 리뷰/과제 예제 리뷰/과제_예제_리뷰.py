from ccy import Ccy
#from transaction import Transaction
import datetime
import time

class Account:
    def logfunc():
        def add_log(self, y):
            if type(y) == Ccy:
                y.change_to('KRW')
                total  = y.value
            else:
                total = 0

            now = datetime.datetime.now()
            if self.lasttime.date() < now.date():
                self.total = 0

            total += self.total
            if total >= 1000000:
                print('******* 하루 출금 한도 초과: 1000000.00_KRW', total)
            time_interval = now - self.lasttime
            if time_interval.total_seconds() <= 1:


    def transaction_func(func):
        def function_wrapper(self, other):
            it_return = func(self, other)
            self.transactions.append([other, self.isIn, self.balance.value])
            return it_return
        return function_wrapper

    def __init__(self, name, day, ccy=Ccy(10000, 'KRW')):
        self.name = name
        self.day = day
        self.balance = Ccy(ccy.value, ccy.unit)
        #self.transactions = [Transaction(ccy, True, self.balance.value)]
        self.transactions = []
        self.transactions.append([ccy, True, self.balance.value])

    def show_detail(self):
        for i, t in enumerate(self.transactions, 1):
            if t[1]:
                put_str = "%12s      %13s %12d원" % (t[0], "", t[2])
            else:
                put_str = "%12s      -%12s %12d원" % ("", t[0], t[2])

            print("[{}] {}".format(i, put_str))
        print("예금주 {} / 잔고 {}".format(self.name, self.balance))
    
    @transaction_func
    def __iadd__(self, other):
        if type(other) == int:
            other = Ccy(other)
        self.balance += other
        self.isIn = True
        #self.transactions.append(transaction_func(other, True, self.balance.value))
        return self

    @transaction_func
    def __isub__(self, other):
        if type(other) == int:
            other = Ccy(other)
        self.balance -= other
        self.isIn = False
        #self.transactions.append(transaction_func(other, False, self.balance.value))
        return self

            
if __name__ == '__main__':
    a = Account('lee', '2018-05-10', Ccy(10000, 'KRW'))
    a += Ccy(100, 'USD')
    a -= 2000
    a += Ccy(3500, 'JPY')
    time.sleep(0.5)
    a += Ccy(1500, 'JPY')
    a += 2500000
    a -= Ccy(800, 'USD')
    time.sleep(1.1)
    Ccy.add_currency('HKD', 138.86)
    a -= Ccy(1000, 'HKD')
    a.show_detail()

