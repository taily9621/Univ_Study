class Ccy:

    currencies = {'KRW': 1, 'USD': 1081.00, 'JPY': 9.8497, 'EUR': 1281.96, 'CNY': 169.9}
    
    def __init__(self, value, unit="KRW"):
        self.value = value
        self.unit = unit

    @staticmethod
    def add_currency(u, v):
        Ccy.currencies[u] = v
        
    def __str__(self):
        return "{0:5.2f}".format(self.value) + "_" + self.unit

    def __repr__(self):
        return 'Ccy(' + str(self.value) + ', "' + self.unit + '")'

    def change_to(self, new_unit):
        self.value = (self.value / Ccy.currencies[self.unit] * Ccy.currencies[new_unit])
        self.unit = new_unit

    def __add__(self, other):
        if type(other) == int or type(other) == float:
            x = (other / Ccy.currencies[self.unit])
        else:
            x = (other.value * Ccy.currencies[other.unit] / Ccy.currencies[self.unit]) 
        return Ccy(x + self.value, self.unit)

    def __iadd__(self, other):
        if type(other) == int or type(other) == float:
            x = (other / Ccy.currencies[self.unit])
        else:
            x = (other.value * Ccy.currencies[other.unit] / Ccy.currencies[self.unit])
        self.value += x
        return self

    def __radd__(self, other):
        res = self + other
        return res

    def __sub__(self, other):
        if type(other) == int or type(other) == float:
            x = (other / Ccy.currencies[self.unit])
        else:
            x = (other.value * Ccy.currencies[other.unit] / Ccy.currencies[self.unit]) 
        return Ccy(self.value - x, self.unit)

    def __isub__(self, other):
        if type(other) == int or type(other) == float:
            x = (other / Ccy.currencies[self.unit])
        else:
            x = (other.value * Ccy.currencies[other.unit] / Ccy.currencies[self.unit]) 
        self.value -= x
        return self

    def __rsub__(self, other):
        res = self - other
        return res
