﻿class Transaction:

    def __init__(self, amount, b_deposit, balance):
        self.amount = amount
        self.b_deposit = b_deposit
        self.balance = balance

    def __str__(self):
        if self.b_deposit:
            return "%12s      %13s %12d원" % (self.amount, "", self.balance)
        else:
            return "%12s      -%12s %12d원" % ("", self.amount, self.balance)
