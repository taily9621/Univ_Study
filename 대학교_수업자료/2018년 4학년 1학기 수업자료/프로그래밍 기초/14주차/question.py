import random


class Question:
    word_dic = {}
    word_keys = []
    answer_dic = {}

    def __init__(self, word):
        self.word_obj = Question.word_dic[word]
        self.correct_ans = ''
        self.user_ans = ''

    def write(self):
        print('[뜻] :', self.word_obj.mean)

    def unscramble(self):
        w = self.word_obj.word
        self.correct_ans = w
        question = ''.join(random.sample(w, len(w)))
        print('[스크램블] : ', question)

    def choose(self):
        w = self.word_obj.word
        print('choose ', w)
        alts = random.sample(Question.word_keys, 3)
        alts.append(w)
        random.shuffle(alts)
        self.correct_ans = str(alts.index(w)+1)
        print("[사지선다] 뜻 : ", self.word_obj.mean)
        for i, x in enumerate(alts):
            print('({}) {}  '.format(i + 1, x))

    def run(self, menu):
        func_list = [self.write, self.unscramble, self.choose]
        # print('run ', self.word_obj)
        func_list[menu - 1]()
        self.check_answer()

    def check_answer(self):
        self.user_ans = input('=> ')
        if self.user_ans == str(self.correct_ans):
            print('맞았습니다.')
            Question.answer_dic[self.word_obj.word] = True
            return True
        else:
            print('정답은 {}입니다.'.format(self.correct_ans))
            Question.answer_dic[self.word_obj.word] = False
            return False


