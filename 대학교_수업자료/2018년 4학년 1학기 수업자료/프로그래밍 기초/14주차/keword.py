class Word:
    def __init__(self, idx='', word='', part='', mean='', eg=''):
        self.__idx = idx
        self.__word = word
        self.__part = part
        self.__mean = mean
        self.__eg = eg

    @property
    def idx(self):
        return self.__idx

    @idx.setter
    def idx(self, value):
        self.__idx = value

    @property
    def word(self):
        return self.__word

    @word.setter
    def word(self, value):
        self.__word = value

    @property
    def part(self):
        return self.__part

    @part.setter
    def part(self, value):
        self.__part = value

    @property
    def mean(self):
        return self.__mean

    @mean.setter
    def mean(self, value):
        self.__mean = value

    @property
    def eg(self):
        return self.__eg

    @eg.setter
    def eg(self, value):
        self.__eg = value

    def __str__(self):
        return self.word + ':' + self.mean
