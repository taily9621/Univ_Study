import codecs


class Menu:

    menu_dic = {}

    def __init__(self, id, name, value):
        self.id = id
        self.name = name
        self.value = int(value)


class Order:

    def __init__(self, user_id=''):
        self.user_id = user_id
        self.ordered = {}

    def get_orders(self, line):
        comp = line.split()
        self.user_id = comp[0]
        comp = comp[1:]
        self.ordered = dict(zip(comp[::2], [int(x) for x in comp[1::2]]))

    def __str__(self):
        str_format = ''
        str_format += '-'*19 + '+\n'
        str_format += '    고객명 : {:<6}|\n'.format(self.user_id)
        str_format += '-'*60 + '\n'
        str_format += '{}\t{:10}{:^10}{:10}\n'.format('  주문상품', '    주문개수', '상품가격', '총가격')
        total = 0
        for k in self.ordered:
            count = self.ordered[k]
            menu = Menu.menu_dic[k]
            subtotal = menu.value * count
            str_format += '  {}\t\t{:<8}{:8}{:8}\n'.format(menu.name, count, menu.value, subtotal)
            total += subtotal
        str_format += '-'*60 + '\n'
        str_format += ' '*36 + '총합 : {}\n\n'.format(total)
        return str_format


order_list = []


def read_file():

    f = codecs.open('items.txt', 'r', encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        m = Menu(*line.split())
        Menu.menu_dic[m.id] = m

    f = codecs.open('orders.txt', 'r', encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        order = Order()
        order.get_orders(line)
        order_list.append(order)


if __name__ == '__main__':
    read_file()
    for o in order_list:
        print(str(o))
