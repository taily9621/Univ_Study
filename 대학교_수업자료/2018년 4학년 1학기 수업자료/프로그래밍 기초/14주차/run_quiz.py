import random
import codecs
from quiz.question import Question
from quiz.keword import Word


def read_file():
    f = codecs.open('voca-330.txt', 'r', encoding='utf-8')
    f.readline()
    lines = f.readlines()
    for line_no in range(0, len(lines), 5):
        word = lines[line_no].split()[0]
        word_obj = Word(line_no // 5 + 1, word)
        word_obj.part = lines[line_no + 1].split()[0]
        word_obj.mean = lines[line_no + 2].split()[0]
        word_obj.eg = lines[line_no + 4].split()[0]
        Question.word_dic[word] = word_obj
    Question.word_keys = list(Question.word_dic.keys())


def show_result():
    correct = [x for x in Question.answer_dic if Question.answer_dic[x]]
    print("맞은 단어: ", correct)
    incorrect = [x for x in Question.answer_dic if not Question.answer_dic[x]]
    print("틀린 단어: ", incorrect)


if __name__ == '__main__':
    read_file()
    while True:
        print("(1) 단어입력  (2) 언스크램블   (3) 사지선다  (0) 종료 ")
        menu = int(input('=>'))
        if menu < 1 or menu > 3:
            break
        word = random.choice(Question.word_keys)
        q = Question(word)
        q.run(menu)

    show_result()
