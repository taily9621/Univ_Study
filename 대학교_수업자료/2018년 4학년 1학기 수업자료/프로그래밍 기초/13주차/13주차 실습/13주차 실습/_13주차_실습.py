"""
from random import random, randint, choice

def our_decorator(func):
    def function_wrapper(*args, **kwargs):
        print("Before calling " + func.__name__)
        res = func(*args, **kwargs)
        print(res)
        print("After calling " + func.__name__)
    return function_wrapper

random = our_decorator(random)
randint = our_decorator(randint)
choice = our_decorator(choice)

random()
randint(3, 8)
choice([4, 5, 6])


import random

log = []
def param_log(func):
    def function_wrapper(*args, **kwargs):
        unit = str(*args, **kwargs)
        res = func(*args, **kwargs)
        log_cod(unit, func.__name__)
        return res
    return function_wrapper

def log_cod(x,y):
    if type(x) == str:
        setting = '[LOG]%s :%s'%(y,x)
    elif type(x) == list:
        setting = '[LOG]%s :%s'%(y,x)
    log.append(setting)

len = param_log(len)
shuffle = param_log(random.shuffle)

mylist = [3, 56, 1234, 654, 6, 3432, 3]

for x in mylist:
    print(len(str(x)), end=' ')
print('\n')

shuffle(mylist)

for x in log:
    print(x)


other_cities = ["Strasbourg", "Freiburg", "Stuttgart", "Vienna / Wien","Hannover", "Berlin", "Zurich"]
city_iterator = iter(other_cities)
while city_iterator:
    try:
        city = next(city_iterator)
        print(city)
    except StopIteration:
        break
"""
def next_number():
    while True:
        line = input('=>')
        if not line:
            break
        set_line = line.split()
        print(set_line)
        for ln in set_line:
            if not ln.isdigit():
                continue
            elif 0 > int(line):
                line = 0
                print('입력값',ln)
            elif int(line) > 100:
                line = 100
                print('입력값',ln)
            elif len(line) > 1:
                for x in line:
                    yield x
                continue
            else:
                yield line
    return

if __name__ == '__main__':
    m = []
    for x in next_number():
        m.append(x)
        print('echo: {} 입력'.format(x))
    print(m)
"""
import random

def next_number(x,y):
    for num in range(5):
        yield random.randint(x,y)

myrandrange = next_number(10, 20)
for x in myrandrange:
    print('echo: {} 입력'.format(x))
print()

echo: 13 입력
echo: 18 입력
echo: 18 입력
echo: 17 입력
echo: 14 입력

"""