"""
def func_decorator(func):
    def function_wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return result
    return function_wrapper
class menu:
    def menu_result(order_dict):
        menu_list = []
        price = 0
        item_list = []
        menu_dict = {}

        items_file = open("C:\\Users\\GongmuKim\\Desktop\\2018년 1학기 수업\\프로그래밍 기초\\13주차\\12주차 과제\\12주차 과제\\items.txt")
    
        ord_dict_iterator = iter(order_dict)

        while True:
            line = items_file.readline()
            if not line: break
            split_line = line.split('\n')
            split_line_unit = split_line[0].split(' ')
            item_list.append(split_line_unit)

        while ord_dict_iterator:
            try:
                ord = next(ord_dict_iterator)
            except StopIteration:
                break
            for x in range(len(order_dict[ord])):
                for total_menu in item_list:
                    if order_dict[ord][x][0] == total_menu[0]:
                        menu_list.append(total_menu[1])
                        price += int(order_dict[ord][x][1]) * int(total_menu[2])
            menu_dict[ord] = [menu_list,price]
            menu_list = []
            price = 0

        items_file.close
        return menu_dict

class order:
    def set_order():
        order_dict = {}
        menu_file = open("C:\\Users\\GongmuKim\\Desktop\\2018년 1학기 수업\\프로그래밍 기초\\13주차\\12주차 과제\\12주차 과제\\orders.txt")
        while True:
            line = menu_file.readline()
            if not line: break
            split_line = line.split('0')
            split_sentence = split_line[0].split(' ')
            order_dict[split_sentence[0]] = [[split_sentence[x],split_sentence[x+1]] for x in range(1,len(split_sentence)-1,2)]
    
        menu_file.close
        print(order_dict)
        return order_dict
    


if __name__ == '__main__':
    set_orderlist = func_decorator(order)
    calculate_result = func_decorator(menu)

    result_list = set_orderlist()
    items_result = calculate_result(result_list)
    items_result_keys = items_result.keys()
    for keys in items_result_keys:
        print('{}님 리스트 : {} 총액 : {}'.format(keys,items_result[keys][0],items_result[keys][1]))
"""

import codecs


class Menu:

    menu_dic = {}

    def __init__(self, id, name, value):
        self.id = id
        self.name = name
        self.value = int(value)


class Order:

    def __init__(self, user_id=''):
        self.user_id = user_id
        self.ordered = {}

    def get_orders(self, line):
        comp = line.split()
        self.user_id = comp[0]
        comp = comp[1:]
        self.ordered = dict(zip(comp[::2], [int(x) for x in comp[1::2]]))

    def __str__(self):
        str_format = ''
        str_format += '-'*19 + '+\n'
        str_format += '    고객명 : {:<6}|\n'.format(self.user_id)
        str_format += '-'*60 + '\n'
        str_format += '{}\t{:10}{:^10}{:10}\n'.format('  주문상품', '    주문개수', '상품가격', '총가격')
        total = 0
        for k in self.ordered:
            count = self.ordered[k]
            menu = Menu.menu_dic[k]
            subtotal = menu.value * count
            str_format += '  {}\t\t{:<8}{:8}{:8}\n'.format(menu.name, count, menu.value, subtotal)
            total += subtotal
        str_format += '-'*60 + '\n'
        str_format += ' '*36 + '총합 : {}\n\n'.format(total)
        return str_format


order_list = []


def read_file():

    f = codecs.open('items.txt', 'r', encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        m = Menu(*line.split())
        Menu.menu_dic[m.id] = m

    f = codecs.open('orders.txt', 'r', encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        order = Order()
        order.get_orders(line)
        order_list.append(order)


if __name__ == '__main__':
    read_file()
    for o in order_list:
        print(str(o))


