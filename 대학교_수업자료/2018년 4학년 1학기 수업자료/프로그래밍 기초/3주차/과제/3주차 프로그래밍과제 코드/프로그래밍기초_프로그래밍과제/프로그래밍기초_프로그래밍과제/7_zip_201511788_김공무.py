''' 
    프로그램명 : (7) zip
    제출자 : 김공무(201511788)
    작성일 : 2018. 3. 20.
    기능완성 : O
    설명 :  a 부터 z까지 랜덤한 낱말을 생성하기 위해 string 모듈을 불러와 random.choice(string.ascii_lowercase) 으로 최소 5개에서 최대 30개의 랜덤한 낱말을 생성했으며
    교차 후 랜덤으로 뽑은 영문자 리스트와 숫자 리스트 중 길이를 비교하여 남은 것을 리스트 끝에 붙이도록 했습니다.
'''

import string
import random

alpabet_list = [random.choice(string.ascii_lowercase) for _ in range(5,random.randint(6,30))]
num_list = [str(random.randint(1,10)) for _ in range(5,random.randint(6,30))]
zip_list = list(zip(alpabet_list,num_list))
result = [zip_list[L][T] for L in range(0,len(zip_list)) for T in range(0,2)]

if len(alpabet_list) > len(num_list):
    result += alpabet_list[len(zip_list):]
else:
    result += num_list[len(zip_list):]

print(alpabet_list)
print(num_list)
print('교차후: ',result)

