''' 
    프로그램명 : (6) 리스트에서 중복값 제거
    제출자 : 김공무(201511788)
    작성일 : 2018. 3. 20.
    기능완성 : X
    설명 : 

'''
import random

input_list = [str(random.randint(1,10)) for _ in range(5,random.randint(6,30))]
result = list(dict.fromkeys(input_list))

print(input_list)
print('중복값제거: ',result)
