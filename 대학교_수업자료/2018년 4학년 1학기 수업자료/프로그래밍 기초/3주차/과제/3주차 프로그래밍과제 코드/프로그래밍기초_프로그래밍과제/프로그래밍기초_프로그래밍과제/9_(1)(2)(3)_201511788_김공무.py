''' 
    프로그램명 : (9) (1) 리스트의 앞에 다른 리스트를 붙이는 법
                     (2) 리스트의 제일 뒤에 원소를 다른 리스트로 바꿈
                     (3) 리스트 뒤에 같은 길이만큼을 다른 리스트로 바꿈
    제출자 : 김공무(201511788)
    작성일 : 2018. 3. 22.
    기능완성 : O
    설명 :  enumerate를 이용하여 반환되는 원소가 위치해 있는 배열 번호와 원소값을 사용하여 target_list에 concat_list를 붙이거나
            제일 뒤에 원소를 바꾸거나 concat_list 길이만큼 target_list를 concat_list로 바꿀 수 있도록 했습니다.
'''

import random

result_1 = []
result_2 = []
result_3 = []
target_list = []
concat_list = []

target_num = input('target -> ')
concat_num = input('concat/change -> ')
target_num = target_num.split(' ')
concat_num = concat_num.split(' ')
target_list = [int(t_num) for t_num in target_num]
concat_list = [int(c_num) for c_num in concat_num]


result_1 = [in_num1 for in_num1 in target_list]
for i,num1 in enumerate(concat_list):
    result_1.insert(i,num1)

result_2 = [in_num2 for in_num2 in target_list]
for j,num2 in enumerate(concat_list):
    result_2.insert(j+(len(target_list)-1),num2)

result_3 = [in_num3 for in_num3 in target_list]
for k,num3 in enumerate(concat_list):
    result_3[k+(len(target_list)-len(concat_list))] = num3

print('(1) : ',result_1)
print('(2) : ',result_2)
print('(3) : ',result_3)