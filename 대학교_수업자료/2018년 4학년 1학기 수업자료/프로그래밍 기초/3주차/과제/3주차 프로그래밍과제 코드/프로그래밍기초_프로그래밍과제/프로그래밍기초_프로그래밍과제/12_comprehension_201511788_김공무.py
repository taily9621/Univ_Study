''' 
    프로그램명 : (12) (1) 리스트의 앞에 다른 리스트를 붙이는 법
                     (2) 리스트의 제일 뒤에 원소를 다른 리스트로 바꿈
                     (3) 리스트 뒤에 같은 길이만큼을 다른 리스트로 바꿈
    제출자 : 김공무(201511788)
    작성일 : 2018. 3. 22.
    기능완성 : O
    설명 : 랜덤으로 수를 뽑아내어 저장한 num_list로
    첫 번째, 요소가 2로 나눈 나머지 값이 0 아니면 1일 경우로 나누어 짝수 및 홀수를
    나누었으며
    두 번째, 리스트의 요소 갯수를 쪼개려는 수로 나눈 뒤 divide_size 배열에 쪼개려는 갯수만큼 저장한 뒤 리스트의 요소 갯수
    와 쪼개려는 수를 나눈 나머지 값 만큼 반복문을 돌려 나온 수에 위치한 divide_size 배열의 요소(숫자)를 1을 더해 몇개 씩 
    나누려는 수 만큼 나눌 수 있는지 구할 수 있었습니다.
    세 번째, j만큼 건너뛰기 위해 num_list 배열문에 st::jump_num 으로 작성하게 되었습니다.
'''

import random

odd_list = []
even_list = []
result = []
divide_size = []
divide_result = []
jump_list = []

num_list = [random.randint(1,10) for _ in range(5,random.randint(6,15))]
print(num_list)

''' (1) '''
odd_list = [odd_num for odd_num in num_list if odd_num%2==1]
even_list = [even_num for even_num in num_list if even_num%2==0]
result.append(even_list)
result.append(odd_list)
print('짝수,홀수 리스트: ',result)

''' (2) '''
divide_num = int(input('n => '))
list_num = len(num_list)
divide_size = [int(list_num/divide_num) for _ in range(0,divide_num)]
for plus_num in range(list_num%divide_num):
    divide_size[plus_num] += 1
state_num = 0
for div_size in divide_size:
    divide_result.append(num_list[state_num:state_num+div_size])
    state_num += div_size

print('n개로 쪼갠 리스트: ',divide_result)

''' (3) '''
jump_num = int(input('j = '))
jump_list = [num_list[st::jump_num] for st in range(0,jump_num)]
print('j번 만큼 뛴 리스트: ',jump_list)