﻿''' 
    프로그램명 : (6) 리스트에서 중복값 제거
    제출자 : 김공무(201511788)
    작성일 : 2018. 3. 20.
    기능완성 : O
    설명 : comprehension을 이용하여 1부터 10까지 랜덤한 수를 최소 5개부터 최대 30개 까지 리스트에 넣을 수 있도록 했으며
    결과가 등장 순서대로 출력되기 위해 dict.fromkeys 내장함수를 이용해서 풀었습니다.
'''

import random

input_list = [str(random.randint(1,11)) for _ in range(5,random.randint(6,30))]
result = list(dict.fromkeys(input_list))

print(input_list)
print('중복값제거: ',result)
