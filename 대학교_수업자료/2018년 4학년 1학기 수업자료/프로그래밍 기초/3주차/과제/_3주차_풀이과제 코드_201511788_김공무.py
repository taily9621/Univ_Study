
''' 1번 '''
'''
subject = ['I','You']
verb = ['play','love']
object = ['baseball','football']
sentence = ''

sentence = [sentence+s+' '+v+' '+o for s in subject for v in verb for o in object]

print('결과 리스트: ',sentence)
'''

''' 2번 '''
'''
import random
num_list = []

num_list = [random.randint(1,100) for _ in range(5,random.randint(6,30))]
result = [x for x in num_list if x%2 == 1]
print('원본 리스트: ',num_list)
print('결과 리스트: ',result)
'''

''' 3번 '''
'''
import random
num_list = []

num_list = [random.randint(1,100) for _ in range(5,random.randint(6,30))]
result = [x for x in num_list if x%3!=0 and x%5!=0]
print('원본 리스트: ',num_list)
print('결과 리스트: ',result)
'''

''' 4번 '''
'''
sentence = input('=>')
sentence = sentence.split(' ')
result = [word for word in sentence if len(word) > 1]
print('결과 리스트: ',result)
'''

''' 5번 '''
'''
input_list = input('=>')
result = [word for i,word in enumerate(input_list) if i!=0 and i!=4 and i!=5 and i!=7]

print('원본 리스트: ',input_list)
print('결과 리스트: ',result)
'''

''' 6번 '''
'''
import random
num_list = []

num_list = [random.randint(1,100) for _ in range(5,random.randint(6,30))]
result = [x for x in num_list if '3' in str(x)]

print('원본 리스트: ',num_list)
print('결과 리스트: ',result)
'''

''' 7번 '''
'''
import random
num_list = []
zip_list = []
result = []

num_list = [random.randint(1,100) for _ in range(5,random.randint(6,30))]
zip_list = list(zip(num_list,num_list))
result = [zip_list[L][T] for L in range(0,len(zip_list)) for T in range(0,2)]

print('원본 리스트: ',num_list)
print('결과 리스트: ',result)
'''

''' 11번 '''
'''
import random
num_list = []
result = []

num_list = [random.randint(1,100) for _ in range(5,random.randint(6,30))]
result = [num_list[x] for x in range(-1,-4,-1)]

print('원본 리스트: ',num_list)
print('결과 리스트: ',result)
'''

''' 12번 '''
'''
import random
num_list = []
result = []

num_list = [random.randint(1,100) for _ in range(5,random.randint(6,30))]
result = [odd for odd in num_list if odd%2==0]

print('랜덤 리스트: ',num_list)
print('짝수 리스트: ',result)
print('결과 리스트: ',result[3])
'''

''' 13번 '''
duodecimality = [x for x in range(0,12)]

print('0부터 11까지 입력, 띄어쓰기 구분 필수...')
x = input('x = ')
y = input('y = ')
x = x.split(' ')
y = y.split(' ')
if len(x)>len(y):
    for _ in range(0,len(x)): y.insert(0,'0')
elif len(x)<len(y):
    for _ in range(0,len(y)): x.insert(0,'0')
result = []

point_x = len(x)-1
point_y = len(y)-1
upper_num = 0

for cnt_x in range(point_x,-1,-1):
    duo_x = [dec_x for dec_x,num_x in enumerate(duodecimality) if x[cnt_x]==str(num_x)]
    duo_y = [dec_y for dec_y,num_y in enumerate(duodecimality) if y[point_y]==str(num_y)]
    sum = duo_x[0]+duo_y[0]+upper_num
    upper_num = 0
    if sum > 11:
        upper_num += 1
        result.append(duodecimality[sum-12])
    else:
        result.append(duodecimality[sum])
    point_y -= 1
result.reverse()
print(result)