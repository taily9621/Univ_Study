class Word:
    def __init__(self, word=None, part=None, mean=None, eg=None, example=None):
        self.word = word
        self.part = part
        self.mean = mean
        self.eg = eg
        self.__str__(self, example)

    def __str__(self, example):
        return example


if __name__ == '__main__':
    f = open("C:\\Users\\GongmuKim\\Desktop\\2018년 1학기 수업\\프로그래밍 기초\\기말 실습연습\\기말 실습연습\\voca-330.txt",'r')
    cnt = 1
    word = ''
    part = ''
    mean = ''
    eg = ''
    example = ''

    while True:
        line = f.readline()
        if not line: break
        elif line == '\n':
            print('빈줄!')
            continue

        if cnt == 1:
            word = line
            cnt += 1
        elif cnt == 2:
            part = line
            cnt += 1
        elif cnt == 3:
            mean = line
            cnt += 1
        elif cnt == 4:
            eg = line
            cnt += 1
        else:
            example = line
            #w = Word(word, part, mean, eg, example)
            #print(str(w))
        print('%d 라인 %s: '%(cnt,line))
        cnt += 1
       

    f.close()