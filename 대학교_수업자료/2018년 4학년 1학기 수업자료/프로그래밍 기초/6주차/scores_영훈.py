import random
import string

def compare_students(s1, s2):
    if (s1[1][3] > s2[1][3]):#평균은 앞이 더 크면 안바꾸고 true리턴
        return True
    if (s1[1][3] == s2[1][3]):#평균이같으면
        if (s1[1][0] < s2[1][0]):#국어는 앞쪽이 더 작아야함
            return True
        elif (s1[1][0] == s2[1][0]):
            if (s1[1][1] < s2[1][1]):#영어는 앞쪽이 더 작아야함
                return True
            elif (s1[1][1] == s2[1][1]):
                if (s1[1][2] < s2[1][2]):#수학은 앞쪽이 더 작아야함
                    return True
                elif (s1[1][2] == s2[1][2]):#이름은 앞쪽이 더 작아야함
                    return s1[0] < s2[0]
    return False    



def bubble_sort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if not compare_students(alist[i],alist[i+1]):
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp


myDict = {}
atoz = string.ascii_lowercase
for name in atoz :
    
    myDict[name] = [random.randrange(85,105,5) for _ in range(3)]
    myDict[name].append( sum(myDict[name])/3)
students = list(myDict.items())

bubble_sort(students)

cnt = 1
for s in students:
    print('[{0:2d}]  {1} : {2:3d}, {3:3d}, {4:3d} ({5:5.1f})'.format(cnt, s[0], s[1][0], s[1][1], s[1][2], s[1][3]))
    cnt += 1