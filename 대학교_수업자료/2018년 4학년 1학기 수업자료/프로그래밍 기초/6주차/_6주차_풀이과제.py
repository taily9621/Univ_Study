import string
import random

def bubble_sort(alist): # 버블정렬
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if not compare_students(alist[i],alist[i+1]):
                # False는 뒷 사람 평균이 더 클 경우이므로 위치를 바꿈
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp
                
def compare_students(s1, s2):
    a1 = sum(s1[1])/3
    a2 = sum(s2[1])/3
    if (a1 > a2): # 앞 사람 평균이 더 클 경우
        return True
    if (a1 == a2): # 평균이 같을 경우
        if (s1[1][0] < s2[1][0]): # 국어 점수를 비교
            return True
        elif (s1[1][0] == s2[1][0]): # 국어 점수가 같으면
            if (s1[1][1] < s2[1][1]): # 영어 점수를 비교
                return True
            elif (s1[1][1] == s2[1][1]): # 영어 점수가 같으면
                if (s1[1][2] < s2[1][2]):# 수학 점수를 비교
                    return True
                elif (s1[1][2] == s2[1][2]):# 수학점수가 같으면
                    return s1[0] < s2[0] # 이름의 알파벳 순서를 비교
    return False # 뒷 사람 평균이 더 클 경우    

print('[ 201511788_김공무_6주차 풀이과제 ]')

my_dic = {}
compare_list = [0,0,0]
cnt_rank = 1
same_bool = False

for name in string.ascii_lowercase: #사전생성
    s = [random.randrange(80, 105, 5) for _ in range(3)]
    my_dic[name] = s;

'''
for k in my_dic:
    print('{0} : {1:3d}, {2:3d}, {3:3d} ({4:5.1f})'.format(k, *my_dic[k], sum(my_dic[k])/3))
'''

students = list(my_dic.items()) #사전의 key와 값을 묶은 튜플 값으로 돌려줌
bubble_sort(students) # 정렬
for s in students: # 출력
    # 이전 학생의 점수와 현재 학생의 점수가 완전히 일치하는지 검사
    if compare_list[0] == s[1][0] and compare_list[1] == s[1][1] and compare_list[2] == s[1][2] and sum(compare_list)/3 == sum(s[1])/3:
        #일치할 경우
        cnt_rank -= 1
        same_bool = True
    print('[{0}]{1} : {2:3d}, {3:3d}, {4:3d} ({5:5.1f})'.format(cnt_rank, s[0], *s[1],sum(s[1])/3))
    # 이전 점수들을 compare_list에 저장
    compare_list[0] = s[1][0]
    compare_list[1] = s[1][1]
    compare_list[2] = s[1][2]
    if same_bool: # 같은 등수가 나오게 될 경우
        cnt_rank += 2
        same_bool = False
    else:
        cnt_rank += 1
        
