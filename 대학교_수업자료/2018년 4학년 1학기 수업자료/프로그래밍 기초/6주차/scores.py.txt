import string
import random

def bubble_sort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if not compare_students(alist[i],alist[i+1]):
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp
                
def compare_students(s1, s2):
    a1 = sum(s1[1])/3
    a2 = sum(s2[1])/3
    if (a1 > a2):
        return True
    if (a1 == a2):
        if (s1[1][0] > s2[1][0]):
            return True
        elif (s1[1][0] == s2[1][0]):
            if (s1[1][1] > s2[1][1]):
                return True
            elif (s1[1][1] == s2[1][1]):
                if (s1[1][2] > s2[1][2]):
                    return True
                elif (s1[1][2] == s2[1][2]):
                    return s1[0] < s2[0]
    return False    


my_dic = {}
for name in string.ascii_lowercase:
    s = [random.randrange(80, 105, 5) for _ in range(3)]
    my_dic[name] = s;

for k in my_dic:
    print('{0} : {1:3d}, {2:3d}, {3:3d} ({4:5.1f})'.format(k, *my_dic[k], sum(my_dic[k])/3))
    
students = list(my_dic.items())
bubble_sort(students)
for s in students:
    print('{0} : {1:3d}, {2:3d}, {3:3d} ({4:5.1f})'.format(s[0], *s[1],
                                                           sum(s[1])/3))
