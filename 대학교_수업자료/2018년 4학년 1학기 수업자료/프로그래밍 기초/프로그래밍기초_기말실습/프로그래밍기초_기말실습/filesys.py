def read_file():
    f = open("C:\\Users\\GongmuKim\\Desktop\\프로그래밍기초_기말실습\\step1.txt",'r')
    result_list = [[],[]]
    is_itemline = False
    line_cnt = 0

    while True:
        line = f.readline()
        if not line: break
        if line == '\n' and line_cnt == 0:
            is_itemline = True
            line_cnt += 1
            continue
        elif line == '\n' and line_cnt > 0:
            is_itemline = False
            line_cnt = 0
            continue

        if is_itemline == True:
             result_list[0].append(line)
        else:
             result_list[1].append(line)

    return result_list

