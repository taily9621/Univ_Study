from filesys import read_file

type_item = {1:'상의',2:'자켓',3:'하의',4:'상하복',5:'원피스',6:'기타'}
brand = {'H':'하프클럽','S':'쉬즈미즈','P':'폴햄','T':'탑텐','W':'후아유','L':'리복','J':'지프','V':'브이컷'}

class Setting:
    def __init__(self, item_list = None):
        self.item_list = item_list
        self.set_list()

    def set_list(self):
        result_str = ''
        for x in self.item_list:
            brand_name = []
            item1 = x.split('\n')
            item2 = item1[0].split(' ')
            self.id = item2[0]
            for x in brand:
                for y in self.id:
                    if y in x:
                        self.brand_name = brand[y]
            self.code = item2[1]
            self.name = item2[2]
            item_num = item2[3].split('\t')
            for x in type_item:
                if item_num[len(item_num)-1] in str(x):
                    self.item_type = type_item[x]

            self.price = item2[4]
            item_size = item2[5].split('/')
            size_str = ''
            for g in item_size:
                size_str += g+' '
            self.size = size_str
            item_color = ''
            for j in item2[6:]:
                item_color += j + ' '
            self.color = item_color

            result_str += self.id + ' ' + self.brand_name + ' ' + self.code + ' ' + self.name + ' ' + self.item_type + ' ' + self.price + '원' + ' ' + self.size + '/ ' + self.color + '\n'

        self.result = result_str

    def __str__(self):
        return self.result

class Order:
    def __init__(self, order_list = None, item_set = None):
        self.order_list = order_list
        self.item_set = item_set
        self.set_order()
       
    def set_order(self):
        item_set = self.item_list.split('\n')
        print(item_set)
        for order in self.order_list:
            split_order = order.split(' ')
            #print(split_order)
            for buy_list in split_order[1:]:
                split_buy = buy_list.split('/')
                print(split_buy[0])
                for item in item_set:
                    if split_buy[0] in item:
                        split_correct_item = item.split(' ')
                        print(split_correct_item)
                        self.brand = split_correct_item[1]
                        self.name = split_correct_item[3]
                        self.size = split_buy[1]
                        
                    
                    


if __name__ == '__main__':
    receive_line = read_file()
    item_set = Setting(receive_line[0])
    print(str(item_set))
    item_list = str(item_set)
    order_set = Order(receive_line[1], item_set)

