class Person:
    def __init__(self, n='', h='', a='',g=''):
        self.name = n
        self.hp = h
        self.age = a
        self.gender = g

    def __str__(self):
        return self.name + ' - ' + self.age + ', HP:' + self.hp + ' [' + self.gender + ']'
    
    def input(self):
        age = input('나이 : ')
        phone = input('핸폰 : ')
        gen = input('성별 : ')
        self.age = age
        self.hp = phone
        self.gender = gen

class Student(Person):
    def __init__(self, n='', h='', a='', g='', m='', s=''):
        Person.__init__(self, n, h, a, g)
        self.major = m
        self.classnum = s

    def __str__(self):
        return self.name + ' - ' + self.age + ', HP:' + self.hp + ' [' + self.gender + ']'+'[' + self.classnum + ' ' + self.major + ']'
 
    def input(self):
        age = input('나이 : ')
        phone = input('핸폰 : ')
        gen = input('성별 : ')
        major = input('전공 : ')
        classnum = input('학번 : ')
        self.age = age
        self.hp = phone
        self.gender = gen
        self.major = major
        self.classnum = classnum

b = Student('peter')
b.input()
print(b)