import random

set_1 = {random.randint(1,10) for _ in range(random.randint(5,10))}
set_2 = {random.randint(1,10) for _ in range(random.randint(5,10))}

print(set_1)
print(set_2)
print('교집합: ',set_1.intersection(set_2))
print('합집합: ',set_1.union(set_2))
print('차집합: ',set_1.difference(set_2))