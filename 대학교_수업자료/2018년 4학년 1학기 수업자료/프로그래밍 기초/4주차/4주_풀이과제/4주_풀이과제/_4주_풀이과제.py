# 1. 랜덤 길이의 랜덤 튜플 다섯 개를 만드는 코드를 쓰시오.
'''
import random

result = [(),(),(),(),()]

for tu in result:
    tu = tuple(random.randint(1,10) for _ in range(0,random.randint(6,11)))
    print(tu)
'''
# 2. 1번 문제에서 튜플에 두 번 이상 나오는 원소만 가지는 집합을 출력하도록 코드를 작성하시오.
'''
import random

result = [(),(),(),(),()]
overlap_num = set()

for tu in result:
    tu = tuple(random.randint(1,10) for _ in range(0,random.randint(6,11)))
    print(tu)
    check_num = [inner for inner in tu]
    check_num.sort()
    for pin in range(0,len(check_num)-1):
        if check_num[pin] == check_num[pin+1]:
            overlap_num.add(check_num[pin])
    print('=> {}'.format(overlap_num))
    overlap_num.clear()
'''
# 3. 문자열 튜플에서 모든 원소가 입력된 문자열로 끝나는지를 검사하는 프로그램을 작성하시오.
''' 
string_tuple = ('wonderful','beautiful','careful','joyful')
result = ()

postfix = input('postfix : ')
result = tuple(word for word in string_tuple if word.endswith(postfix))

print('{}는 모든 단어가 {}로 끝납니다.'.format(result,postfix))
'''
# 4. 랜덤 숫자 튜플을 생성한 후 모두가 짝수인지, 또 0을 포함하고 있는지를 계산하여 출력하는 프로그램을 all과 any를 이용하여 작성하시오.
'''
import random

random_tuple = tuple(random.randint(0,10) for _ in range(0,random.randint(6,11)))
#random_tuple = (2,4,6,8) -> 모두 짝수 True, 0 포함 False
#random_tuple = (2,4,6,8,0) -> 모두 짝수 True, 0 포함 True
is_odd = all(0 for num1 in random_tuple if num1%2==1)
is_zero = any(1 for num2 in random_tuple if num2==0)

print('{} : 모두 짝수 {}, 0 포함 {}'.format(random_tuple,is_odd,is_zero))
'''
# 5. 세 개의 랜덤하게 생성된 튜플에서 각 튜플의 min 값의 합을 출력하는 프로그램을 작성하시오.
'''
import random

result = [(),(),()]
for tu_num in range(0,len(result)):
    result[tu_num] = tuple(random.randint(1,10) for _ in range(0,random.randint(3,6)))

print(result)
print('=> sum of min = {}'.format(min(sum(tu) for tu in result)))
'''
# 6. 랜덤하게 생성된 두 집합에서 공통되지 않은 것들만 요소로 가지는 집합을 다음과 같이 출력하도록 작성하시오.
'''
import random

result1 = set(random.randint(1,10) for _ in range(0,random.randint(3,6)))
result2 = set(random.randint(1,10) for _ in range(0,random.randint(3,6)))

print('{} x {} = {}'.format(result1,result2,result1|result2))
'''
# 7. 주어진 길이의 튜플 3개를 생성하고 그것의 합계 리스트를 출력하는 프로그램을 작성하시오.
'''
import random

result = [(tuple(random.randint(1,10) for _ in range(0,4))) for _ in range(3)]
sum_result = []

sum_result = [result[0][set_point]+result[1][set_point]+result[2][set_point] for set_point in range(0,4)]

print('{} + {} + {} = {}'.format(result[0],result[1],result[2],sum_result))
'''
# 8. 입력으로 주어진 모든 숫자에 대해 다음과 같이 gcd를 구하여 출력하는 프로그램을 작성하시오.
'''
def gcd(num1,num2,point,num_tuple):
    if point+1 == len(num_tuple):
        return abs(num1)
    while(num2 != 0):
        temp = num1 % num2
        num1 = num2
        num2 = temp
    return gcd(num1,num_tuple[point+1],point+1,num_tuple)

input_tuple = ()

inner = input('=> ').split(' ')
input_tuple = tuple(int(num) for num in inner)
gcd_result = gcd(input_tuple[0],input_tuple[1],1,input_tuple)

print('gcd {} = {}'.format(input_tuple,gcd_result))
'''